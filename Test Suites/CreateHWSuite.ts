<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>CreateHWSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>903849b3-0e2e-442c-9acf-10d8738dbe4c</testSuiteGuid>
   <testCaseLink>
      <guid>17682e7c-ca00-46d2-afd5-9593d50c4dc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CommonTest/Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4c78319e-b6ba-4266-8fb2-b9aa53b4b7cc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/UserCredintials/UserData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>4c78319e-b6ba-4266-8fb2-b9aa53b4b7cc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>5b4483b0-92fe-4a27-bc02-eee6d62660f6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4c78319e-b6ba-4266-8fb2-b9aa53b4b7cc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>600bc1fc-d52e-4d65-9aec-0311c5c22b68</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f1dc519b-72a6-4472-a0d8-7d70773cf643</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CommonTest/CreateCourse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>839378ef-c621-416b-ae66-876a4fc00143</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CreateAssignment/CreateHomeWork</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bd7265a-1ae9-48ea-8313-6568eaba3457</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CreateAssignment/CreateTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5af02ba1-7bee-4a7e-b118-e8627c246cbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CreateAssignment/CreateQuiz</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
