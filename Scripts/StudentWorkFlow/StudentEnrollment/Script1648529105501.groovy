import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.mathxl.com/login_mxl.htm')

WebUI.setText(findTestObject('Object Repository/StudentenrollmentPO/Page_Pearson Sign In/input_Username_username'), 'lara_std_prod_10')

WebUI.setEncryptedText(findTestObject('Object Repository/StudentenrollmentPO/Page_Pearson Sign In/input_Password_password'), 
    'p4y+y39Ir5Oy1MY8jPt0uQ==')

WebUI.click(findTestObject('Object Repository/StudentenrollmentPO/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/StudentenrollmentPO/Page_Course Home - Lahiru std-prod-10/a_Lahiru std-prod-10'))

WebUI.click(findTestObject('Object Repository/StudentenrollmentPO/Page_Course Home - Lahiru std-prod-10/a_Enroll in Course'))

WebUI.click(findTestObject('Object Repository/StudentenrollmentPO/Page_Enroll in a new course/a_Switch to single box for pasting your Course ID'))

WebUI.setText(findTestObject('Object Repository/StudentenrollmentPO/Page_Enroll in a new course/input_Switch to single box for pasting your_f7dad8'), 
    'XL44-B1X3-00L7-6Y82')

WebUI.click(findTestObject('Object Repository/StudentenrollmentPO/Page_Enroll in a new course/a_Enroll'))

WebUI.click(findTestObject('Object Repository/StudentenrollmentPO/Page_Enrollment Confirmation/a_OK'))

