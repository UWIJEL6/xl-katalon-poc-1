import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('StudentWorkFlow/StudentEnrollment'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Course Home - Lahiru std-prod-10/a_Homework and Tests'))

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Homework and Tests - Lahiru std-prod-10/a_hw-03'))

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Do Homework - hw-03/a_Question1'))

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Do Homework - hw-03/button_'))

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Do Homework - hw-03/td_Apply correct answer'))

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Do Homework - hw-03/button_Next question'))

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Do Homework - hw-03/td_Apply correct answer_1'))

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Do Homework - hw-03/button_Next question_1'))

WebUI.click(findTestObject('Object Repository/DOHomeworkPO/Page_Do Homework - hw-03/button_Save'))

