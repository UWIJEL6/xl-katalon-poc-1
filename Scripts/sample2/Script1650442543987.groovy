import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://spider.mathxl.com/login_mxl.htm')

WebUI.setText(findTestObject('Object Repository/Sample2/Page_Pearson Sign In/input_Username_username'), 'prabhashi_stu1')

WebUI.setEncryptedText(findTestObject('Object Repository/Sample2/Page_Pearson Sign In/input_Password_password'), 'p4y+y39Ir5Oy1MY8jPt0uQ==')

WebUI.click(findTestObject('Object Repository/Sample2/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Course Home - Prabhashi Hettiarachchi/a_Homework and Tests'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Homework and Tests - Prabhashi Hettiarachchi/a_TestingHomeworkAssignment'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/a_Start'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button_'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button_Next question'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button__1'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button__1_2'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1_2'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1_2'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button__1_2_3'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1_2_3'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1_2_3'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button__1_2_3_4'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1_2_3_4'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button_OK'))

WebUI.click(findTestObject('Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/button_Save'))

