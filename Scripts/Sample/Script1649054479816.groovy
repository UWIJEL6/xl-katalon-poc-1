import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://mlm.pearson.com/northamerica/mathxl/')

WebUI.click(findTestObject('Object Repository/Sample1/Page_MathXL  Pearson/a_Sign in'))

WebUI.setText(findTestObject('Object Repository/Sample1/Page_Pearson Sign In/input_Username_username'), 'stu_hiranthi')

WebUI.setEncryptedText(findTestObject('Object Repository/Sample1/Page_Pearson Sign In/input_Password_password'), 'p4y+y39Ir5Oy1MY8jPt0uQ==')

WebUI.click(findTestObject('Object Repository/Sample1/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_Instructor Home - Hiranthi Yashoda/a_Hiranthi Yashoda'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_Instructor Home - Hiranthi Yashoda/a_Log Out'))

WebUI.setText(findTestObject('Object Repository/Sample1/Page_Pearson Sign In/input_Username_username'), 'prabhashi_stu1')

WebUI.setEncryptedText(findTestObject('Object Repository/Sample1/Page_Pearson Sign In/input_Password_password'), 'p4y+y39Ir5Oy1MY8jPt0uQ==')

WebUI.click(findTestObject('Object Repository/Sample1/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_My Courses - Prabhashi Hettiarachchi/h1_Welcome to MathXL, Prabhashi'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_My Courses - Prabhashi Hettiarachchi/h5_Books I Am Working In'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_My Courses - Prabhashi Hettiarachchi/a_Enroll in a new course'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_Enroll in a new course/a_Switch to single box for pasting your Course ID'))

