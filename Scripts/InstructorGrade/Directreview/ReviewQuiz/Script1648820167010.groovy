import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/DirectReviewQuizPO/Page_Assignment Manager - Lahiru CERT std-02/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/DirectReviewQuizPO/Page_Gradebook - Lahiru CERT std-02/a_CERT std-01, Lahiru'))

WebUI.click(findTestObject('Object Repository/DirectReviewQuizPO/Page_Results - Lahiru CERT std-02/a_Review quiz direct multiple attempts'))

WebUI.verifyElementText(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_Results for'), 
    'Results for:')

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_Lahiru CERT std-01'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_Lahiru CERT std-01_imgCaretDown'), 
    0)

WebUI.click(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_Lahiru CERT std-01_imgCaretDown'))

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/ul_Attempt3                                _a279f6'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/a_Next'), 
    0)

WebUI.verifyElementVisible(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/a_Next'))

WebUI.click(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/a_Next'))

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/a_Previous'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/a_Next'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_Results for_1'), 
    'Results for:')

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_Lahiru CERT std-02'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/i_Next_xlicon-help'), 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/i_Next_xlicon-help'))

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_040122 701 PM'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/a_Email Student            Opens in a new window'), 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_Email Student'))

WebUI.click(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/span_Email Student'))

WebUI.switchToWindowTitle('Email Student - Lahiru CERT std-02')

WebUI.setText(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/textarea_Contents required_ctl00ctl00Inside_07b8dc'), 
    'ggghhhh')

WebUI.verifyElementClickable(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/a_Cancel'))

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/a_Cancel'), 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/a_Send'))

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/a_Send'), 
    0)

WebUI.click(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/a_Send'))

WebUI.verifyElementClickable(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/a_Close'))

WebUI.verifyElementPresent(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/a_Close'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/DirectReviewQuizPO/Page_Email Student - Lahiru CERT std-02/span_Email was successfully sent to lahiru._64ce94'), 
    'Email was successfully sent to: lahiru.wijesooriya@pearson.com')

WebUI.switchToWindowTitle('quiz direct multiple attempts')

WebUI.click(findTestObject('Object Repository/DirectReviewQuizPO/Page_quiz direct multiple attempts/button_Close'))

