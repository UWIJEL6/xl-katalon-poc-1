import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('CreateTestPO/Page_Instructor Home - Lahiru Wijesooriya/a_Assignment Manager'))

WebUI.click(findTestObject('CreateHomeWorkPo/Page_Assignment Manager - Lahiru Wijesooriya/a_Create Assignment'))

WebUI.click(findTestObject('CreateTestPO/Page_Assignment Manager - Lahiru Wijesooriya/a_Create Test'))

WebUI.setText(findTestObject('CreateTestPO/Page_Create Homework and Tests - Lahiru Wij_2185b1/input_Test Name_ctl00ctl00InsideFormMasterC_ef2303'), 
    'Test-01')

WebUI.click(findTestObject('CreateTestPO/Page_Create Homework and Tests - Lahiru Wij_2185b1/a_Next'))

WebUI.click(findTestObject('CreateTestPO/Page_Create Homework and Tests - Lahiru Wij_2185b1/input_(P) Video, eText 2_chk990080.1.1.45.17.0'))

WebUI.click(findTestObject('CreateTestPO/Page_Create Homework and Tests - Lahiru Wij_2185b1/input_(P) 1.1.45_chk990080.1.1.47.19.0'))

WebUI.click(findTestObject('CreateTestPO/Page_Create Homework and Tests - Lahiru Wij_2185b1/input_(P) 1.1.47_chk990080.1.1.51.21.0'))

WebUI.click(findTestObject('CreateTestPO/Page_Create Homework and Tests - Lahiru Wij_2185b1/a_Add'))

WebUI.click(findTestObject('CreateTestPO/Page_Create Homework and Tests - Lahiru Wij_2185b1/a_Next'))

WebUI.click(findTestObject('CreateTestPO/Page_Create Homework and Tests - Lahiru Wij_2185b1/a_Save  Assign'))

