import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.mathxl.com/login_mxl.htm')

WebUI.setText(findTestObject('Object Repository/LoginPO/Page_Pearson Sign In/input_Username_username'), username)

WebUI.setText(findTestObject('LoginPO/Page_Pearson Sign In/input_Password_password', [('variable') : '']), password)

WebUI.click(findTestObject('Object Repository/LoginPO/Page_Pearson Sign In/button_Sign in'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LoginPO/Page_Launch - Lahiru Wijesooriya/a_Enter MathXL'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/LoginPO/Page_Launch - Lahiru Wijesooriya/h1_Welcome to MathXL'), 
    'Welcome to MathXL!')

WebUI.verifyElementPresent(findTestObject('Object Repository/LoginPO/Page_Launch - Lahiru Wijesooriya/a_Back to MathXL Log In'), 
    0)

WebUI.click(findTestObject('Object Repository/LoginPO/Page_Launch - Lahiru Wijesooriya/a_Enter MathXL'))

