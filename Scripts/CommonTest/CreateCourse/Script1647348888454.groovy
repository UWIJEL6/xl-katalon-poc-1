import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_Instructor Home - Lahiru Wijesooriya/a_Course Manager'))

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_Course Manager - Lahiru Wijesooriya/a_Create or copy a course'))

WebUI.selectOptionByValue(findTestObject('Object Repository/CreateCoursePO/Page_New Course/select_-- Choose --StandardCoordinatorMember'), 
    'Standard', true)

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_New Course/input_Course name_ctl00ctl00InsideFormMaste_92e586'))

WebUI.setText(findTestObject('Object Repository/CreateCoursePO/Page_New Course/input_Course name_ctl00ctl00InsideFormMaste_92e586'), 
    'site smoke course')

WebUI.selectOptionByValue(findTestObject('Object Repository/CreateCoursePO/Page_New Course/select_-- Choose Book --   Levine Statistic_0673c2'), 
    '990080', true)

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_New Course/label_Adjust Automatically for Daylight Sav_0dc987'))

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/CreateCoursePO/Page_New Course/a_Save'))

WebUI.verifyElementText(findTestObject('Object Repository/CreateCoursePO/Page_Course Settings Summary - Lahiru Wijesooriya/span_(UTC0530) Chennai, Kolkata, Mumbai, New Delhi'), 
    '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi')

