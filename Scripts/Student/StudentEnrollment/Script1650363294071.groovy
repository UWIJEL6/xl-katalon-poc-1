import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

boolean status = WebUI.verifyElementPresent(findTestObject('Object Repository/Sample1/Page_My Courses - Prabhashi Hettiarachchi/h1_Welcome to MathXL, Prabhashi'), 
    10, FailureHandling.OPTIONAL)

if (status == true) {
    WebUI.click(findTestObject('Object Repository/Sample1/Page_My Courses - Prabhashi Hettiarachchi/a_Enroll in a new course'))
} else {
    WebUI.click(findTestObject('Object Repository/StudentEnrollment/Page_Course Home - Hiranthi Yashoda/a_Course Home'))

    WebUI.click(findTestObject('Object Repository/StudentEnrollment/Page_Course Home - Hiranthi Yashoda/a_Hiranthi Yashoda'))

    WebUI.click(findTestObject('Object Repository/StudentEnrollment/Page_Course Home - Hiranthi Yashoda/a_Enrol in Course'))
}

WebUI.click(findTestObject('Object Repository/StudentEnrollment/Page_Enrol in a new course/a_Switch to single box for pasting your Course ID'))

WebUI.setText(findTestObject('Object Repository/StudentEnrollment/Page_Enrol in a new course/input_Switch to single box for pasting your_f7dad8'), 
    GlobalVariable.CourseID)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentEnrollment/Page_Enrol in a new course/p_Is this your course                      _40a23b'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentEnrollment/Page_Enrol in a new course/a_Enrol'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentEnrollment/Page_Enrolment Confirmation/h2_Enrolment Confirmation'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentEnrollment/Page_Enrolment Confirmation/a_OK'))

