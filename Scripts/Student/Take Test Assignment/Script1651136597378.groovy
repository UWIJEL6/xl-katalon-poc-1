import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_Course Home - Prabhashi Hettiarachchi/a_Homework and Tests'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Tests/Page_Homework and Tests - Prabhashi Hettiarachchi/h2_Homework and Tests'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Tests/Page_Homework and Tests - Prabhashi Hettiarachchi/span_All Assignments'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_Homework and Tests - Prabhashi Hettiarachchi/a_TestingTestAssignment'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Tests/Page_Are you ready to start/h2_Are you ready to start'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Tests/Page_Are you ready to start/div_TestingTestAssignment'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_Are you ready to start/a_Start Test'))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_'))
WebUI.sleep(2000)
WebUI.click(findTestObject('StudentAssignments/Tests/Page_TestingTestAssignment/td_Apply correct answer'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Next'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button__1'))
WebUI.sleep(2000)
WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/td_Apply correct answer_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Next_1'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Question 3 of 5_xl_dijit-bootstrap_Button_2'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Test_xl_dijit-bootstrap_Button_1'))

WebUI.sleep(4000)

WebUI.click(findTestObject('StudentAssignments/Tests/Page_TestingTestAssignment/button__1_2'))
WebUI.sleep(2000)
WebUI.click(findTestObject('StudentAssignments/Tests/Page_TestingTestAssignment/td_Apply correct answer_1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Next_1_2'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button__1_2_3'))
WebUI.sleep(2000)
WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/td_Apply correct answer_1_2_3'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Next_1_2_3'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button__1_2_3_4'))
WebUI.sleep(2000)
WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/td_Apply correct answer_1_2_3_4'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Submit test'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Submit test (1)'))

WebUI.sleep(2000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Tests/Page_Test Summary/h2_Test Summary'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Tests/Page_Test Summary/span_TestingTestAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Tests/Page_Test Summary/span_100 (55 pts)'), 
    0)

WebUI.click(findTestObject('Object Repository/Student/TakeTest/Page_Test Summary/a_Review Test'))

WebUI.click(findTestObject('Object Repository/Student/TakeTest/Page_TestingTestAssignment/span_Review  Test TestingTestAssignment'))

WebUI.click(findTestObject('Object Repository/Student/TakeTest/Page_TestingTestAssignment/button_Question 1,_xl_dijit-bootstrap_Button_2 (1)'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/Student/TakeTest/Page_TestingTestAssignment/button_Question 1,_xl_dijit-bootstrap_Button_2 (1)'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/Student/TakeTest/Page_TestingTestAssignment/button_Question 1,_xl_dijit-bootstrap_Button_2 (1)'))

WebUI.sleep(2000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/div_Test Score 100, 5 of 5 points'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/button_Close'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/Student/TakeTest/Page_Homework and Tests - Prabhashi Hettiarachchi/h2_Homework and Tests'))

