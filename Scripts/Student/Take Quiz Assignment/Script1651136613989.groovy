import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Course Home - Prabhashi Hettiarachchi/a_Homework and Tests'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Homework and Tests - Prabhashi Hettiarachchi/h2_Homework and Tests'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Homework and Tests - Prabhashi Hettiarachchi/button_All Assignments'))

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Homework and Tests - Prabhashi Hettiarachchi/a_Quizzes  Tests'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Homework and Tests - Prabhashi Hettiarachchi/span_All Quizzes and Tests'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Homework and Tests - Prabhashi Hettiarachchi/a_TestingQuizAssignment'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Are you ready to start/h2_Are you ready to start'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Are you ready to start/div_TestingQuizAssignment'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Are you ready to start/a_Start Test'))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button_'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/td_Apply correct answer'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button_Next'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button__1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/td_Apply correct answer_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button_Next_1'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button_Next_1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button_Quiz_xl_dijit-bootstrap_Button_1'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button__1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/td_Apply correct answer_1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button_Next_1_2_3'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button_Next_1_2_3_4'))

WebUI.sleep(4000)

WebUI.click(findTestObject('StudentAssignments/Quiz/Page_TestingQuizAssignment/button_Submit quiz'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Quiz/Page_TestingQuizAssignment/button_Submit quiz (1)'))

WebUI.sleep(2000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Quiz Summary/h2_Quiz Summary'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Quiz Summary/span_TestingQuizAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Quiz/Page_Quiz Summary/span_60 (35 pts)'), 
    0)

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Quiz Summary/a_Review Test'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_TestingQuizAssignment/span_Review  Quiz TestingQuizAssignment'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Student/TakeQuiz/Page_TestingQuizAssignment/button_Next'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Student/TakeQuiz/Page_TestingQuizAssignment/button_Next_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Student/TakeQuiz/Page_TestingQuizAssignment/button_Next_1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_TestingQuizAssignment/button_Next_1_2_3'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Student/TakeQuiz/Page_TestingQuizAssignment/button_Close'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Homework and Tests - Prabhashi Hettiarachchi/h2_Homework and Tests'))

