import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Course Home - Prabhashi Hettiarachchi/a_Homework and Tests'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Homework and Tests - Prabhashi Hettiarachchi/h2_Homework and Tests'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Homework and Tests - Prabhashi Hettiarachchi/span_All Assignments'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Homework and Tests - Prabhashi Hettiarachchi/a_TestingHomeworkAssignment'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/h2_Do Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/span_TestingHomeworkAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/span_0 (0 points out of 5)'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/a_Start'))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_'))

WebUI.sleep(2000)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button__1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button__1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1_2'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Question 4,_xl_dijit-bootstrap_Button_2'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button__1_2_3'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1_2_3'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_OK'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Save'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/span_80 (4 points out of 5)'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/a_Review'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework - TestingHomeworkAssignment/h2_Review Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework - TestingHomeworkAssignment/div_Warning            You are reviewing yo_14d351'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework - TestingHomeworkAssignment/a_Question1'))

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework - TestingHomeworkAssignment/button_Continue to Review Homework'))

WebUI.sleep(2000)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Review Homework/button_Next'))

WebUI.sleep(2000)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Review Homework/button_Next_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework/button_Next_1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework/span_Points 0 of 1'))

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework/div_HW Score 80, 4 of 5 points'))

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework/button_Next_1_2_3'))

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Review Homework/button_Close'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/h2_Do Homework'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/a_Results'))

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Results - Prabhashi Hettiarachchi/a_Homework and Tests'))

