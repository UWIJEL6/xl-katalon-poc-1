import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Sample1/Page_Instructor Home - Prabhashi ins1/a_Course Manager'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_Course Manager - Prabhashi ins1/a_Create or copy a course'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Sample1/Page_New Course/select_-- Choose --StandardCoordinatorMember'), 
    'Standard', true)

WebUI.setText(findTestObject('Object Repository/Sample1/Page_New Course/input_Course name_ctl00ctl00InsideFormMaste_92e586'), 
    GlobalVariable.CourseName)

WebUI.selectOptionByValue(findTestObject('Object Repository/Sample1/Page_New Course/select_-- Choose Book --   Levine Statistic_0673c2'), 
    GlobalVariable.BookMaterial, true)

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/a_Next'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Sample1/Page_New Course/span_Standard'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Sample1/Page_New Course/span_PortTestCourse'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Sample1/Page_New Course/span_NextGen Media Math Test BookRH'), 
    0)

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/img_Expand All_ctl00_ctl00_InsideForm_Maste_c03b4e'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/a_Save'))

WebUI.sleep(8000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Sample1/Page_Course Settings Summary - Prabhashi ins1/h2_Course Settings Summary'), 
    2)

CourseID = WebUI.getText(findTestObject('Object Repository/Sample1/Page_Course Settings Summary - Prabhashi ins1/a_XL44-E1JP-40L9-74V2'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_Course Settings Summary - Prabhashi ins1/a_XL44-E1JP-40L9-74V2'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_Course Settings Summary - Prabhashi ins1/input_Browser Check_ctl00ctl00InsideFormMas_e7786d'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCourse/Page_Course Manager - Prabhashi ins1/h2_Course Manager'), 
    0)

GlobalVariable.CourseID = this.CourseID


