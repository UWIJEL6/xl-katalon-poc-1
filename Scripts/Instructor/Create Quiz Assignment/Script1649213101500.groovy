import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Instructor Home - Hiranthi Insnew/a_Assignment Manager'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Assignment Manager - Hiranthi Insnew/a_Create Assignment'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Assignment Manager - Hiranthi Insnew/a_Create Quiz'))

WebUI.setText(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/input_Quiz Name_ctl00ctl00InsideFormMasterC_4ce300'), 
    'TestingQuizAssignment')

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/a_Next'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/select_All SBSSBSection 1.1 This is a very _357309'), 
    '3', true)

this.Course = GlobalVariable.CourseName

for (def i = 1; i < 4; i++) {
    WebUI.click(findTestObject('Instructor/CreateQuiz/global/Page_Create Homework and Tests - Hiranthi Insnew/input_Estimated time0s_chk990213.1.3.19.54.0', 
            [('AssignmentID') : findTestData('AssignmentData/QuizQuestions').getValue(this.Course, i)]))

    System.out.println(findTestData('AssignmentData/QuizQuestions').getValue(this.Course, i))
}

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/a_Pool'))

for (def i = 4; i < 7; i++) {
    WebUI.click(findTestObject('Instructor/CreateQuiz/global/Page_Create Homework and Tests - Hiranthi Insnew/input_Estimated time0s_chk990213.1.3.19.54.0', 
            [('AssignmentID') : findTestData('AssignmentData/QuizQuestions').getValue(this.Course, i)]))

    System.out.println(findTestData('AssignmentData/QuizQuestions').getValue(this.Course, i))
}

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/a_Pool'))

for (def i = 7; i < 10; i++) {
    WebUI.click(findTestObject('Instructor/CreateQuiz/global/Page_Create Homework and Tests - Hiranthi Insnew/input_Estimated time0s_chk990213.1.3.19.54.0', 
            [('AssignmentID') : findTestData('AssignmentData/QuizQuestions').getValue(this.Course, i)]))

    System.out.println(findTestData('AssignmentData/QuizQuestions').getValue(this.Course, i))
}

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/a_Pool'))

for (def i = 10; i < 12; i++) {
    WebUI.click(findTestObject('Instructor/CreateQuiz/global/Page_Create Homework and Tests - Hiranthi Insnew/input_Estimated time0s_chk990213.1.3.19.54.0', 
            [('AssignmentID') : findTestData('AssignmentData/QuizQuestions').getValue(this.Course, i)]))

    System.out.println(findTestData('AssignmentData/QuizQuestions').getValue(this.Course, i))
}

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/a_Add'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/a_Next'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Create Homework and Tests - Hiranthi Insnew/a_Save  Assign'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/Instructor/CreateQuiz/Page_Assignment Manager - Hiranthi Insnew/h2_Assignment Manager'))

