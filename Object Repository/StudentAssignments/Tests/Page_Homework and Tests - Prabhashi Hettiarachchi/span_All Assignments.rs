<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_All Assignments</name>
   <tag></tag>
   <elementGuidId>8d0b8857-76c4-4e13-bb10-507bdcb7d7a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.PagerText</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h3[@id='gridtitle']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9f2c6cc6-04ec-46eb-908a-aa4a93f6bc00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>PagerText</value>
      <webElementGuid>4b272fbd-e7ee-48ef-9a52-56e80858987d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					            All Assignments
					        </value>
      <webElementGuid>a5590395-d9b9-416b-96d7-747876c18cac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gridtitle&quot;)/span[@class=&quot;PagerText&quot;]</value>
      <webElementGuid>991b0b2f-a73a-4615-904a-2e462476a590</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//h3[@id='gridtitle']/span</value>
      <webElementGuid>2e536be0-e759-41e3-8499-5db2f70a0afa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[2]/following::span[1]</value>
      <webElementGuid>493e9d22-ac6b-4993-a2c5-8ad612a39f14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[1]/following::span[2]</value>
      <webElementGuid>61c7d812-b4aa-4889-a5c3-25176c98bb1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Due'])[1]/preceding::span[1]</value>
      <webElementGuid>f8c9c51c-1bf6-42db-a895-319c5e37baf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prereq'])[1]/preceding::span[1]</value>
      <webElementGuid>2fab8662-326d-4ddf-bde2-4a55b4e2f488</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3/span</value>
      <webElementGuid>6498ec12-ae8e-4d49-8236-c6cd975716fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
					            All Assignments
					        ' or . = '
					            All Assignments
					        ')]</value>
      <webElementGuid>eecb1356-3d4a-4aba-a3fc-f35cc133452d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
