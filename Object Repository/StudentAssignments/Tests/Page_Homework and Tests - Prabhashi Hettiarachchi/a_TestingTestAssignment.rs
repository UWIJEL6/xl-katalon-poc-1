<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_TestingTestAssignment</name>
   <tag></tag>
   <elementGuidId>252e289f-7437-4681-b047-c255d90b439c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'TestingTestAssignment')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2f602706-c4e1-4187-9722-f9f38b55bf1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doTest(12643782, false);</value>
      <webElementGuid>c47c5640-c2b4-48eb-a8e8-3bc35e5db852</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TestingTestAssignment</value>
      <webElementGuid>55223fe8-66d8-43b9-b7b3-b261ae874b63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[11]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>76719b2d-3727-4b27-b686-53e96bc83d48</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[11]/th/a</value>
      <webElementGuid>932f4068-b5c4-464d-8978-a1cca3b0f765</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'TestingTestAssignment')]</value>
      <webElementGuid>73f98e8b-e7fe-48dc-9d08-2335b480b380</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[2]/following::a[1]</value>
      <webElementGuid>71483268-99af-4971-9b00-c7888d55342b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/preceding::a[1]</value>
      <webElementGuid>7bff3194-a2ff-4812-b899-4bb6e305db83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='TestingTestAssignment']/parent::*</value>
      <webElementGuid>2a69d6ad-4e8c-489f-bae0-d79835301925</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doTest(12643782, false);')]</value>
      <webElementGuid>96b3c5fd-6414-4fa1-9bbd-dc7e0e49a3c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[11]/th/a</value>
      <webElementGuid>dc6a0501-51e3-4636-b674-381cbaac5119</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doTest(12643782, false);' and (text() = 'TestingTestAssignment' or . = 'TestingTestAssignment')]</value>
      <webElementGuid>f5af4d70-8054-41b3-8713-b4988428eca5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
