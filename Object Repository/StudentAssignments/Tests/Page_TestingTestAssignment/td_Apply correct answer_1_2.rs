<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Apply correct answer_1_2</name>
   <tag></tag>
   <elementGuidId>badce778-a552-4c14-a61e-9ccc4efe85b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#dijit_MenuItem_78_text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[starts-with(@id, 'dijit_MenuItem_') and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>77a8bc49-2b00-41cf-8661-2d3bf9dac64b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dijitReset dijitMenuItemLabel</value>
      <webElementGuid>e546d86a-c1ec-42af-8fa0-f0954d683b95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>34a26a16-a45e-43fa-abec-85dd98dbe77c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>containerNode,textDirNode</value>
      <webElementGuid>c27a303d-713b-4f6a-a09a-b2e015663fb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dijit_MenuItem_78_text</value>
      <webElementGuid>25a1566f-ee50-4434-a912-4172971140ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apply correct answer</value>
      <webElementGuid>31d9c0d0-e38d-489e-a94e-ba46a059877a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_MenuItem_78_text&quot;)</value>
      <webElementGuid>f82ea61c-a207-44bc-ae8d-110daada0f38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>d3c3e695-c18a-4818-bd83-b6885d255671</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='dijit_MenuItem_78_text']</value>
      <webElementGuid>fc81846e-c955-4977-900b-0f51c41d940d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='dijit_MenuItem_78']/td[2]</value>
      <webElementGuid>3695883c-c1f8-4049-b600-fa40449f3fca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show alt text'])[1]/following::td[4]</value>
      <webElementGuid>47492688-3037-4617-95a7-a590597ff195</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[1]/following::td[5]</value>
      <webElementGuid>3fd8485a-5165-4b94-a14a-fffbc58eeca9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[7]/preceding::td[2]</value>
      <webElementGuid>cd736964-79f7-4cb4-827f-b07cddb26300</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[2]/preceding::td[3]</value>
      <webElementGuid>e9fdb342-7094-492a-947a-6ec485c2ee81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply correct answer']/parent::*</value>
      <webElementGuid>4a12fa66-8193-44fd-9c4a-4a36121090d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[5]/td[2]</value>
      <webElementGuid>feb298bf-b658-4085-805a-f361aa76b541</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'dijit_MenuItem_78_text' and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      <webElementGuid>13f0e1c4-3dea-47f7-a843-9f5d2b4bd469</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
