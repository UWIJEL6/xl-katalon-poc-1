<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Apply correct answer</name>
   <tag></tag>
   <elementGuidId>a8498af0-c031-4da7-86d4-98dfe4216b18</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#dijit_MenuItem_2_text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[starts-with(@id, 'dijit_MenuItem_') and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>f667f6e1-27a3-4606-969b-a7fe55966c2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dijitReset dijitMenuItemLabel</value>
      <webElementGuid>708af1bf-4bfd-4446-993a-1ec2a4da7c7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>ec9da650-4618-4ee6-ab65-82834a7b7ec5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>containerNode,textDirNode</value>
      <webElementGuid>5b71b191-3b92-474d-a3f2-0041cb5a11e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dijit_MenuItem_2_text</value>
      <webElementGuid>e0a645c2-8f1d-4a80-b840-1f7aa886c327</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apply correct answer</value>
      <webElementGuid>ffaeebd6-8091-4be3-9655-9b43499bddaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_MenuItem_2_text&quot;)</value>
      <webElementGuid>7ac27b5a-e23f-4857-8c96-5649e59c1275</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>f3c65eb8-0f1b-4e67-a032-4b859fe4afed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='dijit_MenuItem_2_text']</value>
      <webElementGuid>f305406c-4e84-4d25-9b46-6e73f9b5d00c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='dijit_MenuItem_2']/td[2]</value>
      <webElementGuid>89c6d122-f2ab-4e74-9f09-a3d145b67b20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show alt text'])[1]/following::td[4]</value>
      <webElementGuid>993a648b-d856-4553-bf8a-a2b036aa1254</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[1]/following::td[5]</value>
      <webElementGuid>3594d64d-f842-47de-83f1-abae08a44bf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[7]/preceding::td[2]</value>
      <webElementGuid>721c42a7-17b6-41d9-86c8-5ba8614c3b09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[2]/preceding::td[3]</value>
      <webElementGuid>b87e30f7-452e-4441-a149-e60da5190463</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply correct answer']/parent::*</value>
      <webElementGuid>cf25d4c3-2a4a-43df-bb7d-a5179c254af5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[5]/td[2]</value>
      <webElementGuid>fbab23e3-6b00-4617-bbc7-bb152f33aed2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'dijit_MenuItem_2_text' and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      <webElementGuid>5eadcc6e-797e-436c-9450-908c1cd6768d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
