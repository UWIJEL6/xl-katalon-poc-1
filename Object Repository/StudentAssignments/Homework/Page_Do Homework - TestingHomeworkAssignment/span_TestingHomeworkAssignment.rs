<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_TestingHomeworkAssignment</name>
   <tag></tag>
   <elementGuidId>e67c822b-dd3b-4198-ad47-e27dde8ea24c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_LblName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_LblName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6500a785-2a10-43e3-ba76-09ccfb90bd1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_LblName</value>
      <webElementGuid>1242bcab-ff69-48a7-ba87-8e5c512272d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TestingHomeworkAssignment</value>
      <webElementGuid>2c52bb81-0fd0-4aeb-a616-791b79f5a118</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_LblName&quot;)</value>
      <webElementGuid>f5810346-252c-4b3b-afbd-d990e399e1a4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_LblName']</value>
      <webElementGuid>81772b33-ab5b-480a-acd2-fc526b6bbcf9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_TrAssignmentName']/td[2]/span</value>
      <webElementGuid>ee85563b-5310-4a6c-a470-786f4733f71f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name:'])[1]/following::span[1]</value>
      <webElementGuid>4af6ac37-a2e9-493d-816b-f24b5d5d3c7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Get a tutor'])[1]/following::span[1]</value>
      <webElementGuid>204a08f6-7aff-45ee-b96f-8592dee21720</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Current Score'])[1]/preceding::span[1]</value>
      <webElementGuid>4cc927ec-0b74-4fa4-be20-fe6c392101fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='TestingHomeworkAssignment']/parent::*</value>
      <webElementGuid>e468d7c4-252c-4833-b962-d1a07510e470</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/span</value>
      <webElementGuid>9de649fa-a0c5-4539-9dea-091364ba8ce8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ctl00_ctl00_InsideForm_MasterContent_LblName' and (text() = 'TestingHomeworkAssignment' or . = 'TestingHomeworkAssignment')]</value>
      <webElementGuid>38910219-3fed-465c-ac86-5c81be95001c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
