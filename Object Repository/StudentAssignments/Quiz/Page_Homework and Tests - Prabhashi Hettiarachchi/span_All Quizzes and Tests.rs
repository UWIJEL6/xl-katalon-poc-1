<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_All Quizzes and Tests</name>
   <tag></tag>
   <elementGuidId>00c50be1-3647-4003-a8f4-277adc313e38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.PagerText</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h3[@id='gridtitle']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>312aac93-e921-477a-8786-27e8fc61a037</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>PagerText</value>
      <webElementGuid>dc32d397-7e8a-4503-9556-930988801406</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					            All Quizzes and Tests
					        </value>
      <webElementGuid>2167ca68-b58b-47da-b3db-4fd3e6aa0e15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gridtitle&quot;)/span[@class=&quot;PagerText&quot;]</value>
      <webElementGuid>8916a90e-37ae-4a3a-9126-f246ae947029</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//h3[@id='gridtitle']/span</value>
      <webElementGuid>91839ae1-b619-4eb6-9d48-557718233165</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[2]/following::span[1]</value>
      <webElementGuid>899e8956-03c5-4e1e-b9b1-e8b157e0b4ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[1]/following::span[2]</value>
      <webElementGuid>4186707b-9f66-41c5-9d96-43fa5a7811bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Due'])[1]/preceding::span[1]</value>
      <webElementGuid>6e7def65-66fd-4924-891b-2ce39dd0bc70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prereq'])[1]/preceding::span[1]</value>
      <webElementGuid>230bd0ce-2ff4-479b-bf26-c8098a254c26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Quizzes and Tests']/parent::*</value>
      <webElementGuid>2790ad37-4142-4cb4-b43d-0b1014f5719b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3/span</value>
      <webElementGuid>62026c19-76b8-4cea-84cc-57c5e8c77bc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
					            All Quizzes and Tests
					        ' or . = '
					            All Quizzes and Tests
					        ')]</value>
      <webElementGuid>d159ea92-d0b8-4756-9573-d1e8c89ffce6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
