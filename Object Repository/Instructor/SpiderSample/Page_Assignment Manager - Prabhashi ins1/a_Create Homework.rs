<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create Homework</name>
   <tag></tag>
   <elementGuidId>4a0084b6-be66-48ee-a5dd-d73d8293097c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Toolbar_collapse_hwAndTestManagerToolbar']/ul/li/div/div[2]/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > ul > li > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d485b4e3-5c19-4282-a9f9-a936097ba81e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:GoCreate('homework')</value>
      <webElementGuid>d8731a15-ab50-463b-92bd-4776d3bceab7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create Homework</value>
      <webElementGuid>7cd583c2-8ab8-4eba-ae34-c9435ea54655</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_hwAndTestManagerToolbar&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-dropdown&quot;]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[1]/a[1]</value>
      <webElementGuid>c59ef94e-f839-4504-9e49-88311467e1b1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_hwAndTestManagerToolbar']/ul/li/div/div[2]/ul/li/a</value>
      <webElementGuid>201f618a-6ed1-4c62-9c5f-7a635271383a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create Homework')]</value>
      <webElementGuid>55ebc7e4-83d3-44ef-ad23-0a04aa057fc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::a[2]</value>
      <webElementGuid>441072e2-1695-412c-b96d-cbf4d371bc3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Assignment Manager'])[1]/following::a[3]</value>
      <webElementGuid>fe804ff3-4855-4d85-a415-103932d1e955</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Quiz'])[1]/preceding::a[1]</value>
      <webElementGuid>7a3a3df4-ef46-4a65-b44e-97aabf43f478</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Test'])[1]/preceding::a[2]</value>
      <webElementGuid>119a1939-70ae-40e0-a436-e22f2b6042df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create Homework']/parent::*</value>
      <webElementGuid>6c2bebbe-ae51-4884-b968-2367cfbb975c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:GoCreate('homework')&quot;)]</value>
      <webElementGuid>e1df7acd-d336-42bb-afc2-6876982a63cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a</value>
      <webElementGuid>2efa74b1-cbde-47a5-868e-249d05632475</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:GoCreate(&quot; , &quot;'&quot; , &quot;homework&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Create Homework' or . = 'Create Homework')]</value>
      <webElementGuid>ce118931-cf2f-40c0-a943-c9df1d739ae1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
