<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --   eT1 Math Test Bo_7aa5f9</name>
   <tag></tag>
   <elementGuidId>1cf39b89-a150-4f66-84c4-6ab204ba418b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>02a8ac33-7934-4225-a4f6-e98d66567425</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
      <webElementGuid>d22b68ab-7b5c-437e-9be9-21bdb05725d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      <webElementGuid>fb792b76-6b61-480f-9242-e33a5a529caa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
      <webElementGuid>f4e5ac3a-cf3a-496c-a2d8-76df0b80ef5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>4ad2a3bf-f109-4513-b080-82bcbd3a0515</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				  -- Choose Book --  
				 eT1 Math Test Book 10 - Non Course Aware 10
				 Levine: Statistics for Managers Using Microsoft Excel, 9e, Global Edition
				9780857761071 Stoorvogel EMA
				Abel/Bernanke: Macroeconomics, 5th Edition, MATH REVIEW
				Adams: Calculus, 6e ENHANCED  DEMO
				Adams: Calculus, 7e
				ADP: Algebra II Online Course
				Agresti, Statistics, 4/e, Global Edition
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				AIS Test: Chemistry: Structure and Properties - withSite
				AIS Test: Chemistry: Structure and Properties v1
				AIS Test: Chemistry: Structure and Properties v3
				AIS Test: Chemistry: Structure and Properties v4
				AIS Test: Chemistry: Structure and Properties v5
				AIS Test: Human Anatomy and Physiology v3
				Akst: Basic Mathematics through Applications, 3e ENHANCED
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Fundamental Mathematics through Applications, 3e ENHANCED
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Intermediate Algebra through Applications, 1e ENHANCED
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 3e
				Akst: Introductory Algebra through Applications, 1e ENHANCED
				Akst: Introductory Algebra Through Applications, 2e
				Alec's Delta Book:  Non-combo
				Alicia's book for menu options
				Alicia's MNL Pharmacology XL style
				Alicia's tiny test book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Angel: A Survey of Mathematics with Applications, 7e ENHANCED
				Angel: A Survey of Mathematics with Applications, Expanded 7e ENHANCED
				Angel: Algebra for College Students, 3e ENHANCED
				Angel: Elementary Algebra for College Students Early Graphing, 3e  ENHANCED
				Angel: Elementary Algebra for College Students, 9e
				Angel: Elementary and Intermediate Algebra for College Students, 3e (ENHANCED)
				Angel: Intermediate Algebra for College Students, 10e
				Angel: Intermediate Algebra for College Students, 7e (ENHANCED)
				Anne Arundel Community College: MAT 044/045/145/146 (2020)
				Austin CC: Basic College Math
				Bade/Parkin: Essential Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, MATH REVIEW
				Baker College: Basic College Mathematics (Tobey)
				Barnett, College Math for Bus, Econ, Life Sci, and Soc Sci, 14/e, Global Edition
				Barnett, Finite Math for Bus, Econ, Life Sci, and Soc Sci, 14/e, Global Edition
				Barnett: Calculus for Bus, Econ, Life and Social Sci, 14/e, Global Edition
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13/e, Global Ed
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13e
				Barnett: College Math for Bus, Econ, Life&amp;Social Sci, 13/e, Global Edition
				Barnett: Finite Mathematics, 10e ENHANCED
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Beecher: Algebra &amp; Trigonometry, 2e ENHANCED
				Beecher: Algebra &amp; Trigonometry, 3e ENHANCED
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 2e ENHANCED
				Beecher: Precalculus, 2e ENHANCED
				Beecher: Precalculus, 3e
				Bennett: Essentials of Using &amp; Understanding Mathematics
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Using and Understanding Mathematics, 3e ENHANCED
				Bennett: Using and Understanding Mathematics, 4e
				Berenson, Basic Business Statistics, 13/e, Global Edition
				Berenson: Basic Business Statistics, 14/e, Global Edition
				Berenson: Basic Business Statistics, 5e (Aus)
				Berk/DeMarzo/Harford: Fundamentals of Corporate Finance, 5/e, Global Edition
				Berk/DeMarzo: Corporate Finance: The Core, 5e, Global Edition
				Betsy Smoketest 1-19-10
				Betsy Smoketest 9-28
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 11e
				Billstein: Mathematics for Elementary School Teachers, 8e
				Bittinger, Basic College Mathematics, 12/e, Global Edition
				Bittinger: Algebra and Trig: Graphs &amp; Models, 2e
				Bittinger: Algebra and Trig: Graphs &amp; Models, Unit Circle Approach, 1e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 5e
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 9e ENHANCED
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: Calculus and Its Applications, 9e DEMO
				Bittinger: College Algebra: Graphs &amp; Models, 2e
				Bittinger: College Algebra: Graphs &amp; Models, 3e
				Bittinger: College Algebra: Graphs &amp; Models, 5e
				Bittinger: Developmental Mathematics, 6e ENHANCED
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Developmental Mathematics, 8e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e ENHANCED
				Bittinger: Elementary Algebra, Concepts and Applications, 8e (2010)
				Bittinger: Elementary Algebra, Concepts and Applications, 9e
				Bittinger: Elementary Algebra: Concepts &amp; Apps, 6e
				Bittinger: Elementary Algebra: Graphs &amp; Models, 1e ENHANCED
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Elementary and Intermediate Algebra: Concepts &amp; Apps, 3e
				Bittinger: Elementary and Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Elementary and Intermediate Algebra: Graphs &amp; Models: 1e
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3e ENHANCED
				Bittinger: Fundamental Mathematics, 4e
				Bittinger: Fundamentals of College Algebra: Graphs &amp; Models
				Bittinger: Houston Community College Prealgebra, 3e
				Bittinger: Intermediate Algebra, 10e ENHANCED
				Bittinger: Intermediate Algebra, 9e ENHANCED
				Bittinger: Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Intermediate Algebra: 8e Alternate Version
				Bittinger: Intermediate Algebra: Concepts &amp; Apps, 6e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Intermediate Algebra: Graphs &amp; Models: 1e
				Bittinger: Introductory Algebra, 10e ENHANCED
				Bittinger: Introductory Algebra, 11e
				Bittinger: Introductory Algebra, 9e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 2e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 3e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 4e
				Bittinger: Prealgebra &amp; Introductory Algebra, 3e
				Bittinger: Prealgebra and Introductory Algebra, 1e ENHANCED
				Bittinger: Prealgebra, 4e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, A Unit Circle Approach
				Bittinger: Precalculus: Graphs &amp; Models: 2e
				Bittinger: Trigonometry: Graphs &amp; Models, 2e
				Blair/Tobey/Slater: Prealgebra, 3e ENHANCED
				Blair/Tobey: Prealgebra, 4e
				Blair: Intermediate Algebra, 1e
				Blair: Prealgebra, 5e 
				Blitzer:  College Algebra, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 2e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 5e ENHANCED
				Blitzer: College Algebra, 4e ENHANCED
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: Essentials of Intermediate Algebra for College Students ENHANCED
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra ENHANCED
				Blitzer: Intermediate Algebra for College Students, 4e ENHANCED
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 4e ENHANCED
				Blitzer: Introductory and Intermediate Algebra for College Students, 2e ENHANCED
				Blitzer: Precalculus Essentials, 2e ENHANCED
				Blitzer: Precalculus, 2e ENHANCED
				Blitzer: Precalculus, 3e ENHANCED
				Blitzer: Thinking Mathematically with Integrated Review
				Blitzer: Thinking Mathematically, 3e ENHANCED
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Bluegrass Community and Technical College: MA 111 Contemporary Math (2021)
				Briggs, Calculus, 2/e, Global Edition
				Briggs/Cochran/Gillett: Calculus: Early Transcendentals, 2/e, Global Edition
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update Aida Pilot
				Briggs/Cochran: Calculus, 2e
				Business Algebra, Second Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT final
				Carson: Elementary Algebra with Early Systems of Equations, ENHANCED
				Carson: Elementary Algebra, 2e ENHANCED
				Carson: Elementary Algebra, ENHANCED
				Carson: Elementary and Intermediate Algebra, ENHANCED
				Carson: Intermediate Algebra, 2nd Edition ENHANCED
				Carson: Intermediate Algebra, ENHANCED
				Carson: Prealgebra, 2e ENHANCED
				Carson: Prealgebra: 1e
				CCNG Trigsted Intermediate Alg Test Book
				CFO Online Finance Course. DEMO
				Chris Feb Tier 3 Test Math
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 2e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving: 1e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv, 2e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv, 3e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv: 1e
				Consortium: Math in Action: Prealgebra Problem Solving, 1e
				Consortium: Math In Action: Prealgebra Problem Solving, 2e (2008) ENHANCED
				Contemporary Business Mathematics (BMAT 110)
				Corporate Finance, 4e for University of California-Berkeley
				Croft/Davison: Foundation Maths 7e
				De Veaux, Stats: Data &amp; Models, 4/e, Global Edition
				De Veaux: Stats: Data &amp; Models, 5e, Global Edition
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana: Calculus: Graphical, Numerical, Algebraic, 3e ENHANCED
				Demana: Precalculus: Functions and Graphs, 5e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9/e, Global Edition
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core, 9/e, GE 
				DeVry MAT190 (Washington)
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra and Trigonometry, 3e
				Dugopolski: College Algebra, 3e
				Dugopolski: College Algebra, 4e ENHANCED
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus ENHANCED Re-release
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus with Limits: Functions &amp; Graphs
				Dugopolski: Precalculus, 3e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs: 1e
				Dugopolski: Trigonometry
				ECPI MTH099: Beginning Algebra
				ECPI MTH115: Thinking Mathematically
				ECPI MTH125/MTH131: Intermediate Algebra
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e, GE
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Eileen's Preloaded Testing Book
				eT1 Math Test Book
				ET1 Math Test Book 10 CA
				eT1 Math Test Book 11 - Non Course Aware 11
				ET1 Math Test Book 12 CA
				eT1 Math Test Book 15
				eT1 Math Test Book 15 - Non Course Aware 15
				eT1 Math Test Book 16
				eT1 Math Test Book 16 - Non Course Aware 16
				eT1 Math Test Book 17
				eT1 Math Test Book 17 - Non Course Aware 17
				eT1 Math Test Book 2
				eT1 Math Test Book 20 - Non Course Aware 20
				eT1 Math Test Book 20 CA
				eT1 Math Test Book 3
				eT1 Math Test Book 3 - Non Course Aware
				eT1 Math Test Book 3 - Non Course Aware 2
				eT1 Math Test Book 3 - Non Course Aware 3
				eT1 Math Test Book 4
				ET1 Math Test Book 4 CA
				eT1 Math Test Book 5 - Non Course Aware 5
				eT1 Math Test Book 6 - Non Course Aware 6
				ET1 Math Test Book 6 CA
				eT1 Math Test Book 7 - Non Course Aware 7
				ET1 Math Test Book 7 CA
				eT1 Math Test Book 8 - Non Course Aware 8
				ET1 Math Test Book 8 CA
				eT1 Math Test Book 9 - Non Course Aware 9
				ET1 Math Test Book 9 CA
				eT1 Math Test Book K2
				ET2 Cert - Pxe_2
				eT2 Disaggregated - NON course aware test book
				eT2 Math Test Book - CERT CITE
				eT2 Math Test Book - CERT PXE
				eT2 Math Test Book - CERT with Print ID
				eT2 Math Test Book CERT
				eText Integration Trigsted College Alg Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e, Global Edition
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				FIN 321 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 325 Corporate Finance for Arcuri at SUNY at Oswego
				FIN256 Corporation Finance for Barkley at Syracuse University
				Financial Markets and International Finance, Second Custom Edition for RMU
				Florida CLAST Guide
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Fundamentals of Corporate Finance Third Custom Ed. for Boston University FE 101 
				Fundamentals of Corporate Finance Third Custom Edition for Boston University
				Fundamentals of Corporate Finance, 3rd Edition for California State U. Fullerton
				Fundamentals of Finance, by Cole for UT Knoville
				Gitman/Zutter: Principles of Managerial Finance, BRIEF 6e
				Goetz: Basic Mathematics
				Goldstein, Calculus and Its Applications, 14/e, Global Edition
				Goldstein: Brief Calculus and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 10e
				Gould, Essential Statistics, 2/e, Global Edition
				Gould: Introductory Statistics: Exploring the World through Data, 1e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2/e, Global Edition
				Greenwell: Calculus for the Life Sciences
				Greenwich Quantitative Methods Custom MyMath Lab 2020
				Greenwich Quantitative Methods Custom MyMath Lab 2021
				Grimaldo/Robichaud: Introductory Algebra with Layers for Learning, 1e DEMO
				Groebner, Business Statistics: A Decision-Making Approach, 10/e, Global Edition
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler, Introductory Mathematical Analysis, 14e, Global Edition 
				Haeussler: Introductory Mathematical Analysis, 13e
				Harshbarger: College Algebra In Context, 2e ENHANCED
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, ENHANCED
				Hass, Thomas’ Calculus Early Transcendentals, 14/e, Global Edition
				Hass, Thomas’ Calculus, 14e in SI Units
				Hass, University Calculus Early Transcendentals, 3/e, Global Edition
				Hass, University Calculus: Early Transcendentals, 4/e in SI Units
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 4e ENHANCED
				Hornsby: A Graphical Approach to College Algebra, 4e ENHANCED
				Hornsby: A Graphical Approach to Precalculus, 4e ENHANCED
				Hornsby: Graphical Approach to College Algebra &amp; Trigonometry, 3e
				Hornsby: Graphical Approach to College Algebra, 3e
				Hornsby: Graphical Approach to Precalculus with Limits, 3e
				Hornsby: Graphical Approach to Precalculus, 3e
				Hubbard: Money, the Financial System, and the Economy, 5th Edition, MATH REVIEW
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				IPRO Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				James, Modern Engineering Mathematics, 4e - DEMO EMA
				John Jay: Thinking Mathematically with Algebra (Blitzer)
				Johnston: Calculus
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e ENHANCED
				Jordan: Integrated Arithmetic and Basic Algebra: 2e
				Just-In-Time Online Algebra
				Just-In-Time Online Algebra &amp; Trigonometry
				Kaplan Math 103
				Kaplan MM201-A
				Kaplan MM201-B
				Kaplan MM207 Statistics
				Knewton Math Enabling Test
				Knewton Math test book
				Knewton tiny test book
				Lancaster University Custom MyLab Math 2020
				Larson, Elementary Statistics: Picturing the World, 6/e, Global Edition
				Larson: Elementary Statistics: Picturing the World, 5e
				Larson: Elementary Statistics: Picturing the World, Global Edition, 7/e
				Lay, Linear Algebra and Its Applications, 5/e, Global Edition
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 3e Update
				Lay: Linear Algebra and Its Applications, 6/e, Global Edition
				Lehmann: Elementary Algebra: Graphs and Authentic Applications (ENHANCED)
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Levine, Statistics for Managers Using Microsoft Excel, 8/e, Global Edition
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Business Statistics: A First Course, 8/e, Global Edition
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial Finite Mathematics, 10e
				Lial, Introductory Algebra, 11/e, Global Edition
				Lial/Greenwell/Ritchey: Calculus with Applications, 11/e, Global Edition
				Lial/Hungerford: Finite Mathematics with Applications, 11/e, Global Edition
				Lial: Algebra for College Students, 5e ENHANCED
				Lial: Basic College Mathematics, 6e
				Lial: Basic College Mathematics, 7e ENHANCED
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Beginning Algebra, 10e
				Lial: Beginning Algebra, 11e
				Lial: Beginning Algebra, 9e ENHANCED
				Lial: Beginning Algebra: 8e
				Lial: Beginning and Intermediate Algebra, 3e ENHANCED
				Lial: Calculus with Applications, 10e
				Lial: College Algebra and Trigonometry, 3e ENHANCED
				Lial: College Algebra and Trigonometry, 5e
				Lial: College Algebra and Trigonometry, 6/e, Global Edition
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra, 11e
				Lial: College Algebra, 9e ENHANCED
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Essential Mathematics
				Lial: Essential Mathematics, 2e ENHANCED
				Lial: Essentials of College Algebra Alternate Edition, 1e ENHANCED
				Lial: Essentials of College Algebra, 11/e, Global Edition
				Lial: Essentials of College Algebra, 1e ENHANCED
				Lial: Finite Mathematics, 10e-Lil
				Lial: Finite Mathematics, 8e ENHANCED
				Lial: Intermediate Algebra with Early Functions and Graphing, 7e
				Lial: Intermediate Algebra, 8e ENHANCED
				Lial: Intermediate Algebra, 9e
				Lial: Intermediate Algebra: Alternate 8e
				Lial: Introductory &amp; Intermediate Algebra, 4e
				Lial: Introductory Algebra, 10e
				Lial: Introductory Algebra, 7e
				Lial: Introductory and Intermediate Algebra, 2e
				Lial: Introductory and Intermediate Algebra, 3e ENHANCED
				Lial: Mathematics with Applications, 8e
				Lial: Mathematics with Applications, Finite Version, 8e
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra and Introductory Algebra
				Lial: Prealgebra, 2e
				Lial: Prealgebra, 3e ENHANCED
				Lial: Prealgebra, 4e
				Lial: Precalculus with Limits
				Lial: Precalculus, 3e ENHANCED
				Lial: Trigonometry, 8e ENHANCED
				Long: Mathematical Reasoning for Elementary Teachers, 3e
				Long: Mathematical Reasoning for Elementary Teachers, 4e ENHANCED
				Long: Mathematical Reasoning for Elementary Teachers, 7/e, Global Edition
				Martin-Gay Algebra A Combined Approach, 2e ENHANCED
				Martin-Gay: Algebra 1
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra: A Combined Approach, 4e
				Martin-Gay: Basic College Mathematics with Early Integers, 2e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 2e ENHANCED
				Martin-Gay: Basic College Mathematics, 3e ENHANCED
				Martin-Gay: Basic College Mathematics, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e ENHANCED
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 2e ENHANCED
				Martin-Gay: Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Intermediate Algebra, 4e ENHANCED
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e ENHANCED
				Martin-Gay: Introductory Algebra 2e ENHANCED
				Martin-Gay: Introductory Algebra, 3e ENHANCED
				Martin-Gay: Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra and Introductory Algebra, ENHANCED
				Martin-Gay: Prealgebra, 4e ENHANCED
				Martin-Gay: Prealgebra, 6e
				Math Preloaded Course Test Book 1
				Math Preloaded Course Test Book 2
				Math Preloaded Course Test Book 3
				Math Preloaded Course Test Book 4
				Math Preloaded Course Test Book 5
				Math TG Only
				Mathematica V8 testing (SBnetpreview)
				Maths Booster UK
				Mathspace Master TOC of Exercises (version 1)
				McClave: A First Course in Statistics, 12/e, Global Edition
				McClave: Statistics for Business and Economics, Global Edition, 13/e
				McClave: Statistics, 13/e, Global Edition
				Middlesex Community College: MTH001/002/003: Preparation for College Math 2021
				Miller: Business Mathematics, 10e ENHANCED
				Miller: Business Mathematics, 9e
				Miller: Economics Today, 12th Edition, MATH REVIEW
				Miller: Economics Today: MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, MyEconLab Edition, MATH REVIEW
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 10e ENHANCED
				Miller: Mathematical Ideas, 12e
				Miller: Mathematical Ideas, Expanded 10e ENHANCED
				Mishkin: The Economics of Money, Banking, &amp; Financial Markets, 7th Ed, MATH REV
				MML-CoCo: MyBeginningAlgebraCourse (Martin-Gay 5e)
				Morris: Quantitative Approaches in Business Studies 7e, EMA
				Multinational Business Finance Custom Edition for Ryerson
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyAccountingLab CD Demo
				MyFoundationsLab Prototype
				MyHealthProfessionsLab for Math Basics for the Health Care Professional, 5/e
				MyMathLab Global, Australian/New Zealand edition
				MyMathLab test book A (from Blitzer TM5)
				MyMathLab test book A2
				MyMathLab test book A3 - Lilani 6/15
				MyMathLab test book B (from Briggs Calc)
				MyMathLab test book B2
				MyMathLab test book B3
				MyMathLab test book C2
				MyMathLab test book C3
				MyMathLab test book D3 MOBILE ONLY
				MyMathTest: Developmental Mathematics
				Nagle, Fundamentals of Differential Equations, 9/e, Global Edition
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				National University - custom 03/2002
				National University: BST322: Introduction to Biomedical Statistics
				New Import Test
				Newbold: Statistics for Business and Economics, Global Edition, 9/e
				Newbold: Statistics for Business and Economics: Global Edition, 7e
				NextGen Media Econ Test Book-Janus-Active
				NextGen Media Math Test Book-- Early Alerts
				NextGen Media Math Test Book--dulanjali
				NextGen Media Math Test Book--EA Auto
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL--Rush
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL_BigInt2
				NextGen Media Math Test Book--REAL_upetha
				NG Math Media 2
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				Nottingham University Custom MyLab Maths 2021
				O'Daffer: Mathematics for Elementary Teachers, 3e ENHANCED
				O'Daffer: Mathematics for Elementary Teachers: 2e
				Olivier: Business Mathematics Interactive
				Parkin: Economics, 6th Edition, MATH REVIEW
				Parkin: Macroeconomics, 6th Edition, MATH REVIEW
				Parkin: Microeconomics, 6th Edition, MATH REVIEW
				Pasco Hernando Community Colle: MAT 0018 &amp; 0028: Prealgebra &amp; Elementary Algebra
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Perloff: Microeconomics, 3rd Edition, MATH REVIEW
				Phil's Test Book
				Pirnot: Mathematics All Around, 2e
				Pirnot: Mathematics All Around, 3e
				Pirnot: Mathematics All Around, 5e
				Portsmouth Uni Foundation Maths Custom MyLab Math 2021
				Prior: Basic Mathematics, 1e
				Prior: Prealgebra, 1e
				Queensborough CC: Intermediate Algebra with Trigonometry (Blitzer)
				Ratti, College Algebra and Trigonometry, 3/e, Global Edition
				Ratti: College Algebra, 4e
				Req 54 Test 2 level math
				Req 54 Test book
				Req54Test-no 0 section numbers
				Rockswold/Krieger: Interactive Developmental Mathematics, 2e TEST
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 3e ENHANCED
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Applications &amp; Visualization, 1e ENHANCED
				Rockswold: Beginning and Intermediate Algebra ENHANCED
				Rockswold: College Alg and Trig through Modeling &amp; Visualization, 2e
				Rockswold: College Algebra through Modeling &amp; Visualization, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e ENHANCED
				Rockswold: Developmental Math, 1e
				Rockswold: Intermediate Algebra through Modeling &amp; Visualization: 1e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e ENHANCED
				Rockswold: Precalculus through Modeling &amp; Visualization, 2e
				Rockswold: Precalculus with Modeling and Visualization, 3e ENHANCED
				Royal Holloway Custom MyLab Math 2020
				Ruth SPP Test Book
				Ruth Test Course for Pre-Made Assignments
				Salzman: Mathematics for Business, 7e
				Sam Math Book_Flash
				Sample Math Book
				Saunders: Mathematics for the Trades POOL DEMO
				Sharpe: Business Statistics, 3/e, Global Edition
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, 4e, GE
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course, 3e
				Shihab, Numeracy in Nursing and Healthcare 1e - EMA DEMO
				SI Notation testing (from sbtest)
				Sinclair Community College: MAT 1130 2018
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Small: Basic Mathematics, 3e
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Basic Mathematics - Demo
				Squires: Developmental Math, 2e RIO PILOT
				Squires: Introductory Algebra
				StatCrunch Question Library (Online Only)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Statistics Online, 1e DEMO chapter 10
				Sullivan/Woodbury: Statistics Online, 1e DEMO--OLD IRAS
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 4e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 7e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 8e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: College Algebra w/ Graphing Utilities, 4e ENHANCED
				Sullivan: College Algebra, 7e ENHANCED
				Sullivan: College Algebra, 9e
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: Intermediate Algebra, 1e (2007)
				Sullivan: Intermediate Algebra, 1e (2007) DEMO
				Sullivan: Precalculus w/ Graphing Utilities, 4e ENHANCED
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 10/e, Global Edition
				Sullivan: Precalculus, 7e ENHANCED
				Sullivan: Precalculus, 9e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5/e, Global Edition
				Sullivan: Trigonometry, 7e ENHANCED
				Swansea University Custom MyLab Maths 2021
				Swansea University: Wedlake Custom MLG 2018
				Tannenbaum: Excursions in Modern Mathematics, 7e
				Tanya Mathspace test
				TESTGEN TESTING Lial Calc
				The University of Texas at Arlington: Math 1327
				Thomas' Calculus Early Transcendentals Media Upgrade, 11e
				Thomas' Calculus Media Upgrade, 11e
				Thomas' Calculus, Early Transcendentals, Updated 10e
				Thomas' Calculus, Updated 10e
				Thomas, Thomas' Calculus in SI Units, 13/e
				Thomas, Thomas' Calculus: Early Transcendentals in SI Units, 13/e
				Tidewater Community College:  Math Essentials MTE 1-9
				Titman/Martin/Keown: Financial Management, 14e,GE
				Tobey: Basic College Mathematics, 5e ENHANCED
				Tobey: Basic College Mathematics, 7e
				Tobey: Beginning &amp; Intermediate Algebra, 2e ENHANCED
				Tobey: Beginning Algebra, 6e ENHANCED
				Tobey: Beginning Algebra, 8e
				Tobey: Beginning Algebra: Early Graphing, 1e ENHANCED
				Tobey: Beginning and Intermediate Algebra, 3e
				Tobey: Beginning and Intermediate Algebra, 4e
				Tobey: Essentials of Basic College Mathematics, 1e ENHANCED
				Tobey: Intermediate Algebra, 5e ENHANCED
				Trigsted: Beginning &amp; Intermediate Algebra, 2e
				Trigsted: College Algebra 1e
				Trigsted: College Algebra 1e DEMO
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra Interactive, Chapters R-5
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Developmental Mathematics, 2e
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trigsted: Intermediate Algebra
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Biostatistics for the Biological and Health Sci, 2/e, Global Edition
				Triola: Elementary Statistics Using Excel: 1e
				Triola: Elementary Statistics, 10e ENHANCED
				Triola: Elementary Statistics, 11e Technology Update
				Triola: Elementary Statistics, 3ce DEMO CANADA
				Triola: Essentials of Statistics: 1e
				UK Test Cicchitelli, D'Urso, Minozzo: Statistics
				Univ of Florida: Precalculus 1140/1147/1114
				University of Alberta: Calculus
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: STAT 200: Elementary Statistics
				Valencia Community College - Bittinger: Prealgebra
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e, Global Edition
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Introduction to Technical Mathematics, 5e ENHANCED
				Waters, Quantitative Methods for Business 4e - EMA
				Weiss, Introductory Statistics, 10/e, Global Edition
				Weiss: Elementary Statistics: 5e
				Weiss: Introductory Statistics, 8e
				Weiss: Introductory Statistics: 6e
				Wisniewski, Quantitative Methods for Decision Makers 5e - EMA
				Wisniewski/Shafti: Quantitative Analysis for Decision Makers 7e
				Woodbury: Elementary &amp; Intermediate Algebra, 1e ENHANCED
				Workspace Test Book
				XL Load Testbook
				Young: Finite Mathematics, 3e ENHANCED
				Zutter/Smart: Principles of Managerial Finance, 16/e, Global Edition
				ZZIA Interactive testing
				zzTesting eT2 book export
				zzz-Trigsted test book for Interactive Dev Math 2e

			</value>
      <webElementGuid>15dce828-942b-4b46-b21e-ee6076cf30ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
      <webElementGuid>753dbe77-ec87-4268-8a5d-1f8a20bf0121</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      <webElementGuid>171665f0-b4df-46cd-a5a0-6ca7e1aaafb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
      <webElementGuid>c43da6b3-2a82-4f92-b963-ec18e8e31932</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
      <webElementGuid>c4b8bb66-ba7c-4dc2-bd5f-b950f9dd09d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
      <webElementGuid>69f1d04b-3815-412a-b495-c3776c30c957</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>da1d28f5-7cca-4da4-b713-be81786be6aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = concat(&quot;
				  -- Choose Book --  
				 eT1 Math Test Book 10 - Non Course Aware 10
				 Levine: Statistics for Managers Using Microsoft Excel, 9e, Global Edition
				9780857761071 Stoorvogel EMA
				Abel/Bernanke: Macroeconomics, 5th Edition, MATH REVIEW
				Adams: Calculus, 6e ENHANCED  DEMO
				Adams: Calculus, 7e
				ADP: Algebra II Online Course
				Agresti, Statistics, 4/e, Global Edition
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				AIS Test: Chemistry: Structure and Properties - withSite
				AIS Test: Chemistry: Structure and Properties v1
				AIS Test: Chemistry: Structure and Properties v3
				AIS Test: Chemistry: Structure and Properties v4
				AIS Test: Chemistry: Structure and Properties v5
				AIS Test: Human Anatomy and Physiology v3
				Akst: Basic Mathematics through Applications, 3e ENHANCED
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Fundamental Mathematics through Applications, 3e ENHANCED
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Intermediate Algebra through Applications, 1e ENHANCED
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 3e
				Akst: Introductory Algebra through Applications, 1e ENHANCED
				Akst: Introductory Algebra Through Applications, 2e
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book:  Non-combo
				Alicia&quot; , &quot;'&quot; , &quot;s book for menu options
				Alicia&quot; , &quot;'&quot; , &quot;s MNL Pharmacology XL style
				Alicia&quot; , &quot;'&quot; , &quot;s tiny test book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Angel: A Survey of Mathematics with Applications, 7e ENHANCED
				Angel: A Survey of Mathematics with Applications, Expanded 7e ENHANCED
				Angel: Algebra for College Students, 3e ENHANCED
				Angel: Elementary Algebra for College Students Early Graphing, 3e  ENHANCED
				Angel: Elementary Algebra for College Students, 9e
				Angel: Elementary and Intermediate Algebra for College Students, 3e (ENHANCED)
				Angel: Intermediate Algebra for College Students, 10e
				Angel: Intermediate Algebra for College Students, 7e (ENHANCED)
				Anne Arundel Community College: MAT 044/045/145/146 (2020)
				Austin CC: Basic College Math
				Bade/Parkin: Essential Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, MATH REVIEW
				Baker College: Basic College Mathematics (Tobey)
				Barnett, College Math for Bus, Econ, Life Sci, and Soc Sci, 14/e, Global Edition
				Barnett, Finite Math for Bus, Econ, Life Sci, and Soc Sci, 14/e, Global Edition
				Barnett: Calculus for Bus, Econ, Life and Social Sci, 14/e, Global Edition
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13/e, Global Ed
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13e
				Barnett: College Math for Bus, Econ, Life&amp;Social Sci, 13/e, Global Edition
				Barnett: Finite Mathematics, 10e ENHANCED
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Beecher: Algebra &amp; Trigonometry, 2e ENHANCED
				Beecher: Algebra &amp; Trigonometry, 3e ENHANCED
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 2e ENHANCED
				Beecher: Precalculus, 2e ENHANCED
				Beecher: Precalculus, 3e
				Bennett: Essentials of Using &amp; Understanding Mathematics
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Using and Understanding Mathematics, 3e ENHANCED
				Bennett: Using and Understanding Mathematics, 4e
				Berenson, Basic Business Statistics, 13/e, Global Edition
				Berenson: Basic Business Statistics, 14/e, Global Edition
				Berenson: Basic Business Statistics, 5e (Aus)
				Berk/DeMarzo/Harford: Fundamentals of Corporate Finance, 5/e, Global Edition
				Berk/DeMarzo: Corporate Finance: The Core, 5e, Global Edition
				Betsy Smoketest 1-19-10
				Betsy Smoketest 9-28
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 11e
				Billstein: Mathematics for Elementary School Teachers, 8e
				Bittinger, Basic College Mathematics, 12/e, Global Edition
				Bittinger: Algebra and Trig: Graphs &amp; Models, 2e
				Bittinger: Algebra and Trig: Graphs &amp; Models, Unit Circle Approach, 1e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 5e
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 9e ENHANCED
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: Calculus and Its Applications, 9e DEMO
				Bittinger: College Algebra: Graphs &amp; Models, 2e
				Bittinger: College Algebra: Graphs &amp; Models, 3e
				Bittinger: College Algebra: Graphs &amp; Models, 5e
				Bittinger: Developmental Mathematics, 6e ENHANCED
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Developmental Mathematics, 8e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e ENHANCED
				Bittinger: Elementary Algebra, Concepts and Applications, 8e (2010)
				Bittinger: Elementary Algebra, Concepts and Applications, 9e
				Bittinger: Elementary Algebra: Concepts &amp; Apps, 6e
				Bittinger: Elementary Algebra: Graphs &amp; Models, 1e ENHANCED
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Elementary and Intermediate Algebra: Concepts &amp; Apps, 3e
				Bittinger: Elementary and Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Elementary and Intermediate Algebra: Graphs &amp; Models: 1e
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3e ENHANCED
				Bittinger: Fundamental Mathematics, 4e
				Bittinger: Fundamentals of College Algebra: Graphs &amp; Models
				Bittinger: Houston Community College Prealgebra, 3e
				Bittinger: Intermediate Algebra, 10e ENHANCED
				Bittinger: Intermediate Algebra, 9e ENHANCED
				Bittinger: Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Intermediate Algebra: 8e Alternate Version
				Bittinger: Intermediate Algebra: Concepts &amp; Apps, 6e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Intermediate Algebra: Graphs &amp; Models: 1e
				Bittinger: Introductory Algebra, 10e ENHANCED
				Bittinger: Introductory Algebra, 11e
				Bittinger: Introductory Algebra, 9e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 2e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 3e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 4e
				Bittinger: Prealgebra &amp; Introductory Algebra, 3e
				Bittinger: Prealgebra and Introductory Algebra, 1e ENHANCED
				Bittinger: Prealgebra, 4e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, A Unit Circle Approach
				Bittinger: Precalculus: Graphs &amp; Models: 2e
				Bittinger: Trigonometry: Graphs &amp; Models, 2e
				Blair/Tobey/Slater: Prealgebra, 3e ENHANCED
				Blair/Tobey: Prealgebra, 4e
				Blair: Intermediate Algebra, 1e
				Blair: Prealgebra, 5e 
				Blitzer:  College Algebra, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 2e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 5e ENHANCED
				Blitzer: College Algebra, 4e ENHANCED
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: Essentials of Intermediate Algebra for College Students ENHANCED
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra ENHANCED
				Blitzer: Intermediate Algebra for College Students, 4e ENHANCED
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 4e ENHANCED
				Blitzer: Introductory and Intermediate Algebra for College Students, 2e ENHANCED
				Blitzer: Precalculus Essentials, 2e ENHANCED
				Blitzer: Precalculus, 2e ENHANCED
				Blitzer: Precalculus, 3e ENHANCED
				Blitzer: Thinking Mathematically with Integrated Review
				Blitzer: Thinking Mathematically, 3e ENHANCED
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Bluegrass Community and Technical College: MA 111 Contemporary Math (2021)
				Briggs, Calculus, 2/e, Global Edition
				Briggs/Cochran/Gillett: Calculus: Early Transcendentals, 2/e, Global Edition
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update Aida Pilot
				Briggs/Cochran: Calculus, 2e
				Business Algebra, Second Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT final
				Carson: Elementary Algebra with Early Systems of Equations, ENHANCED
				Carson: Elementary Algebra, 2e ENHANCED
				Carson: Elementary Algebra, ENHANCED
				Carson: Elementary and Intermediate Algebra, ENHANCED
				Carson: Intermediate Algebra, 2nd Edition ENHANCED
				Carson: Intermediate Algebra, ENHANCED
				Carson: Prealgebra, 2e ENHANCED
				Carson: Prealgebra: 1e
				CCNG Trigsted Intermediate Alg Test Book
				CFO Online Finance Course. DEMO
				Chris Feb Tier 3 Test Math
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 2e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving: 1e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv, 2e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv, 3e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv: 1e
				Consortium: Math in Action: Prealgebra Problem Solving, 1e
				Consortium: Math In Action: Prealgebra Problem Solving, 2e (2008) ENHANCED
				Contemporary Business Mathematics (BMAT 110)
				Corporate Finance, 4e for University of California-Berkeley
				Croft/Davison: Foundation Maths 7e
				De Veaux, Stats: Data &amp; Models, 4/e, Global Edition
				De Veaux: Stats: Data &amp; Models, 5e, Global Edition
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana: Calculus: Graphical, Numerical, Algebraic, 3e ENHANCED
				Demana: Precalculus: Functions and Graphs, 5e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9/e, Global Edition
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core, 9/e, GE 
				DeVry MAT190 (Washington)
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra and Trigonometry, 3e
				Dugopolski: College Algebra, 3e
				Dugopolski: College Algebra, 4e ENHANCED
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus ENHANCED Re-release
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus with Limits: Functions &amp; Graphs
				Dugopolski: Precalculus, 3e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs: 1e
				Dugopolski: Trigonometry
				ECPI MTH099: Beginning Algebra
				ECPI MTH115: Thinking Mathematically
				ECPI MTH125/MTH131: Intermediate Algebra
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e, GE
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Eileen&quot; , &quot;'&quot; , &quot;s Preloaded Testing Book
				eT1 Math Test Book
				ET1 Math Test Book 10 CA
				eT1 Math Test Book 11 - Non Course Aware 11
				ET1 Math Test Book 12 CA
				eT1 Math Test Book 15
				eT1 Math Test Book 15 - Non Course Aware 15
				eT1 Math Test Book 16
				eT1 Math Test Book 16 - Non Course Aware 16
				eT1 Math Test Book 17
				eT1 Math Test Book 17 - Non Course Aware 17
				eT1 Math Test Book 2
				eT1 Math Test Book 20 - Non Course Aware 20
				eT1 Math Test Book 20 CA
				eT1 Math Test Book 3
				eT1 Math Test Book 3 - Non Course Aware
				eT1 Math Test Book 3 - Non Course Aware 2
				eT1 Math Test Book 3 - Non Course Aware 3
				eT1 Math Test Book 4
				ET1 Math Test Book 4 CA
				eT1 Math Test Book 5 - Non Course Aware 5
				eT1 Math Test Book 6 - Non Course Aware 6
				ET1 Math Test Book 6 CA
				eT1 Math Test Book 7 - Non Course Aware 7
				ET1 Math Test Book 7 CA
				eT1 Math Test Book 8 - Non Course Aware 8
				ET1 Math Test Book 8 CA
				eT1 Math Test Book 9 - Non Course Aware 9
				ET1 Math Test Book 9 CA
				eT1 Math Test Book K2
				ET2 Cert - Pxe_2
				eT2 Disaggregated - NON course aware test book
				eT2 Math Test Book - CERT CITE
				eT2 Math Test Book - CERT PXE
				eT2 Math Test Book - CERT with Print ID
				eT2 Math Test Book CERT
				eText Integration Trigsted College Alg Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e, Global Edition
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				FIN 321 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 325 Corporate Finance for Arcuri at SUNY at Oswego
				FIN256 Corporation Finance for Barkley at Syracuse University
				Financial Markets and International Finance, Second Custom Edition for RMU
				Florida CLAST Guide
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Fundamentals of Corporate Finance Third Custom Ed. for Boston University FE 101 
				Fundamentals of Corporate Finance Third Custom Edition for Boston University
				Fundamentals of Corporate Finance, 3rd Edition for California State U. Fullerton
				Fundamentals of Finance, by Cole for UT Knoville
				Gitman/Zutter: Principles of Managerial Finance, BRIEF 6e
				Goetz: Basic Mathematics
				Goldstein, Calculus and Its Applications, 14/e, Global Edition
				Goldstein: Brief Calculus and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 10e
				Gould, Essential Statistics, 2/e, Global Edition
				Gould: Introductory Statistics: Exploring the World through Data, 1e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2/e, Global Edition
				Greenwell: Calculus for the Life Sciences
				Greenwich Quantitative Methods Custom MyMath Lab 2020
				Greenwich Quantitative Methods Custom MyMath Lab 2021
				Grimaldo/Robichaud: Introductory Algebra with Layers for Learning, 1e DEMO
				Groebner, Business Statistics: A Decision-Making Approach, 10/e, Global Edition
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler, Introductory Mathematical Analysis, 14e, Global Edition 
				Haeussler: Introductory Mathematical Analysis, 13e
				Harshbarger: College Algebra In Context, 2e ENHANCED
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, ENHANCED
				Hass, Thomas’ Calculus Early Transcendentals, 14/e, Global Edition
				Hass, Thomas’ Calculus, 14e in SI Units
				Hass, University Calculus Early Transcendentals, 3/e, Global Edition
				Hass, University Calculus: Early Transcendentals, 4/e in SI Units
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 4e ENHANCED
				Hornsby: A Graphical Approach to College Algebra, 4e ENHANCED
				Hornsby: A Graphical Approach to Precalculus, 4e ENHANCED
				Hornsby: Graphical Approach to College Algebra &amp; Trigonometry, 3e
				Hornsby: Graphical Approach to College Algebra, 3e
				Hornsby: Graphical Approach to Precalculus with Limits, 3e
				Hornsby: Graphical Approach to Precalculus, 3e
				Hubbard: Money, the Financial System, and the Economy, 5th Edition, MATH REVIEW
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				IPRO Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				James, Modern Engineering Mathematics, 4e - DEMO EMA
				John Jay: Thinking Mathematically with Algebra (Blitzer)
				Johnston: Calculus
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e ENHANCED
				Jordan: Integrated Arithmetic and Basic Algebra: 2e
				Just-In-Time Online Algebra
				Just-In-Time Online Algebra &amp; Trigonometry
				Kaplan Math 103
				Kaplan MM201-A
				Kaplan MM201-B
				Kaplan MM207 Statistics
				Knewton Math Enabling Test
				Knewton Math test book
				Knewton tiny test book
				Lancaster University Custom MyLab Math 2020
				Larson, Elementary Statistics: Picturing the World, 6/e, Global Edition
				Larson: Elementary Statistics: Picturing the World, 5e
				Larson: Elementary Statistics: Picturing the World, Global Edition, 7/e
				Lay, Linear Algebra and Its Applications, 5/e, Global Edition
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 3e Update
				Lay: Linear Algebra and Its Applications, 6/e, Global Edition
				Lehmann: Elementary Algebra: Graphs and Authentic Applications (ENHANCED)
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Levine, Statistics for Managers Using Microsoft Excel, 8/e, Global Edition
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Business Statistics: A First Course, 8/e, Global Edition
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial Finite Mathematics, 10e
				Lial, Introductory Algebra, 11/e, Global Edition
				Lial/Greenwell/Ritchey: Calculus with Applications, 11/e, Global Edition
				Lial/Hungerford: Finite Mathematics with Applications, 11/e, Global Edition
				Lial: Algebra for College Students, 5e ENHANCED
				Lial: Basic College Mathematics, 6e
				Lial: Basic College Mathematics, 7e ENHANCED
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Beginning Algebra, 10e
				Lial: Beginning Algebra, 11e
				Lial: Beginning Algebra, 9e ENHANCED
				Lial: Beginning Algebra: 8e
				Lial: Beginning and Intermediate Algebra, 3e ENHANCED
				Lial: Calculus with Applications, 10e
				Lial: College Algebra and Trigonometry, 3e ENHANCED
				Lial: College Algebra and Trigonometry, 5e
				Lial: College Algebra and Trigonometry, 6/e, Global Edition
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra, 11e
				Lial: College Algebra, 9e ENHANCED
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Essential Mathematics
				Lial: Essential Mathematics, 2e ENHANCED
				Lial: Essentials of College Algebra Alternate Edition, 1e ENHANCED
				Lial: Essentials of College Algebra, 11/e, Global Edition
				Lial: Essentials of College Algebra, 1e ENHANCED
				Lial: Finite Mathematics, 10e-Lil
				Lial: Finite Mathematics, 8e ENHANCED
				Lial: Intermediate Algebra with Early Functions and Graphing, 7e
				Lial: Intermediate Algebra, 8e ENHANCED
				Lial: Intermediate Algebra, 9e
				Lial: Intermediate Algebra: Alternate 8e
				Lial: Introductory &amp; Intermediate Algebra, 4e
				Lial: Introductory Algebra, 10e
				Lial: Introductory Algebra, 7e
				Lial: Introductory and Intermediate Algebra, 2e
				Lial: Introductory and Intermediate Algebra, 3e ENHANCED
				Lial: Mathematics with Applications, 8e
				Lial: Mathematics with Applications, Finite Version, 8e
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra and Introductory Algebra
				Lial: Prealgebra, 2e
				Lial: Prealgebra, 3e ENHANCED
				Lial: Prealgebra, 4e
				Lial: Precalculus with Limits
				Lial: Precalculus, 3e ENHANCED
				Lial: Trigonometry, 8e ENHANCED
				Long: Mathematical Reasoning for Elementary Teachers, 3e
				Long: Mathematical Reasoning for Elementary Teachers, 4e ENHANCED
				Long: Mathematical Reasoning for Elementary Teachers, 7/e, Global Edition
				Martin-Gay Algebra A Combined Approach, 2e ENHANCED
				Martin-Gay: Algebra 1
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra: A Combined Approach, 4e
				Martin-Gay: Basic College Mathematics with Early Integers, 2e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 2e ENHANCED
				Martin-Gay: Basic College Mathematics, 3e ENHANCED
				Martin-Gay: Basic College Mathematics, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e ENHANCED
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 2e ENHANCED
				Martin-Gay: Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Intermediate Algebra, 4e ENHANCED
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e ENHANCED
				Martin-Gay: Introductory Algebra 2e ENHANCED
				Martin-Gay: Introductory Algebra, 3e ENHANCED
				Martin-Gay: Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra and Introductory Algebra, ENHANCED
				Martin-Gay: Prealgebra, 4e ENHANCED
				Martin-Gay: Prealgebra, 6e
				Math Preloaded Course Test Book 1
				Math Preloaded Course Test Book 2
				Math Preloaded Course Test Book 3
				Math Preloaded Course Test Book 4
				Math Preloaded Course Test Book 5
				Math TG Only
				Mathematica V8 testing (SBnetpreview)
				Maths Booster UK
				Mathspace Master TOC of Exercises (version 1)
				McClave: A First Course in Statistics, 12/e, Global Edition
				McClave: Statistics for Business and Economics, Global Edition, 13/e
				McClave: Statistics, 13/e, Global Edition
				Middlesex Community College: MTH001/002/003: Preparation for College Math 2021
				Miller: Business Mathematics, 10e ENHANCED
				Miller: Business Mathematics, 9e
				Miller: Economics Today, 12th Edition, MATH REVIEW
				Miller: Economics Today: MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, MyEconLab Edition, MATH REVIEW
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 10e ENHANCED
				Miller: Mathematical Ideas, 12e
				Miller: Mathematical Ideas, Expanded 10e ENHANCED
				Mishkin: The Economics of Money, Banking, &amp; Financial Markets, 7th Ed, MATH REV
				MML-CoCo: MyBeginningAlgebraCourse (Martin-Gay 5e)
				Morris: Quantitative Approaches in Business Studies 7e, EMA
				Multinational Business Finance Custom Edition for Ryerson
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyAccountingLab CD Demo
				MyFoundationsLab Prototype
				MyHealthProfessionsLab for Math Basics for the Health Care Professional, 5/e
				MyMathLab Global, Australian/New Zealand edition
				MyMathLab test book A (from Blitzer TM5)
				MyMathLab test book A2
				MyMathLab test book A3 - Lilani 6/15
				MyMathLab test book B (from Briggs Calc)
				MyMathLab test book B2
				MyMathLab test book B3
				MyMathLab test book C2
				MyMathLab test book C3
				MyMathLab test book D3 MOBILE ONLY
				MyMathTest: Developmental Mathematics
				Nagle, Fundamentals of Differential Equations, 9/e, Global Edition
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				National University - custom 03/2002
				National University: BST322: Introduction to Biomedical Statistics
				New Import Test
				Newbold: Statistics for Business and Economics, Global Edition, 9/e
				Newbold: Statistics for Business and Economics: Global Edition, 7e
				NextGen Media Econ Test Book-Janus-Active
				NextGen Media Math Test Book-- Early Alerts
				NextGen Media Math Test Book--dulanjali
				NextGen Media Math Test Book--EA Auto
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL--Rush
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL_BigInt2
				NextGen Media Math Test Book--REAL_upetha
				NG Math Media 2
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				Nottingham University Custom MyLab Maths 2021
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 3e ENHANCED
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers: 2e
				Olivier: Business Mathematics Interactive
				Parkin: Economics, 6th Edition, MATH REVIEW
				Parkin: Macroeconomics, 6th Edition, MATH REVIEW
				Parkin: Microeconomics, 6th Edition, MATH REVIEW
				Pasco Hernando Community Colle: MAT 0018 &amp; 0028: Prealgebra &amp; Elementary Algebra
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Perloff: Microeconomics, 3rd Edition, MATH REVIEW
				Phil&quot; , &quot;'&quot; , &quot;s Test Book
				Pirnot: Mathematics All Around, 2e
				Pirnot: Mathematics All Around, 3e
				Pirnot: Mathematics All Around, 5e
				Portsmouth Uni Foundation Maths Custom MyLab Math 2021
				Prior: Basic Mathematics, 1e
				Prior: Prealgebra, 1e
				Queensborough CC: Intermediate Algebra with Trigonometry (Blitzer)
				Ratti, College Algebra and Trigonometry, 3/e, Global Edition
				Ratti: College Algebra, 4e
				Req 54 Test 2 level math
				Req 54 Test book
				Req54Test-no 0 section numbers
				Rockswold/Krieger: Interactive Developmental Mathematics, 2e TEST
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 3e ENHANCED
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Applications &amp; Visualization, 1e ENHANCED
				Rockswold: Beginning and Intermediate Algebra ENHANCED
				Rockswold: College Alg and Trig through Modeling &amp; Visualization, 2e
				Rockswold: College Algebra through Modeling &amp; Visualization, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e ENHANCED
				Rockswold: Developmental Math, 1e
				Rockswold: Intermediate Algebra through Modeling &amp; Visualization: 1e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e ENHANCED
				Rockswold: Precalculus through Modeling &amp; Visualization, 2e
				Rockswold: Precalculus with Modeling and Visualization, 3e ENHANCED
				Royal Holloway Custom MyLab Math 2020
				Ruth SPP Test Book
				Ruth Test Course for Pre-Made Assignments
				Salzman: Mathematics for Business, 7e
				Sam Math Book_Flash
				Sample Math Book
				Saunders: Mathematics for the Trades POOL DEMO
				Sharpe: Business Statistics, 3/e, Global Edition
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, 4e, GE
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course, 3e
				Shihab, Numeracy in Nursing and Healthcare 1e - EMA DEMO
				SI Notation testing (from sbtest)
				Sinclair Community College: MAT 1130 2018
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Small: Basic Mathematics, 3e
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Basic Mathematics - Demo
				Squires: Developmental Math, 2e RIO PILOT
				Squires: Introductory Algebra
				StatCrunch Question Library (Online Only)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Statistics Online, 1e DEMO chapter 10
				Sullivan/Woodbury: Statistics Online, 1e DEMO--OLD IRAS
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 4e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 7e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 8e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: College Algebra w/ Graphing Utilities, 4e ENHANCED
				Sullivan: College Algebra, 7e ENHANCED
				Sullivan: College Algebra, 9e
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: Intermediate Algebra, 1e (2007)
				Sullivan: Intermediate Algebra, 1e (2007) DEMO
				Sullivan: Precalculus w/ Graphing Utilities, 4e ENHANCED
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 10/e, Global Edition
				Sullivan: Precalculus, 7e ENHANCED
				Sullivan: Precalculus, 9e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5/e, Global Edition
				Sullivan: Trigonometry, 7e ENHANCED
				Swansea University Custom MyLab Maths 2021
				Swansea University: Wedlake Custom MLG 2018
				Tannenbaum: Excursions in Modern Mathematics, 7e
				Tanya Mathspace test
				TESTGEN TESTING Lial Calc
				The University of Texas at Arlington: Math 1327
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, Early Transcendentals, Updated 10e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, Updated 10e
				Thomas, Thomas&quot; , &quot;'&quot; , &quot; Calculus in SI Units, 13/e
				Thomas, Thomas&quot; , &quot;'&quot; , &quot; Calculus: Early Transcendentals in SI Units, 13/e
				Tidewater Community College:  Math Essentials MTE 1-9
				Titman/Martin/Keown: Financial Management, 14e,GE
				Tobey: Basic College Mathematics, 5e ENHANCED
				Tobey: Basic College Mathematics, 7e
				Tobey: Beginning &amp; Intermediate Algebra, 2e ENHANCED
				Tobey: Beginning Algebra, 6e ENHANCED
				Tobey: Beginning Algebra, 8e
				Tobey: Beginning Algebra: Early Graphing, 1e ENHANCED
				Tobey: Beginning and Intermediate Algebra, 3e
				Tobey: Beginning and Intermediate Algebra, 4e
				Tobey: Essentials of Basic College Mathematics, 1e ENHANCED
				Tobey: Intermediate Algebra, 5e ENHANCED
				Trigsted: Beginning &amp; Intermediate Algebra, 2e
				Trigsted: College Algebra 1e
				Trigsted: College Algebra 1e DEMO
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra Interactive, Chapters R-5
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Developmental Mathematics, 2e
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trigsted: Intermediate Algebra
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Biostatistics for the Biological and Health Sci, 2/e, Global Edition
				Triola: Elementary Statistics Using Excel: 1e
				Triola: Elementary Statistics, 10e ENHANCED
				Triola: Elementary Statistics, 11e Technology Update
				Triola: Elementary Statistics, 3ce DEMO CANADA
				Triola: Essentials of Statistics: 1e
				UK Test Cicchitelli, D&quot; , &quot;'&quot; , &quot;Urso, Minozzo: Statistics
				Univ of Florida: Precalculus 1140/1147/1114
				University of Alberta: Calculus
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: STAT 200: Elementary Statistics
				Valencia Community College - Bittinger: Prealgebra
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e, Global Edition
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Introduction to Technical Mathematics, 5e ENHANCED
				Waters, Quantitative Methods for Business 4e - EMA
				Weiss, Introductory Statistics, 10/e, Global Edition
				Weiss: Elementary Statistics: 5e
				Weiss: Introductory Statistics, 8e
				Weiss: Introductory Statistics: 6e
				Wisniewski, Quantitative Methods for Decision Makers 5e - EMA
				Wisniewski/Shafti: Quantitative Analysis for Decision Makers 7e
				Woodbury: Elementary &amp; Intermediate Algebra, 1e ENHANCED
				Workspace Test Book
				XL Load Testbook
				Young: Finite Mathematics, 3e ENHANCED
				Zutter/Smart: Principles of Managerial Finance, 16/e, Global Edition
				ZZIA Interactive testing
				zzTesting eT2 book export
				zzz-Trigsted test book for Interactive Dev Math 2e

			&quot;) or . = concat(&quot;
				  -- Choose Book --  
				 eT1 Math Test Book 10 - Non Course Aware 10
				 Levine: Statistics for Managers Using Microsoft Excel, 9e, Global Edition
				9780857761071 Stoorvogel EMA
				Abel/Bernanke: Macroeconomics, 5th Edition, MATH REVIEW
				Adams: Calculus, 6e ENHANCED  DEMO
				Adams: Calculus, 7e
				ADP: Algebra II Online Course
				Agresti, Statistics, 4/e, Global Edition
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				AIS Test: Chemistry: Structure and Properties - withSite
				AIS Test: Chemistry: Structure and Properties v1
				AIS Test: Chemistry: Structure and Properties v3
				AIS Test: Chemistry: Structure and Properties v4
				AIS Test: Chemistry: Structure and Properties v5
				AIS Test: Human Anatomy and Physiology v3
				Akst: Basic Mathematics through Applications, 3e ENHANCED
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Fundamental Mathematics through Applications, 3e ENHANCED
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Intermediate Algebra through Applications, 1e ENHANCED
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 3e
				Akst: Introductory Algebra through Applications, 1e ENHANCED
				Akst: Introductory Algebra Through Applications, 2e
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book:  Non-combo
				Alicia&quot; , &quot;'&quot; , &quot;s book for menu options
				Alicia&quot; , &quot;'&quot; , &quot;s MNL Pharmacology XL style
				Alicia&quot; , &quot;'&quot; , &quot;s tiny test book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Angel: A Survey of Mathematics with Applications, 7e ENHANCED
				Angel: A Survey of Mathematics with Applications, Expanded 7e ENHANCED
				Angel: Algebra for College Students, 3e ENHANCED
				Angel: Elementary Algebra for College Students Early Graphing, 3e  ENHANCED
				Angel: Elementary Algebra for College Students, 9e
				Angel: Elementary and Intermediate Algebra for College Students, 3e (ENHANCED)
				Angel: Intermediate Algebra for College Students, 10e
				Angel: Intermediate Algebra for College Students, 7e (ENHANCED)
				Anne Arundel Community College: MAT 044/045/145/146 (2020)
				Austin CC: Basic College Math
				Bade/Parkin: Essential Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, MATH REVIEW
				Baker College: Basic College Mathematics (Tobey)
				Barnett, College Math for Bus, Econ, Life Sci, and Soc Sci, 14/e, Global Edition
				Barnett, Finite Math for Bus, Econ, Life Sci, and Soc Sci, 14/e, Global Edition
				Barnett: Calculus for Bus, Econ, Life and Social Sci, 14/e, Global Edition
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13/e, Global Ed
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13e
				Barnett: College Math for Bus, Econ, Life&amp;Social Sci, 13/e, Global Edition
				Barnett: Finite Mathematics, 10e ENHANCED
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Beecher: Algebra &amp; Trigonometry, 2e ENHANCED
				Beecher: Algebra &amp; Trigonometry, 3e ENHANCED
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 2e ENHANCED
				Beecher: Precalculus, 2e ENHANCED
				Beecher: Precalculus, 3e
				Bennett: Essentials of Using &amp; Understanding Mathematics
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Using and Understanding Mathematics, 3e ENHANCED
				Bennett: Using and Understanding Mathematics, 4e
				Berenson, Basic Business Statistics, 13/e, Global Edition
				Berenson: Basic Business Statistics, 14/e, Global Edition
				Berenson: Basic Business Statistics, 5e (Aus)
				Berk/DeMarzo/Harford: Fundamentals of Corporate Finance, 5/e, Global Edition
				Berk/DeMarzo: Corporate Finance: The Core, 5e, Global Edition
				Betsy Smoketest 1-19-10
				Betsy Smoketest 9-28
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 11e
				Billstein: Mathematics for Elementary School Teachers, 8e
				Bittinger, Basic College Mathematics, 12/e, Global Edition
				Bittinger: Algebra and Trig: Graphs &amp; Models, 2e
				Bittinger: Algebra and Trig: Graphs &amp; Models, Unit Circle Approach, 1e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 5e
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 9e ENHANCED
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: Calculus and Its Applications, 9e DEMO
				Bittinger: College Algebra: Graphs &amp; Models, 2e
				Bittinger: College Algebra: Graphs &amp; Models, 3e
				Bittinger: College Algebra: Graphs &amp; Models, 5e
				Bittinger: Developmental Mathematics, 6e ENHANCED
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Developmental Mathematics, 8e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e ENHANCED
				Bittinger: Elementary Algebra, Concepts and Applications, 8e (2010)
				Bittinger: Elementary Algebra, Concepts and Applications, 9e
				Bittinger: Elementary Algebra: Concepts &amp; Apps, 6e
				Bittinger: Elementary Algebra: Graphs &amp; Models, 1e ENHANCED
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Elementary and Intermediate Algebra: Concepts &amp; Apps, 3e
				Bittinger: Elementary and Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Elementary and Intermediate Algebra: Graphs &amp; Models: 1e
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3e ENHANCED
				Bittinger: Fundamental Mathematics, 4e
				Bittinger: Fundamentals of College Algebra: Graphs &amp; Models
				Bittinger: Houston Community College Prealgebra, 3e
				Bittinger: Intermediate Algebra, 10e ENHANCED
				Bittinger: Intermediate Algebra, 9e ENHANCED
				Bittinger: Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Intermediate Algebra: 8e Alternate Version
				Bittinger: Intermediate Algebra: Concepts &amp; Apps, 6e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Intermediate Algebra: Graphs &amp; Models: 1e
				Bittinger: Introductory Algebra, 10e ENHANCED
				Bittinger: Introductory Algebra, 11e
				Bittinger: Introductory Algebra, 9e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 2e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 3e ENHANCED
				Bittinger: Introductory and Intermediate Algebra, 4e
				Bittinger: Prealgebra &amp; Introductory Algebra, 3e
				Bittinger: Prealgebra and Introductory Algebra, 1e ENHANCED
				Bittinger: Prealgebra, 4e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, A Unit Circle Approach
				Bittinger: Precalculus: Graphs &amp; Models: 2e
				Bittinger: Trigonometry: Graphs &amp; Models, 2e
				Blair/Tobey/Slater: Prealgebra, 3e ENHANCED
				Blair/Tobey: Prealgebra, 4e
				Blair: Intermediate Algebra, 1e
				Blair: Prealgebra, 5e 
				Blitzer:  College Algebra, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 2e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 5e ENHANCED
				Blitzer: College Algebra, 4e ENHANCED
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: Essentials of Intermediate Algebra for College Students ENHANCED
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra ENHANCED
				Blitzer: Intermediate Algebra for College Students, 4e ENHANCED
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 4e ENHANCED
				Blitzer: Introductory and Intermediate Algebra for College Students, 2e ENHANCED
				Blitzer: Precalculus Essentials, 2e ENHANCED
				Blitzer: Precalculus, 2e ENHANCED
				Blitzer: Precalculus, 3e ENHANCED
				Blitzer: Thinking Mathematically with Integrated Review
				Blitzer: Thinking Mathematically, 3e ENHANCED
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Bluegrass Community and Technical College: MA 111 Contemporary Math (2021)
				Briggs, Calculus, 2/e, Global Edition
				Briggs/Cochran/Gillett: Calculus: Early Transcendentals, 2/e, Global Edition
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update Aida Pilot
				Briggs/Cochran: Calculus, 2e
				Business Algebra, Second Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT final
				Carson: Elementary Algebra with Early Systems of Equations, ENHANCED
				Carson: Elementary Algebra, 2e ENHANCED
				Carson: Elementary Algebra, ENHANCED
				Carson: Elementary and Intermediate Algebra, ENHANCED
				Carson: Intermediate Algebra, 2nd Edition ENHANCED
				Carson: Intermediate Algebra, ENHANCED
				Carson: Prealgebra, 2e ENHANCED
				Carson: Prealgebra: 1e
				CCNG Trigsted Intermediate Alg Test Book
				CFO Online Finance Course. DEMO
				Chris Feb Tier 3 Test Math
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 2e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving: 1e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv, 2e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv, 3e
				Consortium: Math in Action: Intro to Algebraic, Graphical &amp; Num Prob Solv: 1e
				Consortium: Math in Action: Prealgebra Problem Solving, 1e
				Consortium: Math In Action: Prealgebra Problem Solving, 2e (2008) ENHANCED
				Contemporary Business Mathematics (BMAT 110)
				Corporate Finance, 4e for University of California-Berkeley
				Croft/Davison: Foundation Maths 7e
				De Veaux, Stats: Data &amp; Models, 4/e, Global Edition
				De Veaux: Stats: Data &amp; Models, 5e, Global Edition
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana: Calculus: Graphical, Numerical, Algebraic, 3e ENHANCED
				Demana: Precalculus: Functions and Graphs, 5e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9/e, Global Edition
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core, 9/e, GE 
				DeVry MAT190 (Washington)
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra and Trigonometry, 3e
				Dugopolski: College Algebra, 3e
				Dugopolski: College Algebra, 4e ENHANCED
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus ENHANCED Re-release
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus with Limits: Functions &amp; Graphs
				Dugopolski: Precalculus, 3e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs: 1e
				Dugopolski: Trigonometry
				ECPI MTH099: Beginning Algebra
				ECPI MTH115: Thinking Mathematically
				ECPI MTH125/MTH131: Intermediate Algebra
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e, GE
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Eileen&quot; , &quot;'&quot; , &quot;s Preloaded Testing Book
				eT1 Math Test Book
				ET1 Math Test Book 10 CA
				eT1 Math Test Book 11 - Non Course Aware 11
				ET1 Math Test Book 12 CA
				eT1 Math Test Book 15
				eT1 Math Test Book 15 - Non Course Aware 15
				eT1 Math Test Book 16
				eT1 Math Test Book 16 - Non Course Aware 16
				eT1 Math Test Book 17
				eT1 Math Test Book 17 - Non Course Aware 17
				eT1 Math Test Book 2
				eT1 Math Test Book 20 - Non Course Aware 20
				eT1 Math Test Book 20 CA
				eT1 Math Test Book 3
				eT1 Math Test Book 3 - Non Course Aware
				eT1 Math Test Book 3 - Non Course Aware 2
				eT1 Math Test Book 3 - Non Course Aware 3
				eT1 Math Test Book 4
				ET1 Math Test Book 4 CA
				eT1 Math Test Book 5 - Non Course Aware 5
				eT1 Math Test Book 6 - Non Course Aware 6
				ET1 Math Test Book 6 CA
				eT1 Math Test Book 7 - Non Course Aware 7
				ET1 Math Test Book 7 CA
				eT1 Math Test Book 8 - Non Course Aware 8
				ET1 Math Test Book 8 CA
				eT1 Math Test Book 9 - Non Course Aware 9
				ET1 Math Test Book 9 CA
				eT1 Math Test Book K2
				ET2 Cert - Pxe_2
				eT2 Disaggregated - NON course aware test book
				eT2 Math Test Book - CERT CITE
				eT2 Math Test Book - CERT PXE
				eT2 Math Test Book - CERT with Print ID
				eT2 Math Test Book CERT
				eText Integration Trigsted College Alg Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e, Global Edition
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				FIN 321 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 325 Corporate Finance for Arcuri at SUNY at Oswego
				FIN256 Corporation Finance for Barkley at Syracuse University
				Financial Markets and International Finance, Second Custom Edition for RMU
				Florida CLAST Guide
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Fundamentals of Corporate Finance Third Custom Ed. for Boston University FE 101 
				Fundamentals of Corporate Finance Third Custom Edition for Boston University
				Fundamentals of Corporate Finance, 3rd Edition for California State U. Fullerton
				Fundamentals of Finance, by Cole for UT Knoville
				Gitman/Zutter: Principles of Managerial Finance, BRIEF 6e
				Goetz: Basic Mathematics
				Goldstein, Calculus and Its Applications, 14/e, Global Edition
				Goldstein: Brief Calculus and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 10e
				Gould, Essential Statistics, 2/e, Global Edition
				Gould: Introductory Statistics: Exploring the World through Data, 1e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2/e, Global Edition
				Greenwell: Calculus for the Life Sciences
				Greenwich Quantitative Methods Custom MyMath Lab 2020
				Greenwich Quantitative Methods Custom MyMath Lab 2021
				Grimaldo/Robichaud: Introductory Algebra with Layers for Learning, 1e DEMO
				Groebner, Business Statistics: A Decision-Making Approach, 10/e, Global Edition
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler, Introductory Mathematical Analysis, 14e, Global Edition 
				Haeussler: Introductory Mathematical Analysis, 13e
				Harshbarger: College Algebra In Context, 2e ENHANCED
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, ENHANCED
				Hass, Thomas’ Calculus Early Transcendentals, 14/e, Global Edition
				Hass, Thomas’ Calculus, 14e in SI Units
				Hass, University Calculus Early Transcendentals, 3/e, Global Edition
				Hass, University Calculus: Early Transcendentals, 4/e in SI Units
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 4e ENHANCED
				Hornsby: A Graphical Approach to College Algebra, 4e ENHANCED
				Hornsby: A Graphical Approach to Precalculus, 4e ENHANCED
				Hornsby: Graphical Approach to College Algebra &amp; Trigonometry, 3e
				Hornsby: Graphical Approach to College Algebra, 3e
				Hornsby: Graphical Approach to Precalculus with Limits, 3e
				Hornsby: Graphical Approach to Precalculus, 3e
				Hubbard: Money, the Financial System, and the Economy, 5th Edition, MATH REVIEW
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				IPRO Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				James, Modern Engineering Mathematics, 4e - DEMO EMA
				John Jay: Thinking Mathematically with Algebra (Blitzer)
				Johnston: Calculus
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e ENHANCED
				Jordan: Integrated Arithmetic and Basic Algebra: 2e
				Just-In-Time Online Algebra
				Just-In-Time Online Algebra &amp; Trigonometry
				Kaplan Math 103
				Kaplan MM201-A
				Kaplan MM201-B
				Kaplan MM207 Statistics
				Knewton Math Enabling Test
				Knewton Math test book
				Knewton tiny test book
				Lancaster University Custom MyLab Math 2020
				Larson, Elementary Statistics: Picturing the World, 6/e, Global Edition
				Larson: Elementary Statistics: Picturing the World, 5e
				Larson: Elementary Statistics: Picturing the World, Global Edition, 7/e
				Lay, Linear Algebra and Its Applications, 5/e, Global Edition
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 3e Update
				Lay: Linear Algebra and Its Applications, 6/e, Global Edition
				Lehmann: Elementary Algebra: Graphs and Authentic Applications (ENHANCED)
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Levine, Statistics for Managers Using Microsoft Excel, 8/e, Global Edition
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Business Statistics: A First Course, 8/e, Global Edition
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial Finite Mathematics, 10e
				Lial, Introductory Algebra, 11/e, Global Edition
				Lial/Greenwell/Ritchey: Calculus with Applications, 11/e, Global Edition
				Lial/Hungerford: Finite Mathematics with Applications, 11/e, Global Edition
				Lial: Algebra for College Students, 5e ENHANCED
				Lial: Basic College Mathematics, 6e
				Lial: Basic College Mathematics, 7e ENHANCED
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Beginning Algebra, 10e
				Lial: Beginning Algebra, 11e
				Lial: Beginning Algebra, 9e ENHANCED
				Lial: Beginning Algebra: 8e
				Lial: Beginning and Intermediate Algebra, 3e ENHANCED
				Lial: Calculus with Applications, 10e
				Lial: College Algebra and Trigonometry, 3e ENHANCED
				Lial: College Algebra and Trigonometry, 5e
				Lial: College Algebra and Trigonometry, 6/e, Global Edition
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra, 11e
				Lial: College Algebra, 9e ENHANCED
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Essential Mathematics
				Lial: Essential Mathematics, 2e ENHANCED
				Lial: Essentials of College Algebra Alternate Edition, 1e ENHANCED
				Lial: Essentials of College Algebra, 11/e, Global Edition
				Lial: Essentials of College Algebra, 1e ENHANCED
				Lial: Finite Mathematics, 10e-Lil
				Lial: Finite Mathematics, 8e ENHANCED
				Lial: Intermediate Algebra with Early Functions and Graphing, 7e
				Lial: Intermediate Algebra, 8e ENHANCED
				Lial: Intermediate Algebra, 9e
				Lial: Intermediate Algebra: Alternate 8e
				Lial: Introductory &amp; Intermediate Algebra, 4e
				Lial: Introductory Algebra, 10e
				Lial: Introductory Algebra, 7e
				Lial: Introductory and Intermediate Algebra, 2e
				Lial: Introductory and Intermediate Algebra, 3e ENHANCED
				Lial: Mathematics with Applications, 8e
				Lial: Mathematics with Applications, Finite Version, 8e
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra and Introductory Algebra
				Lial: Prealgebra, 2e
				Lial: Prealgebra, 3e ENHANCED
				Lial: Prealgebra, 4e
				Lial: Precalculus with Limits
				Lial: Precalculus, 3e ENHANCED
				Lial: Trigonometry, 8e ENHANCED
				Long: Mathematical Reasoning for Elementary Teachers, 3e
				Long: Mathematical Reasoning for Elementary Teachers, 4e ENHANCED
				Long: Mathematical Reasoning for Elementary Teachers, 7/e, Global Edition
				Martin-Gay Algebra A Combined Approach, 2e ENHANCED
				Martin-Gay: Algebra 1
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra: A Combined Approach, 4e
				Martin-Gay: Basic College Mathematics with Early Integers, 2e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 2e ENHANCED
				Martin-Gay: Basic College Mathematics, 3e ENHANCED
				Martin-Gay: Basic College Mathematics, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e ENHANCED
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 2e ENHANCED
				Martin-Gay: Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Intermediate Algebra, 4e ENHANCED
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e ENHANCED
				Martin-Gay: Introductory Algebra 2e ENHANCED
				Martin-Gay: Introductory Algebra, 3e ENHANCED
				Martin-Gay: Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra and Introductory Algebra, ENHANCED
				Martin-Gay: Prealgebra, 4e ENHANCED
				Martin-Gay: Prealgebra, 6e
				Math Preloaded Course Test Book 1
				Math Preloaded Course Test Book 2
				Math Preloaded Course Test Book 3
				Math Preloaded Course Test Book 4
				Math Preloaded Course Test Book 5
				Math TG Only
				Mathematica V8 testing (SBnetpreview)
				Maths Booster UK
				Mathspace Master TOC of Exercises (version 1)
				McClave: A First Course in Statistics, 12/e, Global Edition
				McClave: Statistics for Business and Economics, Global Edition, 13/e
				McClave: Statistics, 13/e, Global Edition
				Middlesex Community College: MTH001/002/003: Preparation for College Math 2021
				Miller: Business Mathematics, 10e ENHANCED
				Miller: Business Mathematics, 9e
				Miller: Economics Today, 12th Edition, MATH REVIEW
				Miller: Economics Today: MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, MyEconLab Edition, MATH REVIEW
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 10e ENHANCED
				Miller: Mathematical Ideas, 12e
				Miller: Mathematical Ideas, Expanded 10e ENHANCED
				Mishkin: The Economics of Money, Banking, &amp; Financial Markets, 7th Ed, MATH REV
				MML-CoCo: MyBeginningAlgebraCourse (Martin-Gay 5e)
				Morris: Quantitative Approaches in Business Studies 7e, EMA
				Multinational Business Finance Custom Edition for Ryerson
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyAccountingLab CD Demo
				MyFoundationsLab Prototype
				MyHealthProfessionsLab for Math Basics for the Health Care Professional, 5/e
				MyMathLab Global, Australian/New Zealand edition
				MyMathLab test book A (from Blitzer TM5)
				MyMathLab test book A2
				MyMathLab test book A3 - Lilani 6/15
				MyMathLab test book B (from Briggs Calc)
				MyMathLab test book B2
				MyMathLab test book B3
				MyMathLab test book C2
				MyMathLab test book C3
				MyMathLab test book D3 MOBILE ONLY
				MyMathTest: Developmental Mathematics
				Nagle, Fundamentals of Differential Equations, 9/e, Global Edition
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				National University - custom 03/2002
				National University: BST322: Introduction to Biomedical Statistics
				New Import Test
				Newbold: Statistics for Business and Economics, Global Edition, 9/e
				Newbold: Statistics for Business and Economics: Global Edition, 7e
				NextGen Media Econ Test Book-Janus-Active
				NextGen Media Math Test Book-- Early Alerts
				NextGen Media Math Test Book--dulanjali
				NextGen Media Math Test Book--EA Auto
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL--Rush
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL_BigInt2
				NextGen Media Math Test Book--REAL_upetha
				NG Math Media 2
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				Nottingham University Custom MyLab Maths 2021
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 3e ENHANCED
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers: 2e
				Olivier: Business Mathematics Interactive
				Parkin: Economics, 6th Edition, MATH REVIEW
				Parkin: Macroeconomics, 6th Edition, MATH REVIEW
				Parkin: Microeconomics, 6th Edition, MATH REVIEW
				Pasco Hernando Community Colle: MAT 0018 &amp; 0028: Prealgebra &amp; Elementary Algebra
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Perloff: Microeconomics, 3rd Edition, MATH REVIEW
				Phil&quot; , &quot;'&quot; , &quot;s Test Book
				Pirnot: Mathematics All Around, 2e
				Pirnot: Mathematics All Around, 3e
				Pirnot: Mathematics All Around, 5e
				Portsmouth Uni Foundation Maths Custom MyLab Math 2021
				Prior: Basic Mathematics, 1e
				Prior: Prealgebra, 1e
				Queensborough CC: Intermediate Algebra with Trigonometry (Blitzer)
				Ratti, College Algebra and Trigonometry, 3/e, Global Edition
				Ratti: College Algebra, 4e
				Req 54 Test 2 level math
				Req 54 Test book
				Req54Test-no 0 section numbers
				Rockswold/Krieger: Interactive Developmental Mathematics, 2e TEST
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 3e ENHANCED
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Applications &amp; Visualization, 1e ENHANCED
				Rockswold: Beginning and Intermediate Algebra ENHANCED
				Rockswold: College Alg and Trig through Modeling &amp; Visualization, 2e
				Rockswold: College Algebra through Modeling &amp; Visualization, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e ENHANCED
				Rockswold: Developmental Math, 1e
				Rockswold: Intermediate Algebra through Modeling &amp; Visualization: 1e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e ENHANCED
				Rockswold: Precalculus through Modeling &amp; Visualization, 2e
				Rockswold: Precalculus with Modeling and Visualization, 3e ENHANCED
				Royal Holloway Custom MyLab Math 2020
				Ruth SPP Test Book
				Ruth Test Course for Pre-Made Assignments
				Salzman: Mathematics for Business, 7e
				Sam Math Book_Flash
				Sample Math Book
				Saunders: Mathematics for the Trades POOL DEMO
				Sharpe: Business Statistics, 3/e, Global Edition
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, 4e, GE
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course, 3e
				Shihab, Numeracy in Nursing and Healthcare 1e - EMA DEMO
				SI Notation testing (from sbtest)
				Sinclair Community College: MAT 1130 2018
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Small: Basic Mathematics, 3e
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Basic Mathematics - Demo
				Squires: Developmental Math, 2e RIO PILOT
				Squires: Introductory Algebra
				StatCrunch Question Library (Online Only)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Statistics Online, 1e DEMO chapter 10
				Sullivan/Woodbury: Statistics Online, 1e DEMO--OLD IRAS
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 4e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 7e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 8e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: College Algebra w/ Graphing Utilities, 4e ENHANCED
				Sullivan: College Algebra, 7e ENHANCED
				Sullivan: College Algebra, 9e
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: Intermediate Algebra, 1e (2007)
				Sullivan: Intermediate Algebra, 1e (2007) DEMO
				Sullivan: Precalculus w/ Graphing Utilities, 4e ENHANCED
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 10/e, Global Edition
				Sullivan: Precalculus, 7e ENHANCED
				Sullivan: Precalculus, 9e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5/e, Global Edition
				Sullivan: Trigonometry, 7e ENHANCED
				Swansea University Custom MyLab Maths 2021
				Swansea University: Wedlake Custom MLG 2018
				Tannenbaum: Excursions in Modern Mathematics, 7e
				Tanya Mathspace test
				TESTGEN TESTING Lial Calc
				The University of Texas at Arlington: Math 1327
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, Early Transcendentals, Updated 10e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, Updated 10e
				Thomas, Thomas&quot; , &quot;'&quot; , &quot; Calculus in SI Units, 13/e
				Thomas, Thomas&quot; , &quot;'&quot; , &quot; Calculus: Early Transcendentals in SI Units, 13/e
				Tidewater Community College:  Math Essentials MTE 1-9
				Titman/Martin/Keown: Financial Management, 14e,GE
				Tobey: Basic College Mathematics, 5e ENHANCED
				Tobey: Basic College Mathematics, 7e
				Tobey: Beginning &amp; Intermediate Algebra, 2e ENHANCED
				Tobey: Beginning Algebra, 6e ENHANCED
				Tobey: Beginning Algebra, 8e
				Tobey: Beginning Algebra: Early Graphing, 1e ENHANCED
				Tobey: Beginning and Intermediate Algebra, 3e
				Tobey: Beginning and Intermediate Algebra, 4e
				Tobey: Essentials of Basic College Mathematics, 1e ENHANCED
				Tobey: Intermediate Algebra, 5e ENHANCED
				Trigsted: Beginning &amp; Intermediate Algebra, 2e
				Trigsted: College Algebra 1e
				Trigsted: College Algebra 1e DEMO
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra Interactive, Chapters R-5
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Developmental Mathematics, 2e
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trigsted: Intermediate Algebra
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Biostatistics for the Biological and Health Sci, 2/e, Global Edition
				Triola: Elementary Statistics Using Excel: 1e
				Triola: Elementary Statistics, 10e ENHANCED
				Triola: Elementary Statistics, 11e Technology Update
				Triola: Elementary Statistics, 3ce DEMO CANADA
				Triola: Essentials of Statistics: 1e
				UK Test Cicchitelli, D&quot; , &quot;'&quot; , &quot;Urso, Minozzo: Statistics
				Univ of Florida: Precalculus 1140/1147/1114
				University of Alberta: Calculus
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: STAT 200: Elementary Statistics
				Valencia Community College - Bittinger: Prealgebra
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e, Global Edition
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Introduction to Technical Mathematics, 5e ENHANCED
				Waters, Quantitative Methods for Business 4e - EMA
				Weiss, Introductory Statistics, 10/e, Global Edition
				Weiss: Elementary Statistics: 5e
				Weiss: Introductory Statistics, 8e
				Weiss: Introductory Statistics: 6e
				Wisniewski, Quantitative Methods for Decision Makers 5e - EMA
				Wisniewski/Shafti: Quantitative Analysis for Decision Makers 7e
				Woodbury: Elementary &amp; Intermediate Algebra, 1e ENHANCED
				Workspace Test Book
				XL Load Testbook
				Young: Finite Mathematics, 3e ENHANCED
				Zutter/Smart: Principles of Managerial Finance, 16/e, Global Edition
				ZZIA Interactive testing
				zzTesting eT2 book export
				zzz-Trigsted test book for Interactive Dev Math 2e

			&quot;))]</value>
      <webElementGuid>a89adfcc-d1e1-40e1-9e79-91d6c7da8fae</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
