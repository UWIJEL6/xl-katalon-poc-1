<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 2.1 The Addition Pr_d6236b</name>
   <tag></tag>
   <elementGuidId>999a9d33-89a4-4182-9be6-bd1b13cc9526</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b995a6c1-0086-4028-bf5a-a8664f91d6cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>163e6d6d-2a22-46f0-ba88-d75ec46b7e32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
      <webElementGuid>a01b0ff8-b0ad-4f38-a4da-3f2ae5551755</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>ddcc1bbd-4a29-4adc-b1c5-b03fb73ccbd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>c921aa57-e90e-4100-8ce0-0e12aead50c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	</value>
      <webElementGuid>3f584dd0-503d-4d0c-80a0-efed354d512e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>a2aa08a7-ac13-47c1-9b25-164020933bb1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>4b04ea40-22fe-44bf-b238-de37416b96bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>90a28f44-f5b2-43eb-9fb0-0f8c5dda8cbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
      <webElementGuid>4083a6a6-3b93-43a8-8bbe-6b0ff7ae611a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
      <webElementGuid>a3833ba2-3546-4736-9bb7-0ed6bdc8abde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
      <webElementGuid>286e9252-aeb9-4469-af71-339e98e1dd52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>b5077c7d-ccf1-4b6d-bc14-8926fccd02f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>83a13938-0d3b-4542-a472-62f818485080</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	' or . = '
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	')]</value>
      <webElementGuid>04ac627f-652d-4418-bdd0-480f75de9684</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
