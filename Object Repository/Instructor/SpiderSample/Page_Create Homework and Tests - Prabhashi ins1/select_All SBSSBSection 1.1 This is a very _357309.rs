<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 1.1 This is a very _357309</name>
   <tag></tag>
   <elementGuidId>0e4cf1f3-0d57-470a-a761-de5f5c4513cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>ec8cd214-dcdb-4a63-b97a-d341386b88e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>bae27069-463c-4957-9610-68df23b370a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
      <webElementGuid>0742e43b-9e38-494c-95a0-56b8b38e8487</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>0441f155-ee4d-479e-b333-f621590f2d35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>b2daeab2-1d1a-4140-9469-d65dc61bef3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>e5ccdd70-f18c-45d9-9d84-904f1b5677b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions</value>
      <webElementGuid>74895db1-a57d-44b9-a713-103f6c833138</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>23f00b58-fec5-42d6-8a5a-246c54fcc593</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>6ce22a16-87c7-415f-9ab8-9fc0c0c197a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>8b57a200-da94-4406-a05b-0139dc6a6414</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
      <webElementGuid>bef96e12-95f8-45c7-a3de-1443fe771587</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
      <webElementGuid>044c43be-78a3-42a0-b947-ffc1180eaa5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
      <webElementGuid>3c42d9bb-29c4-4cd9-9169-6784ae92e9fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>0bb05bcc-3481-4455-8535-7f52fd37bdbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>39a362b3-fde1-4c49-b156-05f4604d636c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions' or . = '
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions')]</value>
      <webElementGuid>b88eb2ba-2050-4652-8262-c920f250e8b7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
