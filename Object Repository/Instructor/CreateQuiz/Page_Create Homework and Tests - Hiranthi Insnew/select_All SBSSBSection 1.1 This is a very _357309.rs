<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 1.1 This is a very _357309</name>
   <tag></tag>
   <elementGuidId>087abb4a-34ff-4979-a1aa-59d699dfcad8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions' or . = '
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions')]</value>
   </webElementXpaths>
</WebElementEntity>
