<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --   Levine Statistic_0673c2</name>
   <tag></tag>
   <elementGuidId>5cfd6836-55b0-412e-a360-bf049e3e0edb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				  -- Choose Book --  
				 Levine: Statistics for Managers Using Microsoft Excel, 9e, Global Edition
				(Virtual School exercises)
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Geometry Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Geometry
				A&amp;B Testgen Browser Check book
				AaSam Site Builder 10
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Adams: A Complete Course, 9ce
				Adams: Calculus, 6e
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 7e
				Adams: Calculus: A Complete Course, 10ce
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data, 4e
				Agresti: Statistics: The Art and Science of Learning from Data, 5e
				Akst: Developmental Mathematics through Applications, 1e
				Alec's Delta Book
				Algebra Review for Calculus
				Alicia's MNL Pharmacology XL style
				Alicia's SBNet feature testing book
				All Wizard Book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Almy: Math Lit: A Pathway to College Mathematics, 3e
				American River College: Math 41, 42, 131, 132, &amp; 1331
				Angel: A Survey of Mathematics with Applications, 10e
				Angel: A Survey of Mathematics with Applications, 10e with Integrated Review
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 5e
				Angel: Elementary Algebra for College Students, 10e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Intermediate Algebra for College Students, 10e
				Anne Arundel Commuity College: MAT 044/045/145/146
				Anne Arundel Community College: Mat 037/137: College Algebra Pathway
				Applied Calculus Conceptual Question Library
				Applied Calculus Interactive Demo
				Arizona State University: MAT 142: College Mathematics
				Arkansas State University, Beebe: Math 1013: Technical Math
				Aron: Statistics for Psychology, 6e
				Ashland Community and Technical College: Math 126
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Atlanta Technical College (2017)
				Austin Community College: MATD 0485
				Austin Peay State University: Math 1530: Statistics
				Babson College: QTM 1000/1010: Bus Analytics 1 &amp; 2
				Barnett: Calculus for Business, Economics, Life and Social Sci Brief Ver, 14e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 14e
				Barnett: College Mathematics for Business, Econ, Life and Social Sci, 14e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 14e
				Basic Technical Mathematics with Calculus, 10e, SI Version for Mohawk College
				Bass: Math Study Skills, 2e
				Beckmann: Skills Review for Math for Elementary and Middle School Teachers, 6e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 5e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: Algebra &amp; Trigonometry, 5e Media Update
				Beecher: Algebra and Trigonometry, 5e 
				Beecher: College Algebra with Integrated Review, 5e
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 5e
				Beecher: College Algebra, 5e Media Update
				Beecher: Precalculus A Right Triangle Approach, 5e
				Beecher: Precalculus A Right Triangle Approach, 5e Media Update
				Bellevue University: MBA 624: Business Analysis for Decision Making
				Bennett and Blitzer Sample Application Demos
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Statistical Reasoning For Everyday Life, 5e
				Bennett: Using and Understanding Mathematics, 6e
				Bennett: Using and Understanding Mathematics, 7e
				Berenson: Basic Business Statistics 2e, Australian Edition
				Berenson: Basic Business Statistics, 14e
				Berenson: Basic Business Statistics, 4e (Australia)
				Berenson: Basic Business Statistics, 5e (Aus)
				Berk/DeMarzo: Corporate Finance: The Core, 5/e, Global Edition
				Berk: Fundamentals of Corporate Finance, 3ce
				Betsy Smoketest 11-19-09
				Betsy Smoketest 3-3-10
				BI Norway: MET3431 Statistics
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 12e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 13e
				Bittinger: Algebra &amp; Trigonometry Graphs &amp; Models, 6e MyLab Revision
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Algebra Foundations, 1e DIGITAL UPDATE
				Bittinger: Basic College Mathematics, 13e
				Bittinger: Calculus and Its Applications Brief, 12e
				Bittinger: Calculus and Its Applications Expanded Ed, 1e Media Update
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 2e
				Bittinger: College Algebra Graphs &amp; Models, 6e Digital Update
				Bittinger: College Algebra: Graphs and Models, 6e
				Bittinger: Developmental Mathematics, 10e
				Bittinger: Developmental Mathematics, 9e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts &amp; Apps, 7e DIGITAL UPDATE
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, 10e
				Bittinger: Intermediate Algebra, 13e
				Bittinger: Intermediate Algebra, Concepts and Applications, 10e
				Bittinger: Introductory &amp; Intermediate Algebra with Integrated Review, 6e
				Bittinger: Introductory &amp; Intermediate Algebra, 6e
				Bittinger: Introductory Algebra, 13e
				Bittinger: Introductory Algebra, 13e DEMO (2019)
				Bittinger: Prealgebra &amp; Introductory Algebra, 4e
				Bittinger: Prealgebra, 7e
				Bittinger: Prealgebra, 8e
				Bittinger: Precalculus Graphs &amp; Models, 6e Digital Update
				Bittinger: Precalculus: Graphs and Models, 6e
				Blair/Tobey/Slater: Prealgebra, 6e
				Blitzer: Algebra &amp; Trigonometry with Integrated Review, 6e 
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 6e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 7e
				Blitzer: Algebra for College Students, 8e
				Blitzer: College Algebra An Early Functions Approach, 4e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra Essentials, 5e
				Blitzer: College Algebra Essentials, 6e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra, 8e
				Blitzer: College Algebra: An Early Functions Approach, 3e
				Blitzer: Developmental Mathematics, 1e
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Intermediate Algebra for College Students, 8e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra, 6e
				Blitzer: Introductory Algebra for College Students, 7e
				Blitzer: Introductory Algebra, 8e
				Blitzer: Math for Your World, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Pathways to College Mathematics, 2e
				Blitzer: Precalculus Essentials, 5e
				Blitzer: Precalculus Essentials, 6e
				Blitzer: Precalculus with Integrated Review, 6e (2018)
				Blitzer: Precalculus, 5e
				Blitzer: Precalculus, 6e
				Blitzer: Precalculus, 7e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Thinking Mathematically, 8e
				Blitzer: Trigonometry, 2e
				Blitzer: Trigonometry, 3e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bluegrass Community &amp; Technical College: MAT 105: Business Math (2021)
				Bluegrass Community and Technical College: MA 111 Contemporary Math
				Bluegrass Community and Technical College: MA 111 Contemporary Math (2021)
				Bluegrass Community and Technical College: MAT 105
				Bock/Bullard/Velleman/De Veaux: Stats: Modeling the World, 6e
				Bock: Stats In Your World, 2e
				Bock: Stats In Your World, 3e
				Bock: Stats: Modeling the World, 4e
				Bock: Stats: Modeling the World, 5e
				Borough of Manhattan CC: Introductory Algebra with Arithmetic Review
				Borough of Manhattan CC: MAT 008: Basic Math
				Borough of Manhattan CC: MAT 012: Basic Arithmetic and Algebra
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus Early Transcendentals, 3e
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update Aida Pilot
				Briggs/Cochran: Calculus for Scientists and Engineers, 1e
				Briggs/Cochran: Calculus, 3e
				Briggs/Cochran: Calculus, 3e Digital Update
				Briggs: AP Calculus, 2e
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Brigham Young University: Math 118: Finite Mathematics
				Brookdale Community College: Math 145
				Brooks: Financial Management: Core Concepts, 4e
				Broward College: STA 1001: Pathways to Statistics
				Bryant and Stratton College: Math 103: Survey of Math
				Business Algebra, Third Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT (del)
				Carman/Saunders: Mathematics for the Trades: A Guided Approach, 10e
				Carman: Mathematics for the Trades: A Guided Approach, Second Canadian Edition
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				CFO Online Finance Course. CHAPTER DEMO
				Cincinnati State TCC: AFM-091: Prealgebra 1
				Cincinnati State TCC: AFM-092: Prealgebra 2
				Cincinnati State TCC: AFM-094: Basic Algebra
				Cincinnati State TCC: AFM-097: Intermediate Algebra
				Clark: Applied Basic Mathematics, 2e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 11e
				Cleaves: College Mathematics, 10e
				Cleaves: College Mathematics, 9e
				Clemson University: MATH 1040/1060/2080
				Clendenen: Business Mathematics, 13e
				Clendenen: Business Mathematics, 14e
				Cleveland State Community College: Math 0130/1000
				Cleveland State Community College: Math 0530/1530 (2021)
				Cleveland State University: MATH 87, 3e 2015
				Cleveland State University: MTH 147/347: Statistics
				College of Southern Nevada: Math 96: Intermediate Algebra
				Collin County Community College District: MATH 1324
				Collins/Nunley: Navigating Through Mathematics, 1e DEMO chapter 7
				Columbus State Community College: Math 1130/1131
				Conceptual Question Library for Business Statistics (Online Only)
				Conceptual Question Library for Statistics (Online Only)
				Concordia University: Math 208
				Concordia University: Math 209
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematics for College and Career Readiness, 1e
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 5e
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 6e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 5e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 6e
				Contemporary Business Math with Canadian Applications for MacEwan University
				Contemporary Business Mathematics (BMAT 110)
				Corequisite Support for Sullivan: Precalc: CTF, A Right Triangle Approach, 4e
				Corequisite Support for Sullivan: Precalculus: CTF, A Unit Circle Approach, 4e
				Corequisite Support Modules for College Algebra or Precalculus
				Corequisite Support Modules for Quantitative Reasoning or Liberal Arts Math
				Corequisite Support Modules for Statistics
				Corporate Finance, 4e for University of California-Berkeley
				Craven Community College: MAT 110: Math Measurement &amp; Literacy
				Croft/Davison: Foundation Maths 7e
				Cumberland County College: MA 091/094: Developmental Math
				Custom MML- Las Positas College: Math 65/55/50: Beginning &amp; Intermediate Algebra
				CUSTOM- American River College: Math 41-133 (2021)
				Cuyahoga Community College (Tri-C): Math 091/095/096
				Cypress College: MATH 24: Pre-Statistics 2e 2017
				Dana Center: Statistical Reasoning - Preview and CQL
				De Veaux: Intro Stats, 4e
				De Veaux: Intro Stats, 5e
				De Veaux: Intro Stats, 6e
				De Veaux: Stats: Data &amp; Models, 4e
				De Veaux: Stats: Data &amp; Models, 5e
				De Veaux: Stats: Data &amp; Models, 5e, Global Edition
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce DEMO
				De Veaux: Stats: Data and Models, Fourth Canadian Edition
				De Veaux: Stats: Data and Models, Second Canadian Edition
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana/Waits/Kennedy/Bressoud: Calculus, 6e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 10e
				Demo GE Skill Builder Course
				DeVry University: MATH104
				Diagnostic Test for British Columbia Institute of Technology
				Donnelly: Business Statistics, 2e
				Donnelly: Business Statistics, 3e
				Drexel University: Math 101
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 6e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 4e MML Update
				Dugopolski: Trigonometry, 4e
				Dugopolski: Trigonometry, 5e
				Dutchess Community College: Intermediate Algebra
				Dyersburg State Community College: MATH 0530-1530
				EAS Mathematics
				ECPI MTH099: Beginning Algebra
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics (2009)
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e, GE
				Edwards/Penney/Calvis: Differential Equations Computing and Modeling, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 6e
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Eiteman/Stonehill/Moffett: Multinational Business Finance, 14e
				Eiteman/Stonehill/Moffett: Multinational Business Finance, 15e
				eT1 Math Test Book
				eT2 Math Test Book
				eText Integration Intermediate Alg Test Book
				eText Integration Trigsted College Alg Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e
				Evans: Business Analytics: Methods, Models, and Decisions, 3e Demo
				Evans: Business Analytics: Methods, Models, and Decisions, 3e, Global Edition
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				Fanshawe College: Math 1024
				Fayetteville Technical Community College: Volume 1: DMA 010-030
				Fayetteville Technical Community College: Volume 2: DMA 040-050
				Fayetteville Technical Community College: Volume 3: DMA 060-080
				FIN 320 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 321 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 325 Corporate Finance for State University of New York College at Oswego
				FIN256 Corporation Finance for Barkley at Syracuse University
				Financial Markets and International Finance, Second Custom Edition for RMU
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Finney: Calculus (Virtual School exercises)
				Florida International University: Finite Math 1060
				Florida International University: Finite Math 1106 (2009 Update)
				Florida Southwestern State College: MATH1033: Intermediate Algebra with Review
				Florida State College at Jacksonville: MAC 1105: College Algebra 2012
				Florida State College at Jacksonville: MAT 0018 Basic Mathematics
				Florida State College at Jacksonville: MAT 0024 - Elementary Algebra
				Florida State College at Jacksonville: MAT 1033 - Intermediate Algebra
				Florida State College at Jacksonville: MGF 1106/1107 Topics in College Math
				Florida State College at Jacksonville: STA 2023 - Elementary Statistics
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada HED) 
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Foundations of Mathematical Reasoning DEMO (October 2014)
				Front Range Community College, Larimer Campus: MAT050: Quantitative Literacy
				Front Range Community College: MAT050/055: Quantitative &amp; Algebraic Literacy 3e
				Fundamentals of Corporate Finance Third Custom Ed. for Boston University FE 101 
				Fundamentals of Corporate Finance Third Custom Edition for Boston University
				Fundamentals of Finance, by Cole for UT Knoville
				Gaze: Thinking Quantitatively: Communicating with Numbers, 2e
				Geometry YTIs (Virtual Schools exercises)
				Georgia Institute of Technology: MATH 1551-2551: Calculus with Linear Algebra
				Georgia State University: MATH 0999/1111 Coreq 2017
				Glendale Community College: Math 30
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 15e
				Goldstein: Finite Mathematics and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 13e
				Gould: Essential Statistics: Exploring the World Through Data, 2e
				Gould: Essential Statistics: Exploring the World Through Data, 3e
				Gould: Introductory Statistics, 3e Data Project Demo
				Gould: Introductory Statistics: Exploring the World through Data, 2e
				Gould: Introductory Statistics: Exploring the World through Data, 3e
				Gould: Introductory Statistics: Exploring the World Through Data, Ce
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Greenwich Quantitative Methods Custom MyMath Lab 2020
				Greenwich Quantitative Methods Custom MyMath Lab 2021
				Grimaldo: Developmental Math: Prealgebra, Intro Algebra, Intermediate Algebra 1e
				Groebner: Business Statistics: A Decision-Making Approach, 10e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				GTCC Demo 2012
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler, Introductory Mathematical Analysis, 14e, Global Edition 
				Haeussler: Introductory Mathematical Analysis, 13e
				Hagerstown Community College: Math 98/99/100
				Harrisburg Area Community College: MATH 008/022/033/044/055
				Harshbarger: College Algebra in Context w/ Integrated Review, 5e
				Harshbarger: College Algebra in Context, 4e
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, 6e
				Hass: University Calculus Early Transcendentals, 3e
				Hass: University Calculus Early Transcendentals, 4e
				Heartland Community College: Math 091-094 2e (2014)
				Henslin: Sociology
				Hillsborough Community College: MAC 1105
				Hornsby: A Graphical Approach to Algebra &amp; Trigonometry, 7e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to College Algebra, 7e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 7e
				Hostos Community College: MAT 150: College Algebra w/ Trig
				Hostos Community College: MAT 150: College Algebra with Trigonometry
				Houston Community College: Math 0309/0310/0314
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e DEMO 
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 11e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 12e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Indiana University Bloomington: SPEA-K300: Statistics
				Interactive Applied Calculus Chapter 1 Sampler
				Interactive Calculus Early Transcendentals, 1e Demo
				Introduction to Finance, First Custom Edition for RMU
				Introductory Mathematical Analysis, Fourthteen Edition
				IPRO-Irv-XLTestbk1
				James/Dyke: Modern Engineering Mathematics 6e
				Jamestown CC: Precalculus: Concepts Through Functions 4th Custom Ed 2015
				Jamestown Community College: MAT 005/006: Introductory and Intermediate Algebra
				Jamestown Community College: MAT 1590/1600
				Janus Math NextGen2 - Lil
				Joliet Junior College: Math 090/094/098
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 5e
				Kaplan Math 103: College Mathematics
				Kaplan MM201-A
				Kaplan MM201-B
				KCTCS: Bittinger Prealgebra &amp; Introductory Algebra 3e, Open Access
				KCTCS: Southcentral Kentucky Community&amp;Tech College: MAT 105/110/116/126/146/150
				Kentucky Community and Technical College System: MAT 161
				Keown/Martin/Petty: Foundations of Finance, 10e
				Keown: Personal Finance: Turning Money into Wealth, 8e
				Knewton Math test book
				KU122: Introduction to Math Skills and Strategies
				Lancaster University Custom MyLab Math 2020
				Larson: Elementary Statistics: Picturing the World, 6e
				Larson: Elementary Statistics: Picturing the World, 7e
				Larson: Elementary Statistics: Picturing the World, 7e with Integrated Review
				Larson: Elementary Statistics: Picturing the World, 8e
				Larson: Elementary Statistics: Picturing the World, Global Edition, 7/e
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 5e
				Lay: Linear Algebra and Its Applications, 6/e, Global Edition
				Lay: Linear Algebra and Its Applications, 6e
				Lehigh Carbon Community College: Mathematical Literacy, Custom Edition
				Lehmann: A Pathway to Introductory Statistics, 1e
				Lehmann: A Pathway to Introductory Statistics, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 3e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 3e
				Lehmann: Intermediate Algebra: Functions &amp; Auth Apps, 6e w/ Integrated Review
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 4e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 6e
				Lesmeister: Math Basics for the Health Care Professional, 4e
				Lethbridge College: MTH 0009/0010/0020/0030
				Levine: Business Statistics: A First Course, 6e
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Business Statistics: A First Course, 7e
				Levine: Business Statistics: A First Course, 8/e, Global Edition
				Levine: Business Statistics: A First Course, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 9e
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial/Greenwell/Ritchey: Calculus with Applications Brief, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, Brief Version, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 10e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 11e
				Lial/Hungerford/Holcomb/Mullins: Finite Mathematics with Applications, 12e
				Lial/Hungerford/Holcomb/Mullins: Mathematics with Applications, 12e
				Lial: Algebra &amp; Trigonometry for College Readiness Media Update
				Lial: Algebra for College Students , 9e
				Lial: Algebra for College Students, 8e
				Lial: Basic College Mathematics, 10e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra with Integrated Review, 12e
				Lial: Beginning Algebra, 12e
				Lial: Beginning Algebra, 13e
				Lial: Beginning and Intermediate Algebra and College Algebra Co-Requisite 1e
				Lial: Beginning and Intermediate Algebra, 7e
				Lial: Calculus with Applications, 10e
				Lial: Calculus with Applications, 11e
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra and Trigonometry, 7e
				Lial: College Algebra w/ Integrated Review, 12e
				Lial: College Algebra, 10e for CLEP
				Lial: College Algebra, 12e
				Lial: College Algebra, 13e
				Lial: Developmental Mathematics, 2e (Retired)
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics, 4e
				Lial: Essentials of College Algebra, 11e
				Lial: Essentials of College Algebra, 12e
				Lial: Finite Mathematics with Applications, 11e
				Lial: Finite Mathematics, 11e
				Lial: Finite Mathematics, 12e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 13e
				Lial: Intermediate Algebra, 13e V2
				Lial: Introductory Algebra, 11e
				Lial: Introductory and Intermediate Algebra, 6e
				Lial: Mathematics with Applications, 11e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra, 4e
				Lial: Prealgebra, 5e
				Lial: Prealgebra, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 7e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Liberty University: MATH 115: Math for Liberal Arts
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Long: Mathematical Reasoning for Elementary Teachers, 7e Media Update
				Madison College: ABE Dev Math Series
				Madura: Personal Finance, 7e
				Maricopa District: Developmental Math Modules
				Maricopa: Custom Module Demo
				Marion Technical College: Math 0920: Algebraic Literacy
				Martin-Gay Algebra Foundations
				Martin-Gay: Algebra 1, MyLab Homeschool Edition
				Martin-Gay: Algebra 2, MyLab Homeschool Edition
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Algebra: A Combined Approach, 5e
				Martin-Gay: Algebra: A Combined Approach, 6e 
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics with Early Integers, 3e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 5e
				Martin-Gay: Basic College Mathematics, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra with Integrated Review, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra with Integrated Review, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra with Integrated Review, 6e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Beginning Algebra, 8e
				Martin-Gay: Developmental Mathematics, 3e
				Martin-Gay: Developmental Mathematics, 4e
				Martin-Gay: Geometry
				Martin-Gay: Inter. Algebra: Math for College Readiness (FL Edition)
				Martin-Gay: Interactive Algebra Foundations, 1e
				Martin-Gay: Interactive Algebra Foundations, 1e DEMO
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 6e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 5e
				Martin-Gay: Introductory Algebra, 6e
				Martin-Gay: Path to College Mathematics, 1e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 5e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 7e (HTML5 Preview Version)
				Martin-Gay: Prealgebra, 8e
				MassBay Community College: MA090/095/098
				MasteringX 
				McClave: A First Course in Statistics, 12e
				McClave: Statistics for Business and Economics, 13e
				McClave: Statistics for Business and Economics, 14e
				McClave: Statistics for Business and Economics, 14e, Global Edition
				McClave: Statistics, 13e
				McClave: Statistics, 13e MyLab Revision with Technology Updates
				Merrimack College: MTH 1007
				Metropolitan Community College: MATH 0910/1220 Fall 2018
				Metropolitan Community College: MATH 0910/1240
				Metropolitan Community College: MATH 0910/1240 (2019)
				Metropolitan Community College: MATH-0910/1220 Fall 2018
				Miami Dade College: MAT 0022C: Developmental Mathematics Combined
				Middle Tennessee State University: Math 1530K: Applied Statistics 2017
				Middlesex Community College: MTH001/002/003: Preparation for College Math 2021
				Midlands Technical College: MAT 122
				Miller: Mathematical Ideas, 13e
				Miller: Mathematical Ideas, 14e
				Mineral Area College: MTH 0930/1205/1240/1260
				Mineral Area College: MTH 1270/1180
				Missouri Southern State University: Math 140
				Montgomery County Community College: MAT10: Concepts of Numbers
				Multinational Business Finance Custom Edition for Ryerson
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyFoundationsLab Prototype
				MyHealthProfessionsLab for Math Basics for the Health Care Professional, 5/e
				MyLab Math, Australian/New Zealand edition
				MyMathTest: Precalculus and Calculus
				MyStatLab for De Veaux: Stats: Data and Models, 3ce
				Nagle: Fundamentals of Differential Eq w/ Boundary Value Prob, 7e Digital Update
				Nagle: Fundamentals of Differential Equations w/ Boundary Value Problems, 7e
				Nagle: Fundamentals of Differential Equations, 9e
				Nagle: Fundamentals of Differential Equations, 9e Digital Update
				Nassau CC: Lial Intermediate 9e with Trig 7e
				National University: BST322: Introduction to Biomedical Statistics
				National University: Statistics and Data Analysis for Nursing Research
				NES Prep, Elementary Education II: Math
				Neuhauser/Roper: Calculus for Biology and Medicine, 4e
				New Additional Conceptual Exercises (NC Redesign)
				New Book Test 6
				Newbold: Statistics for Business and Economics, 8e
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL_BigInt1
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--Shehan
				NextGen Media Math Test Book--ShehanNew
				NextGen Media Math Test BookRH
				Norman/Wolczuk: Introduction to Linear Algebra for Science - Delete
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				North Arkansas College: MCT MAT 1011/1012
				North Carolina Community Clgs: Math 110: Mathematical Measurement &amp; Literacy
				North Carolina Community Clgs: Math 110: Mathematical Measurement &amp; Literacy, 4e
				North Carolina: Developmental Math Redesign Modules (Akst/Bragg)
				Northern Virginia Community College--Annandale: BUS 224
				Northern Virginia Community College: MTT 1-4
				Northwest Arkansas Community College: MATH 1003
				Nottingham University Custom MyLab Maths 2021
				Oakton Community College: MAT085: Intermediate Alg for General Education
				Olivier: Business Mathematics Interactive
				Orientation Questions for Students
				Palm Beach State College: MAT 0018: Prealgebra
				Palm Beach State College: MAT 0028: Introductory Algebra
				Pasadena City College: MATH 8
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Phil's Test Book
				Piedmont Technical College: MAT 122: Finite College Math (2011)
				Pirnot: Mathematics All Around, 5e
				Pirnot: Mathematics All Around, 6e
				Pirnot: Mathematics All Around, 7e
				Pitt Community College: Math 110: Mathematical Measurement and Literacy
				Portsmouth Uni Foundation Maths Custom MyLab Math 2021
				Praxis 1 Math
				Prealgebra YTIs (Virtual School exercises)
				Precalculus YTIs (Virtual Schools exercises)
				Principles of Finance, FIN 3000, 5th Custom Edition for BC
				Pueblo Community College: MAT 107/108
				Purdue University: MA262
				Queens College: ECON249: Business Statistics
				Ratti/McWaters/Skrzypek/Fresh/Bernard: Precalculus A Right Triangle Approach, 5e
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra &amp; Trigonometry, 4e
				Ratti: College Algebra, 3e
				Ratti: College Algebra, 4e
				Ratti: Precalculus A Right Triangle Approach, 4e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach, 3e 
				Ritchey/Kapanjie/Fisher: Applied Calculus Interactive
				Ritchey/Rickard/Merkin: Interactive Finite Mathematics
				Ritchey: Applied Calculus Interactive Test Book
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold/Krieger: Interactive Developmental Mathematics, 2e
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 6e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 6e
				Rockswold: Developmental Mathematics, 2e
				Rockswold: Essentials of College Algebra, 4e
				Rockswold: Interactive Developmental Math, Chapter 10 DEMO
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 5e
				Rockswold: Precalculus with Modeling &amp; Visualization, 6e
				Rowan-Cabarrus Community College: DMA Modules 1-8 (Martin-Gay)
				Royal Holloway Custom MyLab Math 2020
				Ruth C-L Test Book 12
				Ruth SPP Test Book
				Ruth Test Book 2 (publish)
				Ryerson University: QMS 102: Business Statistics, Volume 1
				Ryerson University: QMS 202: Business Statistics, Volume 2
				Ryerson University: QMS 230
				SAILS Tennessee: Martin-Gay Developmental Math 4e &amp; Triola Elementary Stats 13e
				Saint Charles Community College: MAT 096/098/121
				Salzman: Mathematics for Business, 10e
				Sam Houston State U: BAN232 Business Analysis &amp; BAN 363 International Bus Anal
				Sam Math Test Book
				Sam Prod Test Book A
				San Antonio College: MATH 1314
				San Antonio College: MATH 2412
				San Antonio: Math 1325
				Santa Ana College: MATH 84 - Beginning &amp; Intermediate Algebra
				Saunders: Mathematics for the Trades: A Guided Approach, 11e
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Schulz: Precalculus, 2e
				Schulz: Precalculus, 2e Digital Update
				Sharpe Business Statistics Custom MyLab
				Sharpe/De Veaux/Velleman: Business Statistics, 4e Digital Update
				Sharpe: Business Statistics, 3ce
				Sharpe: Business Statistics, 3e
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, 4e, GE
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Fourth Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course, 3e
				Sharpe: Business Statistics: A First Course, First Canadian Edition
				Sharpe: Business Statistics: A First Course, Second Canadian Edition
				SI Notation testing (from sbtest)
				Sinclair Community College: MAT 1130
				Sinclair Community College: MAT 1130 2018
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Smart/Zutter: Fundamentals of Investing, 14e
				Smart: Fundamentals of Investing, First Canadian Edition
				Southeastern Arkansas: Math 1063/1333
				Southern New Hampshire University: MAT130
				Southwest TN: Math 1530 Developmental Mathematics
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Developmental Math: Basic Math, Introductory &amp; Intermediate Algebra, 2e
				Squires: Developmental Math: Prealgebra, Introductory, and Intermediate Algebra
				St. Philip's College: MATH 8: Thinking Mathematically
				StatCrunch Test Book for Prod
				Statistics YTIs (Virtual School exercises)
				Stine: Statistics for Business: Decision Making and Analysis, 3e
				Stony Brook University: AMS 102: Elements of Statistics 2e
				Suffolk County Community College, Grant Campus: MAT 006/007 (2012)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions, 2e with IR
				Sullivan: Algebra &amp; Trigonometry, 10e
				Sullivan: Algebra &amp; Trigonometry, 11e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 7e
				Sullivan: College Algebra Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra with Integrated Review, 10e
				Sullivan: College Algebra, 10e
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra: Concepts Through Functions 3e
				Sullivan: College Algebra: Concepts Through Functions Corequisite, 4e (2019)
				Sullivan: College Algebra: Concepts Through Functions, 4e
				Sullivan: Developmental Math, 1e
				Sullivan: Developmental Math, 2e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary &amp; Intermediate Algebra, 4e
				Sullivan: Elementary Algebra, 4e
				Sullivan: Fundamentals of Statistics, 2e (Retired)
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Fundamentals of Statistics, 6e
				Sullivan: Intermediate Algebra, 4e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 7e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 8e
				Sullivan: Precalculus, 10e
				Sullivan: Precalculus, 11e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 4e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5e
				Sullivan: Statistics: Informed Decisions Using Data, 5e with Integrated Review
				Sullivan: Statistics: Informed Decisions Using Data, 6e
				Sullivan: Trigonometry: A Unit Circle Approach, 10e
				Sullivan: Trigonometry: A Unit Circle Approach, 11e
				Suny Oswego: MAT 102: Foundations of Mathematics in the Real World
				Swansea University Custom MyLab Maths 2021
				Swansea University: Wedlake Custom MLG 2018
				Tannenbaum: Excursions in Modern Mathematics, 10e
				Tannenbaum: Excursions in Modern Mathematics, 9e
				Technical College System of Georgia: Math 0090
				Temple University: STAT 1001: Quantitative Methods for Business I
				Tennessee State University: ECON 2040/3050: Economics and Finance
				Test Book--MyMathLab Load
				Texas State: Math 1314 Course Redesign
				The University of Akron: 2010 085: Fundamentals of Math V
				The University of Akron: 3450 145: College Algebra: Graphs and Models
				The University of Texas at Arlington: Math 1327
				The University of Texas at Arlington: Math 1327 (2021)
				Thomas' Calc demo
				Thomas' Calculus 11e DEMO RETIRED
				Thomas' Calculus Early Transcendentals, 15e
				Thomas' Calculus Global Edition, 12e
				Thomas' Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Thomas’ Calculus Early Transcendentals, 14e
				Thomas’ Calculus, 14e
				Tidewater Community College: Math 130: Fundamentals of Reasoning
				Titman/Martin/Keown: Financial Management, 13e
				Titman/Martin/Keown: Financial Management, 14e,GE
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 8e
				Tobey: Basic College Mathematics, 9e
				Tobey: Beginning Algebra, 9e
				Tobey: Beginning Algebra: Early Graphing, 4e
				Tobey: Beginning and Intermediate Algebra, 5e
				Tobey: Beginning and Intermediate Algebra, 6e
				Tobey: Developmental Mathematics, 1e DIGITAL UPDATE
				Tobey: Developmental Mathematics: Mobile Enhanced MyMathLab
				Tobey: Intermediate Algebra, 8e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trade Application Library
				Tri-County Technical College: MAT 137/138
				Trigsted: Algebra and Trigonometry, 2e
				Trigsted: Algebra and Trigonometry, 3e
				Trigsted: Beginning &amp; Intermediate Algebra, 2e
				Trigsted: College Algebra Interactive Update
				Trigsted: College Algebra Interactive, Chapters R-5
				Trigsted: College Algebra, 3e
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: College Algebra, 4e
				Trigsted: College Algebra: A Corequisite Solution, 4e 
				Trigsted: Interactive Developmental Math: Prealg, Beginning &amp; Inter Algebra, 2e
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trigsted: Intermediate Algebra, 2e
				Trigsted: Trigonometry 2e
				Trigsted: Trigonometry, 3e
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Biostatistics for the Biological and Health Sciences, 2e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics using Excel, 7e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 12e with Integrated Review
				Triola: Elementary Statistics, 13e
				Triola: Elementary Statistics, 13e with Integrated Review
				Triola: Elementary Statistics, 14e
				Triola: Elementary Statistics, 3ce DEMO
				Triola: Elementary Statistics, California Edition
				Triola: Elementary Statistics, Second California Edition
				Triola: Elementary Statistics, Third California Edition
				Triola: Essentials of Statistics, 5e
				Triola: Essentials of Statistics, 6e
				Triola: Essentials of Statistics, 7e
				Tro, Introductory Chemistry, 5e
				TuDelft - Combined MathLab of Thomas Calculus 13E and Lay Linear Algebra 5E
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				UMass Dartmouth: Math 140: Quantitative Reasoning: Critical Thinking and Problem
				UMass Dartmouth: Math 146: Finite Mathematics
				Universal Algebra I YTIs (Virtual Schools exercises)
				Universal Course: Algebra Readiness
				Universal Course: Applied Calculus
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Alberta: Calculus
				University of Arkansas Little Rock: MATH 0321-0324: Foundations of Math 1-4
				University of Bridgeport: MATH 102: The Nature of Mathematics
				University of Bridgeport: Math 103: Introduction to College Algebra &amp; Statistics
				University of Bridgeport: Math 106/109: Precalculus
				University of California, Berkeley: MATH 54
				University of California, Irvine: Econ 15A/B: Business Statistics
				University of Connecticut: BADM 5103
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: MAT106: Finite Mathematics
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: Math 200
				University of Maryland University College: STAT 200: Elementary Statistics
				University of Maryland University College: STAT 230: Stats for Business &amp; Econ
				University of Massachusetts, Amherst: Voyaging Through Precalculus, Second Editi
				University of Memphis: Math 1420
				University of Nottingham: Quantitative Modules
				University of Phoenix: QRB/501 Quantitative Reasoning for Bus V3 2011
				University of Phoenix: QRB/501 Quantitative Reasoning for Business V4 2012
				University of Tennessee at Chattanooga: MATH 1130
				University of Tennessee at Chattanooga: MATH 1130/1830
				University of Texas at San Antonio: MATH 0203/0213
				University of Texas Dana Center: Foundations of Mathematical Reasoning
				University of Texas Dana Center: Quantitative Reasoning
				University of the Fraser Valley: Math 140/141: Precalculus and Applied Calculus
				University of Victoria: Finite Math and Its Applications
				University of Victoria: MATH 100/101/200
				Utah Valley Univ: A Quantitative Reasoning Approach: Math Applied to Modern Life
				Utah Valley University: MAT 0950
				Vincennes University: MATT 107 Applied Mathematics
				Volunteer State Community College: MATH1010: Math for General Studies
				Volunteer State Community College: MATH1010: Math for General Studies (2021)
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e MyLab Update
				Walters State Community College: MATH 1530
				Washington/Evans: Basic Technical Mathematics with Calculus, 12e
				Washington/Evans: Basic Technical Mathematics, 12e
				Washington: Basic Tech Math w Calculus SI CANADA (Update)  Ready To Go
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math, 11e
				Washington: Basic Technical Mathematics with Calculus, 10e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics with Calculus, SI Version, 11e
				Washington: Basic Technical Mathematics, 10e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 9e
				Weiss: Introductory Statistics, 10e
				Weiss: Introductory Statistics, 10e MyLab Revision with Tech Updates
				Westmoreland County Community College: MAT 050/052: Prealgebra
				Westmoreland County Community College: Math 100/157: Intermediate &amp; College Alge
				Winthrop University: MATH 150: Mathematics with Applications and Logic
				Wisniewski/Shafti: Quantitative Analysis for Decision Makers 7e
				Wolczuk/Norman: Introduction to Linear Algebra for Science and Engineering, 3e
				Woodbury: Elementary &amp; Intermediate Algebra, 4e
				Woodbury: Intermediate Algebra: A STEM Approach, 1e
				Workspace Test Book
				Wright State Univ: EC 1050: Elem Mathematical Economic &amp; Bus Models &amp; Methods
				Wright State University: Math 1450: Quantitative Reasoning
				Wright State University: MS 2040: Introduction to Business Statistics
				Wright State University: MS 2050: Introduction to Bus Stats &amp; Mgmt Science
				XL Content Options Showcase
				XL Load Testbook
				z MathXL Freelancer Test Book
				Zuro: Discovering Mathematics, 1e DEMO
				Zutter/Smart: Principles of Managerial Finance, 15e
				Zutter/Smart: Principles of Managerial Finance, 16/e, Global Edition
				Zutter/Smart: Principles of Managerial Finance, 16e
				ZZIA Interactive testing

			</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = concat(&quot;
				  -- Choose Book --  
				 Levine: Statistics for Managers Using Microsoft Excel, 9e, Global Edition
				(Virtual School exercises)
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Geometry Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Geometry
				A&amp;B Testgen Browser Check book
				AaSam Site Builder 10
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Adams: A Complete Course, 9ce
				Adams: Calculus, 6e
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 7e
				Adams: Calculus: A Complete Course, 10ce
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data, 4e
				Agresti: Statistics: The Art and Science of Learning from Data, 5e
				Akst: Developmental Mathematics through Applications, 1e
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book
				Algebra Review for Calculus
				Alicia&quot; , &quot;'&quot; , &quot;s MNL Pharmacology XL style
				Alicia&quot; , &quot;'&quot; , &quot;s SBNet feature testing book
				All Wizard Book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Almy: Math Lit: A Pathway to College Mathematics, 3e
				American River College: Math 41, 42, 131, 132, &amp; 1331
				Angel: A Survey of Mathematics with Applications, 10e
				Angel: A Survey of Mathematics with Applications, 10e with Integrated Review
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 5e
				Angel: Elementary Algebra for College Students, 10e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Intermediate Algebra for College Students, 10e
				Anne Arundel Commuity College: MAT 044/045/145/146
				Anne Arundel Community College: Mat 037/137: College Algebra Pathway
				Applied Calculus Conceptual Question Library
				Applied Calculus Interactive Demo
				Arizona State University: MAT 142: College Mathematics
				Arkansas State University, Beebe: Math 1013: Technical Math
				Aron: Statistics for Psychology, 6e
				Ashland Community and Technical College: Math 126
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Atlanta Technical College (2017)
				Austin Community College: MATD 0485
				Austin Peay State University: Math 1530: Statistics
				Babson College: QTM 1000/1010: Bus Analytics 1 &amp; 2
				Barnett: Calculus for Business, Economics, Life and Social Sci Brief Ver, 14e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 14e
				Barnett: College Mathematics for Business, Econ, Life and Social Sci, 14e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 14e
				Basic Technical Mathematics with Calculus, 10e, SI Version for Mohawk College
				Bass: Math Study Skills, 2e
				Beckmann: Skills Review for Math for Elementary and Middle School Teachers, 6e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 5e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: Algebra &amp; Trigonometry, 5e Media Update
				Beecher: Algebra and Trigonometry, 5e 
				Beecher: College Algebra with Integrated Review, 5e
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 5e
				Beecher: College Algebra, 5e Media Update
				Beecher: Precalculus A Right Triangle Approach, 5e
				Beecher: Precalculus A Right Triangle Approach, 5e Media Update
				Bellevue University: MBA 624: Business Analysis for Decision Making
				Bennett and Blitzer Sample Application Demos
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Statistical Reasoning For Everyday Life, 5e
				Bennett: Using and Understanding Mathematics, 6e
				Bennett: Using and Understanding Mathematics, 7e
				Berenson: Basic Business Statistics 2e, Australian Edition
				Berenson: Basic Business Statistics, 14e
				Berenson: Basic Business Statistics, 4e (Australia)
				Berenson: Basic Business Statistics, 5e (Aus)
				Berk/DeMarzo: Corporate Finance: The Core, 5/e, Global Edition
				Berk: Fundamentals of Corporate Finance, 3ce
				Betsy Smoketest 11-19-09
				Betsy Smoketest 3-3-10
				BI Norway: MET3431 Statistics
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 12e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 13e
				Bittinger: Algebra &amp; Trigonometry Graphs &amp; Models, 6e MyLab Revision
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Algebra Foundations, 1e DIGITAL UPDATE
				Bittinger: Basic College Mathematics, 13e
				Bittinger: Calculus and Its Applications Brief, 12e
				Bittinger: Calculus and Its Applications Expanded Ed, 1e Media Update
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 2e
				Bittinger: College Algebra Graphs &amp; Models, 6e Digital Update
				Bittinger: College Algebra: Graphs and Models, 6e
				Bittinger: Developmental Mathematics, 10e
				Bittinger: Developmental Mathematics, 9e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts &amp; Apps, 7e DIGITAL UPDATE
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, 10e
				Bittinger: Intermediate Algebra, 13e
				Bittinger: Intermediate Algebra, Concepts and Applications, 10e
				Bittinger: Introductory &amp; Intermediate Algebra with Integrated Review, 6e
				Bittinger: Introductory &amp; Intermediate Algebra, 6e
				Bittinger: Introductory Algebra, 13e
				Bittinger: Introductory Algebra, 13e DEMO (2019)
				Bittinger: Prealgebra &amp; Introductory Algebra, 4e
				Bittinger: Prealgebra, 7e
				Bittinger: Prealgebra, 8e
				Bittinger: Precalculus Graphs &amp; Models, 6e Digital Update
				Bittinger: Precalculus: Graphs and Models, 6e
				Blair/Tobey/Slater: Prealgebra, 6e
				Blitzer: Algebra &amp; Trigonometry with Integrated Review, 6e 
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 6e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 7e
				Blitzer: Algebra for College Students, 8e
				Blitzer: College Algebra An Early Functions Approach, 4e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra Essentials, 5e
				Blitzer: College Algebra Essentials, 6e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra, 8e
				Blitzer: College Algebra: An Early Functions Approach, 3e
				Blitzer: Developmental Mathematics, 1e
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Intermediate Algebra for College Students, 8e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra, 6e
				Blitzer: Introductory Algebra for College Students, 7e
				Blitzer: Introductory Algebra, 8e
				Blitzer: Math for Your World, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Pathways to College Mathematics, 2e
				Blitzer: Precalculus Essentials, 5e
				Blitzer: Precalculus Essentials, 6e
				Blitzer: Precalculus with Integrated Review, 6e (2018)
				Blitzer: Precalculus, 5e
				Blitzer: Precalculus, 6e
				Blitzer: Precalculus, 7e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Thinking Mathematically, 8e
				Blitzer: Trigonometry, 2e
				Blitzer: Trigonometry, 3e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bluegrass Community &amp; Technical College: MAT 105: Business Math (2021)
				Bluegrass Community and Technical College: MA 111 Contemporary Math
				Bluegrass Community and Technical College: MA 111 Contemporary Math (2021)
				Bluegrass Community and Technical College: MAT 105
				Bock/Bullard/Velleman/De Veaux: Stats: Modeling the World, 6e
				Bock: Stats In Your World, 2e
				Bock: Stats In Your World, 3e
				Bock: Stats: Modeling the World, 4e
				Bock: Stats: Modeling the World, 5e
				Borough of Manhattan CC: Introductory Algebra with Arithmetic Review
				Borough of Manhattan CC: MAT 008: Basic Math
				Borough of Manhattan CC: MAT 012: Basic Arithmetic and Algebra
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus Early Transcendentals, 3e
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update Aida Pilot
				Briggs/Cochran: Calculus for Scientists and Engineers, 1e
				Briggs/Cochran: Calculus, 3e
				Briggs/Cochran: Calculus, 3e Digital Update
				Briggs: AP Calculus, 2e
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Brigham Young University: Math 118: Finite Mathematics
				Brookdale Community College: Math 145
				Brooks: Financial Management: Core Concepts, 4e
				Broward College: STA 1001: Pathways to Statistics
				Bryant and Stratton College: Math 103: Survey of Math
				Business Algebra, Third Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT (del)
				Carman/Saunders: Mathematics for the Trades: A Guided Approach, 10e
				Carman: Mathematics for the Trades: A Guided Approach, Second Canadian Edition
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				CFO Online Finance Course. CHAPTER DEMO
				Cincinnati State TCC: AFM-091: Prealgebra 1
				Cincinnati State TCC: AFM-092: Prealgebra 2
				Cincinnati State TCC: AFM-094: Basic Algebra
				Cincinnati State TCC: AFM-097: Intermediate Algebra
				Clark: Applied Basic Mathematics, 2e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 11e
				Cleaves: College Mathematics, 10e
				Cleaves: College Mathematics, 9e
				Clemson University: MATH 1040/1060/2080
				Clendenen: Business Mathematics, 13e
				Clendenen: Business Mathematics, 14e
				Cleveland State Community College: Math 0130/1000
				Cleveland State Community College: Math 0530/1530 (2021)
				Cleveland State University: MATH 87, 3e 2015
				Cleveland State University: MTH 147/347: Statistics
				College of Southern Nevada: Math 96: Intermediate Algebra
				Collin County Community College District: MATH 1324
				Collins/Nunley: Navigating Through Mathematics, 1e DEMO chapter 7
				Columbus State Community College: Math 1130/1131
				Conceptual Question Library for Business Statistics (Online Only)
				Conceptual Question Library for Statistics (Online Only)
				Concordia University: Math 208
				Concordia University: Math 209
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematics for College and Career Readiness, 1e
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 5e
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 6e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 5e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 6e
				Contemporary Business Math with Canadian Applications for MacEwan University
				Contemporary Business Mathematics (BMAT 110)
				Corequisite Support for Sullivan: Precalc: CTF, A Right Triangle Approach, 4e
				Corequisite Support for Sullivan: Precalculus: CTF, A Unit Circle Approach, 4e
				Corequisite Support Modules for College Algebra or Precalculus
				Corequisite Support Modules for Quantitative Reasoning or Liberal Arts Math
				Corequisite Support Modules for Statistics
				Corporate Finance, 4e for University of California-Berkeley
				Craven Community College: MAT 110: Math Measurement &amp; Literacy
				Croft/Davison: Foundation Maths 7e
				Cumberland County College: MA 091/094: Developmental Math
				Custom MML- Las Positas College: Math 65/55/50: Beginning &amp; Intermediate Algebra
				CUSTOM- American River College: Math 41-133 (2021)
				Cuyahoga Community College (Tri-C): Math 091/095/096
				Cypress College: MATH 24: Pre-Statistics 2e 2017
				Dana Center: Statistical Reasoning - Preview and CQL
				De Veaux: Intro Stats, 4e
				De Veaux: Intro Stats, 5e
				De Veaux: Intro Stats, 6e
				De Veaux: Stats: Data &amp; Models, 4e
				De Veaux: Stats: Data &amp; Models, 5e
				De Veaux: Stats: Data &amp; Models, 5e, Global Edition
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce DEMO
				De Veaux: Stats: Data and Models, Fourth Canadian Edition
				De Veaux: Stats: Data and Models, Second Canadian Edition
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana/Waits/Kennedy/Bressoud: Calculus, 6e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 10e
				Demo GE Skill Builder Course
				DeVry University: MATH104
				Diagnostic Test for British Columbia Institute of Technology
				Donnelly: Business Statistics, 2e
				Donnelly: Business Statistics, 3e
				Drexel University: Math 101
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 6e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 4e MML Update
				Dugopolski: Trigonometry, 4e
				Dugopolski: Trigonometry, 5e
				Dutchess Community College: Intermediate Algebra
				Dyersburg State Community College: MATH 0530-1530
				EAS Mathematics
				ECPI MTH099: Beginning Algebra
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics (2009)
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e, GE
				Edwards/Penney/Calvis: Differential Equations Computing and Modeling, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 6e
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Eiteman/Stonehill/Moffett: Multinational Business Finance, 14e
				Eiteman/Stonehill/Moffett: Multinational Business Finance, 15e
				eT1 Math Test Book
				eT2 Math Test Book
				eText Integration Intermediate Alg Test Book
				eText Integration Trigsted College Alg Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e
				Evans: Business Analytics: Methods, Models, and Decisions, 3e Demo
				Evans: Business Analytics: Methods, Models, and Decisions, 3e, Global Edition
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				Fanshawe College: Math 1024
				Fayetteville Technical Community College: Volume 1: DMA 010-030
				Fayetteville Technical Community College: Volume 2: DMA 040-050
				Fayetteville Technical Community College: Volume 3: DMA 060-080
				FIN 320 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 321 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 325 Corporate Finance for State University of New York College at Oswego
				FIN256 Corporation Finance for Barkley at Syracuse University
				Financial Markets and International Finance, Second Custom Edition for RMU
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Finney: Calculus (Virtual School exercises)
				Florida International University: Finite Math 1060
				Florida International University: Finite Math 1106 (2009 Update)
				Florida Southwestern State College: MATH1033: Intermediate Algebra with Review
				Florida State College at Jacksonville: MAC 1105: College Algebra 2012
				Florida State College at Jacksonville: MAT 0018 Basic Mathematics
				Florida State College at Jacksonville: MAT 0024 - Elementary Algebra
				Florida State College at Jacksonville: MAT 1033 - Intermediate Algebra
				Florida State College at Jacksonville: MGF 1106/1107 Topics in College Math
				Florida State College at Jacksonville: STA 2023 - Elementary Statistics
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada HED) 
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Foundations of Mathematical Reasoning DEMO (October 2014)
				Front Range Community College, Larimer Campus: MAT050: Quantitative Literacy
				Front Range Community College: MAT050/055: Quantitative &amp; Algebraic Literacy 3e
				Fundamentals of Corporate Finance Third Custom Ed. for Boston University FE 101 
				Fundamentals of Corporate Finance Third Custom Edition for Boston University
				Fundamentals of Finance, by Cole for UT Knoville
				Gaze: Thinking Quantitatively: Communicating with Numbers, 2e
				Geometry YTIs (Virtual Schools exercises)
				Georgia Institute of Technology: MATH 1551-2551: Calculus with Linear Algebra
				Georgia State University: MATH 0999/1111 Coreq 2017
				Glendale Community College: Math 30
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 15e
				Goldstein: Finite Mathematics and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 13e
				Gould: Essential Statistics: Exploring the World Through Data, 2e
				Gould: Essential Statistics: Exploring the World Through Data, 3e
				Gould: Introductory Statistics, 3e Data Project Demo
				Gould: Introductory Statistics: Exploring the World through Data, 2e
				Gould: Introductory Statistics: Exploring the World through Data, 3e
				Gould: Introductory Statistics: Exploring the World Through Data, Ce
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Greenwich Quantitative Methods Custom MyMath Lab 2020
				Greenwich Quantitative Methods Custom MyMath Lab 2021
				Grimaldo: Developmental Math: Prealgebra, Intro Algebra, Intermediate Algebra 1e
				Groebner: Business Statistics: A Decision-Making Approach, 10e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				GTCC Demo 2012
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler, Introductory Mathematical Analysis, 14e, Global Edition 
				Haeussler: Introductory Mathematical Analysis, 13e
				Hagerstown Community College: Math 98/99/100
				Harrisburg Area Community College: MATH 008/022/033/044/055
				Harshbarger: College Algebra in Context w/ Integrated Review, 5e
				Harshbarger: College Algebra in Context, 4e
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, 6e
				Hass: University Calculus Early Transcendentals, 3e
				Hass: University Calculus Early Transcendentals, 4e
				Heartland Community College: Math 091-094 2e (2014)
				Henslin: Sociology
				Hillsborough Community College: MAC 1105
				Hornsby: A Graphical Approach to Algebra &amp; Trigonometry, 7e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to College Algebra, 7e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 7e
				Hostos Community College: MAT 150: College Algebra w/ Trig
				Hostos Community College: MAT 150: College Algebra with Trigonometry
				Houston Community College: Math 0309/0310/0314
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e DEMO 
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 11e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 12e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Indiana University Bloomington: SPEA-K300: Statistics
				Interactive Applied Calculus Chapter 1 Sampler
				Interactive Calculus Early Transcendentals, 1e Demo
				Introduction to Finance, First Custom Edition for RMU
				Introductory Mathematical Analysis, Fourthteen Edition
				IPRO-Irv-XLTestbk1
				James/Dyke: Modern Engineering Mathematics 6e
				Jamestown CC: Precalculus: Concepts Through Functions 4th Custom Ed 2015
				Jamestown Community College: MAT 005/006: Introductory and Intermediate Algebra
				Jamestown Community College: MAT 1590/1600
				Janus Math NextGen2 - Lil
				Joliet Junior College: Math 090/094/098
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 5e
				Kaplan Math 103: College Mathematics
				Kaplan MM201-A
				Kaplan MM201-B
				KCTCS: Bittinger Prealgebra &amp; Introductory Algebra 3e, Open Access
				KCTCS: Southcentral Kentucky Community&amp;Tech College: MAT 105/110/116/126/146/150
				Kentucky Community and Technical College System: MAT 161
				Keown/Martin/Petty: Foundations of Finance, 10e
				Keown: Personal Finance: Turning Money into Wealth, 8e
				Knewton Math test book
				KU122: Introduction to Math Skills and Strategies
				Lancaster University Custom MyLab Math 2020
				Larson: Elementary Statistics: Picturing the World, 6e
				Larson: Elementary Statistics: Picturing the World, 7e
				Larson: Elementary Statistics: Picturing the World, 7e with Integrated Review
				Larson: Elementary Statistics: Picturing the World, 8e
				Larson: Elementary Statistics: Picturing the World, Global Edition, 7/e
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 5e
				Lay: Linear Algebra and Its Applications, 6/e, Global Edition
				Lay: Linear Algebra and Its Applications, 6e
				Lehigh Carbon Community College: Mathematical Literacy, Custom Edition
				Lehmann: A Pathway to Introductory Statistics, 1e
				Lehmann: A Pathway to Introductory Statistics, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 3e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 3e
				Lehmann: Intermediate Algebra: Functions &amp; Auth Apps, 6e w/ Integrated Review
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 4e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 6e
				Lesmeister: Math Basics for the Health Care Professional, 4e
				Lethbridge College: MTH 0009/0010/0020/0030
				Levine: Business Statistics: A First Course, 6e
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Business Statistics: A First Course, 7e
				Levine: Business Statistics: A First Course, 8/e, Global Edition
				Levine: Business Statistics: A First Course, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 9e
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial/Greenwell/Ritchey: Calculus with Applications Brief, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, Brief Version, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 10e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 11e
				Lial/Hungerford/Holcomb/Mullins: Finite Mathematics with Applications, 12e
				Lial/Hungerford/Holcomb/Mullins: Mathematics with Applications, 12e
				Lial: Algebra &amp; Trigonometry for College Readiness Media Update
				Lial: Algebra for College Students , 9e
				Lial: Algebra for College Students, 8e
				Lial: Basic College Mathematics, 10e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra with Integrated Review, 12e
				Lial: Beginning Algebra, 12e
				Lial: Beginning Algebra, 13e
				Lial: Beginning and Intermediate Algebra and College Algebra Co-Requisite 1e
				Lial: Beginning and Intermediate Algebra, 7e
				Lial: Calculus with Applications, 10e
				Lial: Calculus with Applications, 11e
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra and Trigonometry, 7e
				Lial: College Algebra w/ Integrated Review, 12e
				Lial: College Algebra, 10e for CLEP
				Lial: College Algebra, 12e
				Lial: College Algebra, 13e
				Lial: Developmental Mathematics, 2e (Retired)
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics, 4e
				Lial: Essentials of College Algebra, 11e
				Lial: Essentials of College Algebra, 12e
				Lial: Finite Mathematics with Applications, 11e
				Lial: Finite Mathematics, 11e
				Lial: Finite Mathematics, 12e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 13e
				Lial: Intermediate Algebra, 13e V2
				Lial: Introductory Algebra, 11e
				Lial: Introductory and Intermediate Algebra, 6e
				Lial: Mathematics with Applications, 11e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra, 4e
				Lial: Prealgebra, 5e
				Lial: Prealgebra, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 7e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Liberty University: MATH 115: Math for Liberal Arts
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Long: Mathematical Reasoning for Elementary Teachers, 7e Media Update
				Madison College: ABE Dev Math Series
				Madura: Personal Finance, 7e
				Maricopa District: Developmental Math Modules
				Maricopa: Custom Module Demo
				Marion Technical College: Math 0920: Algebraic Literacy
				Martin-Gay Algebra Foundations
				Martin-Gay: Algebra 1, MyLab Homeschool Edition
				Martin-Gay: Algebra 2, MyLab Homeschool Edition
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Algebra: A Combined Approach, 5e
				Martin-Gay: Algebra: A Combined Approach, 6e 
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics with Early Integers, 3e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 5e
				Martin-Gay: Basic College Mathematics, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra with Integrated Review, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra with Integrated Review, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra with Integrated Review, 6e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Beginning Algebra, 8e
				Martin-Gay: Developmental Mathematics, 3e
				Martin-Gay: Developmental Mathematics, 4e
				Martin-Gay: Geometry
				Martin-Gay: Inter. Algebra: Math for College Readiness (FL Edition)
				Martin-Gay: Interactive Algebra Foundations, 1e
				Martin-Gay: Interactive Algebra Foundations, 1e DEMO
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 6e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 5e
				Martin-Gay: Introductory Algebra, 6e
				Martin-Gay: Path to College Mathematics, 1e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 5e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 7e (HTML5 Preview Version)
				Martin-Gay: Prealgebra, 8e
				MassBay Community College: MA090/095/098
				MasteringX 
				McClave: A First Course in Statistics, 12e
				McClave: Statistics for Business and Economics, 13e
				McClave: Statistics for Business and Economics, 14e
				McClave: Statistics for Business and Economics, 14e, Global Edition
				McClave: Statistics, 13e
				McClave: Statistics, 13e MyLab Revision with Technology Updates
				Merrimack College: MTH 1007
				Metropolitan Community College: MATH 0910/1220 Fall 2018
				Metropolitan Community College: MATH 0910/1240
				Metropolitan Community College: MATH 0910/1240 (2019)
				Metropolitan Community College: MATH-0910/1220 Fall 2018
				Miami Dade College: MAT 0022C: Developmental Mathematics Combined
				Middle Tennessee State University: Math 1530K: Applied Statistics 2017
				Middlesex Community College: MTH001/002/003: Preparation for College Math 2021
				Midlands Technical College: MAT 122
				Miller: Mathematical Ideas, 13e
				Miller: Mathematical Ideas, 14e
				Mineral Area College: MTH 0930/1205/1240/1260
				Mineral Area College: MTH 1270/1180
				Missouri Southern State University: Math 140
				Montgomery County Community College: MAT10: Concepts of Numbers
				Multinational Business Finance Custom Edition for Ryerson
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyFoundationsLab Prototype
				MyHealthProfessionsLab for Math Basics for the Health Care Professional, 5/e
				MyLab Math, Australian/New Zealand edition
				MyMathTest: Precalculus and Calculus
				MyStatLab for De Veaux: Stats: Data and Models, 3ce
				Nagle: Fundamentals of Differential Eq w/ Boundary Value Prob, 7e Digital Update
				Nagle: Fundamentals of Differential Equations w/ Boundary Value Problems, 7e
				Nagle: Fundamentals of Differential Equations, 9e
				Nagle: Fundamentals of Differential Equations, 9e Digital Update
				Nassau CC: Lial Intermediate 9e with Trig 7e
				National University: BST322: Introduction to Biomedical Statistics
				National University: Statistics and Data Analysis for Nursing Research
				NES Prep, Elementary Education II: Math
				Neuhauser/Roper: Calculus for Biology and Medicine, 4e
				New Additional Conceptual Exercises (NC Redesign)
				New Book Test 6
				Newbold: Statistics for Business and Economics, 8e
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL_BigInt1
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--Shehan
				NextGen Media Math Test Book--ShehanNew
				NextGen Media Math Test BookRH
				Norman/Wolczuk: Introduction to Linear Algebra for Science - Delete
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				North Arkansas College: MCT MAT 1011/1012
				North Carolina Community Clgs: Math 110: Mathematical Measurement &amp; Literacy
				North Carolina Community Clgs: Math 110: Mathematical Measurement &amp; Literacy, 4e
				North Carolina: Developmental Math Redesign Modules (Akst/Bragg)
				Northern Virginia Community College--Annandale: BUS 224
				Northern Virginia Community College: MTT 1-4
				Northwest Arkansas Community College: MATH 1003
				Nottingham University Custom MyLab Maths 2021
				Oakton Community College: MAT085: Intermediate Alg for General Education
				Olivier: Business Mathematics Interactive
				Orientation Questions for Students
				Palm Beach State College: MAT 0018: Prealgebra
				Palm Beach State College: MAT 0028: Introductory Algebra
				Pasadena City College: MATH 8
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Phil&quot; , &quot;'&quot; , &quot;s Test Book
				Piedmont Technical College: MAT 122: Finite College Math (2011)
				Pirnot: Mathematics All Around, 5e
				Pirnot: Mathematics All Around, 6e
				Pirnot: Mathematics All Around, 7e
				Pitt Community College: Math 110: Mathematical Measurement and Literacy
				Portsmouth Uni Foundation Maths Custom MyLab Math 2021
				Praxis 1 Math
				Prealgebra YTIs (Virtual School exercises)
				Precalculus YTIs (Virtual Schools exercises)
				Principles of Finance, FIN 3000, 5th Custom Edition for BC
				Pueblo Community College: MAT 107/108
				Purdue University: MA262
				Queens College: ECON249: Business Statistics
				Ratti/McWaters/Skrzypek/Fresh/Bernard: Precalculus A Right Triangle Approach, 5e
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra &amp; Trigonometry, 4e
				Ratti: College Algebra, 3e
				Ratti: College Algebra, 4e
				Ratti: Precalculus A Right Triangle Approach, 4e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach, 3e 
				Ritchey/Kapanjie/Fisher: Applied Calculus Interactive
				Ritchey/Rickard/Merkin: Interactive Finite Mathematics
				Ritchey: Applied Calculus Interactive Test Book
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold/Krieger: Interactive Developmental Mathematics, 2e
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 6e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 6e
				Rockswold: Developmental Mathematics, 2e
				Rockswold: Essentials of College Algebra, 4e
				Rockswold: Interactive Developmental Math, Chapter 10 DEMO
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 5e
				Rockswold: Precalculus with Modeling &amp; Visualization, 6e
				Rowan-Cabarrus Community College: DMA Modules 1-8 (Martin-Gay)
				Royal Holloway Custom MyLab Math 2020
				Ruth C-L Test Book 12
				Ruth SPP Test Book
				Ruth Test Book 2 (publish)
				Ryerson University: QMS 102: Business Statistics, Volume 1
				Ryerson University: QMS 202: Business Statistics, Volume 2
				Ryerson University: QMS 230
				SAILS Tennessee: Martin-Gay Developmental Math 4e &amp; Triola Elementary Stats 13e
				Saint Charles Community College: MAT 096/098/121
				Salzman: Mathematics for Business, 10e
				Sam Houston State U: BAN232 Business Analysis &amp; BAN 363 International Bus Anal
				Sam Math Test Book
				Sam Prod Test Book A
				San Antonio College: MATH 1314
				San Antonio College: MATH 2412
				San Antonio: Math 1325
				Santa Ana College: MATH 84 - Beginning &amp; Intermediate Algebra
				Saunders: Mathematics for the Trades: A Guided Approach, 11e
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Schulz: Precalculus, 2e
				Schulz: Precalculus, 2e Digital Update
				Sharpe Business Statistics Custom MyLab
				Sharpe/De Veaux/Velleman: Business Statistics, 4e Digital Update
				Sharpe: Business Statistics, 3ce
				Sharpe: Business Statistics, 3e
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, 4e, GE
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Fourth Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course, 3e
				Sharpe: Business Statistics: A First Course, First Canadian Edition
				Sharpe: Business Statistics: A First Course, Second Canadian Edition
				SI Notation testing (from sbtest)
				Sinclair Community College: MAT 1130
				Sinclair Community College: MAT 1130 2018
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Smart/Zutter: Fundamentals of Investing, 14e
				Smart: Fundamentals of Investing, First Canadian Edition
				Southeastern Arkansas: Math 1063/1333
				Southern New Hampshire University: MAT130
				Southwest TN: Math 1530 Developmental Mathematics
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Developmental Math: Basic Math, Introductory &amp; Intermediate Algebra, 2e
				Squires: Developmental Math: Prealgebra, Introductory, and Intermediate Algebra
				St. Philip&quot; , &quot;'&quot; , &quot;s College: MATH 8: Thinking Mathematically
				StatCrunch Test Book for Prod
				Statistics YTIs (Virtual School exercises)
				Stine: Statistics for Business: Decision Making and Analysis, 3e
				Stony Brook University: AMS 102: Elements of Statistics 2e
				Suffolk County Community College, Grant Campus: MAT 006/007 (2012)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions, 2e with IR
				Sullivan: Algebra &amp; Trigonometry, 10e
				Sullivan: Algebra &amp; Trigonometry, 11e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 7e
				Sullivan: College Algebra Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra with Integrated Review, 10e
				Sullivan: College Algebra, 10e
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra: Concepts Through Functions 3e
				Sullivan: College Algebra: Concepts Through Functions Corequisite, 4e (2019)
				Sullivan: College Algebra: Concepts Through Functions, 4e
				Sullivan: Developmental Math, 1e
				Sullivan: Developmental Math, 2e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary &amp; Intermediate Algebra, 4e
				Sullivan: Elementary Algebra, 4e
				Sullivan: Fundamentals of Statistics, 2e (Retired)
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Fundamentals of Statistics, 6e
				Sullivan: Intermediate Algebra, 4e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 7e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 8e
				Sullivan: Precalculus, 10e
				Sullivan: Precalculus, 11e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 4e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5e
				Sullivan: Statistics: Informed Decisions Using Data, 5e with Integrated Review
				Sullivan: Statistics: Informed Decisions Using Data, 6e
				Sullivan: Trigonometry: A Unit Circle Approach, 10e
				Sullivan: Trigonometry: A Unit Circle Approach, 11e
				Suny Oswego: MAT 102: Foundations of Mathematics in the Real World
				Swansea University Custom MyLab Maths 2021
				Swansea University: Wedlake Custom MLG 2018
				Tannenbaum: Excursions in Modern Mathematics, 10e
				Tannenbaum: Excursions in Modern Mathematics, 9e
				Technical College System of Georgia: Math 0090
				Temple University: STAT 1001: Quantitative Methods for Business I
				Tennessee State University: ECON 2040/3050: Economics and Finance
				Test Book--MyMathLab Load
				Texas State: Math 1314 Course Redesign
				The University of Akron: 2010 085: Fundamentals of Math V
				The University of Akron: 3450 145: College Algebra: Graphs and Models
				The University of Texas at Arlington: Math 1327
				The University of Texas at Arlington: Math 1327 (2021)
				Thomas&quot; , &quot;'&quot; , &quot; Calc demo
				Thomas&quot; , &quot;'&quot; , &quot; Calculus 11e DEMO RETIRED
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals, 15e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Global Edition, 12e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Thomas’ Calculus Early Transcendentals, 14e
				Thomas’ Calculus, 14e
				Tidewater Community College: Math 130: Fundamentals of Reasoning
				Titman/Martin/Keown: Financial Management, 13e
				Titman/Martin/Keown: Financial Management, 14e,GE
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 8e
				Tobey: Basic College Mathematics, 9e
				Tobey: Beginning Algebra, 9e
				Tobey: Beginning Algebra: Early Graphing, 4e
				Tobey: Beginning and Intermediate Algebra, 5e
				Tobey: Beginning and Intermediate Algebra, 6e
				Tobey: Developmental Mathematics, 1e DIGITAL UPDATE
				Tobey: Developmental Mathematics: Mobile Enhanced MyMathLab
				Tobey: Intermediate Algebra, 8e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trade Application Library
				Tri-County Technical College: MAT 137/138
				Trigsted: Algebra and Trigonometry, 2e
				Trigsted: Algebra and Trigonometry, 3e
				Trigsted: Beginning &amp; Intermediate Algebra, 2e
				Trigsted: College Algebra Interactive Update
				Trigsted: College Algebra Interactive, Chapters R-5
				Trigsted: College Algebra, 3e
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: College Algebra, 4e
				Trigsted: College Algebra: A Corequisite Solution, 4e 
				Trigsted: Interactive Developmental Math: Prealg, Beginning &amp; Inter Algebra, 2e
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trigsted: Intermediate Algebra, 2e
				Trigsted: Trigonometry 2e
				Trigsted: Trigonometry, 3e
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Biostatistics for the Biological and Health Sciences, 2e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics using Excel, 7e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 12e with Integrated Review
				Triola: Elementary Statistics, 13e
				Triola: Elementary Statistics, 13e with Integrated Review
				Triola: Elementary Statistics, 14e
				Triola: Elementary Statistics, 3ce DEMO
				Triola: Elementary Statistics, California Edition
				Triola: Elementary Statistics, Second California Edition
				Triola: Elementary Statistics, Third California Edition
				Triola: Essentials of Statistics, 5e
				Triola: Essentials of Statistics, 6e
				Triola: Essentials of Statistics, 7e
				Tro, Introductory Chemistry, 5e
				TuDelft - Combined MathLab of Thomas Calculus 13E and Lay Linear Algebra 5E
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				UMass Dartmouth: Math 140: Quantitative Reasoning: Critical Thinking and Problem
				UMass Dartmouth: Math 146: Finite Mathematics
				Universal Algebra I YTIs (Virtual Schools exercises)
				Universal Course: Algebra Readiness
				Universal Course: Applied Calculus
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Alberta: Calculus
				University of Arkansas Little Rock: MATH 0321-0324: Foundations of Math 1-4
				University of Bridgeport: MATH 102: The Nature of Mathematics
				University of Bridgeport: Math 103: Introduction to College Algebra &amp; Statistics
				University of Bridgeport: Math 106/109: Precalculus
				University of California, Berkeley: MATH 54
				University of California, Irvine: Econ 15A/B: Business Statistics
				University of Connecticut: BADM 5103
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: MAT106: Finite Mathematics
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: Math 200
				University of Maryland University College: STAT 200: Elementary Statistics
				University of Maryland University College: STAT 230: Stats for Business &amp; Econ
				University of Massachusetts, Amherst: Voyaging Through Precalculus, Second Editi
				University of Memphis: Math 1420
				University of Nottingham: Quantitative Modules
				University of Phoenix: QRB/501 Quantitative Reasoning for Bus V3 2011
				University of Phoenix: QRB/501 Quantitative Reasoning for Business V4 2012
				University of Tennessee at Chattanooga: MATH 1130
				University of Tennessee at Chattanooga: MATH 1130/1830
				University of Texas at San Antonio: MATH 0203/0213
				University of Texas Dana Center: Foundations of Mathematical Reasoning
				University of Texas Dana Center: Quantitative Reasoning
				University of the Fraser Valley: Math 140/141: Precalculus and Applied Calculus
				University of Victoria: Finite Math and Its Applications
				University of Victoria: MATH 100/101/200
				Utah Valley Univ: A Quantitative Reasoning Approach: Math Applied to Modern Life
				Utah Valley University: MAT 0950
				Vincennes University: MATT 107 Applied Mathematics
				Volunteer State Community College: MATH1010: Math for General Studies
				Volunteer State Community College: MATH1010: Math for General Studies (2021)
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e MyLab Update
				Walters State Community College: MATH 1530
				Washington/Evans: Basic Technical Mathematics with Calculus, 12e
				Washington/Evans: Basic Technical Mathematics, 12e
				Washington: Basic Tech Math w Calculus SI CANADA (Update)  Ready To Go
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math, 11e
				Washington: Basic Technical Mathematics with Calculus, 10e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics with Calculus, SI Version, 11e
				Washington: Basic Technical Mathematics, 10e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 9e
				Weiss: Introductory Statistics, 10e
				Weiss: Introductory Statistics, 10e MyLab Revision with Tech Updates
				Westmoreland County Community College: MAT 050/052: Prealgebra
				Westmoreland County Community College: Math 100/157: Intermediate &amp; College Alge
				Winthrop University: MATH 150: Mathematics with Applications and Logic
				Wisniewski/Shafti: Quantitative Analysis for Decision Makers 7e
				Wolczuk/Norman: Introduction to Linear Algebra for Science and Engineering, 3e
				Woodbury: Elementary &amp; Intermediate Algebra, 4e
				Woodbury: Intermediate Algebra: A STEM Approach, 1e
				Workspace Test Book
				Wright State Univ: EC 1050: Elem Mathematical Economic &amp; Bus Models &amp; Methods
				Wright State University: Math 1450: Quantitative Reasoning
				Wright State University: MS 2040: Introduction to Business Statistics
				Wright State University: MS 2050: Introduction to Bus Stats &amp; Mgmt Science
				XL Content Options Showcase
				XL Load Testbook
				z MathXL Freelancer Test Book
				Zuro: Discovering Mathematics, 1e DEMO
				Zutter/Smart: Principles of Managerial Finance, 15e
				Zutter/Smart: Principles of Managerial Finance, 16/e, Global Edition
				Zutter/Smart: Principles of Managerial Finance, 16e
				ZZIA Interactive testing

			&quot;) or . = concat(&quot;
				  -- Choose Book --  
				 Levine: Statistics for Managers Using Microsoft Excel, 9e, Global Edition
				(Virtual School exercises)
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Geometry Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Geometry
				A&amp;B Testgen Browser Check book
				AaSam Site Builder 10
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Adams: A Complete Course, 9ce
				Adams: Calculus, 6e
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 7e
				Adams: Calculus: A Complete Course, 10ce
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data, 4e
				Agresti: Statistics: The Art and Science of Learning from Data, 5e
				Akst: Developmental Mathematics through Applications, 1e
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book
				Algebra Review for Calculus
				Alicia&quot; , &quot;'&quot; , &quot;s MNL Pharmacology XL style
				Alicia&quot; , &quot;'&quot; , &quot;s SBNet feature testing book
				All Wizard Book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Almy: Math Lit: A Pathway to College Mathematics, 3e
				American River College: Math 41, 42, 131, 132, &amp; 1331
				Angel: A Survey of Mathematics with Applications, 10e
				Angel: A Survey of Mathematics with Applications, 10e with Integrated Review
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 5e
				Angel: Elementary Algebra for College Students, 10e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Intermediate Algebra for College Students, 10e
				Anne Arundel Commuity College: MAT 044/045/145/146
				Anne Arundel Community College: Mat 037/137: College Algebra Pathway
				Applied Calculus Conceptual Question Library
				Applied Calculus Interactive Demo
				Arizona State University: MAT 142: College Mathematics
				Arkansas State University, Beebe: Math 1013: Technical Math
				Aron: Statistics for Psychology, 6e
				Ashland Community and Technical College: Math 126
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Atlanta Technical College (2017)
				Austin Community College: MATD 0485
				Austin Peay State University: Math 1530: Statistics
				Babson College: QTM 1000/1010: Bus Analytics 1 &amp; 2
				Barnett: Calculus for Business, Economics, Life and Social Sci Brief Ver, 14e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 14e
				Barnett: College Mathematics for Business, Econ, Life and Social Sci, 14e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 14e
				Basic Technical Mathematics with Calculus, 10e, SI Version for Mohawk College
				Bass: Math Study Skills, 2e
				Beckmann: Skills Review for Math for Elementary and Middle School Teachers, 6e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 5e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: Algebra &amp; Trigonometry, 5e Media Update
				Beecher: Algebra and Trigonometry, 5e 
				Beecher: College Algebra with Integrated Review, 5e
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 5e
				Beecher: College Algebra, 5e Media Update
				Beecher: Precalculus A Right Triangle Approach, 5e
				Beecher: Precalculus A Right Triangle Approach, 5e Media Update
				Bellevue University: MBA 624: Business Analysis for Decision Making
				Bennett and Blitzer Sample Application Demos
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Statistical Reasoning For Everyday Life, 5e
				Bennett: Using and Understanding Mathematics, 6e
				Bennett: Using and Understanding Mathematics, 7e
				Berenson: Basic Business Statistics 2e, Australian Edition
				Berenson: Basic Business Statistics, 14e
				Berenson: Basic Business Statistics, 4e (Australia)
				Berenson: Basic Business Statistics, 5e (Aus)
				Berk/DeMarzo: Corporate Finance: The Core, 5/e, Global Edition
				Berk: Fundamentals of Corporate Finance, 3ce
				Betsy Smoketest 11-19-09
				Betsy Smoketest 3-3-10
				BI Norway: MET3431 Statistics
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 12e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 13e
				Bittinger: Algebra &amp; Trigonometry Graphs &amp; Models, 6e MyLab Revision
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Algebra Foundations, 1e DIGITAL UPDATE
				Bittinger: Basic College Mathematics, 13e
				Bittinger: Calculus and Its Applications Brief, 12e
				Bittinger: Calculus and Its Applications Expanded Ed, 1e Media Update
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 2e
				Bittinger: College Algebra Graphs &amp; Models, 6e Digital Update
				Bittinger: College Algebra: Graphs and Models, 6e
				Bittinger: Developmental Mathematics, 10e
				Bittinger: Developmental Mathematics, 9e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts &amp; Apps, 7e DIGITAL UPDATE
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, 10e
				Bittinger: Intermediate Algebra, 13e
				Bittinger: Intermediate Algebra, Concepts and Applications, 10e
				Bittinger: Introductory &amp; Intermediate Algebra with Integrated Review, 6e
				Bittinger: Introductory &amp; Intermediate Algebra, 6e
				Bittinger: Introductory Algebra, 13e
				Bittinger: Introductory Algebra, 13e DEMO (2019)
				Bittinger: Prealgebra &amp; Introductory Algebra, 4e
				Bittinger: Prealgebra, 7e
				Bittinger: Prealgebra, 8e
				Bittinger: Precalculus Graphs &amp; Models, 6e Digital Update
				Bittinger: Precalculus: Graphs and Models, 6e
				Blair/Tobey/Slater: Prealgebra, 6e
				Blitzer: Algebra &amp; Trigonometry with Integrated Review, 6e 
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 6e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 7e
				Blitzer: Algebra for College Students, 8e
				Blitzer: College Algebra An Early Functions Approach, 4e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra Essentials, 5e
				Blitzer: College Algebra Essentials, 6e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra, 8e
				Blitzer: College Algebra: An Early Functions Approach, 3e
				Blitzer: Developmental Mathematics, 1e
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Intermediate Algebra for College Students, 8e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra, 6e
				Blitzer: Introductory Algebra for College Students, 7e
				Blitzer: Introductory Algebra, 8e
				Blitzer: Math for Your World, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Pathways to College Mathematics, 2e
				Blitzer: Precalculus Essentials, 5e
				Blitzer: Precalculus Essentials, 6e
				Blitzer: Precalculus with Integrated Review, 6e (2018)
				Blitzer: Precalculus, 5e
				Blitzer: Precalculus, 6e
				Blitzer: Precalculus, 7e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Thinking Mathematically, 8e
				Blitzer: Trigonometry, 2e
				Blitzer: Trigonometry, 3e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bluegrass Community &amp; Technical College: MAT 105: Business Math (2021)
				Bluegrass Community and Technical College: MA 111 Contemporary Math
				Bluegrass Community and Technical College: MA 111 Contemporary Math (2021)
				Bluegrass Community and Technical College: MAT 105
				Bock/Bullard/Velleman/De Veaux: Stats: Modeling the World, 6e
				Bock: Stats In Your World, 2e
				Bock: Stats In Your World, 3e
				Bock: Stats: Modeling the World, 4e
				Bock: Stats: Modeling the World, 5e
				Borough of Manhattan CC: Introductory Algebra with Arithmetic Review
				Borough of Manhattan CC: MAT 008: Basic Math
				Borough of Manhattan CC: MAT 012: Basic Arithmetic and Algebra
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus Early Transcendentals, 3e
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update Aida Pilot
				Briggs/Cochran: Calculus for Scientists and Engineers, 1e
				Briggs/Cochran: Calculus, 3e
				Briggs/Cochran: Calculus, 3e Digital Update
				Briggs: AP Calculus, 2e
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Brigham Young University: Math 118: Finite Mathematics
				Brookdale Community College: Math 145
				Brooks: Financial Management: Core Concepts, 4e
				Broward College: STA 1001: Pathways to Statistics
				Bryant and Stratton College: Math 103: Survey of Math
				Business Algebra, Third Custom Edition for UOIT
				Business Algebra, Third Custom Edition for UOIT (del)
				Carman/Saunders: Mathematics for the Trades: A Guided Approach, 10e
				Carman: Mathematics for the Trades: A Guided Approach, Second Canadian Edition
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				CFO Online Finance Course. CHAPTER DEMO
				Cincinnati State TCC: AFM-091: Prealgebra 1
				Cincinnati State TCC: AFM-092: Prealgebra 2
				Cincinnati State TCC: AFM-094: Basic Algebra
				Cincinnati State TCC: AFM-097: Intermediate Algebra
				Clark: Applied Basic Mathematics, 2e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 11e
				Cleaves: College Mathematics, 10e
				Cleaves: College Mathematics, 9e
				Clemson University: MATH 1040/1060/2080
				Clendenen: Business Mathematics, 13e
				Clendenen: Business Mathematics, 14e
				Cleveland State Community College: Math 0130/1000
				Cleveland State Community College: Math 0530/1530 (2021)
				Cleveland State University: MATH 87, 3e 2015
				Cleveland State University: MTH 147/347: Statistics
				College of Southern Nevada: Math 96: Intermediate Algebra
				Collin County Community College District: MATH 1324
				Collins/Nunley: Navigating Through Mathematics, 1e DEMO chapter 7
				Columbus State Community College: Math 1130/1131
				Conceptual Question Library for Business Statistics (Online Only)
				Conceptual Question Library for Statistics (Online Only)
				Concordia University: Math 208
				Concordia University: Math 209
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematics for College and Career Readiness, 1e
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 5e
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 6e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 5e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 6e
				Contemporary Business Math with Canadian Applications for MacEwan University
				Contemporary Business Mathematics (BMAT 110)
				Corequisite Support for Sullivan: Precalc: CTF, A Right Triangle Approach, 4e
				Corequisite Support for Sullivan: Precalculus: CTF, A Unit Circle Approach, 4e
				Corequisite Support Modules for College Algebra or Precalculus
				Corequisite Support Modules for Quantitative Reasoning or Liberal Arts Math
				Corequisite Support Modules for Statistics
				Corporate Finance, 4e for University of California-Berkeley
				Craven Community College: MAT 110: Math Measurement &amp; Literacy
				Croft/Davison: Foundation Maths 7e
				Cumberland County College: MA 091/094: Developmental Math
				Custom MML- Las Positas College: Math 65/55/50: Beginning &amp; Intermediate Algebra
				CUSTOM- American River College: Math 41-133 (2021)
				Cuyahoga Community College (Tri-C): Math 091/095/096
				Cypress College: MATH 24: Pre-Statistics 2e 2017
				Dana Center: Statistical Reasoning - Preview and CQL
				De Veaux: Intro Stats, 4e
				De Veaux: Intro Stats, 5e
				De Veaux: Intro Stats, 6e
				De Veaux: Stats: Data &amp; Models, 4e
				De Veaux: Stats: Data &amp; Models, 5e
				De Veaux: Stats: Data &amp; Models, 5e, Global Edition
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce DEMO
				De Veaux: Stats: Data and Models, Fourth Canadian Edition
				De Veaux: Stats: Data and Models, Second Canadian Edition
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana/Waits/Kennedy/Bressoud: Calculus, 6e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 10e
				Demo GE Skill Builder Course
				DeVry University: MATH104
				Diagnostic Test for British Columbia Institute of Technology
				Donnelly: Business Statistics, 2e
				Donnelly: Business Statistics, 3e
				Drexel University: Math 101
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 6e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 4e MML Update
				Dugopolski: Trigonometry, 4e
				Dugopolski: Trigonometry, 5e
				Dutchess Community College: Intermediate Algebra
				Dyersburg State Community College: MATH 0530-1530
				EAS Mathematics
				ECPI MTH099: Beginning Algebra
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics (2009)
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e, GE
				Edwards/Penney/Calvis: Differential Equations Computing and Modeling, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 6e
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Eiteman/Stonehill/Moffett: Multinational Business Finance, 14e
				Eiteman/Stonehill/Moffett: Multinational Business Finance, 15e
				eT1 Math Test Book
				eT2 Math Test Book
				eText Integration Intermediate Alg Test Book
				eText Integration Trigsted College Alg Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e
				Evans: Business Analytics: Methods, Models, and Decisions, 3e Demo
				Evans: Business Analytics: Methods, Models, and Decisions, 3e, Global Edition
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				Fanshawe College: Math 1024
				Fayetteville Technical Community College: Volume 1: DMA 010-030
				Fayetteville Technical Community College: Volume 2: DMA 040-050
				Fayetteville Technical Community College: Volume 3: DMA 060-080
				FIN 320 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 321 Fundamentals of Corporate Finance, 3rd Edition for CA State Fullerton
				FIN 325 Corporate Finance for State University of New York College at Oswego
				FIN256 Corporation Finance for Barkley at Syracuse University
				Financial Markets and International Finance, Second Custom Edition for RMU
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Finney: Calculus (Virtual School exercises)
				Florida International University: Finite Math 1060
				Florida International University: Finite Math 1106 (2009 Update)
				Florida Southwestern State College: MATH1033: Intermediate Algebra with Review
				Florida State College at Jacksonville: MAC 1105: College Algebra 2012
				Florida State College at Jacksonville: MAT 0018 Basic Mathematics
				Florida State College at Jacksonville: MAT 0024 - Elementary Algebra
				Florida State College at Jacksonville: MAT 1033 - Intermediate Algebra
				Florida State College at Jacksonville: MGF 1106/1107 Topics in College Math
				Florida State College at Jacksonville: STA 2023 - Elementary Statistics
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada HED) 
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Foundations of Mathematical Reasoning DEMO (October 2014)
				Front Range Community College, Larimer Campus: MAT050: Quantitative Literacy
				Front Range Community College: MAT050/055: Quantitative &amp; Algebraic Literacy 3e
				Fundamentals of Corporate Finance Third Custom Ed. for Boston University FE 101 
				Fundamentals of Corporate Finance Third Custom Edition for Boston University
				Fundamentals of Finance, by Cole for UT Knoville
				Gaze: Thinking Quantitatively: Communicating with Numbers, 2e
				Geometry YTIs (Virtual Schools exercises)
				Georgia Institute of Technology: MATH 1551-2551: Calculus with Linear Algebra
				Georgia State University: MATH 0999/1111 Coreq 2017
				Glendale Community College: Math 30
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 15e
				Goldstein: Finite Mathematics and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 13e
				Gould: Essential Statistics: Exploring the World Through Data, 2e
				Gould: Essential Statistics: Exploring the World Through Data, 3e
				Gould: Introductory Statistics, 3e Data Project Demo
				Gould: Introductory Statistics: Exploring the World through Data, 2e
				Gould: Introductory Statistics: Exploring the World through Data, 3e
				Gould: Introductory Statistics: Exploring the World Through Data, Ce
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Greenwich Quantitative Methods Custom MyMath Lab 2020
				Greenwich Quantitative Methods Custom MyMath Lab 2021
				Grimaldo: Developmental Math: Prealgebra, Intro Algebra, Intermediate Algebra 1e
				Groebner: Business Statistics: A Decision-Making Approach, 10e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				GTCC Demo 2012
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler, Introductory Mathematical Analysis, 14e, Global Edition 
				Haeussler: Introductory Mathematical Analysis, 13e
				Hagerstown Community College: Math 98/99/100
				Harrisburg Area Community College: MATH 008/022/033/044/055
				Harshbarger: College Algebra in Context w/ Integrated Review, 5e
				Harshbarger: College Algebra in Context, 4e
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, 6e
				Hass: University Calculus Early Transcendentals, 3e
				Hass: University Calculus Early Transcendentals, 4e
				Heartland Community College: Math 091-094 2e (2014)
				Henslin: Sociology
				Hillsborough Community College: MAC 1105
				Hornsby: A Graphical Approach to Algebra &amp; Trigonometry, 7e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to College Algebra, 7e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 7e
				Hostos Community College: MAT 150: College Algebra w/ Trig
				Hostos Community College: MAT 150: College Algebra with Trigonometry
				Houston Community College: Math 0309/0310/0314
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e DEMO 
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 11e
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 12e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Indiana University Bloomington: SPEA-K300: Statistics
				Interactive Applied Calculus Chapter 1 Sampler
				Interactive Calculus Early Transcendentals, 1e Demo
				Introduction to Finance, First Custom Edition for RMU
				Introductory Mathematical Analysis, Fourthteen Edition
				IPRO-Irv-XLTestbk1
				James/Dyke: Modern Engineering Mathematics 6e
				Jamestown CC: Precalculus: Concepts Through Functions 4th Custom Ed 2015
				Jamestown Community College: MAT 005/006: Introductory and Intermediate Algebra
				Jamestown Community College: MAT 1590/1600
				Janus Math NextGen2 - Lil
				Joliet Junior College: Math 090/094/098
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 5e
				Kaplan Math 103: College Mathematics
				Kaplan MM201-A
				Kaplan MM201-B
				KCTCS: Bittinger Prealgebra &amp; Introductory Algebra 3e, Open Access
				KCTCS: Southcentral Kentucky Community&amp;Tech College: MAT 105/110/116/126/146/150
				Kentucky Community and Technical College System: MAT 161
				Keown/Martin/Petty: Foundations of Finance, 10e
				Keown: Personal Finance: Turning Money into Wealth, 8e
				Knewton Math test book
				KU122: Introduction to Math Skills and Strategies
				Lancaster University Custom MyLab Math 2020
				Larson: Elementary Statistics: Picturing the World, 6e
				Larson: Elementary Statistics: Picturing the World, 7e
				Larson: Elementary Statistics: Picturing the World, 7e with Integrated Review
				Larson: Elementary Statistics: Picturing the World, 8e
				Larson: Elementary Statistics: Picturing the World, Global Edition, 7/e
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 5e
				Lay: Linear Algebra and Its Applications, 6/e, Global Edition
				Lay: Linear Algebra and Its Applications, 6e
				Lehigh Carbon Community College: Mathematical Literacy, Custom Edition
				Lehmann: A Pathway to Introductory Statistics, 1e
				Lehmann: A Pathway to Introductory Statistics, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 3e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 3e
				Lehmann: Intermediate Algebra: Functions &amp; Auth Apps, 6e w/ Integrated Review
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 4e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 6e
				Lesmeister: Math Basics for the Health Care Professional, 4e
				Lethbridge College: MTH 0009/0010/0020/0030
				Levine: Business Statistics: A First Course, 6e
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Business Statistics: A First Course, 7e
				Levine: Business Statistics: A First Course, 8/e, Global Edition
				Levine: Business Statistics: A First Course, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 9e
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial/Greenwell/Ritchey: Calculus with Applications Brief, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, Brief Version, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 10e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 11e
				Lial/Hungerford/Holcomb/Mullins: Finite Mathematics with Applications, 12e
				Lial/Hungerford/Holcomb/Mullins: Mathematics with Applications, 12e
				Lial: Algebra &amp; Trigonometry for College Readiness Media Update
				Lial: Algebra for College Students , 9e
				Lial: Algebra for College Students, 8e
				Lial: Basic College Mathematics, 10e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra with Integrated Review, 12e
				Lial: Beginning Algebra, 12e
				Lial: Beginning Algebra, 13e
				Lial: Beginning and Intermediate Algebra and College Algebra Co-Requisite 1e
				Lial: Beginning and Intermediate Algebra, 7e
				Lial: Calculus with Applications, 10e
				Lial: Calculus with Applications, 11e
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra and Trigonometry, 7e
				Lial: College Algebra w/ Integrated Review, 12e
				Lial: College Algebra, 10e for CLEP
				Lial: College Algebra, 12e
				Lial: College Algebra, 13e
				Lial: Developmental Mathematics, 2e (Retired)
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics, 4e
				Lial: Essentials of College Algebra, 11e
				Lial: Essentials of College Algebra, 12e
				Lial: Finite Mathematics with Applications, 11e
				Lial: Finite Mathematics, 11e
				Lial: Finite Mathematics, 12e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 13e
				Lial: Intermediate Algebra, 13e V2
				Lial: Introductory Algebra, 11e
				Lial: Introductory and Intermediate Algebra, 6e
				Lial: Mathematics with Applications, 11e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra, 4e
				Lial: Prealgebra, 5e
				Lial: Prealgebra, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 7e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Liberty University: MATH 115: Math for Liberal Arts
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Long: Mathematical Reasoning for Elementary Teachers, 7e Media Update
				Madison College: ABE Dev Math Series
				Madura: Personal Finance, 7e
				Maricopa District: Developmental Math Modules
				Maricopa: Custom Module Demo
				Marion Technical College: Math 0920: Algebraic Literacy
				Martin-Gay Algebra Foundations
				Martin-Gay: Algebra 1, MyLab Homeschool Edition
				Martin-Gay: Algebra 2, MyLab Homeschool Edition
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Algebra: A Combined Approach, 5e
				Martin-Gay: Algebra: A Combined Approach, 6e 
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics with Early Integers, 3e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 5e
				Martin-Gay: Basic College Mathematics, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra with Integrated Review, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra with Integrated Review, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra with Integrated Review, 6e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Beginning Algebra, 8e
				Martin-Gay: Developmental Mathematics, 3e
				Martin-Gay: Developmental Mathematics, 4e
				Martin-Gay: Geometry
				Martin-Gay: Inter. Algebra: Math for College Readiness (FL Edition)
				Martin-Gay: Interactive Algebra Foundations, 1e
				Martin-Gay: Interactive Algebra Foundations, 1e DEMO
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 6e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 5e
				Martin-Gay: Introductory Algebra, 6e
				Martin-Gay: Path to College Mathematics, 1e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 5e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 7e (HTML5 Preview Version)
				Martin-Gay: Prealgebra, 8e
				MassBay Community College: MA090/095/098
				MasteringX 
				McClave: A First Course in Statistics, 12e
				McClave: Statistics for Business and Economics, 13e
				McClave: Statistics for Business and Economics, 14e
				McClave: Statistics for Business and Economics, 14e, Global Edition
				McClave: Statistics, 13e
				McClave: Statistics, 13e MyLab Revision with Technology Updates
				Merrimack College: MTH 1007
				Metropolitan Community College: MATH 0910/1220 Fall 2018
				Metropolitan Community College: MATH 0910/1240
				Metropolitan Community College: MATH 0910/1240 (2019)
				Metropolitan Community College: MATH-0910/1220 Fall 2018
				Miami Dade College: MAT 0022C: Developmental Mathematics Combined
				Middle Tennessee State University: Math 1530K: Applied Statistics 2017
				Middlesex Community College: MTH001/002/003: Preparation for College Math 2021
				Midlands Technical College: MAT 122
				Miller: Mathematical Ideas, 13e
				Miller: Mathematical Ideas, 14e
				Mineral Area College: MTH 0930/1205/1240/1260
				Mineral Area College: MTH 1270/1180
				Missouri Southern State University: Math 140
				Montgomery County Community College: MAT10: Concepts of Numbers
				Multinational Business Finance Custom Edition for Ryerson
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyFoundationsLab Prototype
				MyHealthProfessionsLab for Math Basics for the Health Care Professional, 5/e
				MyLab Math, Australian/New Zealand edition
				MyMathTest: Precalculus and Calculus
				MyStatLab for De Veaux: Stats: Data and Models, 3ce
				Nagle: Fundamentals of Differential Eq w/ Boundary Value Prob, 7e Digital Update
				Nagle: Fundamentals of Differential Equations w/ Boundary Value Problems, 7e
				Nagle: Fundamentals of Differential Equations, 9e
				Nagle: Fundamentals of Differential Equations, 9e Digital Update
				Nassau CC: Lial Intermediate 9e with Trig 7e
				National University: BST322: Introduction to Biomedical Statistics
				National University: Statistics and Data Analysis for Nursing Research
				NES Prep, Elementary Education II: Math
				Neuhauser/Roper: Calculus for Biology and Medicine, 4e
				New Additional Conceptual Exercises (NC Redesign)
				New Book Test 6
				Newbold: Statistics for Business and Economics, 8e
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL_BigInt1
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--Shehan
				NextGen Media Math Test Book--ShehanNew
				NextGen Media Math Test BookRH
				Norman/Wolczuk: Introduction to Linear Algebra for Science - Delete
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				North Arkansas College: MCT MAT 1011/1012
				North Carolina Community Clgs: Math 110: Mathematical Measurement &amp; Literacy
				North Carolina Community Clgs: Math 110: Mathematical Measurement &amp; Literacy, 4e
				North Carolina: Developmental Math Redesign Modules (Akst/Bragg)
				Northern Virginia Community College--Annandale: BUS 224
				Northern Virginia Community College: MTT 1-4
				Northwest Arkansas Community College: MATH 1003
				Nottingham University Custom MyLab Maths 2021
				Oakton Community College: MAT085: Intermediate Alg for General Education
				Olivier: Business Mathematics Interactive
				Orientation Questions for Students
				Palm Beach State College: MAT 0018: Prealgebra
				Palm Beach State College: MAT 0028: Introductory Algebra
				Pasadena City College: MATH 8
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Phil&quot; , &quot;'&quot; , &quot;s Test Book
				Piedmont Technical College: MAT 122: Finite College Math (2011)
				Pirnot: Mathematics All Around, 5e
				Pirnot: Mathematics All Around, 6e
				Pirnot: Mathematics All Around, 7e
				Pitt Community College: Math 110: Mathematical Measurement and Literacy
				Portsmouth Uni Foundation Maths Custom MyLab Math 2021
				Praxis 1 Math
				Prealgebra YTIs (Virtual School exercises)
				Precalculus YTIs (Virtual Schools exercises)
				Principles of Finance, FIN 3000, 5th Custom Edition for BC
				Pueblo Community College: MAT 107/108
				Purdue University: MA262
				Queens College: ECON249: Business Statistics
				Ratti/McWaters/Skrzypek/Fresh/Bernard: Precalculus A Right Triangle Approach, 5e
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra &amp; Trigonometry, 4e
				Ratti: College Algebra, 3e
				Ratti: College Algebra, 4e
				Ratti: Precalculus A Right Triangle Approach, 4e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach, 3e 
				Ritchey/Kapanjie/Fisher: Applied Calculus Interactive
				Ritchey/Rickard/Merkin: Interactive Finite Mathematics
				Ritchey: Applied Calculus Interactive Test Book
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold/Krieger: Interactive Developmental Mathematics, 2e
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 6e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 6e
				Rockswold: Developmental Mathematics, 2e
				Rockswold: Essentials of College Algebra, 4e
				Rockswold: Interactive Developmental Math, Chapter 10 DEMO
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 5e
				Rockswold: Precalculus with Modeling &amp; Visualization, 6e
				Rowan-Cabarrus Community College: DMA Modules 1-8 (Martin-Gay)
				Royal Holloway Custom MyLab Math 2020
				Ruth C-L Test Book 12
				Ruth SPP Test Book
				Ruth Test Book 2 (publish)
				Ryerson University: QMS 102: Business Statistics, Volume 1
				Ryerson University: QMS 202: Business Statistics, Volume 2
				Ryerson University: QMS 230
				SAILS Tennessee: Martin-Gay Developmental Math 4e &amp; Triola Elementary Stats 13e
				Saint Charles Community College: MAT 096/098/121
				Salzman: Mathematics for Business, 10e
				Sam Houston State U: BAN232 Business Analysis &amp; BAN 363 International Bus Anal
				Sam Math Test Book
				Sam Prod Test Book A
				San Antonio College: MATH 1314
				San Antonio College: MATH 2412
				San Antonio: Math 1325
				Santa Ana College: MATH 84 - Beginning &amp; Intermediate Algebra
				Saunders: Mathematics for the Trades: A Guided Approach, 11e
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Schulz: Precalculus, 2e
				Schulz: Precalculus, 2e Digital Update
				Sharpe Business Statistics Custom MyLab
				Sharpe/De Veaux/Velleman: Business Statistics, 4e Digital Update
				Sharpe: Business Statistics, 3ce
				Sharpe: Business Statistics, 3e
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, 4e, GE
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Fourth Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course, 3e
				Sharpe: Business Statistics: A First Course, First Canadian Edition
				Sharpe: Business Statistics: A First Course, Second Canadian Edition
				SI Notation testing (from sbtest)
				Sinclair Community College: MAT 1130
				Sinclair Community College: MAT 1130 2018
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Smart/Zutter: Fundamentals of Investing, 14e
				Smart: Fundamentals of Investing, First Canadian Edition
				Southeastern Arkansas: Math 1063/1333
				Southern New Hampshire University: MAT130
				Southwest TN: Math 1530 Developmental Mathematics
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Developmental Math: Basic Math, Introductory &amp; Intermediate Algebra, 2e
				Squires: Developmental Math: Prealgebra, Introductory, and Intermediate Algebra
				St. Philip&quot; , &quot;'&quot; , &quot;s College: MATH 8: Thinking Mathematically
				StatCrunch Test Book for Prod
				Statistics YTIs (Virtual School exercises)
				Stine: Statistics for Business: Decision Making and Analysis, 3e
				Stony Brook University: AMS 102: Elements of Statistics 2e
				Suffolk County Community College, Grant Campus: MAT 006/007 (2012)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions, 2e with IR
				Sullivan: Algebra &amp; Trigonometry, 10e
				Sullivan: Algebra &amp; Trigonometry, 11e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 7e
				Sullivan: College Algebra Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra with Integrated Review, 10e
				Sullivan: College Algebra, 10e
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra: Concepts Through Functions 3e
				Sullivan: College Algebra: Concepts Through Functions Corequisite, 4e (2019)
				Sullivan: College Algebra: Concepts Through Functions, 4e
				Sullivan: Developmental Math, 1e
				Sullivan: Developmental Math, 2e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary &amp; Intermediate Algebra, 4e
				Sullivan: Elementary Algebra, 4e
				Sullivan: Fundamentals of Statistics, 2e (Retired)
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Fundamentals of Statistics, 6e
				Sullivan: Intermediate Algebra, 4e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 7e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 8e
				Sullivan: Precalculus, 10e
				Sullivan: Precalculus, 11e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 4e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5e
				Sullivan: Statistics: Informed Decisions Using Data, 5e with Integrated Review
				Sullivan: Statistics: Informed Decisions Using Data, 6e
				Sullivan: Trigonometry: A Unit Circle Approach, 10e
				Sullivan: Trigonometry: A Unit Circle Approach, 11e
				Suny Oswego: MAT 102: Foundations of Mathematics in the Real World
				Swansea University Custom MyLab Maths 2021
				Swansea University: Wedlake Custom MLG 2018
				Tannenbaum: Excursions in Modern Mathematics, 10e
				Tannenbaum: Excursions in Modern Mathematics, 9e
				Technical College System of Georgia: Math 0090
				Temple University: STAT 1001: Quantitative Methods for Business I
				Tennessee State University: ECON 2040/3050: Economics and Finance
				Test Book--MyMathLab Load
				Texas State: Math 1314 Course Redesign
				The University of Akron: 2010 085: Fundamentals of Math V
				The University of Akron: 3450 145: College Algebra: Graphs and Models
				The University of Texas at Arlington: Math 1327
				The University of Texas at Arlington: Math 1327 (2021)
				Thomas&quot; , &quot;'&quot; , &quot; Calc demo
				Thomas&quot; , &quot;'&quot; , &quot; Calculus 11e DEMO RETIRED
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals, 15e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Global Edition, 12e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Thomas’ Calculus Early Transcendentals, 14e
				Thomas’ Calculus, 14e
				Tidewater Community College: Math 130: Fundamentals of Reasoning
				Titman/Martin/Keown: Financial Management, 13e
				Titman/Martin/Keown: Financial Management, 14e,GE
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 8e
				Tobey: Basic College Mathematics, 9e
				Tobey: Beginning Algebra, 9e
				Tobey: Beginning Algebra: Early Graphing, 4e
				Tobey: Beginning and Intermediate Algebra, 5e
				Tobey: Beginning and Intermediate Algebra, 6e
				Tobey: Developmental Mathematics, 1e DIGITAL UPDATE
				Tobey: Developmental Mathematics: Mobile Enhanced MyMathLab
				Tobey: Intermediate Algebra, 8e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trade Application Library
				Tri-County Technical College: MAT 137/138
				Trigsted: Algebra and Trigonometry, 2e
				Trigsted: Algebra and Trigonometry, 3e
				Trigsted: Beginning &amp; Intermediate Algebra, 2e
				Trigsted: College Algebra Interactive Update
				Trigsted: College Algebra Interactive, Chapters R-5
				Trigsted: College Algebra, 3e
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: College Algebra, 4e
				Trigsted: College Algebra: A Corequisite Solution, 4e 
				Trigsted: Interactive Developmental Math: Prealg, Beginning &amp; Inter Algebra, 2e
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trigsted: Intermediate Algebra, 2e
				Trigsted: Trigonometry 2e
				Trigsted: Trigonometry, 3e
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Biostatistics for the Biological and Health Sciences, 2e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics using Excel, 7e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 12e with Integrated Review
				Triola: Elementary Statistics, 13e
				Triola: Elementary Statistics, 13e with Integrated Review
				Triola: Elementary Statistics, 14e
				Triola: Elementary Statistics, 3ce DEMO
				Triola: Elementary Statistics, California Edition
				Triola: Elementary Statistics, Second California Edition
				Triola: Elementary Statistics, Third California Edition
				Triola: Essentials of Statistics, 5e
				Triola: Essentials of Statistics, 6e
				Triola: Essentials of Statistics, 7e
				Tro, Introductory Chemistry, 5e
				TuDelft - Combined MathLab of Thomas Calculus 13E and Lay Linear Algebra 5E
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				UMass Dartmouth: Math 140: Quantitative Reasoning: Critical Thinking and Problem
				UMass Dartmouth: Math 146: Finite Mathematics
				Universal Algebra I YTIs (Virtual Schools exercises)
				Universal Course: Algebra Readiness
				Universal Course: Applied Calculus
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Alberta: Calculus
				University of Arkansas Little Rock: MATH 0321-0324: Foundations of Math 1-4
				University of Bridgeport: MATH 102: The Nature of Mathematics
				University of Bridgeport: Math 103: Introduction to College Algebra &amp; Statistics
				University of Bridgeport: Math 106/109: Precalculus
				University of California, Berkeley: MATH 54
				University of California, Irvine: Econ 15A/B: Business Statistics
				University of Connecticut: BADM 5103
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: MAT106: Finite Mathematics
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: Math 200
				University of Maryland University College: STAT 200: Elementary Statistics
				University of Maryland University College: STAT 230: Stats for Business &amp; Econ
				University of Massachusetts, Amherst: Voyaging Through Precalculus, Second Editi
				University of Memphis: Math 1420
				University of Nottingham: Quantitative Modules
				University of Phoenix: QRB/501 Quantitative Reasoning for Bus V3 2011
				University of Phoenix: QRB/501 Quantitative Reasoning for Business V4 2012
				University of Tennessee at Chattanooga: MATH 1130
				University of Tennessee at Chattanooga: MATH 1130/1830
				University of Texas at San Antonio: MATH 0203/0213
				University of Texas Dana Center: Foundations of Mathematical Reasoning
				University of Texas Dana Center: Quantitative Reasoning
				University of the Fraser Valley: Math 140/141: Precalculus and Applied Calculus
				University of Victoria: Finite Math and Its Applications
				University of Victoria: MATH 100/101/200
				Utah Valley Univ: A Quantitative Reasoning Approach: Math Applied to Modern Life
				Utah Valley University: MAT 0950
				Vincennes University: MATT 107 Applied Mathematics
				Volunteer State Community College: MATH1010: Math for General Studies
				Volunteer State Community College: MATH1010: Math for General Studies (2021)
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e MyLab Update
				Walters State Community College: MATH 1530
				Washington/Evans: Basic Technical Mathematics with Calculus, 12e
				Washington/Evans: Basic Technical Mathematics, 12e
				Washington: Basic Tech Math w Calculus SI CANADA (Update)  Ready To Go
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math, 11e
				Washington: Basic Technical Mathematics with Calculus, 10e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics with Calculus, SI Version, 11e
				Washington: Basic Technical Mathematics, 10e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 9e
				Weiss: Introductory Statistics, 10e
				Weiss: Introductory Statistics, 10e MyLab Revision with Tech Updates
				Westmoreland County Community College: MAT 050/052: Prealgebra
				Westmoreland County Community College: Math 100/157: Intermediate &amp; College Alge
				Winthrop University: MATH 150: Mathematics with Applications and Logic
				Wisniewski/Shafti: Quantitative Analysis for Decision Makers 7e
				Wolczuk/Norman: Introduction to Linear Algebra for Science and Engineering, 3e
				Woodbury: Elementary &amp; Intermediate Algebra, 4e
				Woodbury: Intermediate Algebra: A STEM Approach, 1e
				Workspace Test Book
				Wright State Univ: EC 1050: Elem Mathematical Economic &amp; Bus Models &amp; Methods
				Wright State University: Math 1450: Quantitative Reasoning
				Wright State University: MS 2040: Introduction to Business Statistics
				Wright State University: MS 2050: Introduction to Bus Stats &amp; Mgmt Science
				XL Content Options Showcase
				XL Load Testbook
				z MathXL Freelancer Test Book
				Zuro: Discovering Mathematics, 1e DEMO
				Zutter/Smart: Principles of Managerial Finance, 15e
				Zutter/Smart: Principles of Managerial Finance, 16/e, Global Edition
				Zutter/Smart: Principles of Managerial Finance, 16e
				ZZIA Interactive testing

			&quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
