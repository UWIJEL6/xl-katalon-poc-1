<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --  AdamsEssex Calcul_b5b986</name>
   <tag></tag>
   <elementGuidId>4eb76a4a-2a58-4562-ad57-7f2148f7a584</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				-- Choose Book --  
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Aston Uni Barrow Custom MyLab Math Global 2019
				Barrow: Statistics for Economics, Accounting and Business Studies, 6e
				Custom MyMathLab Global for UWA (2015)
				Introduction to mathematics
				Jacques, Mathematics for Economics and Business 9e
				Kemp, Royal Holloway Custom MyLab Math 2019, 1e
				Levine: Business Statistics: A First Course, 6e: International Edition
				Levine: Statistics for Managers: Using Microsoft Excel, 7e, Global Edition
				MML Global Test Book
				Mowbray: Mathematics for Physicists and Astronomers
				MyMathLab test book A2
				Newbold: Statistics for Business and Economics: Global Edition, 8e
				NextGen Media Math Test BookRH
				Roehampton University: Maths in the Life Sciences
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Swansea University: Wedlake Custom MLG 2018

			</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = '
				-- Choose Book --  
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Aston Uni Barrow Custom MyLab Math Global 2019
				Barrow: Statistics for Economics, Accounting and Business Studies, 6e
				Custom MyMathLab Global for UWA (2015)
				Introduction to mathematics
				Jacques, Mathematics for Economics and Business 9e
				Kemp, Royal Holloway Custom MyLab Math 2019, 1e
				Levine: Business Statistics: A First Course, 6e: International Edition
				Levine: Statistics for Managers: Using Microsoft Excel, 7e, Global Edition
				MML Global Test Book
				Mowbray: Mathematics for Physicists and Astronomers
				MyMathLab test book A2
				Newbold: Statistics for Business and Economics: Global Edition, 8e
				NextGen Media Math Test BookRH
				Roehampton University: Maths in the Life Sciences
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Swansea University: Wedlake Custom MLG 2018

			' or . = '
				-- Choose Book --  
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Aston Uni Barrow Custom MyLab Math Global 2019
				Barrow: Statistics for Economics, Accounting and Business Studies, 6e
				Custom MyMathLab Global for UWA (2015)
				Introduction to mathematics
				Jacques, Mathematics for Economics and Business 9e
				Kemp, Royal Holloway Custom MyLab Math 2019, 1e
				Levine: Business Statistics: A First Course, 6e: International Edition
				Levine: Statistics for Managers: Using Microsoft Excel, 7e, Global Edition
				MML Global Test Book
				Mowbray: Mathematics for Physicists and Astronomers
				MyMathLab test book A2
				Newbold: Statistics for Business and Economics: Global Edition, 8e
				NextGen Media Math Test BookRH
				Roehampton University: Maths in the Life Sciences
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Swansea University: Wedlake Custom MLG 2018

			')]</value>
   </webElementXpaths>
</WebElementEntity>
