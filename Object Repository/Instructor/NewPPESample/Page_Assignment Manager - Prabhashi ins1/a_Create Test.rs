<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create Test</name>
   <tag></tag>
   <elementGuidId>4d238d16-f98b-4cdc-9e9b-bb6b98ab20cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Toolbar_collapse_hwAndTestManagerToolbar']/ul/li/div/div[2]/ul/li[3]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4314dad2-c151-4c9c-81bb-fe549d2780bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:GoCreate('test')</value>
      <webElementGuid>f08da208-5d45-4040-bebd-5acc7e0ed635</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create Test</value>
      <webElementGuid>54371437-0193-457a-aa2c-f8d95ecd8108</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_hwAndTestManagerToolbar&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-dropdown&quot;]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[3]/a[1]</value>
      <webElementGuid>4a182787-3487-44ab-94fd-5131a88c0899</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_hwAndTestManagerToolbar']/ul/li/div/div[2]/ul/li[3]/a</value>
      <webElementGuid>abc4168c-a4dc-4827-bb7a-87d2d4a8383a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create Test')]</value>
      <webElementGuid>7d8e8043-3c59-4e8c-b0a2-58d3b409da34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Quiz'])[1]/following::a[1]</value>
      <webElementGuid>efc7da86-3f68-49b0-9c94-ae533e1ac9c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Homework'])[1]/following::a[2]</value>
      <webElementGuid>3c0ceae3-b824-444b-9265-6850c1190848</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Offline Item'])[1]/preceding::a[1]</value>
      <webElementGuid>9dbadc2a-1335-4502-ac94-48e3c488ccb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Writing Assignment'])[1]/preceding::a[2]</value>
      <webElementGuid>a7bebc22-7f70-40ff-aab9-b741a3114913</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create Test']/parent::*</value>
      <webElementGuid>2c2c51e1-1104-42a7-a535-a4830a9982cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:GoCreate('test')&quot;)]</value>
      <webElementGuid>b8053e9d-649d-40e5-a623-bc88d7e8032f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[3]/a</value>
      <webElementGuid>5545b830-d829-43e1-a00f-b27f8825d7d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:GoCreate(&quot; , &quot;'&quot; , &quot;test&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Create Test' or . = 'Create Test')]</value>
      <webElementGuid>31e29878-00a0-4c81-b01c-44c8e7529565</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
