<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Course name_ctl00ctl00InsideFormMaste_92e586</name>
   <tag></tag>
   <elementGuidId>e922fecf-15b5-4aae-af40-0e367808e707</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_txtCourseName']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_txtCourseName</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7b1c0bf0-7ef4-4790-afa5-8508986310ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$txtCourseName</value>
      <webElementGuid>81b3492e-4bde-4b3e-839d-6a24f1bc9648</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>18adbf69-6dcb-4920-8789-da999ab00f2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>80</value>
      <webElementGuid>a6c36aff-9a3d-4315-83a1-003af9f94fd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_txtCourseName</value>
      <webElementGuid>fd7e1f92-7666-4f5c-974d-f31c00e94521</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>CheckStep1();</value>
      <webElementGuid>04f558ce-6036-4d38-aa56-8a7cc33f861c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>d93473ee-751b-40fa-9000-91389fc9299b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_txtCourseName&quot;)</value>
      <webElementGuid>fde9245a-9349-47a6-bbb9-53eaf29b5eb9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_txtCourseName']</value>
      <webElementGuid>f769a100-76c9-40eb-91bd-a820873dda3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_DivContainer']/table/tbody/tr[2]/td[2]/input</value>
      <webElementGuid>0fbdb8d2-993b-49c2-be38-32595208e001</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/input</value>
      <webElementGuid>8f3c6116-2d4f-48be-8b07-8ebc489d23f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$ctl00$InsideForm$MasterContent$txtCourseName' and @type = 'text' and @id = 'ctl00_ctl00_InsideForm_MasterContent_txtCourseName']</value>
      <webElementGuid>f216eb6d-1d3f-4e2e-99db-78ab4fd1c257</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
