<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --  (Virtual School e_9467d7</name>
   <tag></tag>
   <elementGuidId>0667b211-2901-4042-90cb-0b55de7719d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>4585bc57-d310-48d9-9a59-15c02458e35a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
      <webElementGuid>fcb49e53-4ab4-4482-9c66-c96c0232c3ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      <webElementGuid>32d6501f-d32d-4e43-a989-9f22058369c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
      <webElementGuid>9204af1e-edd2-4fd3-83e4-16b71fd52448</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>e399159e-564b-4a15-a08a-33377988c627</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				  -- Choose Book --  
				(Virtual School exercises)
				* ADP: Algebra II Online Course
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Algebra 2 Common Core (2015)
				*CSU: ELM Exam Prep Course
				*Geometry Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Geometry
				AaSam Site Builder 10
				ACE Load Testbook
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Adams: Calculus, 6e
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 7e
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Agresti: Statistics: The Art and Science of Learning from Data, 3e
				Akst: Basic College Mathematics through Applications, 5e
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Developmental Mathematics through Applications, 1e
				Akst: Intermediate Algebra Through Applications, 3e
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 2e
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 3e
				Akst: Introductory Algebra Through Applications, 3e
				Alec's Delta Book
				Algebra Review for Calculus
				Alicia's SBNet feature testing book
				Alicia's smoke test 2
				All Wizard Book
				Almy: Math Lit: A Pathway to College Mathematics
				American River College: Math 41, 42, 131, 132, &amp; 1331
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, 9e
				Angel: A Survey of Mathematics with Applications, Expanded 8e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 4e MEDIA UPDATE
				Angel: Elementary Algebra for College Students, 8e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Elementary and Intermediate Algebra for College Students, 4e
				Angel: Intermediate Algebra for College Students, 7e
				Angel: Intermediate Algebra for College Students, 8e 
				Angel: Intermediate Algebra for College Students, 9e
				Aron: Statistics for Psychology, 6e
				Aron: Statistics for The Behavioral &amp; Social Sciences: A Brief Course, 5e
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13e
				Barnett: Calculus for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics for Business, Economics, Life and Social Sci, 13e
				Barnett: College Mathematics for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics, 11e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 13e
				Barnett: Finite Mathematics, 11e
				Barnett: Finite Mathematics, 12e
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Bass: Math Study Skills, 2e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 4e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: Algebra and Trigonometry, 4e
				Beecher: College Algebra, 3e
				Beecher: College Algebra, 4e
				Beecher: Precalculus, 3e
				Beecher: Precalculus, 4e
				Bennett and Blitzer Sample Application Demos
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 3e
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Using and Understanding Mathematics, 4e
				Bennett: Using and Understanding Mathematics, 5e
				Bennett: Using and Understanding Mathematics: A Quantitative Reasoning Appr, 6e
				Berenson: Basic Business Statistics 2e, Australian Edition
				Berenson: Basic Business Statistics, 11e
				Berenson: Basic Business Statistics, 12e
				Berenson: Basic Business Statistics, 13e
				Berenson: Basic Business Statistics, Australian Edition
				Berenson: Business Statistics, Australian Edition
				Betsy Smoketest 11-19-09
				Betsy Smoketest 3-3-10
				BI Norway: MET3431 Statistics
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 10
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 11e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 5e
				Bittinger: Algebra Foundations, 1e
				Bittinger: Basic College Mathematics, 12e
				Bittinger: Basic Mathematics with Early Integers, 2e
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 11e
				Bittinger: Basic Mathematics, 7th Edition
				Bittinger: Calculus and Its Applications Expanded Version
				Bittinger: Calculus and Its Applications, 10e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: College Algebra: Graphs &amp; Models, 5e
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Developmental Mathematics, 8e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 3e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 4e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 6e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, Concepts and Applications, 8e
				Bittinger: Elementary Algebra, Concepts and Applications, 9e
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Fundamental College Mathematics, 5e
				Bittinger: Intermediate Algebra, 10e
				Bittinger: Intermediate Algebra, 11e
				Bittinger: Intermediate Algebra, 12e
				Bittinger: Intermediate Algebra, Concepts and Applications, 8e
				Bittinger: Intermediate Algebra, Concepts and Applications, 9e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 4e
				Bittinger: Introductory &amp; Intermediate Algebra, 5e
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 11e
				Bittinger: Introductory Algebra, 12e
				Bittinger: Introductory and Intermediate Algebra, 4e
				Bittinger: Prealgebra &amp; Introductory Algebra, 2e
				Bittinger: Prealgebra &amp; Introductory Algebra, 3e
				Bittinger: Prealgebra, 6e
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Bittinger: Precalculus: Graphs &amp; Models, 5e
				Blair/Tobey: Prealgebra, 4e
				Blair: Prealgebra, 5e 
				Blitzer: Algebra &amp; Trigonometry, 3e
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach, 2e
				Blitzer: Algebra for College Students, 6e
				Blitzer: Algebra for College Students, 7e
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra: An Early Functions Approach, 2e
				Blitzer: College Algebra: An Early Functions Approach, 3e
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Intermediate Algebra for College Students, 6e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 5e
				Blitzer: Introductory Algebra for College Students, 6e
				Blitzer: Math For Your World, 1e
				Blitzer: Precalculus Essentials, 3e
				Blitzer: Precalculus Essentials, 4e
				Blitzer: Precalculus, 3e
				Blitzer: Precalculus, 4e
				Blitzer: Precalculus, 5e
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 1e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bock: Stats In Your World, 1e
				Bock: Stats: Modeling the World, 3e
				Bock: Stats: Modeling the World, 4e
				Borough of Manhattan CC: Introductory Algebra with Arithmetic Review
				Borough of Manhattan CC: MAT 008: Basic Math
				Borough of Manhattan CC: MAT 012: Basic Arithmetic and Algebra
				Briggs/Cochran: Calculus
				Briggs/Cochran: Calculus Early Transcendentals
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus for Scientists and Engineers, 1e
				Briggs/Cochran: Calculus, 2e
				Briggs: AP Calculus
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Brookdale Community College: Math 145
				California Algebra Readiness
				Carman: Mathematics for the Trades, 9e
				Carson: Elementary Algebra, 3e
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 3e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 3e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				CFO Online Finance Course. CHAPTER DEMO
				Clark: Applied Basic Mathematics, 2e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 8e
				Cleaves: Business Math, 9e
				Cleaves: College Mathematics 2009 Update
				Cleaves: College Mathematics, 9e
				Clendenen: Business Mathematics, 12e
				Clendenen: Business Mathematics, 13e
				College of Southern Nevada: Math 96: Intermediate Algebra
				Collins/Nunley: Navigating Through Mathematics, 1e DEMO chapter 7
				Conceptual Question Library for Business Statistics (Online Only)
				Conceptual Question Library for Statistics (Online Only)
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math In Action: Intro to Algebraic, Graph, and Num Prob Solv, 3/e
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 4e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 4e
				Consortium: MIA: Prealgebra Problem Solving, 3e
				Contemporary Business Mathematics (BMAT 110)
				Croft, Foundation Maths, 5e EMA
				Croft, Mathematics for Engineers, 3e EMA
				Croft, Mathematics for Engineers, 3e EMA
				Croft: Foundation Maths, 4e
				Cuyahoga Community College (Tri-C): Math 091/095/096
				De Veaux: Intro Stats, 3e
				De Veaux: Intro Stats, 3e Technology Update
				De Veaux: Intro Stats, 4e
				De Veaux: Stats: Data &amp; Models, 3e
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce DEMO
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 8e
				Donnelly: Business Statistics, 1e
				Donnelly: Business Statistics, 2e
				Dugopolski: College Algebra &amp; Trigonometry, 5e
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 5e
				Dugopolski: College Algebra, 6e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus, 4e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 3e
				Dugopolski: Precalculus: Functions and Graphs, 4e
				Dugopolski: Trigonometry, 3e
				Dugopolski: Trigonometry, 4e
				Dutchess Community College: Intermediate Algebra
				EAS Mathematics
				ECPI MTH099: Beginning Algebra
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics (2009)
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				eT1 Math Test Book - PPE
				eT2 Staging - XL Load
				eText Integration Intermediate Alg Test Book
				eText Integration Trigsted College Alg Test Book
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				Finney: Calculus (Virtual School exercises)
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e Media Update
				Finney: Calculus: Graphical, Numerical, Algebraic, 4e
				Florida International University: Finite Math 1060
				Florida International University: Finite Math 1106 (2009 Update)
				Florida State College at Jacksonville: MAC 1105: College Algebra 2012
				Florida State College at Jacksonville: MAT 0018 Basic Mathematics
				Florida State College at Jacksonville: MAT 0024 - Elementary Algebra
				Florida State College at Jacksonville: MAT 1033 - Intermediate Algebra
				Florida State College at Jacksonville: MGF 1106/1107 Topics in College Math
				Florida State College at Jacksonville: STA 2023 - Elementary Statistics
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada HED) 
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Geometry YTIs (Virtual Schools exercises)
				Georgia Math IV  (Demana and Agresti)
				Georgia Math IV  (Sullivan and Agresti)
				Goetz: Basic Mathematics
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 12e
				Goldstein: Calculus and Its Applications, 12e
				Goldstein: Calculus and Its Applications, 13e
				Goldstein: Finite Mathematics and Its Applications, 10e
				Goldstein: Finite Mathematics and Its Applications, 11e
				Goshaw: Concepts of Calculus with Applications
				Gould: Essential Statistics: Exploring the World Through Data
				Gould: Introductory Statistics: Exploring the World through Data, 1e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Grimaldo: Prealgebra &amp; Introductory Algebra, 1e
				Groebner: Business Statistics: A Decision Making Approach, 8e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				GTCC Demo 2012
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler: Introductory Mathematical Analysis, 13e
				Hagerstown Community College: Math 98/99/100
				Harshbarger: College Algebra In Context, 3e
				Harshbarger: College Algebra in Context, 4e
				Hass: University Calculus
				Hass: University Calculus Alternate Edition
				Hass: University Calculus Elements
				Hass: University Calculus with Early Transcendentals, 2e
				Henslin: Sociology
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 5e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 4e
				Hornsby: A Graphical Approach to College Algebra, 5e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits: A Unit Circle App, 5e
				Hornsby: A Graphical Approach to Precalculus, 4e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e DEMO 
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Jacques: Mathematics for Economics and Business, 6e
				Jamestown Community College: MAT 005/006: Introductory and Intermediate Algebra
				Jamestown Community College: MAT 1590/1600
				Janus Math NextGen2
				Janus Suja A 12
				Janus XL Load Testbook
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 5e
				Kaplan Math 103: College Mathematics
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math test book
				Knewton Math test book 2
				Knewton tiny test book
				KU122: Introduction to Math Skills and Strategies
				Larson: Elementary Statistics: Picturing the World, 4e
				Larson: Elementary Statistics: Picturing the World, 5e
				Larson: Elementary Statistics: Picturing the World, 6e
				Lay: Linear Algebra and Its Applications 4e Custom Version
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 3e Update
				Lay: Linear Algebra and its applications, 4e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 1e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 4e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions and Authentic Applications, 3e
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Levine: Business Statistics: A First Course, 5e
				Levine: Business Statistics: A First Course, 6e
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Statistics for Managers Using Microsoft Excel, 7e
				Levine: Statistics for Managers Using MS Excel, 6e
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial Finite Mathematics, 10e
				Lial/Hungerford: Finite Mathematics with Applications, 11e
				Lial/Hungerford: Mathematics with Applications, 11e
				Lial: Algebra and Trigonometry for College Readiness
				Lial: Algebra for College Students, 7e
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning &amp; Intermediate Algebra, 5e
				Lial: Beginning Algebra, 10e
				Lial: Beginning Algebra, 11e
				Lial: Calculus with Applications, 10e
				Lial: Calculus with Applications, Brief Version 9e
				Lial: Calculus with Applications, Brief Version, 10e
				Lial: College Algebra and Trigonometry, 4e
				Lial: College Algebra and Trigonometry, 5e
				Lial: College Algebra, 10e
				Lial: College Algebra, 10e for CLEP
				Lial: College Algebra, 11e
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics: Basic Mathematics and Algebra
				Lial: Essential Mathematics, 3e
				Lial: Essential Mathematics, 4e
				Lial: Essentials of College Algebra, 10e
				Lial: Essentials of College Algebra, 11e
				Lial: Finite Mathematics &amp; Calculus with Applications, 8e
				Lial: Finite Mathematics &amp; Calculus with Applications, 9e
				Lial: Finite Mathematics with Applications, 10e
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 9e
				Lial: Introductory &amp; Intermediate Algebra, 4e
				Lial: Introductory &amp; Intermediate Algebra, 5e
				Lial: Introductory Algebra, 10e
				Lial: Introductory Algebra, 8e
				Lial: Introductory Algebra, 9e
				Lial: Mathematics with Applications, 10e
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra and Introductory Algebra, 2e
				Lial: Prealgebra, 4e
				Lial: Prealgebra, 5e
				Lial: Precalculus, 4e
				Lial: Precalculus, 5e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 9e
				Lockdown Browser test PPE
				Long: Mathematical Reasoning for Elementary Teachers, 6e
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Madison College: ABE Dev Math Series
				Marecek: Strategies For Success: Study Skills for the College Math Student, 2e
				Maricopa: Custom Module Demo
				Martin-Gay: Algebra 1, MyLab Homeschool Edition
				Martin-Gay: Algebra 2, MyLab Homeschool Edition
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra: A Combined Approach, 4e
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics with Early Integers, 2e
				Martin-Gay: Basic College Mathematics, 3e
				Martin-Gay: Basic College Mathematics, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 3e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 4e
				Martin-Gay: Intermediate Algebra: Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra and Introductory Algebra
				Martin-Gay: Prealgebra, 5e
				Martin-Gay: Prealgebra, 6e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 7e (HTML5 Preview Version)
				MassBay Community College: MA090/095/098
				MasteringX 
				McClave: A First Course in Statistics, 11e
				McClave: Statistics for Business and Economics, 11e
				McClave: Statistics for Business and Economics, 12e
				McClave: Statistics, 12e
				McKenna/Kirk: Beginning &amp; Intermediate Algebra, 1e
				Miami Dade College: MAT 0022C: Developmental Mathematics Combined
				Miller: Business Mathematics, 11e
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 12e
				MML-CoCo: MyBusinessMathCourse (Cleaves 9e) 
				Montgomery County Community College: MAT10: Concepts of Numbers
				MXL Player Content Test Book
				MXL Player Content Test Book
				MyFoundationsLab Prototype
				MyMathLab for Basic Mathematics and Algebra
				MyMathLab Load Test Book
				MyMathLab test book A3
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate 9e with Trig 7e
				National University: Statistics and Data Analysis for Nursing Research
				NES Prep, Elementary Education II: Math
				New Additional Conceptual Exercises (NC Redesign)
				New Book Test 6
				Newbold: Statistics for Business and Economics, 7e
				Newbold: Statistics for Business and Economics, 8e
				Newbold: Statistics for Business and Economics: Global Edition, 7e
				NextGen Media Math PPE testbookDeletion
				NextGen Media Math Test Book-- Early Alerts
				NextGen Media Math Test Book--EA Auto
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL-Thilini
				NextGen Media Math Test Book--REAL_26rollback
				NextGen Media Math Test BookRH
				Norman/Wolczuk: Introduction to Linear Algebra for Science - Delete
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				North Carolina: Developmental Math Redesign Modules (Akst/Bragg)
				O'Daffer: Mathematics for Elementary Teachers, 4e
				Olivier: Business Mathematics Interactive
				Orientation Questions for Students
				Palm Beach State College: MAT 0018: Prealgebra
				Palm Beach State College: MAT 0028: Introductory Algebra
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Phil's Test Book
				Piedmont Technical College: MAT 122: Finite College Math (2011)
				Pirnot: Mathematics All Around, 4e
				Pirnot: Mathematics All Around, 5e
				Praxis 1 Math
				Prealgebra YTIs (Virtual School exercises)
				Precalculus YTIs (Virtual Schools exercises)
				Prince George Community College: Math 0104: Intermediate Algebra
				Prior: Basic Mathematics, 1e
				Prior: Prealgebra, 1e
				Raritan Valley CC: MA 015/020: Developmental Mathematics
				Ratti: College Algebra
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra and Trigonometry, 2e
				Ratti: College Algebra, 2e
				Ratti: College Algebra, 3e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 2e
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach
				Ratti: Precalculus: A Unit Circle Approach, 2e
				Ratti: Trigonometry 
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 5e
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 4e
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning Algebra with Apps and Visualization, 2e
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: Beginning and Intermediate Algebra, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 4e
				Rockswold: College Algebra, 5e
				Rockswold: Developmental Math, 1e
				Rockswold: Essentials of College Algebra, 4e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 3e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 4e
				Rockswold: Prealgebra, 1e 
				Rockswold: Precalculus with Modeling and Visualization, 4e
				Rockswold: Precalculus, 5e
				Salzman: Mathematics for Business, 10e
				Salzman: Mathematics for Business, 8e
				Salzman: Mathematics for Business, 9e
				Saunders: Mathematics for the Trades - A Guided Approach, 10e
				Schulz, Precalculus
				Schulz: Precalculus Demo (2014)
				Sharpe: Business Statistics
				Sharpe: Business Statistics, 2e
				Sharpe: Business Statistics, 3e
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course
				Sharpe: Business Statistics: A First Course, 2e
				SI Notation testing (from sbtest)
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Developmental Math
				StatCrunch Test Book for PPE
				Statistics YTIs (Virtual School exercises)
				Stine: Statistics for Business: Decision Making and Analysis
				Stine: Statistics for Business: Decision Making and Analysis, 2e
				Stony Brook University: AMS 102: Elements of Statistics 2e
				Suffolk County Community College, Grant Campus: MAT 006/007 (2012)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data,1e copy
				Sullivan: Algebra &amp; Trigonometry Enhanced w/ Graphing Utilities, 6e
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 5e
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 6e
				Sullivan: College Algebra, 8e
				Sullivan: College Algebra, 9e
				Sullivan: College Algebra: Concepts through Functions
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: College Algebra: Concepts Through Functions 3e
				Sullivan: Developmental Math, 1e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 2e
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary Algebra, 2e
				Sullivan: Elementary Algebra, 3e
				Sullivan: Fundamentals of Statistics, 3e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 4e copy
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Intermediate Algebra, 3e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus w/ Graphing Utilities, 4e
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus, 9e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach 2e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach 1e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach 2e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle
				Sullivan: Statistics: Informed Decisions Using Data, 3e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Trigonometry: A Right Triangle Approach, 5e
				Sullivan: Trigonometry: A Unit Circle Approach, 9e
				Tannenbaum: Excursions in Modern Mathematics, 7e
				Tannenbaum: Excursions in Modern Mathematics, 8e
				Texas State: Math 1314 Course Redesign
				Thomas' Calc demo
				Thomas' Calculus 11e DEMO RETIRED
				Thomas' Calculus Early Transcendentals Media Upgrade, 11e
				Thomas' Calculus Early Transcendentals, 12e
				Thomas' Calculus Global Edition, 12e
				Thomas' Calculus Media Upgrade, 11e
				Thomas' Calculus, 12e
				Thomas' Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Tidewater Community College:  Math Essentials MTE 1-9
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 6e
				Tobey: Basic College Mathematics, 7e
				Tobey: Beginning Algebra, 6e
				Tobey: Beginning Algebra, 7e
				Tobey: Beginning Algebra, 8e
				Tobey: Beginning Algebra: Early Graphing, 2e
				Tobey: Beginning Algebra: Early Graphing, 3e
				Tobey: Beginning and Intermediate Algebra, 3e
				Tobey: Beginning and Intermediate Algebra, 4e
				Tobey: Intermediate Algebra, 6e
				Tobey: Intermediate Algebra, 7e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Elementary Statistics Using Excel, 4e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 3e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics, 11e
				Triola: Elementary Statistics, 11e Technology Update
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 3ce DEMO
				Triola: Elementary Statistics, California Edition
				Triola: Elementary Statistics, Second California Edition
				Triola: Essentials of Statistics, 4e
				Triola: Essentials of Statistics, 5e
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				Universal Course: Algebra Readiness
				Universal Course: Applied Calculus
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Alberta: Calculus
				University of Florida: MAC1105: College Algebra
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				University of Louisville: ENG 101: Engineering Analysis
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: MAT106: Finite Mathematics
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: STAT 200: Elementary Statistics
				University of Maryland University College: STAT 230: Stats for Business &amp; Econ
				University of Massachusetts, Amherst: Voyaging Through Precalculus, Second Editi
				University of Phoenix: QRB/501 Quantitative Reasoning for Bus V3 2011
				University of Phoenix: QRB/501 Quantitative Reasoning for Business V4 2012
				University of Victoria: MATH 100/101/200
				Varberg: Calculus Early Transcendentals
				Varberg: Calculus, 9e
				Washington: Basic Tech Math w Calculus SI CANADA (Update)  Ready To Go
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math with Calculus, 10e
				Washington: Basic Technical Math, 10e
				Washington: Basic Technical Math, 11e
				Washington: Basic Technical Mathematics with Calculus, 9e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Watson, Corporate Finance Principles and Practice, 5e EMA
				Weiss: Elementary Statistics, 8e
				Weiss: Introductory Statistics, 9e
				Woodbury: Elementary &amp; Intermediate Algebra, 3e
				Woodbury: Elementary Algebra
				Woodbury: Intermediate Algebra
				XL Content Options Showcase
				XL Load Testbook
				z MathXL Freelancer Test Book
				Zuro: Discovering Mathematics, 1e DEMO

			</value>
      <webElementGuid>438cdcb9-2885-442f-9ea7-357acbe1562c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
      <webElementGuid>f17521fb-6eb5-4208-8ee2-6f1f3523647a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      <webElementGuid>433c7595-d3b0-48c1-8e1b-f8685da76e6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
      <webElementGuid>f06c47a5-3574-4e6a-8f8b-a38037719512</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
      <webElementGuid>b7cf39a9-a150-44cf-bdaa-a970f34eb994</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
      <webElementGuid>d34e9b53-a5dd-4a40-a89d-29acf944b58d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>f38a0abf-854f-49a6-92da-38ee82f7d15f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = concat(&quot;
				  -- Choose Book --  
				(Virtual School exercises)
				* ADP: Algebra II Online Course
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Algebra 2 Common Core (2015)
				*CSU: ELM Exam Prep Course
				*Geometry Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Geometry
				AaSam Site Builder 10
				ACE Load Testbook
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Adams: Calculus, 6e
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 7e
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Agresti: Statistics: The Art and Science of Learning from Data, 3e
				Akst: Basic College Mathematics through Applications, 5e
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Developmental Mathematics through Applications, 1e
				Akst: Intermediate Algebra Through Applications, 3e
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 2e
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 3e
				Akst: Introductory Algebra Through Applications, 3e
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book
				Algebra Review for Calculus
				Alicia&quot; , &quot;'&quot; , &quot;s SBNet feature testing book
				Alicia&quot; , &quot;'&quot; , &quot;s smoke test 2
				All Wizard Book
				Almy: Math Lit: A Pathway to College Mathematics
				American River College: Math 41, 42, 131, 132, &amp; 1331
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, 9e
				Angel: A Survey of Mathematics with Applications, Expanded 8e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 4e MEDIA UPDATE
				Angel: Elementary Algebra for College Students, 8e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Elementary and Intermediate Algebra for College Students, 4e
				Angel: Intermediate Algebra for College Students, 7e
				Angel: Intermediate Algebra for College Students, 8e 
				Angel: Intermediate Algebra for College Students, 9e
				Aron: Statistics for Psychology, 6e
				Aron: Statistics for The Behavioral &amp; Social Sciences: A Brief Course, 5e
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13e
				Barnett: Calculus for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics for Business, Economics, Life and Social Sci, 13e
				Barnett: College Mathematics for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics, 11e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 13e
				Barnett: Finite Mathematics, 11e
				Barnett: Finite Mathematics, 12e
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Bass: Math Study Skills, 2e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 4e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: Algebra and Trigonometry, 4e
				Beecher: College Algebra, 3e
				Beecher: College Algebra, 4e
				Beecher: Precalculus, 3e
				Beecher: Precalculus, 4e
				Bennett and Blitzer Sample Application Demos
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 3e
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Using and Understanding Mathematics, 4e
				Bennett: Using and Understanding Mathematics, 5e
				Bennett: Using and Understanding Mathematics: A Quantitative Reasoning Appr, 6e
				Berenson: Basic Business Statistics 2e, Australian Edition
				Berenson: Basic Business Statistics, 11e
				Berenson: Basic Business Statistics, 12e
				Berenson: Basic Business Statistics, 13e
				Berenson: Basic Business Statistics, Australian Edition
				Berenson: Business Statistics, Australian Edition
				Betsy Smoketest 11-19-09
				Betsy Smoketest 3-3-10
				BI Norway: MET3431 Statistics
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 10
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 11e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 5e
				Bittinger: Algebra Foundations, 1e
				Bittinger: Basic College Mathematics, 12e
				Bittinger: Basic Mathematics with Early Integers, 2e
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 11e
				Bittinger: Basic Mathematics, 7th Edition
				Bittinger: Calculus and Its Applications Expanded Version
				Bittinger: Calculus and Its Applications, 10e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: College Algebra: Graphs &amp; Models, 5e
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Developmental Mathematics, 8e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 3e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 4e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 6e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, Concepts and Applications, 8e
				Bittinger: Elementary Algebra, Concepts and Applications, 9e
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Fundamental College Mathematics, 5e
				Bittinger: Intermediate Algebra, 10e
				Bittinger: Intermediate Algebra, 11e
				Bittinger: Intermediate Algebra, 12e
				Bittinger: Intermediate Algebra, Concepts and Applications, 8e
				Bittinger: Intermediate Algebra, Concepts and Applications, 9e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 4e
				Bittinger: Introductory &amp; Intermediate Algebra, 5e
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 11e
				Bittinger: Introductory Algebra, 12e
				Bittinger: Introductory and Intermediate Algebra, 4e
				Bittinger: Prealgebra &amp; Introductory Algebra, 2e
				Bittinger: Prealgebra &amp; Introductory Algebra, 3e
				Bittinger: Prealgebra, 6e
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Bittinger: Precalculus: Graphs &amp; Models, 5e
				Blair/Tobey: Prealgebra, 4e
				Blair: Prealgebra, 5e 
				Blitzer: Algebra &amp; Trigonometry, 3e
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach, 2e
				Blitzer: Algebra for College Students, 6e
				Blitzer: Algebra for College Students, 7e
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra: An Early Functions Approach, 2e
				Blitzer: College Algebra: An Early Functions Approach, 3e
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Intermediate Algebra for College Students, 6e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 5e
				Blitzer: Introductory Algebra for College Students, 6e
				Blitzer: Math For Your World, 1e
				Blitzer: Precalculus Essentials, 3e
				Blitzer: Precalculus Essentials, 4e
				Blitzer: Precalculus, 3e
				Blitzer: Precalculus, 4e
				Blitzer: Precalculus, 5e
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 1e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bock: Stats In Your World, 1e
				Bock: Stats: Modeling the World, 3e
				Bock: Stats: Modeling the World, 4e
				Borough of Manhattan CC: Introductory Algebra with Arithmetic Review
				Borough of Manhattan CC: MAT 008: Basic Math
				Borough of Manhattan CC: MAT 012: Basic Arithmetic and Algebra
				Briggs/Cochran: Calculus
				Briggs/Cochran: Calculus Early Transcendentals
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus for Scientists and Engineers, 1e
				Briggs/Cochran: Calculus, 2e
				Briggs: AP Calculus
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Brookdale Community College: Math 145
				California Algebra Readiness
				Carman: Mathematics for the Trades, 9e
				Carson: Elementary Algebra, 3e
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 3e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 3e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				CFO Online Finance Course. CHAPTER DEMO
				Clark: Applied Basic Mathematics, 2e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 8e
				Cleaves: Business Math, 9e
				Cleaves: College Mathematics 2009 Update
				Cleaves: College Mathematics, 9e
				Clendenen: Business Mathematics, 12e
				Clendenen: Business Mathematics, 13e
				College of Southern Nevada: Math 96: Intermediate Algebra
				Collins/Nunley: Navigating Through Mathematics, 1e DEMO chapter 7
				Conceptual Question Library for Business Statistics (Online Only)
				Conceptual Question Library for Statistics (Online Only)
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math In Action: Intro to Algebraic, Graph, and Num Prob Solv, 3/e
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 4e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 4e
				Consortium: MIA: Prealgebra Problem Solving, 3e
				Contemporary Business Mathematics (BMAT 110)
				Croft, Foundation Maths, 5e EMA
				Croft, Mathematics for Engineers, 3e EMA
				Croft, Mathematics for Engineers, 3e EMA
				Croft: Foundation Maths, 4e
				Cuyahoga Community College (Tri-C): Math 091/095/096
				De Veaux: Intro Stats, 3e
				De Veaux: Intro Stats, 3e Technology Update
				De Veaux: Intro Stats, 4e
				De Veaux: Stats: Data &amp; Models, 3e
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce DEMO
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 8e
				Donnelly: Business Statistics, 1e
				Donnelly: Business Statistics, 2e
				Dugopolski: College Algebra &amp; Trigonometry, 5e
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 5e
				Dugopolski: College Algebra, 6e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus, 4e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 3e
				Dugopolski: Precalculus: Functions and Graphs, 4e
				Dugopolski: Trigonometry, 3e
				Dugopolski: Trigonometry, 4e
				Dutchess Community College: Intermediate Algebra
				EAS Mathematics
				ECPI MTH099: Beginning Algebra
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics (2009)
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				eT1 Math Test Book - PPE
				eT2 Staging - XL Load
				eText Integration Intermediate Alg Test Book
				eText Integration Trigsted College Alg Test Book
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				Finney: Calculus (Virtual School exercises)
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e Media Update
				Finney: Calculus: Graphical, Numerical, Algebraic, 4e
				Florida International University: Finite Math 1060
				Florida International University: Finite Math 1106 (2009 Update)
				Florida State College at Jacksonville: MAC 1105: College Algebra 2012
				Florida State College at Jacksonville: MAT 0018 Basic Mathematics
				Florida State College at Jacksonville: MAT 0024 - Elementary Algebra
				Florida State College at Jacksonville: MAT 1033 - Intermediate Algebra
				Florida State College at Jacksonville: MGF 1106/1107 Topics in College Math
				Florida State College at Jacksonville: STA 2023 - Elementary Statistics
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada HED) 
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Geometry YTIs (Virtual Schools exercises)
				Georgia Math IV  (Demana and Agresti)
				Georgia Math IV  (Sullivan and Agresti)
				Goetz: Basic Mathematics
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 12e
				Goldstein: Calculus and Its Applications, 12e
				Goldstein: Calculus and Its Applications, 13e
				Goldstein: Finite Mathematics and Its Applications, 10e
				Goldstein: Finite Mathematics and Its Applications, 11e
				Goshaw: Concepts of Calculus with Applications
				Gould: Essential Statistics: Exploring the World Through Data
				Gould: Introductory Statistics: Exploring the World through Data, 1e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Grimaldo: Prealgebra &amp; Introductory Algebra, 1e
				Groebner: Business Statistics: A Decision Making Approach, 8e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				GTCC Demo 2012
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler: Introductory Mathematical Analysis, 13e
				Hagerstown Community College: Math 98/99/100
				Harshbarger: College Algebra In Context, 3e
				Harshbarger: College Algebra in Context, 4e
				Hass: University Calculus
				Hass: University Calculus Alternate Edition
				Hass: University Calculus Elements
				Hass: University Calculus with Early Transcendentals, 2e
				Henslin: Sociology
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 5e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 4e
				Hornsby: A Graphical Approach to College Algebra, 5e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits: A Unit Circle App, 5e
				Hornsby: A Graphical Approach to Precalculus, 4e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e DEMO 
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Jacques: Mathematics for Economics and Business, 6e
				Jamestown Community College: MAT 005/006: Introductory and Intermediate Algebra
				Jamestown Community College: MAT 1590/1600
				Janus Math NextGen2
				Janus Suja A 12
				Janus XL Load Testbook
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 5e
				Kaplan Math 103: College Mathematics
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math test book
				Knewton Math test book 2
				Knewton tiny test book
				KU122: Introduction to Math Skills and Strategies
				Larson: Elementary Statistics: Picturing the World, 4e
				Larson: Elementary Statistics: Picturing the World, 5e
				Larson: Elementary Statistics: Picturing the World, 6e
				Lay: Linear Algebra and Its Applications 4e Custom Version
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 3e Update
				Lay: Linear Algebra and its applications, 4e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 1e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 4e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions and Authentic Applications, 3e
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Levine: Business Statistics: A First Course, 5e
				Levine: Business Statistics: A First Course, 6e
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Statistics for Managers Using Microsoft Excel, 7e
				Levine: Statistics for Managers Using MS Excel, 6e
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial Finite Mathematics, 10e
				Lial/Hungerford: Finite Mathematics with Applications, 11e
				Lial/Hungerford: Mathematics with Applications, 11e
				Lial: Algebra and Trigonometry for College Readiness
				Lial: Algebra for College Students, 7e
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning &amp; Intermediate Algebra, 5e
				Lial: Beginning Algebra, 10e
				Lial: Beginning Algebra, 11e
				Lial: Calculus with Applications, 10e
				Lial: Calculus with Applications, Brief Version 9e
				Lial: Calculus with Applications, Brief Version, 10e
				Lial: College Algebra and Trigonometry, 4e
				Lial: College Algebra and Trigonometry, 5e
				Lial: College Algebra, 10e
				Lial: College Algebra, 10e for CLEP
				Lial: College Algebra, 11e
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics: Basic Mathematics and Algebra
				Lial: Essential Mathematics, 3e
				Lial: Essential Mathematics, 4e
				Lial: Essentials of College Algebra, 10e
				Lial: Essentials of College Algebra, 11e
				Lial: Finite Mathematics &amp; Calculus with Applications, 8e
				Lial: Finite Mathematics &amp; Calculus with Applications, 9e
				Lial: Finite Mathematics with Applications, 10e
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 9e
				Lial: Introductory &amp; Intermediate Algebra, 4e
				Lial: Introductory &amp; Intermediate Algebra, 5e
				Lial: Introductory Algebra, 10e
				Lial: Introductory Algebra, 8e
				Lial: Introductory Algebra, 9e
				Lial: Mathematics with Applications, 10e
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra and Introductory Algebra, 2e
				Lial: Prealgebra, 4e
				Lial: Prealgebra, 5e
				Lial: Precalculus, 4e
				Lial: Precalculus, 5e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 9e
				Lockdown Browser test PPE
				Long: Mathematical Reasoning for Elementary Teachers, 6e
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Madison College: ABE Dev Math Series
				Marecek: Strategies For Success: Study Skills for the College Math Student, 2e
				Maricopa: Custom Module Demo
				Martin-Gay: Algebra 1, MyLab Homeschool Edition
				Martin-Gay: Algebra 2, MyLab Homeschool Edition
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra: A Combined Approach, 4e
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics with Early Integers, 2e
				Martin-Gay: Basic College Mathematics, 3e
				Martin-Gay: Basic College Mathematics, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 3e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 4e
				Martin-Gay: Intermediate Algebra: Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra and Introductory Algebra
				Martin-Gay: Prealgebra, 5e
				Martin-Gay: Prealgebra, 6e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 7e (HTML5 Preview Version)
				MassBay Community College: MA090/095/098
				MasteringX 
				McClave: A First Course in Statistics, 11e
				McClave: Statistics for Business and Economics, 11e
				McClave: Statistics for Business and Economics, 12e
				McClave: Statistics, 12e
				McKenna/Kirk: Beginning &amp; Intermediate Algebra, 1e
				Miami Dade College: MAT 0022C: Developmental Mathematics Combined
				Miller: Business Mathematics, 11e
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 12e
				MML-CoCo: MyBusinessMathCourse (Cleaves 9e) 
				Montgomery County Community College: MAT10: Concepts of Numbers
				MXL Player Content Test Book
				MXL Player Content Test Book
				MyFoundationsLab Prototype
				MyMathLab for Basic Mathematics and Algebra
				MyMathLab Load Test Book
				MyMathLab test book A3
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate 9e with Trig 7e
				National University: Statistics and Data Analysis for Nursing Research
				NES Prep, Elementary Education II: Math
				New Additional Conceptual Exercises (NC Redesign)
				New Book Test 6
				Newbold: Statistics for Business and Economics, 7e
				Newbold: Statistics for Business and Economics, 8e
				Newbold: Statistics for Business and Economics: Global Edition, 7e
				NextGen Media Math PPE testbookDeletion
				NextGen Media Math Test Book-- Early Alerts
				NextGen Media Math Test Book--EA Auto
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL-Thilini
				NextGen Media Math Test Book--REAL_26rollback
				NextGen Media Math Test BookRH
				Norman/Wolczuk: Introduction to Linear Algebra for Science - Delete
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				North Carolina: Developmental Math Redesign Modules (Akst/Bragg)
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 4e
				Olivier: Business Mathematics Interactive
				Orientation Questions for Students
				Palm Beach State College: MAT 0018: Prealgebra
				Palm Beach State College: MAT 0028: Introductory Algebra
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Phil&quot; , &quot;'&quot; , &quot;s Test Book
				Piedmont Technical College: MAT 122: Finite College Math (2011)
				Pirnot: Mathematics All Around, 4e
				Pirnot: Mathematics All Around, 5e
				Praxis 1 Math
				Prealgebra YTIs (Virtual School exercises)
				Precalculus YTIs (Virtual Schools exercises)
				Prince George Community College: Math 0104: Intermediate Algebra
				Prior: Basic Mathematics, 1e
				Prior: Prealgebra, 1e
				Raritan Valley CC: MA 015/020: Developmental Mathematics
				Ratti: College Algebra
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra and Trigonometry, 2e
				Ratti: College Algebra, 2e
				Ratti: College Algebra, 3e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 2e
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach
				Ratti: Precalculus: A Unit Circle Approach, 2e
				Ratti: Trigonometry 
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 5e
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 4e
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning Algebra with Apps and Visualization, 2e
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: Beginning and Intermediate Algebra, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 4e
				Rockswold: College Algebra, 5e
				Rockswold: Developmental Math, 1e
				Rockswold: Essentials of College Algebra, 4e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 3e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 4e
				Rockswold: Prealgebra, 1e 
				Rockswold: Precalculus with Modeling and Visualization, 4e
				Rockswold: Precalculus, 5e
				Salzman: Mathematics for Business, 10e
				Salzman: Mathematics for Business, 8e
				Salzman: Mathematics for Business, 9e
				Saunders: Mathematics for the Trades - A Guided Approach, 10e
				Schulz, Precalculus
				Schulz: Precalculus Demo (2014)
				Sharpe: Business Statistics
				Sharpe: Business Statistics, 2e
				Sharpe: Business Statistics, 3e
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course
				Sharpe: Business Statistics: A First Course, 2e
				SI Notation testing (from sbtest)
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Developmental Math
				StatCrunch Test Book for PPE
				Statistics YTIs (Virtual School exercises)
				Stine: Statistics for Business: Decision Making and Analysis
				Stine: Statistics for Business: Decision Making and Analysis, 2e
				Stony Brook University: AMS 102: Elements of Statistics 2e
				Suffolk County Community College, Grant Campus: MAT 006/007 (2012)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data,1e copy
				Sullivan: Algebra &amp; Trigonometry Enhanced w/ Graphing Utilities, 6e
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 5e
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 6e
				Sullivan: College Algebra, 8e
				Sullivan: College Algebra, 9e
				Sullivan: College Algebra: Concepts through Functions
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: College Algebra: Concepts Through Functions 3e
				Sullivan: Developmental Math, 1e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 2e
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary Algebra, 2e
				Sullivan: Elementary Algebra, 3e
				Sullivan: Fundamentals of Statistics, 3e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 4e copy
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Intermediate Algebra, 3e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus w/ Graphing Utilities, 4e
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus, 9e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach 2e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach 1e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach 2e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle
				Sullivan: Statistics: Informed Decisions Using Data, 3e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Trigonometry: A Right Triangle Approach, 5e
				Sullivan: Trigonometry: A Unit Circle Approach, 9e
				Tannenbaum: Excursions in Modern Mathematics, 7e
				Tannenbaum: Excursions in Modern Mathematics, 8e
				Texas State: Math 1314 Course Redesign
				Thomas&quot; , &quot;'&quot; , &quot; Calc demo
				Thomas&quot; , &quot;'&quot; , &quot; Calculus 11e DEMO RETIRED
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals, 12e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Global Edition, 12e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 12e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Tidewater Community College:  Math Essentials MTE 1-9
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 6e
				Tobey: Basic College Mathematics, 7e
				Tobey: Beginning Algebra, 6e
				Tobey: Beginning Algebra, 7e
				Tobey: Beginning Algebra, 8e
				Tobey: Beginning Algebra: Early Graphing, 2e
				Tobey: Beginning Algebra: Early Graphing, 3e
				Tobey: Beginning and Intermediate Algebra, 3e
				Tobey: Beginning and Intermediate Algebra, 4e
				Tobey: Intermediate Algebra, 6e
				Tobey: Intermediate Algebra, 7e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Elementary Statistics Using Excel, 4e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 3e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics, 11e
				Triola: Elementary Statistics, 11e Technology Update
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 3ce DEMO
				Triola: Elementary Statistics, California Edition
				Triola: Elementary Statistics, Second California Edition
				Triola: Essentials of Statistics, 4e
				Triola: Essentials of Statistics, 5e
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				Universal Course: Algebra Readiness
				Universal Course: Applied Calculus
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Alberta: Calculus
				University of Florida: MAC1105: College Algebra
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				University of Louisville: ENG 101: Engineering Analysis
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: MAT106: Finite Mathematics
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: STAT 200: Elementary Statistics
				University of Maryland University College: STAT 230: Stats for Business &amp; Econ
				University of Massachusetts, Amherst: Voyaging Through Precalculus, Second Editi
				University of Phoenix: QRB/501 Quantitative Reasoning for Bus V3 2011
				University of Phoenix: QRB/501 Quantitative Reasoning for Business V4 2012
				University of Victoria: MATH 100/101/200
				Varberg: Calculus Early Transcendentals
				Varberg: Calculus, 9e
				Washington: Basic Tech Math w Calculus SI CANADA (Update)  Ready To Go
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math with Calculus, 10e
				Washington: Basic Technical Math, 10e
				Washington: Basic Technical Math, 11e
				Washington: Basic Technical Mathematics with Calculus, 9e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Watson, Corporate Finance Principles and Practice, 5e EMA
				Weiss: Elementary Statistics, 8e
				Weiss: Introductory Statistics, 9e
				Woodbury: Elementary &amp; Intermediate Algebra, 3e
				Woodbury: Elementary Algebra
				Woodbury: Intermediate Algebra
				XL Content Options Showcase
				XL Load Testbook
				z MathXL Freelancer Test Book
				Zuro: Discovering Mathematics, 1e DEMO

			&quot;) or . = concat(&quot;
				  -- Choose Book --  
				(Virtual School exercises)
				* ADP: Algebra II Online Course
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Algebra 2 Common Core (2015)
				*CSU: ELM Exam Prep Course
				*Geometry Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Geometry
				AaSam Site Builder 10
				ACE Load Testbook
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Adams: Calculus, 6e
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 7e
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Agresti: Statistics: The Art and Science of Learning from Data, 3e
				Akst: Basic College Mathematics through Applications, 5e
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Developmental Mathematics through Applications, 1e
				Akst: Intermediate Algebra Through Applications, 3e
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 2e
				Akst: Introductory &amp; Intermediate Algebra Through Applications, 3e
				Akst: Introductory Algebra Through Applications, 3e
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book
				Algebra Review for Calculus
				Alicia&quot; , &quot;'&quot; , &quot;s SBNet feature testing book
				Alicia&quot; , &quot;'&quot; , &quot;s smoke test 2
				All Wizard Book
				Almy: Math Lit: A Pathway to College Mathematics
				American River College: Math 41, 42, 131, 132, &amp; 1331
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, 9e
				Angel: A Survey of Mathematics with Applications, Expanded 8e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 4e MEDIA UPDATE
				Angel: Elementary Algebra for College Students, 8e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Elementary and Intermediate Algebra for College Students, 4e
				Angel: Intermediate Algebra for College Students, 7e
				Angel: Intermediate Algebra for College Students, 8e 
				Angel: Intermediate Algebra for College Students, 9e
				Aron: Statistics for Psychology, 6e
				Aron: Statistics for The Behavioral &amp; Social Sciences: A Brief Course, 5e
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 13e
				Barnett: Calculus for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics for Business, Economics, Life and Social Sci, 13e
				Barnett: College Mathematics for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics, 11e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 13e
				Barnett: Finite Mathematics, 11e
				Barnett: Finite Mathematics, 12e
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Bass: Math Study Skills, 2e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 4e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: Algebra and Trigonometry, 4e
				Beecher: College Algebra, 3e
				Beecher: College Algebra, 4e
				Beecher: Precalculus, 3e
				Beecher: Precalculus, 4e
				Bennett and Blitzer Sample Application Demos
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 3e
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Using and Understanding Mathematics, 4e
				Bennett: Using and Understanding Mathematics, 5e
				Bennett: Using and Understanding Mathematics: A Quantitative Reasoning Appr, 6e
				Berenson: Basic Business Statistics 2e, Australian Edition
				Berenson: Basic Business Statistics, 11e
				Berenson: Basic Business Statistics, 12e
				Berenson: Basic Business Statistics, 13e
				Berenson: Basic Business Statistics, Australian Edition
				Berenson: Business Statistics, Australian Edition
				Betsy Smoketest 11-19-09
				Betsy Smoketest 3-3-10
				BI Norway: MET3431 Statistics
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 10
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 11e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 5e
				Bittinger: Algebra Foundations, 1e
				Bittinger: Basic College Mathematics, 12e
				Bittinger: Basic Mathematics with Early Integers, 2e
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 11e
				Bittinger: Basic Mathematics, 7th Edition
				Bittinger: Calculus and Its Applications Expanded Version
				Bittinger: Calculus and Its Applications, 10e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: College Algebra: Graphs &amp; Models, 5e
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Developmental Mathematics, 8e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 3e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 4e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 6e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, Concepts and Applications, 8e
				Bittinger: Elementary Algebra, Concepts and Applications, 9e
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Fundamental College Mathematics, 5e
				Bittinger: Intermediate Algebra, 10e
				Bittinger: Intermediate Algebra, 11e
				Bittinger: Intermediate Algebra, 12e
				Bittinger: Intermediate Algebra, Concepts and Applications, 8e
				Bittinger: Intermediate Algebra, Concepts and Applications, 9e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 4e
				Bittinger: Introductory &amp; Intermediate Algebra, 5e
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 11e
				Bittinger: Introductory Algebra, 12e
				Bittinger: Introductory and Intermediate Algebra, 4e
				Bittinger: Prealgebra &amp; Introductory Algebra, 2e
				Bittinger: Prealgebra &amp; Introductory Algebra, 3e
				Bittinger: Prealgebra, 6e
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Bittinger: Precalculus: Graphs &amp; Models, 5e
				Blair/Tobey: Prealgebra, 4e
				Blair: Prealgebra, 5e 
				Blitzer: Algebra &amp; Trigonometry, 3e
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach, 2e
				Blitzer: Algebra for College Students, 6e
				Blitzer: Algebra for College Students, 7e
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra: An Early Functions Approach, 2e
				Blitzer: College Algebra: An Early Functions Approach, 3e
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Intermediate Algebra for College Students, 6e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 5e
				Blitzer: Introductory Algebra for College Students, 6e
				Blitzer: Math For Your World, 1e
				Blitzer: Precalculus Essentials, 3e
				Blitzer: Precalculus Essentials, 4e
				Blitzer: Precalculus, 3e
				Blitzer: Precalculus, 4e
				Blitzer: Precalculus, 5e
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 1e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bock: Stats In Your World, 1e
				Bock: Stats: Modeling the World, 3e
				Bock: Stats: Modeling the World, 4e
				Borough of Manhattan CC: Introductory Algebra with Arithmetic Review
				Borough of Manhattan CC: MAT 008: Basic Math
				Borough of Manhattan CC: MAT 012: Basic Arithmetic and Algebra
				Briggs/Cochran: Calculus
				Briggs/Cochran: Calculus Early Transcendentals
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus for Scientists and Engineers, 1e
				Briggs/Cochran: Calculus, 2e
				Briggs: AP Calculus
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Brookdale Community College: Math 145
				California Algebra Readiness
				Carman: Mathematics for the Trades, 9e
				Carson: Elementary Algebra, 3e
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 3e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 3e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				CFO Online Finance Course. CHAPTER DEMO
				Clark: Applied Basic Mathematics, 2e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 8e
				Cleaves: Business Math, 9e
				Cleaves: College Mathematics 2009 Update
				Cleaves: College Mathematics, 9e
				Clendenen: Business Mathematics, 12e
				Clendenen: Business Mathematics, 13e
				College of Southern Nevada: Math 96: Intermediate Algebra
				Collins/Nunley: Navigating Through Mathematics, 1e DEMO chapter 7
				Conceptual Question Library for Business Statistics (Online Only)
				Conceptual Question Library for Statistics (Online Only)
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math In Action: Intro to Algebraic, Graph, and Num Prob Solv, 3/e
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 4e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 4e
				Consortium: MIA: Prealgebra Problem Solving, 3e
				Contemporary Business Mathematics (BMAT 110)
				Croft, Foundation Maths, 5e EMA
				Croft, Mathematics for Engineers, 3e EMA
				Croft, Mathematics for Engineers, 3e EMA
				Croft: Foundation Maths, 4e
				Cuyahoga Community College (Tri-C): Math 091/095/096
				De Veaux: Intro Stats, 3e
				De Veaux: Intro Stats, 3e Technology Update
				De Veaux: Intro Stats, 4e
				De Veaux: Stats: Data &amp; Models, 3e
				De Veaux: Stats: Data and Models, 1ce
				De Veaux: Stats: Data and Models, 1ce DEMO
				Delgaty - Stepping It Up DEMO
				Delgaty: Stepping it Up: Foundations for Success in Math, 1ce
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 8e
				Donnelly: Business Statistics, 1e
				Donnelly: Business Statistics, 2e
				Dugopolski: College Algebra &amp; Trigonometry, 5e
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 5e
				Dugopolski: College Algebra, 6e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus, 4e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 3e
				Dugopolski: Precalculus: Functions and Graphs, 4e
				Dugopolski: Trigonometry, 3e
				Dugopolski: Trigonometry, 4e
				Dutchess Community College: Intermediate Algebra
				EAS Mathematics
				ECPI MTH099: Beginning Algebra
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics (2009)
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				eT1 Math Test Book - PPE
				eT2 Staging - XL Load
				eText Integration Intermediate Alg Test Book
				eText Integration Trigsted College Alg Test Book
				Fanshawe College: Hummelbrunner, Contemporary Business Math, 8e Update
				Finney: Calculus (Virtual School exercises)
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e Media Update
				Finney: Calculus: Graphical, Numerical, Algebraic, 4e
				Florida International University: Finite Math 1060
				Florida International University: Finite Math 1106 (2009 Update)
				Florida State College at Jacksonville: MAC 1105: College Algebra 2012
				Florida State College at Jacksonville: MAT 0018 Basic Mathematics
				Florida State College at Jacksonville: MAT 0024 - Elementary Algebra
				Florida State College at Jacksonville: MAT 1033 - Intermediate Algebra
				Florida State College at Jacksonville: MGF 1106/1107 Topics in College Math
				Florida State College at Jacksonville: STA 2023 - Elementary Statistics
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada HED) 
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Geometry YTIs (Virtual Schools exercises)
				Georgia Math IV  (Demana and Agresti)
				Georgia Math IV  (Sullivan and Agresti)
				Goetz: Basic Mathematics
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 12e
				Goldstein: Calculus and Its Applications, 12e
				Goldstein: Calculus and Its Applications, 13e
				Goldstein: Finite Mathematics and Its Applications, 10e
				Goldstein: Finite Mathematics and Its Applications, 11e
				Goshaw: Concepts of Calculus with Applications
				Gould: Essential Statistics: Exploring the World Through Data
				Gould: Introductory Statistics: Exploring the World through Data, 1e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Grimaldo: Prealgebra &amp; Introductory Algebra, 1e
				Groebner: Business Statistics: A Decision Making Approach, 8e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				GTCC Demo 2012
				Guilford Technical CC: Eight Modules Correlated with the NC State Standards
				Haeussler: Introductory Mathematical Analysis, 13e
				Hagerstown Community College: Math 98/99/100
				Harshbarger: College Algebra In Context, 3e
				Harshbarger: College Algebra in Context, 4e
				Hass: University Calculus
				Hass: University Calculus Alternate Edition
				Hass: University Calculus Elements
				Hass: University Calculus with Early Transcendentals, 2e
				Henslin: Sociology
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 5e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 4e
				Hornsby: A Graphical Approach to College Algebra, 5e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits: A Unit Circle App, 5e
				Hornsby: A Graphical Approach to Precalculus, 4e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 9e DEMO 
				Hummelbrunner: Contemporary Business Mathematics with Canadian Applications, 10e
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Jacques: Mathematics for Economics and Business, 6e
				Jamestown Community College: MAT 005/006: Introductory and Intermediate Algebra
				Jamestown Community College: MAT 1590/1600
				Janus Math NextGen2
				Janus Suja A 12
				Janus XL Load Testbook
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 5e
				Kaplan Math 103: College Mathematics
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math test book
				Knewton Math test book 2
				Knewton tiny test book
				KU122: Introduction to Math Skills and Strategies
				Larson: Elementary Statistics: Picturing the World, 4e
				Larson: Elementary Statistics: Picturing the World, 5e
				Larson: Elementary Statistics: Picturing the World, 6e
				Lay: Linear Algebra and Its Applications 4e Custom Version
				Lay: Linear Algebra and Its Applications, 3e
				Lay: Linear Algebra and Its Applications, 3e Update
				Lay: Linear Algebra and its applications, 4e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 1e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 4e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions and Authentic Applications, 3e
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Levine: Business Statistics: A First Course, 5e
				Levine: Business Statistics: A First Course, 6e
				Levine: Business Statistics: A First Course, 7/e, Global Edition 
				Levine: Statistics for Managers Using Microsoft Excel, 7e
				Levine: Statistics for Managers Using MS Excel, 6e
				Levine: Statistics for Managers Using MS Excel: Global Edition, 6e
				Lial Finite Mathematics, 10e
				Lial/Hungerford: Finite Mathematics with Applications, 11e
				Lial/Hungerford: Mathematics with Applications, 11e
				Lial: Algebra and Trigonometry for College Readiness
				Lial: Algebra for College Students, 7e
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning &amp; Intermediate Algebra, 5e
				Lial: Beginning Algebra, 10e
				Lial: Beginning Algebra, 11e
				Lial: Calculus with Applications, 10e
				Lial: Calculus with Applications, Brief Version 9e
				Lial: Calculus with Applications, Brief Version, 10e
				Lial: College Algebra and Trigonometry, 4e
				Lial: College Algebra and Trigonometry, 5e
				Lial: College Algebra, 10e
				Lial: College Algebra, 10e for CLEP
				Lial: College Algebra, 11e
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics: Basic Mathematics and Algebra
				Lial: Essential Mathematics, 3e
				Lial: Essential Mathematics, 4e
				Lial: Essentials of College Algebra, 10e
				Lial: Essentials of College Algebra, 11e
				Lial: Finite Mathematics &amp; Calculus with Applications, 8e
				Lial: Finite Mathematics &amp; Calculus with Applications, 9e
				Lial: Finite Mathematics with Applications, 10e
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 9e
				Lial: Introductory &amp; Intermediate Algebra, 4e
				Lial: Introductory &amp; Intermediate Algebra, 5e
				Lial: Introductory Algebra, 10e
				Lial: Introductory Algebra, 8e
				Lial: Introductory Algebra, 9e
				Lial: Mathematics with Applications, 10e
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra and Introductory Algebra, 2e
				Lial: Prealgebra, 4e
				Lial: Prealgebra, 5e
				Lial: Precalculus, 4e
				Lial: Precalculus, 5e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 9e
				Lockdown Browser test PPE
				Long: Mathematical Reasoning for Elementary Teachers, 6e
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Madison College: ABE Dev Math Series
				Marecek: Strategies For Success: Study Skills for the College Math Student, 2e
				Maricopa: Custom Module Demo
				Martin-Gay: Algebra 1, MyLab Homeschool Edition
				Martin-Gay: Algebra 2, MyLab Homeschool Edition
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra: A Combined Approach, 4e
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics with Early Integers, 2e
				Martin-Gay: Basic College Mathematics, 3e
				Martin-Gay: Basic College Mathematics, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 3e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 4e
				Martin-Gay: Intermediate Algebra: Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra and Introductory Algebra
				Martin-Gay: Prealgebra, 5e
				Martin-Gay: Prealgebra, 6e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 7e (HTML5 Preview Version)
				MassBay Community College: MA090/095/098
				MasteringX 
				McClave: A First Course in Statistics, 11e
				McClave: Statistics for Business and Economics, 11e
				McClave: Statistics for Business and Economics, 12e
				McClave: Statistics, 12e
				McKenna/Kirk: Beginning &amp; Intermediate Algebra, 1e
				Miami Dade College: MAT 0022C: Developmental Mathematics Combined
				Miller: Business Mathematics, 11e
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 12e
				MML-CoCo: MyBusinessMathCourse (Cleaves 9e) 
				Montgomery County Community College: MAT10: Concepts of Numbers
				MXL Player Content Test Book
				MXL Player Content Test Book
				MyFoundationsLab Prototype
				MyMathLab for Basic Mathematics and Algebra
				MyMathLab Load Test Book
				MyMathLab test book A3
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate 9e with Trig 7e
				National University: Statistics and Data Analysis for Nursing Research
				NES Prep, Elementary Education II: Math
				New Additional Conceptual Exercises (NC Redesign)
				New Book Test 6
				Newbold: Statistics for Business and Economics, 7e
				Newbold: Statistics for Business and Economics, 8e
				Newbold: Statistics for Business and Economics: Global Edition, 7e
				NextGen Media Math PPE testbookDeletion
				NextGen Media Math Test Book-- Early Alerts
				NextGen Media Math Test Book--EA Auto
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL-Thilini
				NextGen Media Math Test Book--REAL_26rollback
				NextGen Media Math Test BookRH
				Norman/Wolczuk: Introduction to Linear Algebra for Science - Delete
				Norman/Wolczuk: Introduction to Linear Algebra for Science and Engineering, 2e
				North Carolina: Developmental Math Redesign Modules (Akst/Bragg)
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 4e
				Olivier: Business Mathematics Interactive
				Orientation Questions for Students
				Palm Beach State College: MAT 0018: Prealgebra
				Palm Beach State College: MAT 0028: Introductory Algebra
				Pearson: Math Essentials for College, Volumes 1 &amp; 2 (Bittinger)
				Phil&quot; , &quot;'&quot; , &quot;s Test Book
				Piedmont Technical College: MAT 122: Finite College Math (2011)
				Pirnot: Mathematics All Around, 4e
				Pirnot: Mathematics All Around, 5e
				Praxis 1 Math
				Prealgebra YTIs (Virtual School exercises)
				Precalculus YTIs (Virtual Schools exercises)
				Prince George Community College: Math 0104: Intermediate Algebra
				Prior: Basic Mathematics, 1e
				Prior: Prealgebra, 1e
				Raritan Valley CC: MA 015/020: Developmental Mathematics
				Ratti: College Algebra
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra and Trigonometry, 2e
				Ratti: College Algebra, 2e
				Ratti: College Algebra, 3e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 2e
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach
				Ratti: Precalculus: A Unit Circle Approach, 2e
				Ratti: Trigonometry 
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 5e
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 4e
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning Algebra with Apps and Visualization, 2e
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: Beginning and Intermediate Algebra, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 4e
				Rockswold: College Algebra, 5e
				Rockswold: Developmental Math, 1e
				Rockswold: Essentials of College Algebra, 4e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 3e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 4e
				Rockswold: Prealgebra, 1e 
				Rockswold: Precalculus with Modeling and Visualization, 4e
				Rockswold: Precalculus, 5e
				Salzman: Mathematics for Business, 10e
				Salzman: Mathematics for Business, 8e
				Salzman: Mathematics for Business, 9e
				Saunders: Mathematics for the Trades - A Guided Approach, 10e
				Schulz, Precalculus
				Schulz: Precalculus Demo (2014)
				Sharpe: Business Statistics
				Sharpe: Business Statistics, 2e
				Sharpe: Business Statistics, 3e
				Sharpe: Business Statistics, 4e
				Sharpe: Business Statistics, Canadian Edition
				Sharpe: Business Statistics, Second Canadian Edition 
				Sharpe: Business Statistics: A First Course
				Sharpe: Business Statistics: A First Course, 2e
				SI Notation testing (from sbtest)
				Skuce: Analyzing Data and Making Decisions, 2ce
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				Spokane Falls CC: Essential Algebra
				SPPTesting
				Squires: Developmental Math
				StatCrunch Test Book for PPE
				Statistics YTIs (Virtual School exercises)
				Stine: Statistics for Business: Decision Making and Analysis
				Stine: Statistics for Business: Decision Making and Analysis, 2e
				Stony Brook University: AMS 102: Elements of Statistics 2e
				Suffolk County Community College, Grant Campus: MAT 006/007 (2012)
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data,1e copy
				Sullivan: Algebra &amp; Trigonometry Enhanced w/ Graphing Utilities, 6e
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 5e
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 6e
				Sullivan: College Algebra, 8e
				Sullivan: College Algebra, 9e
				Sullivan: College Algebra: Concepts through Functions
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: College Algebra: Concepts Through Functions 3e
				Sullivan: Developmental Math, 1e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 2e
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary Algebra, 2e
				Sullivan: Elementary Algebra, 3e
				Sullivan: Fundamentals of Statistics, 3e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 4e copy
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Intermediate Algebra, 3e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus w/ Graphing Utilities, 4e
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus, 9e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach 2e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach 1e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach 2e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle
				Sullivan: Statistics: Informed Decisions Using Data, 3e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Trigonometry: A Right Triangle Approach, 5e
				Sullivan: Trigonometry: A Unit Circle Approach, 9e
				Tannenbaum: Excursions in Modern Mathematics, 7e
				Tannenbaum: Excursions in Modern Mathematics, 8e
				Texas State: Math 1314 Course Redesign
				Thomas&quot; , &quot;'&quot; , &quot; Calc demo
				Thomas&quot; , &quot;'&quot; , &quot; Calculus 11e DEMO RETIRED
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals, 12e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Global Edition, 12e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 12e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Tidewater Community College:  Math Essentials MTE 1-9
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 6e
				Tobey: Basic College Mathematics, 7e
				Tobey: Beginning Algebra, 6e
				Tobey: Beginning Algebra, 7e
				Tobey: Beginning Algebra, 8e
				Tobey: Beginning Algebra: Early Graphing, 2e
				Tobey: Beginning Algebra: Early Graphing, 3e
				Tobey: Beginning and Intermediate Algebra, 3e
				Tobey: Beginning and Intermediate Algebra, 4e
				Tobey: Intermediate Algebra, 6e
				Tobey: Intermediate Algebra, 7e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Trim: Calculus for Engineers, 4e
				Triola/Goodman/Law/LaBute: Elementary Statistics, 3ce
				Triola: Elementary Statistics Using Excel, 4e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 3e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics, 11e
				Triola: Elementary Statistics, 11e Technology Update
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 3ce DEMO
				Triola: Elementary Statistics, California Edition
				Triola: Elementary Statistics, Second California Edition
				Triola: Essentials of Statistics, 4e
				Triola: Essentials of Statistics, 5e
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				Universal Course: Algebra Readiness
				Universal Course: Applied Calculus
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Alberta: Calculus
				University of Florida: MAC1105: College Algebra
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				University of Louisville: ENG 101: Engineering Analysis
				University of Maryland University College: MAT 009: Introductory Algebra
				University of Maryland University College: MAT 012: Intermediate Algebra
				University of Maryland University College: MAT106: Finite Mathematics
				University of Maryland University College: MAT107/108/115: Alg and Trig
				University of Maryland University College: STAT 200: Elementary Statistics
				University of Maryland University College: STAT 230: Stats for Business &amp; Econ
				University of Massachusetts, Amherst: Voyaging Through Precalculus, Second Editi
				University of Phoenix: QRB/501 Quantitative Reasoning for Bus V3 2011
				University of Phoenix: QRB/501 Quantitative Reasoning for Business V4 2012
				University of Victoria: MATH 100/101/200
				Varberg: Calculus Early Transcendentals
				Varberg: Calculus, 9e
				Washington: Basic Tech Math w Calculus SI CANADA (Update)  Ready To Go
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math with Calculus, 10e
				Washington: Basic Technical Math, 10e
				Washington: Basic Technical Math, 11e
				Washington: Basic Technical Mathematics with Calculus, 9e
				Washington: Basic Technical Mathematics with Calculus, Ninth Edition, SI Version
				Washington: Basic Technical Mathematics with Calculus, SI edition
				Washington: Basic Technical Mathematics with Calculus, SI Version, 10e
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Watson, Corporate Finance Principles and Practice, 5e EMA
				Weiss: Elementary Statistics, 8e
				Weiss: Introductory Statistics, 9e
				Woodbury: Elementary &amp; Intermediate Algebra, 3e
				Woodbury: Elementary Algebra
				Woodbury: Intermediate Algebra
				XL Content Options Showcase
				XL Load Testbook
				z MathXL Freelancer Test Book
				Zuro: Discovering Mathematics, 1e DEMO

			&quot;))]</value>
      <webElementGuid>aa7d2a70-7525-46eb-ae5f-b2b7998a3994</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
