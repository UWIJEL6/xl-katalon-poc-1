<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ul_Attempt3                                _a279f6</name>
   <tag></tag>
   <elementGuidId>fd7d2230-d011-43ed-9c59-07021bf5bdda</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#lstAttempts</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='lstAttempts']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>lstAttempts</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>attempt-menu dropdown-menu dropdown-menu-right</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menu</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>btnName</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                            
                                
                                    Attempt 3
                                    100%
                            
                        
                            
                                
                                    Attempt 2
                                    66.67%
                            
                        
                            
                                
                                    Attempt 1
                                    33.33%
                            
                        
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;lstAttempts&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//ul[@id='lstAttempts']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_StudentPager1_MultipleAttemptGroup']/ul</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lahiru CERT std-01'])[1]/following::ul[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[@id = 'lstAttempts' and (text() = '
                    
                    
                            
                                
                                    Attempt 3
                                    100%
                            
                        
                            
                                
                                    Attempt 2
                                    66.67%
                            
                        
                            
                                
                                    Attempt 1
                                    33.33%
                            
                        
                ' or . = '
                    
                    
                            
                                
                                    Attempt 3
                                    100%
                            
                        
                            
                                
                                    Attempt 2
                                    66.67%
                            
                        
                            
                                
                                    Attempt 1
                                    33.33%
                            
                        
                ')]</value>
   </webElementXpaths>
</WebElementEntity>
