<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Delete</name>
   <tag></tag>
   <elementGuidId>792c4d9a-9bd1-4201-ab84-89d2fe254a38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[@onclick=&quot;return DoAction_Legacy(this,19663797,'Homework','WritingSpaces',false,false,'','None');&quot;])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>c4ed4b18-d574-42bb-aa46-2f2fb7265b0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0);</value>
      <webElementGuid>9d0f6458-2ad3-4acd-b88f-67bb749e2598</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-value</name>
      <type>Main</type>
      <value>Delete</value>
      <webElementGuid>ba06e0ef-5207-499f-9e5a-5ae8a1c72aa3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-id</name>
      <type>Main</type>
      <value>daH19663797</value>
      <webElementGuid>08e9ef82-13b0-4fa6-b28c-66e4acf772d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return DoAction_Legacy(this,19663797,'Homework','WritingSpaces',false,false,'','None');</value>
      <webElementGuid>e206a6ce-3266-429d-9fbd-cd6a8a364b7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>18569d96-2d93-44ed-840e-789f457cca01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delete</value>
      <webElementGuid>6cd2a1ae-31e8-402e-a1ff-6adbd9384b28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[10]/td[@class=&quot;grid-Assignment-Action-Dropdown&quot;]/div[@class=&quot;xlbootstrap3&quot;]/div[@class=&quot;btn-group dropdown-behavior action-dropdown-controller open&quot;]/ul[@class=&quot;dropdown-menu&quot;]/li[2]/a[1]</value>
      <webElementGuid>294d2366-661c-4631-a9fe-100028f77854</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//a[@onclick=&quot;return DoAction_Legacy(this,19663797,'Homework','WritingSpaces',false,false,'','None');&quot;])[2]</value>
      <webElementGuid>e37f8796-4480-4dcc-a7ee-bc487ebe50e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[10]/td[10]/div/div/ul/li[2]/a</value>
      <webElementGuid>ab957e6a-4c03-4e44-a3af-69d1fbc18113</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Delete')])[9]</value>
      <webElementGuid>2858b875-e443-421f-8a18-5ebe1e5721b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit...'])[9]/following::a[1]</value>
      <webElementGuid>a8802439-59a2-4428-98cf-e36c022266cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-- Select --'])[9]/following::a[2]</value>
      <webElementGuid>22fb0d38-79b3-4fdf-b703-c087342c82aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SampleWS1'])[1]/preceding::a[2]</value>
      <webElementGuid>d2412c69-57f3-4521-8eb5-d5a733781a29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[10]/preceding::a[2]</value>
      <webElementGuid>e17e5c58-e913-438e-a627-03173449cd81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0);')])[89]</value>
      <webElementGuid>3b2ad873-934d-4419-84b2-1c4863d56c1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[10]/td[10]/div/div/ul/li[2]/a</value>
      <webElementGuid>e6e7adbe-1239-4079-ba74-1b581eb455eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0);' and (text() = 'Delete' or . = 'Delete')]</value>
      <webElementGuid>b4aaa347-1466-462c-9656-b65d516d4776</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
