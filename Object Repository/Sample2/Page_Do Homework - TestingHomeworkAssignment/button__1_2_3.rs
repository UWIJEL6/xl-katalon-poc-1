<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button__1_2_3</name>
   <tag></tag>
   <elementGuidId>cf7c1664-544e-4172-857d-c01b62b656a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_player_PlayerMenu_']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_player_PlayerMenu_4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0bc6d14c-2fc3-44a0-a551-6c31317645f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>icon, _buttonNode, focusNode</value>
      <webElementGuid>430bf8ce-5ea2-435e-94f9-a9c36c6ba8d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlPlayerMenuIcon dijitDownArrowButton dijitHasDropDownOpen</value>
      <webElementGuid>4f108cc9-0af5-445a-a790-49db97d507f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>15729ce2-3d09-4a3d-bca2-a56d2ab3ad3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>4e257bc2-96fa-4bd8-92f1-b1f5d95c1153</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menu</value>
      <webElementGuid>c37780ae-37d0-48b7-9397-b0cec2d8ac36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>b16a7893-97cc-44d6-9b81-a679790ee05e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_PlayerMenu_4</value>
      <webElementGuid>bc871b9e-5776-4821-ae0a-971091aa4b58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>97f2e9e3-6c25-4b98-8c8e-f53a507ae2b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>popupactive</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7866f198-38d1-4cc9-b03f-8abbafff1584</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9b15c30a-5a4e-414c-8bee-249a4ee2a59b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>dijit_Menu_13</value>
      <webElementGuid>c9bab561-5dbc-4e6c-abbd-ca624308694e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> </value>
      <webElementGuid>4cc6c09c-e3bd-41ac-9a05-5329ed373a6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_PlayerMenu_4&quot;)</value>
      <webElementGuid>8f6890d0-eeb9-4432-be91-fec95b9470a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>83b461a3-6388-4a00-beaa-7fc5a0f6253a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_player_PlayerMenu_4']</value>
      <webElementGuid>b6865b2e-4e76-4ca0-9511-dcdddc6068a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_HigherEdPlayerControlPanel_3']/div/div[5]/div/button</value>
      <webElementGuid>40286e7a-3864-49bd-ad0d-d42c10a1db7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[10]/following::button[2]</value>
      <webElementGuid>495a58a7-8d9e-434b-bdbe-004b0b41f6c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ask my instructor'])[2]/following::button[2]</value>
      <webElementGuid>b4ae62d4-7c4e-4ced-a754-1e225e55eb24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show completed'])[1]/preceding::button[1]</value>
      <webElementGuid>27f2e755-760d-4da8-be8d-473f608aeb48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear all'])[1]/preceding::button[1]</value>
      <webElementGuid>c7ed9666-e667-477b-a180-6b08f38b92bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/button</value>
      <webElementGuid>e6da6fcd-965c-423a-8fb1-5d696dea1921</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'xl_player_PlayerMenu_4' and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      <webElementGuid>d08fd68c-e1d9-48dd-ae32-4c2e13340061</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
