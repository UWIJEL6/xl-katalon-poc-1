<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Apply correct answer</name>
   <tag></tag>
   <elementGuidId>5175a438-20a1-4956-a923-428028e1be0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[starts-with(@id,'dijit_MenuItem_') and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#dijit_MenuItem_2_text</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>a29c77d2-5acf-40af-8dce-fa085f62849a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dijitReset dijitMenuItemLabel</value>
      <webElementGuid>c1f55d26-7f38-48ea-96f7-36654deac43e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>fb8eaaf8-0541-4f09-8d64-b9e8c98a68d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>containerNode,textDirNode</value>
      <webElementGuid>7d2f4ccc-a79d-4597-a658-6162e8ac2b09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dijit_MenuItem_2_text</value>
      <webElementGuid>c9c0c522-721e-48fa-aafb-cd50919bc6f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apply correct answer</value>
      <webElementGuid>3daf93db-fe5a-4415-9051-dbb7cb8cce1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_MenuItem_2_text&quot;)</value>
      <webElementGuid>7dad4e9b-549c-4f34-98fc-bdd2c9232bc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Sample2/Page_Do Homework - TestingHomeworkAssignment/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>6f53f49a-ae33-4261-9a6f-87f1d31ceb00</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='dijit_MenuItem_2_text']</value>
      <webElementGuid>ef12b66c-8199-4da1-b158-5104714693d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='dijit_MenuItem_2']/td[2]</value>
      <webElementGuid>48f423c0-3513-4773-b332-003704e01603</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show alt text'])[1]/following::td[4]</value>
      <webElementGuid>35b4d310-7fef-45d1-9174-d618e3ed913d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[1]/following::td[5]</value>
      <webElementGuid>b0e8a848-3b78-4444-b77f-8856275bc0fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[12]/preceding::td[2]</value>
      <webElementGuid>5830fdf5-f042-4004-8560-a7f730a2570f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[2]/preceding::td[3]</value>
      <webElementGuid>fb442d1b-c5c7-4f66-9cd1-2f675dc3ec08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply correct answer']/parent::*</value>
      <webElementGuid>e5d6fa56-f221-423e-b6fa-f225044f4529</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/table/tbody/tr[5]/td[2]</value>
      <webElementGuid>e5f67ab3-5e31-4590-9ca7-c58a58155655</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'dijit_MenuItem_2_text' and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      <webElementGuid>55eec313-9730-44bc-b96e-0a2985285ac1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
