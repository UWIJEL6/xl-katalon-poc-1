<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Welcome to MathXL, Prabhashi</name>
   <tag></tag>
   <elementGuidId>9202886c-6679-47d9-8ca8-a068548c9e00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_divBackground']/table/tbody/tr/td/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>7f806538-b3b8-4d5d-a5bb-8b66035286ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Welcome to MathXL, Prabhashi

                    </value>
      <webElementGuid>dbef663a-6a9b-4f3a-9a8a-c7e640cd6737</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_divBackground&quot;)/table[@class=&quot;table container&quot;]/tbody[1]/tr[1]/td[1]/div[1]/h1[1]</value>
      <webElementGuid>b03b4ddf-9951-4d79-ae3d-e198294512f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_divBackground']/table/tbody/tr/td/div/h1</value>
      <webElementGuid>c2fc4d07-eb2f-4409-9aaf-a80885b91f30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Browser Check'])[1]/following::h1[1]</value>
      <webElementGuid>033ece33-a8b9-435f-929f-504aef9b2983</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MathXL Support'])[1]/following::h1[1]</value>
      <webElementGuid>b46d4a19-302e-4e97-8087-8403b81ef9ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SMS Default Institution'])[1]/preceding::h1[1]</value>
      <webElementGuid>125c074b-e7a0-407b-87c2-1c2832460fd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Books I Am Working In'])[1]/preceding::h1[1]</value>
      <webElementGuid>0ee8c4fb-a240-48cd-9da2-58a4b1dbd25c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Welcome to MathXL, Prabhashi']/parent::*</value>
      <webElementGuid>0a19cafc-7636-4727-bbff-4755e30a9cec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>67d02646-e28b-4be5-afd1-c660f998738e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Welcome to MathXL, Prabhashi

                    ' or . = 'Welcome to MathXL, Prabhashi

                    ')]</value>
      <webElementGuid>0e9bbf55-008c-4119-bb4d-72713a014d60</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
