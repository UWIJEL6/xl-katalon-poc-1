<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --  Algebra 1 Common _d7f9a4</name>
   <tag></tag>
   <elementGuidId>8b4134e8-95cd-4cec-98a8-6f3d62231d77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				  -- Choose Book --  
				*Algebra 1 Common Core (2012)
				*Algebra 1 Common Core (2015)
				*Algebra 2 Common Core (2012)
				*Algebra 2 Common Core (2015)
				*CME Project Algebra 1 Common Core
				*CME Project Algebra 2 Common Core
				*CME Project Geometry Common Core
				*CME Project Precalculus Common Core
				*Connected Mathematics 3 (Grades 6-8)
				*Create My Course: Algebra II to Calculus
				*Create My Course: Arithmetic to Algebra II
				*digits: Accelerated Grade 7
				*digits: grade 6
				*digits: grade 6 and 7
				*digits: grade 7
				*digits: grade 7 and 8
				*digits: grade 8
				*Geometry Common Core (2012)
				*Geometry Common Core (2015)
				*Integrated CME Project Mathematics I
				*Integrated CME Project Mathematics II
				*Integrated CME Project Mathematics III
				*Integrated High School Mathematics I ©2014
				*Integrated High School Mathematics II ©2014
				*Integrated High School Mathematics III ©2014
				*Middle Grades Math: Grade 6
				*Middle Grades Math: Grade 7
				*Middle Grades Math: Grade 8
				*Pearson Test Prep
				*PH Math, Accelerated Grade 7 Common Core (2013)
				*PH Math, Course 1 Common Core (2013)
				*PH Math, Course 2 Common Core (2013)
				*PH Math, Course 3 Common Core (2013)
				*Prentice Hall Algebra 1 ©2011
				*Prentice Hall Algebra 2 ©2011
				*Prentice Hall Geometry ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Algebra Readiness
				*Universal Course: College Readiness
				*Universal Course: Geometry
				ACE Testbook
				Agresti: Statistics: The Art and Science of Learning from Data, 4e
				Agresti: Statistics: The Art and Science of Learning from Data, 5e
				Akst: Intermediate Algebra Through Applications, 3e
				Algebra Success - Demo
				Alicia's SBNet feature testing book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Almy: Math Lit: A Pathway to College Mathematics, 3e
				Angel: A Survey of Mathematics with Applications, 10e
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 4e MEDIA UPDATE
				Angel: Elementary &amp; Intermediate Algebra for College Students, 5e
				Angel: Elementary Algebra for College Students, 10e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Intermediate Algebra for College Students, 10e
				Angel: Intermediate Algebra for College Students, 9e
				Barnett: Calculus for Business, Economics, Life and Social Sci Brief Ver, 14e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 14e
				Barnett: College Mathematics for Business, Econ, Life and Social Sci, 14e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 14e
				Beckmann: Skills Review for Math for Elementary and Middle School Teachers, 6e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 5e
				Beecher: Algebra &amp; Trigonometry, 5e Media Update
				Beecher: Algebra and Trigonometry, 5e 
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 5e
				Beecher: College Algebra, 5e Media Update
				Beecher: Precalculus A Right Triangle Approach, 5e
				Beecher: Precalculus A Right Triangle Approach, 5e Media Update
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Statistical Reasoning For Everyday Life, 5e
				Bennett: Using and Understanding Mathematics, 7e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 12e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 13e
				Bittinger: Algebra &amp; Trigonometry Graphs &amp; Models, 6e Digital Update
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Basic College Mathematics, 13e
				Bittinger: Basic Mathematics with Early Integers
				Bittinger: Calculus and Its Applications Brief, 12e
				Bittinger: Calculus and Its Applications Expanded Ed, 1e Media Update
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 2e
				Bittinger: College Algebra: Graphs and Models, 6e
				Bittinger: College Algebra: Graphs and Models, 6e Digital Update
				Bittinger: Developmental Mathematics, 10e
				Bittinger: Developmental Mathematics, 9e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts &amp; Apps, 7e DIGITAL UPDATE
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, 10e
				Bittinger: Intermediate Algebra, 13e
				Bittinger: Intermediate Algebra, Concepts and Applications, 10e
				Bittinger: Introductory &amp; Intermediate Algebra, 6e
				Bittinger: Introductory Algebra, 13e
				Bittinger: Introductory Algebra, 13e DEMO (2019)
				Bittinger: Prealgebra &amp; Introductory Algebra, 4e
				Bittinger: Prealgebra, 7e
				Bittinger: Prealgebra, 8e
				Bittinger: Precalculus Graphs &amp; Models, 6e Digital Update
				Bittinger: Precalculus: Graphs and Models, 6e
				Blair/Tobey/Slater: Prealgebra, 6e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 6e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 7e
				Blitzer: Algebra for College Students, 8e
				Blitzer: College Algebra An Early Functions Approach, 4e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra Essentials, 5e
				Blitzer: College Algebra Essentials, 6e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra, 8e
				Blitzer: Developmental Mathematics, 1e
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Intermediate Algebra for College Students, 8e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra, 6e
				Blitzer: Introductory Algebra for College Students, 7e
				Blitzer: Introductory Algebra, 8e
				Blitzer: Math for Your World, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Pathways to College Mathematics, 2e
				Blitzer: Precalculus Essentials, 5e
				Blitzer: Precalculus Essentials, 6e
				Blitzer: Precalculus, 5e
				Blitzer: Precalculus, 6e
				Blitzer: Precalculus, 7e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e
				Blitzer: Thinking Mathematically, 8e
				Blitzer: Trigonometry, 2e
				Blitzer: Trigonometry, 3e
				Bock: Stats In Your World, 2e
				Bock: Stats In Your World, 3e
				Bock: Stats: Modeling the World, 2e
				Bock: Stats: Modeling the World, 4e
				Bock: Stats: Modeling the World, 5e
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus Early Transcendentals, 3e
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update
				Briggs/Cochran: Calculus, 3e
				Briggs/Cochran: Calculus, 3e Digital Update
				Briggs: AP Calculus
				Briggs: AP Calculus, 2e
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Carman/Saunders: Mathematics for the Trades: A Guided Approach, 10e
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 11e
				Cleaves: College Mathematics, 10e
				Cleaves: College Mathematics, 9e
				Clendenen: Business Mathematics, 13e
				Clendenen: Business Mathematics, 14e
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematics (North Carolina Edition)
				Consortium: Applied Mathematics for College and Career Readiness, 1e
				Consortium: Conceptual Mathematics (West Virginia Edition)
				Consortium: Liberal Arts Math (Florida Edition)
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 5e
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 6e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 5e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 6e
				Corequisite Support for Sullivan: Precalc: CTF, A Right Triangle Approach, 4e
				Corequisite Support for Sullivan: Precalculus: CTF, A Unit Circle Approach, 4e
				Corequisite Support Modules for College Algebra or Precalculus
				Corequisite Support Modules for Quantitative Reasoning or Liberal Arts Math
				Corequisite Support Modules for Statistics
				De Veaux: Intro Stats, 5e
				De Veaux: Intro Stats, 6e
				De Veaux: Stats: Data &amp; Models, 4e
				De Veaux: Stats: Data &amp; Models, 5e
				Demana/Waits/Kennedy/Bressoud: Calculus, 6e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 8e
				Donnelly: Business Statistics, 2e
				Donnelly: Business Statistics, 3e
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 4e
				Dugopolski: College Algebra, 6e
				Dugopolski: Precalculus: Functions and Graphs, 4e MML Update
				Dugopolski: Trigonometry, 4e
				Dugopolski: Trigonometry, 5e
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e
				Edwards/Penney/Calvis: Differential Equations Computing and Modeling, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 6e
				eT1 Math Test Book
				eT2 Math Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e
				Evans: Business Analytics: Methods, Models, and Decisions, 3e Demo
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Florida Algebra 1A and 1B
				Foundations and Pre-calculus Mathematics 10 (Canada) DEMO
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 15e
				Goldstein: Finite Mathematics and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 13e
				Gould: Essential Statistics: Exploring the World Through Data, 2e
				Gould: Essential Statistics: Exploring the World Through Data, 3e
				Gould: Introductory Statistics: Exploring the World through Data, 2e
				Gould: Introductory Statistics: Exploring the World through Data, 3e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Grimaldo: Developmental Math: Prealgebra, Intro Algebra, Intermediate Algebra 1e
				Groebner: Business Statistics: A Decision-Making Approach, 10e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				Harshbarger: College Algebra in Context, 4e
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, 6e
				Hass: University Calculus Early Transcendentals, 3e
				Hass: University Calculus Early Transcendentals, 4e
				High School Math Algebra 1 ©2012 (YTIs)
				High School Math Algebra 2 ©2012 (YTIs)
				High School Math Geometry ©2012 (YTIs)
				Hornsby: A Graphical Approach to Algebra &amp; Trigonometry, 7e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to College Algebra, 7e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 7e
				Indiana Algebra 1 ©2017
				Indiana Algebra 2 ©2017
				Indiana Geometry ©2017
				Interactive Calculus Early Transcendentals, 1e Demo
				IPRO-Irv-XLTestbk1
				Janet Testing Book
				Janus Math NextGen2 L
				Knewton Math test book
				Larson: Elementary Statistics: Picturing the World, 6e
				Larson: Elementary Statistics: Picturing the World, 7e
				Larson: Elementary Statistics: Picturing the World, 8e
				Lay: Linear Algebra and Its Applications, 5e
				Lay: Linear Algebra and Its Applications, 6e
				Lehmann: A Pathway to Introductory Statistics, 1e
				Lehmann: A Pathway to Introductory Statistics, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 3e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 3e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 6e
				Levine: Statistics for Managers Using Microsoft Excel, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 9e
				Lial/Greenwell/Ritchey: Calculus with Applications Brief, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, Brief Version, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 10e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics, 12e
				Lial: Algebra &amp; Trigonometry for College Readiness Media Update
				Lial: Algebra and Trigonometry for College Readiness
				Lial: Algebra for College Students , 9e
				Lial: Algebra for College Students, 8e
				Lial: Basic College Mathematics, 10e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra, 12e
				Lial: Beginning Algebra, 13e
				Lial: Beginning and Intermediate Algebra, 7e
				Lial: Calculus with Applications, 11e
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra and Trigonometry, 7e
				Lial: College Algebra, 12e
				Lial: College Algebra, 13e
				Lial: Developmental Mathematics, 4e
				Lial: Essentials of College Algebra, 11e
				Lial: Essentials of College Algebra, 12e
				Lial: Finite Mathematics with Applications, 11e
				Lial: Finite Mathematics with Applications, 12e
				Lial: Finite Mathematics, 11e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 13e
				Lial: Intermediate Algebra, 13e V2
				Lial: Introductory Algebra, 11e
				Lial: Introductory and Intermediate Algebra, 6e
				Lial: Mathematics with Applications, 11e
				Lial: Mathematics with Applications, 12e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra, 3e
				Lial: Prealgebra, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 7e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Long: Mathematical Reasoning for Elementary Teachers, 7e Media Update
				Martin-Gay: Algebra 1
				Martin-Gay: Algebra 1 (Texas Course)
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Algebra: A Combined Approach, 5e
				Martin-Gay: Algebra: A Combined Approach, 6e 
				Martin-Gay: Basic College Mathematics with Early Integers, 3e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 5e
				Martin-Gay: Basic College Mathematics, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Beginning Algebra, 8e
				Martin-Gay: Developmental Mathematics, 3e
				Martin-Gay: Developmental Mathematics, 4e
				Martin-Gay: Geometry
				Martin-Gay: Inter. Algebra: Math for College Readiness (FL Edition)
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 6e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra: Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 5e
				Martin-Gay: Introductory Algebra, 6e
				Martin-Gay: Path to College Mathematics, 1e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 5e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 8e
				McClave: A First Course in Statistics, 12e
				McClave: Statistics for Business and Economics, 13e
				McClave: Statistics for Business and Economics, 14e
				McClave: Statistics, 13e
				McClave: Statistics, 13e MyLab Revision with Technology Updates
				Medway School District: High School Integrated Math Series
				Miller: Mathematical Ideas 11e and Expanded 11e (retired)
				Miller: Mathematical Ideas, 13e
				Miller: Mathematical Ideas, 14e
				MXL Player Functional Test Book
				MyMathLab test book A2
				Nagle: Fundamentals of Differential Eq w/ Boundary Value Prob, 7e Digital Update
				Nagle: Fundamentals of Differential Equations w/ Boundary Value Problems, 7e
				Nagle: Fundamentals of Differential Equations, 9e
				Nagle: Fundamentals of Differential Equations, 9e Digital Update
				Neuhauser/Roper: Calculus for Biology and Medicine, 4e
				Newbold: Statistics for Business and Economics, 8e
				NextGen Media Accounting Test Book
				NextGen Media Math Test Book--REAL 
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--ShehanNew
				NextGen Media Math Test Book--testsamplpe
				NextGen Media Math Test BookRH
				Pearson Pre-Calculus 11 (Canada)
				Pirnot: Mathematics All Around, 5e
				Pirnot: Mathematics All Around, 6e
				Pirnot: Mathematics All Around, 7e
				Pre-calculus 12 (Canada)
				Prentice Hall Algebra 1 ©2011 (YTIs)
				Prentice Hall Algebra 2 ©2011 (YTIs)
				Prentice Hall Algebra 2 and Trigonometry (New York Edition)
				Prentice Hall Geometry (New York Edition)
				Prentice Hall Geometry ©2011 (YTIs)
				Prentice Hall Integrated Algebra (New York Edition)
				Ratti/McWaters/Skrzypek/Fresh/Bernard: Precalculus A Right Triangle Approach, 5e
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra &amp; Trigonometry, 4e
				Ratti: College Algebra, 3e
				Ratti: College Algebra, 4e
				Ratti: Precalculus A Right Triangle Approach, 4e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach, 3e 
				Ritchey/Rickard/Merkin: Interactive Finite Mathematics
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 6e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 6e
				Rockswold: Developmental Mathematics, 2e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 5e
				Rockswold: Precalculus with Modeling &amp; Visualization, 6e
				Salzman: Mathematics for Business, 10e
				Saunders: Mathematics for the Trades: A Guided Approach, 11e
				School of One (New York)
				Schulz: Precalculus, 2e
				Schulz: Precalculus, 2e Digital Update
				Sharpe/De Veaux/Velleman: Business Statistics, 4e Digital Update
				Sharpe: Business Statistics, 3e
				Stine: Statistics for Business: Decision Making and Analysis, 3e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan: Algebra &amp; Trigonometry, 10e
				Sullivan: Algebra &amp; Trigonometry, 11e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 7e
				Sullivan: College Algebra Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra, 10e
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra: Concepts Through Functions, 4e
				Sullivan: Developmental Math, 2e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary &amp; Intermediate Algebra, 4e
				Sullivan: Elementary Algebra
				Sullivan: Elementary Algebra, 4e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Fundamentals of Statistics, 6e
				Sullivan: Intermediate Algebra, 4e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 7e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 8e
				Sullivan: Precalculus, 10e
				Sullivan: Precalculus, 11e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 4e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5e
				Sullivan: Statistics: Informed Decisions Using Data, 6e
				Sullivan: Trigonometry A Unit Circle Approach, 10e
				Sullivan: Trigonometry: A Unit Circle Approach, 11e
				Tannenbaum: Excursions in Modern Mathematics, 10e
				Tannenbaum: Excursions in Modern Mathematics, 9e
				Tennessee Senior Finite Mathematics: An Experiential Approach
				Tennessee: Senior Bridge Mathematics 2011
				Tennessee: Senior Bridge Mathematics 2016
				Texas Algebra I ©2016
				Texas Algebra II ©2016
				Texas Geometry ©2016
				Texas: Mathematical Models with Applications, Texas Edition (2016)
				Texas: Precalculus Enhanced with Graphing Utilities, Texas Edition
				Texas: Precalculus: Graphical, Numerical, Algebraic, Texas Edition
				Thomas' Calculus Early Transcendentals, 15e
				Thomas' Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Thomas’ Calculus Early Transcendentals, 14e
				Thomas’ Calculus, 14e
				Tobey: Basic College Mathematics, 5e
				Tobey: Basic College Mathematics, 8e
				Tobey: Basic College Mathematics, 9e
				Tobey: Beginning Algebra, 9e
				Tobey: Beginning Algebra: Early Graphing, 4e
				Tobey: Beginning and Intermediate Algebra, 5e
				Tobey: Beginning and Intermediate Algebra, 6e
				Tobey: Intermediate Algebra, 8e
				Trigsted: Interactive Developmental Math: Prealg, Beginning &amp; Inter Algebra, 2e
				Triola: Biostatistics for the Biological and Health Sciences, 2e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics using Excel, 7e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 13e
				Triola: Elementary Statistics, 14e
				Triola: Essentials of Statistics, 5e
				Triola: Essentials of Statistics, 6e
				Triola: Essentials of Statistics, 7e
				University of Maryland University College: STAT 200: Elementary Statistics
				Utah School Districts: Mathematics I Common Core
				Utah School Districts: Mathematics II Common Core
				Utah School Districts: Mathematics III Common Core
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e MyLab Update
				Washington/Evans: Basic Technical Mathematics with Calculus, 12e
				Washington/Evans: Basic Technical Mathematics, 12e
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math with Calculus, 10e
				Washington: Basic Technical Math, 10e
				Washington: Basic Technical Math, 11e
				Weiss: Elementary Statistics, 9e
				Weiss: Introductory Statistics, 10e
				Weiss: Introductory Statistics, 10e MyLab Revision with Tech Updates
				Woodbury: Elementary &amp; Intermediate Algebra, 4e
				Woodbury: Intermediate Algebra: A STEM Approach, 1e
				Zuro: Discovering Mathematics, 1e DEMO

			</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = concat(&quot;
				  -- Choose Book --  
				*Algebra 1 Common Core (2012)
				*Algebra 1 Common Core (2015)
				*Algebra 2 Common Core (2012)
				*Algebra 2 Common Core (2015)
				*CME Project Algebra 1 Common Core
				*CME Project Algebra 2 Common Core
				*CME Project Geometry Common Core
				*CME Project Precalculus Common Core
				*Connected Mathematics 3 (Grades 6-8)
				*Create My Course: Algebra II to Calculus
				*Create My Course: Arithmetic to Algebra II
				*digits: Accelerated Grade 7
				*digits: grade 6
				*digits: grade 6 and 7
				*digits: grade 7
				*digits: grade 7 and 8
				*digits: grade 8
				*Geometry Common Core (2012)
				*Geometry Common Core (2015)
				*Integrated CME Project Mathematics I
				*Integrated CME Project Mathematics II
				*Integrated CME Project Mathematics III
				*Integrated High School Mathematics I ©2014
				*Integrated High School Mathematics II ©2014
				*Integrated High School Mathematics III ©2014
				*Middle Grades Math: Grade 6
				*Middle Grades Math: Grade 7
				*Middle Grades Math: Grade 8
				*Pearson Test Prep
				*PH Math, Accelerated Grade 7 Common Core (2013)
				*PH Math, Course 1 Common Core (2013)
				*PH Math, Course 2 Common Core (2013)
				*PH Math, Course 3 Common Core (2013)
				*Prentice Hall Algebra 1 ©2011
				*Prentice Hall Algebra 2 ©2011
				*Prentice Hall Geometry ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Algebra Readiness
				*Universal Course: College Readiness
				*Universal Course: Geometry
				ACE Testbook
				Agresti: Statistics: The Art and Science of Learning from Data, 4e
				Agresti: Statistics: The Art and Science of Learning from Data, 5e
				Akst: Intermediate Algebra Through Applications, 3e
				Algebra Success - Demo
				Alicia&quot; , &quot;'&quot; , &quot;s SBNet feature testing book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Almy: Math Lit: A Pathway to College Mathematics, 3e
				Angel: A Survey of Mathematics with Applications, 10e
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 4e MEDIA UPDATE
				Angel: Elementary &amp; Intermediate Algebra for College Students, 5e
				Angel: Elementary Algebra for College Students, 10e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Intermediate Algebra for College Students, 10e
				Angel: Intermediate Algebra for College Students, 9e
				Barnett: Calculus for Business, Economics, Life and Social Sci Brief Ver, 14e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 14e
				Barnett: College Mathematics for Business, Econ, Life and Social Sci, 14e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 14e
				Beckmann: Skills Review for Math for Elementary and Middle School Teachers, 6e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 5e
				Beecher: Algebra &amp; Trigonometry, 5e Media Update
				Beecher: Algebra and Trigonometry, 5e 
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 5e
				Beecher: College Algebra, 5e Media Update
				Beecher: Precalculus A Right Triangle Approach, 5e
				Beecher: Precalculus A Right Triangle Approach, 5e Media Update
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Statistical Reasoning For Everyday Life, 5e
				Bennett: Using and Understanding Mathematics, 7e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 12e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 13e
				Bittinger: Algebra &amp; Trigonometry Graphs &amp; Models, 6e Digital Update
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Basic College Mathematics, 13e
				Bittinger: Basic Mathematics with Early Integers
				Bittinger: Calculus and Its Applications Brief, 12e
				Bittinger: Calculus and Its Applications Expanded Ed, 1e Media Update
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 2e
				Bittinger: College Algebra: Graphs and Models, 6e
				Bittinger: College Algebra: Graphs and Models, 6e Digital Update
				Bittinger: Developmental Mathematics, 10e
				Bittinger: Developmental Mathematics, 9e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts &amp; Apps, 7e DIGITAL UPDATE
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, 10e
				Bittinger: Intermediate Algebra, 13e
				Bittinger: Intermediate Algebra, Concepts and Applications, 10e
				Bittinger: Introductory &amp; Intermediate Algebra, 6e
				Bittinger: Introductory Algebra, 13e
				Bittinger: Introductory Algebra, 13e DEMO (2019)
				Bittinger: Prealgebra &amp; Introductory Algebra, 4e
				Bittinger: Prealgebra, 7e
				Bittinger: Prealgebra, 8e
				Bittinger: Precalculus Graphs &amp; Models, 6e Digital Update
				Bittinger: Precalculus: Graphs and Models, 6e
				Blair/Tobey/Slater: Prealgebra, 6e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 6e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 7e
				Blitzer: Algebra for College Students, 8e
				Blitzer: College Algebra An Early Functions Approach, 4e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra Essentials, 5e
				Blitzer: College Algebra Essentials, 6e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra, 8e
				Blitzer: Developmental Mathematics, 1e
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Intermediate Algebra for College Students, 8e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra, 6e
				Blitzer: Introductory Algebra for College Students, 7e
				Blitzer: Introductory Algebra, 8e
				Blitzer: Math for Your World, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Pathways to College Mathematics, 2e
				Blitzer: Precalculus Essentials, 5e
				Blitzer: Precalculus Essentials, 6e
				Blitzer: Precalculus, 5e
				Blitzer: Precalculus, 6e
				Blitzer: Precalculus, 7e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e
				Blitzer: Thinking Mathematically, 8e
				Blitzer: Trigonometry, 2e
				Blitzer: Trigonometry, 3e
				Bock: Stats In Your World, 2e
				Bock: Stats In Your World, 3e
				Bock: Stats: Modeling the World, 2e
				Bock: Stats: Modeling the World, 4e
				Bock: Stats: Modeling the World, 5e
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus Early Transcendentals, 3e
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update
				Briggs/Cochran: Calculus, 3e
				Briggs/Cochran: Calculus, 3e Digital Update
				Briggs: AP Calculus
				Briggs: AP Calculus, 2e
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Carman/Saunders: Mathematics for the Trades: A Guided Approach, 10e
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 11e
				Cleaves: College Mathematics, 10e
				Cleaves: College Mathematics, 9e
				Clendenen: Business Mathematics, 13e
				Clendenen: Business Mathematics, 14e
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematics (North Carolina Edition)
				Consortium: Applied Mathematics for College and Career Readiness, 1e
				Consortium: Conceptual Mathematics (West Virginia Edition)
				Consortium: Liberal Arts Math (Florida Edition)
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 5e
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 6e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 5e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 6e
				Corequisite Support for Sullivan: Precalc: CTF, A Right Triangle Approach, 4e
				Corequisite Support for Sullivan: Precalculus: CTF, A Unit Circle Approach, 4e
				Corequisite Support Modules for College Algebra or Precalculus
				Corequisite Support Modules for Quantitative Reasoning or Liberal Arts Math
				Corequisite Support Modules for Statistics
				De Veaux: Intro Stats, 5e
				De Veaux: Intro Stats, 6e
				De Veaux: Stats: Data &amp; Models, 4e
				De Veaux: Stats: Data &amp; Models, 5e
				Demana/Waits/Kennedy/Bressoud: Calculus, 6e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 8e
				Donnelly: Business Statistics, 2e
				Donnelly: Business Statistics, 3e
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 4e
				Dugopolski: College Algebra, 6e
				Dugopolski: Precalculus: Functions and Graphs, 4e MML Update
				Dugopolski: Trigonometry, 4e
				Dugopolski: Trigonometry, 5e
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e
				Edwards/Penney/Calvis: Differential Equations Computing and Modeling, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 6e
				eT1 Math Test Book
				eT2 Math Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e
				Evans: Business Analytics: Methods, Models, and Decisions, 3e Demo
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Florida Algebra 1A and 1B
				Foundations and Pre-calculus Mathematics 10 (Canada) DEMO
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 15e
				Goldstein: Finite Mathematics and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 13e
				Gould: Essential Statistics: Exploring the World Through Data, 2e
				Gould: Essential Statistics: Exploring the World Through Data, 3e
				Gould: Introductory Statistics: Exploring the World through Data, 2e
				Gould: Introductory Statistics: Exploring the World through Data, 3e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Grimaldo: Developmental Math: Prealgebra, Intro Algebra, Intermediate Algebra 1e
				Groebner: Business Statistics: A Decision-Making Approach, 10e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				Harshbarger: College Algebra in Context, 4e
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, 6e
				Hass: University Calculus Early Transcendentals, 3e
				Hass: University Calculus Early Transcendentals, 4e
				High School Math Algebra 1 ©2012 (YTIs)
				High School Math Algebra 2 ©2012 (YTIs)
				High School Math Geometry ©2012 (YTIs)
				Hornsby: A Graphical Approach to Algebra &amp; Trigonometry, 7e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to College Algebra, 7e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 7e
				Indiana Algebra 1 ©2017
				Indiana Algebra 2 ©2017
				Indiana Geometry ©2017
				Interactive Calculus Early Transcendentals, 1e Demo
				IPRO-Irv-XLTestbk1
				Janet Testing Book
				Janus Math NextGen2 L
				Knewton Math test book
				Larson: Elementary Statistics: Picturing the World, 6e
				Larson: Elementary Statistics: Picturing the World, 7e
				Larson: Elementary Statistics: Picturing the World, 8e
				Lay: Linear Algebra and Its Applications, 5e
				Lay: Linear Algebra and Its Applications, 6e
				Lehmann: A Pathway to Introductory Statistics, 1e
				Lehmann: A Pathway to Introductory Statistics, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 3e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 3e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 6e
				Levine: Statistics for Managers Using Microsoft Excel, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 9e
				Lial/Greenwell/Ritchey: Calculus with Applications Brief, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, Brief Version, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 10e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics, 12e
				Lial: Algebra &amp; Trigonometry for College Readiness Media Update
				Lial: Algebra and Trigonometry for College Readiness
				Lial: Algebra for College Students , 9e
				Lial: Algebra for College Students, 8e
				Lial: Basic College Mathematics, 10e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra, 12e
				Lial: Beginning Algebra, 13e
				Lial: Beginning and Intermediate Algebra, 7e
				Lial: Calculus with Applications, 11e
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra and Trigonometry, 7e
				Lial: College Algebra, 12e
				Lial: College Algebra, 13e
				Lial: Developmental Mathematics, 4e
				Lial: Essentials of College Algebra, 11e
				Lial: Essentials of College Algebra, 12e
				Lial: Finite Mathematics with Applications, 11e
				Lial: Finite Mathematics with Applications, 12e
				Lial: Finite Mathematics, 11e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 13e
				Lial: Intermediate Algebra, 13e V2
				Lial: Introductory Algebra, 11e
				Lial: Introductory and Intermediate Algebra, 6e
				Lial: Mathematics with Applications, 11e
				Lial: Mathematics with Applications, 12e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra, 3e
				Lial: Prealgebra, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 7e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Long: Mathematical Reasoning for Elementary Teachers, 7e Media Update
				Martin-Gay: Algebra 1
				Martin-Gay: Algebra 1 (Texas Course)
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Algebra: A Combined Approach, 5e
				Martin-Gay: Algebra: A Combined Approach, 6e 
				Martin-Gay: Basic College Mathematics with Early Integers, 3e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 5e
				Martin-Gay: Basic College Mathematics, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Beginning Algebra, 8e
				Martin-Gay: Developmental Mathematics, 3e
				Martin-Gay: Developmental Mathematics, 4e
				Martin-Gay: Geometry
				Martin-Gay: Inter. Algebra: Math for College Readiness (FL Edition)
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 6e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra: Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 5e
				Martin-Gay: Introductory Algebra, 6e
				Martin-Gay: Path to College Mathematics, 1e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 5e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 8e
				McClave: A First Course in Statistics, 12e
				McClave: Statistics for Business and Economics, 13e
				McClave: Statistics for Business and Economics, 14e
				McClave: Statistics, 13e
				McClave: Statistics, 13e MyLab Revision with Technology Updates
				Medway School District: High School Integrated Math Series
				Miller: Mathematical Ideas 11e and Expanded 11e (retired)
				Miller: Mathematical Ideas, 13e
				Miller: Mathematical Ideas, 14e
				MXL Player Functional Test Book
				MyMathLab test book A2
				Nagle: Fundamentals of Differential Eq w/ Boundary Value Prob, 7e Digital Update
				Nagle: Fundamentals of Differential Equations w/ Boundary Value Problems, 7e
				Nagle: Fundamentals of Differential Equations, 9e
				Nagle: Fundamentals of Differential Equations, 9e Digital Update
				Neuhauser/Roper: Calculus for Biology and Medicine, 4e
				Newbold: Statistics for Business and Economics, 8e
				NextGen Media Accounting Test Book
				NextGen Media Math Test Book--REAL 
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--ShehanNew
				NextGen Media Math Test Book--testsamplpe
				NextGen Media Math Test BookRH
				Pearson Pre-Calculus 11 (Canada)
				Pirnot: Mathematics All Around, 5e
				Pirnot: Mathematics All Around, 6e
				Pirnot: Mathematics All Around, 7e
				Pre-calculus 12 (Canada)
				Prentice Hall Algebra 1 ©2011 (YTIs)
				Prentice Hall Algebra 2 ©2011 (YTIs)
				Prentice Hall Algebra 2 and Trigonometry (New York Edition)
				Prentice Hall Geometry (New York Edition)
				Prentice Hall Geometry ©2011 (YTIs)
				Prentice Hall Integrated Algebra (New York Edition)
				Ratti/McWaters/Skrzypek/Fresh/Bernard: Precalculus A Right Triangle Approach, 5e
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra &amp; Trigonometry, 4e
				Ratti: College Algebra, 3e
				Ratti: College Algebra, 4e
				Ratti: Precalculus A Right Triangle Approach, 4e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach, 3e 
				Ritchey/Rickard/Merkin: Interactive Finite Mathematics
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 6e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 6e
				Rockswold: Developmental Mathematics, 2e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 5e
				Rockswold: Precalculus with Modeling &amp; Visualization, 6e
				Salzman: Mathematics for Business, 10e
				Saunders: Mathematics for the Trades: A Guided Approach, 11e
				School of One (New York)
				Schulz: Precalculus, 2e
				Schulz: Precalculus, 2e Digital Update
				Sharpe/De Veaux/Velleman: Business Statistics, 4e Digital Update
				Sharpe: Business Statistics, 3e
				Stine: Statistics for Business: Decision Making and Analysis, 3e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan: Algebra &amp; Trigonometry, 10e
				Sullivan: Algebra &amp; Trigonometry, 11e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 7e
				Sullivan: College Algebra Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra, 10e
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra: Concepts Through Functions, 4e
				Sullivan: Developmental Math, 2e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary &amp; Intermediate Algebra, 4e
				Sullivan: Elementary Algebra
				Sullivan: Elementary Algebra, 4e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Fundamentals of Statistics, 6e
				Sullivan: Intermediate Algebra, 4e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 7e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 8e
				Sullivan: Precalculus, 10e
				Sullivan: Precalculus, 11e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 4e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5e
				Sullivan: Statistics: Informed Decisions Using Data, 6e
				Sullivan: Trigonometry A Unit Circle Approach, 10e
				Sullivan: Trigonometry: A Unit Circle Approach, 11e
				Tannenbaum: Excursions in Modern Mathematics, 10e
				Tannenbaum: Excursions in Modern Mathematics, 9e
				Tennessee Senior Finite Mathematics: An Experiential Approach
				Tennessee: Senior Bridge Mathematics 2011
				Tennessee: Senior Bridge Mathematics 2016
				Texas Algebra I ©2016
				Texas Algebra II ©2016
				Texas Geometry ©2016
				Texas: Mathematical Models with Applications, Texas Edition (2016)
				Texas: Precalculus Enhanced with Graphing Utilities, Texas Edition
				Texas: Precalculus: Graphical, Numerical, Algebraic, Texas Edition
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals, 15e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Thomas’ Calculus Early Transcendentals, 14e
				Thomas’ Calculus, 14e
				Tobey: Basic College Mathematics, 5e
				Tobey: Basic College Mathematics, 8e
				Tobey: Basic College Mathematics, 9e
				Tobey: Beginning Algebra, 9e
				Tobey: Beginning Algebra: Early Graphing, 4e
				Tobey: Beginning and Intermediate Algebra, 5e
				Tobey: Beginning and Intermediate Algebra, 6e
				Tobey: Intermediate Algebra, 8e
				Trigsted: Interactive Developmental Math: Prealg, Beginning &amp; Inter Algebra, 2e
				Triola: Biostatistics for the Biological and Health Sciences, 2e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics using Excel, 7e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 13e
				Triola: Elementary Statistics, 14e
				Triola: Essentials of Statistics, 5e
				Triola: Essentials of Statistics, 6e
				Triola: Essentials of Statistics, 7e
				University of Maryland University College: STAT 200: Elementary Statistics
				Utah School Districts: Mathematics I Common Core
				Utah School Districts: Mathematics II Common Core
				Utah School Districts: Mathematics III Common Core
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e MyLab Update
				Washington/Evans: Basic Technical Mathematics with Calculus, 12e
				Washington/Evans: Basic Technical Mathematics, 12e
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math with Calculus, 10e
				Washington: Basic Technical Math, 10e
				Washington: Basic Technical Math, 11e
				Weiss: Elementary Statistics, 9e
				Weiss: Introductory Statistics, 10e
				Weiss: Introductory Statistics, 10e MyLab Revision with Tech Updates
				Woodbury: Elementary &amp; Intermediate Algebra, 4e
				Woodbury: Intermediate Algebra: A STEM Approach, 1e
				Zuro: Discovering Mathematics, 1e DEMO

			&quot;) or . = concat(&quot;
				  -- Choose Book --  
				*Algebra 1 Common Core (2012)
				*Algebra 1 Common Core (2015)
				*Algebra 2 Common Core (2012)
				*Algebra 2 Common Core (2015)
				*CME Project Algebra 1 Common Core
				*CME Project Algebra 2 Common Core
				*CME Project Geometry Common Core
				*CME Project Precalculus Common Core
				*Connected Mathematics 3 (Grades 6-8)
				*Create My Course: Algebra II to Calculus
				*Create My Course: Arithmetic to Algebra II
				*digits: Accelerated Grade 7
				*digits: grade 6
				*digits: grade 6 and 7
				*digits: grade 7
				*digits: grade 7 and 8
				*digits: grade 8
				*Geometry Common Core (2012)
				*Geometry Common Core (2015)
				*Integrated CME Project Mathematics I
				*Integrated CME Project Mathematics II
				*Integrated CME Project Mathematics III
				*Integrated High School Mathematics I ©2014
				*Integrated High School Mathematics II ©2014
				*Integrated High School Mathematics III ©2014
				*Middle Grades Math: Grade 6
				*Middle Grades Math: Grade 7
				*Middle Grades Math: Grade 8
				*Pearson Test Prep
				*PH Math, Accelerated Grade 7 Common Core (2013)
				*PH Math, Course 1 Common Core (2013)
				*PH Math, Course 2 Common Core (2013)
				*PH Math, Course 3 Common Core (2013)
				*Prentice Hall Algebra 1 ©2011
				*Prentice Hall Algebra 2 ©2011
				*Prentice Hall Geometry ©2011
				*Universal Course: Algebra I
				*Universal Course: Algebra II
				*Universal Course: Algebra Readiness
				*Universal Course: College Readiness
				*Universal Course: Geometry
				ACE Testbook
				Agresti: Statistics: The Art and Science of Learning from Data, 4e
				Agresti: Statistics: The Art and Science of Learning from Data, 5e
				Akst: Intermediate Algebra Through Applications, 3e
				Algebra Success - Demo
				Alicia&quot; , &quot;'&quot; , &quot;s SBNet feature testing book
				Almy: Math Lit: A Pathway to College Mathematics, 2e
				Almy: Math Lit: A Pathway to College Mathematics, 3e
				Angel: A Survey of Mathematics with Applications, 10e
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: Elementary &amp; Intermediate Algebra for College Students, 4e MEDIA UPDATE
				Angel: Elementary &amp; Intermediate Algebra for College Students, 5e
				Angel: Elementary Algebra for College Students, 10e
				Angel: Elementary Algebra for College Students, 9e
				Angel: Intermediate Algebra for College Students, 10e
				Angel: Intermediate Algebra for College Students, 9e
				Barnett: Calculus for Business, Economics, Life and Social Sci Brief Ver, 14e
				Barnett: Calculus for Business, Economics, Life and Social Sci, 14e
				Barnett: College Mathematics for Business, Econ, Life and Social Sci, 14e
				Barnett: Finite Mathematics for Business, Economics, Life and Social Sci, 14e
				Beckmann: Skills Review for Math for Elementary and Middle School Teachers, 6e
				Beckmann: Skills Review for Mathematics for Elementary Teachers, 5e
				Beecher: Algebra &amp; Trigonometry, 5e Media Update
				Beecher: Algebra and Trigonometry, 5e 
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 5e
				Beecher: College Algebra, 5e Media Update
				Beecher: Precalculus A Right Triangle Approach, 5e
				Beecher: Precalculus A Right Triangle Approach, 5e Media Update
				Bennett: Statistical Reasoning For Everyday Life, 4e
				Bennett: Statistical Reasoning For Everyday Life, 5e
				Bennett: Using and Understanding Mathematics, 7e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 12e
				Billstein: A Problem Solving Approach to Math for Elementary School Teachers 13e
				Bittinger: Algebra &amp; Trigonometry Graphs &amp; Models, 6e Digital Update
				Bittinger: Algebra and Trigonometry: Graphs and Models, 6e
				Bittinger: Basic College Mathematics, 13e
				Bittinger: Basic Mathematics with Early Integers
				Bittinger: Calculus and Its Applications Brief, 12e
				Bittinger: Calculus and Its Applications Expanded Ed, 1e Media Update
				Bittinger: Calculus and Its Applications, 11e
				Bittinger: Calculus and Its Applications, 2e
				Bittinger: College Algebra: Graphs and Models, 6e
				Bittinger: College Algebra: Graphs and Models, 6e Digital Update
				Bittinger: Developmental Mathematics, 10e
				Bittinger: Developmental Mathematics, 9e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 5e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts &amp; Apps, 7e DIGITAL UPDATE
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra, 10e
				Bittinger: Intermediate Algebra, 13e
				Bittinger: Intermediate Algebra, Concepts and Applications, 10e
				Bittinger: Introductory &amp; Intermediate Algebra, 6e
				Bittinger: Introductory Algebra, 13e
				Bittinger: Introductory Algebra, 13e DEMO (2019)
				Bittinger: Prealgebra &amp; Introductory Algebra, 4e
				Bittinger: Prealgebra, 7e
				Bittinger: Prealgebra, 8e
				Bittinger: Precalculus Graphs &amp; Models, 6e Digital Update
				Bittinger: Precalculus: Graphs and Models, 6e
				Blair/Tobey/Slater: Prealgebra, 6e
				Blitzer: Algebra &amp; Trigonometry, 5e
				Blitzer: Algebra and Trigonometry, 6e
				Blitzer: Algebra and Trigonometry, 7e
				Blitzer: Algebra for College Students, 7e
				Blitzer: Algebra for College Students, 8e
				Blitzer: College Algebra An Early Functions Approach, 4e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra Essentials, 5e
				Blitzer: College Algebra Essentials, 6e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra, 8e
				Blitzer: Developmental Mathematics, 1e
				Blitzer: Intermediate Algebra for College Students, 7e
				Blitzer: Intermediate Algebra for College Students, 8e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra, 6e
				Blitzer: Introductory Algebra for College Students, 7e
				Blitzer: Introductory Algebra, 8e
				Blitzer: Math for Your World, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Pathways to College Mathematics, 2e
				Blitzer: Precalculus Essentials, 5e
				Blitzer: Precalculus Essentials, 6e
				Blitzer: Precalculus, 5e
				Blitzer: Precalculus, 6e
				Blitzer: Precalculus, 7e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e
				Blitzer: Thinking Mathematically, 8e
				Blitzer: Trigonometry, 2e
				Blitzer: Trigonometry, 3e
				Bock: Stats In Your World, 2e
				Bock: Stats In Your World, 3e
				Bock: Stats: Modeling the World, 2e
				Bock: Stats: Modeling the World, 4e
				Bock: Stats: Modeling the World, 5e
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus Early Transcendentals, 3e
				Briggs/Cochran: Calculus Early Transcendentals, 3e Digital Update
				Briggs/Cochran: Calculus, 3e
				Briggs/Cochran: Calculus, 3e Digital Update
				Briggs: AP Calculus
				Briggs: AP Calculus, 2e
				Briggs: Calculus for Scientists and Engineers Early Transcendentals, 1e
				Carman/Saunders: Mathematics for the Trades: A Guided Approach, 10e
				Carson: Elementary Algebra, 4e
				Carson: Elementary and Intermediate Algebra, 4e
				Carson: Intermediate Algebra, 4e
				Carson: Prealgebra, 4e
				Cleaves: Business Math, 10e 
				Cleaves: Business Math, 11e
				Cleaves: College Mathematics, 10e
				Cleaves: College Mathematics, 9e
				Clendenen: Business Mathematics, 13e
				Clendenen: Business Mathematics, 14e
				Consortium: Algebra, Functions, and Data Analysis (Virginia Edition)
				Consortium: Applied Mathematical Modeling and Problem Solving, 1e
				Consortium: Applied Mathematics (North Carolina Edition)
				Consortium: Applied Mathematics for College and Career Readiness, 1e
				Consortium: Conceptual Mathematics (West Virginia Edition)
				Consortium: Liberal Arts Math (Florida Edition)
				Consortium: Mathematical Models with Applications (Texas Edition)
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 5e
				Consortium: MIA: Algebraic, Graphical, &amp; Trigonometric Problem Solving, 6e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 5e
				Consortium: MIA: Intro to Algebraic, Graphical, &amp; Numerical Problem Solving, 6e
				Corequisite Support for Sullivan: Precalc: CTF, A Right Triangle Approach, 4e
				Corequisite Support for Sullivan: Precalculus: CTF, A Unit Circle Approach, 4e
				Corequisite Support Modules for College Algebra or Precalculus
				Corequisite Support Modules for Quantitative Reasoning or Liberal Arts Math
				Corequisite Support Modules for Statistics
				De Veaux: Intro Stats, 5e
				De Veaux: Intro Stats, 6e
				De Veaux: Stats: Data &amp; Models, 4e
				De Veaux: Stats: Data &amp; Models, 5e
				Demana/Waits/Kennedy/Bressoud: Calculus, 6e
				Demana: Precalculus: Graphical, Numerical, Algebraic 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic Common Core 9e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 10e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 8e
				Donnelly: Business Statistics, 2e
				Donnelly: Business Statistics, 3e
				Dugopolski: College Algebra &amp; Trigonometry, 6e
				Dugopolski: College Algebra, 4e
				Dugopolski: College Algebra, 6e
				Dugopolski: Precalculus: Functions and Graphs, 4e MML Update
				Dugopolski: Trigonometry, 4e
				Dugopolski: Trigonometry, 5e
				Edwards/Penney/Calvis: Differential Equations &amp; Linear Algebra, 4e
				Edwards/Penney/Calvis: Differential Equations Computing and Modeling, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 5e
				Edwards/Penney: Differential Equations and Boundary Value Problems C &amp; M, 6e
				eT1 Math Test Book
				eT2 Math Test Book
				Evans: Business Analytics: Methods, Models, and Decisions, 3e
				Evans: Business Analytics: Methods, Models, and Decisions, 3e Demo
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Florida Algebra 1A and 1B
				Foundations and Pre-calculus Mathematics 10 (Canada) DEMO
				Foundations and Pre-calculus Mathematics 10 (Pearson Canada) 
				Goldstein: Brief Calculus &amp; Its Applications, 13e
				Goldstein: Brief Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 14e
				Goldstein: Calculus and Its Applications, 15e
				Goldstein: Finite Mathematics and Its Applications, 12e
				Goldstein: Finite Mathematics and Its Applications, 13e
				Gould: Essential Statistics: Exploring the World Through Data, 2e
				Gould: Essential Statistics: Exploring the World Through Data, 3e
				Gould: Introductory Statistics: Exploring the World through Data, 2e
				Gould: Introductory Statistics: Exploring the World through Data, 3e
				Greenwell/Ritchey/Lial: Calculus for the Life Sciences, 2e
				Grimaldo: Developmental Math: Prealgebra, Intro Algebra, Intermediate Algebra 1e
				Groebner: Business Statistics: A Decision-Making Approach, 10e
				Groebner: Business Statistics: A Decision-Making Approach, 9e
				Harshbarger: College Algebra in Context, 4e
				Harshbarger: College Algebra in Context, 5e
				Harshbarger: College Algebra in Context, 6e
				Hass: University Calculus Early Transcendentals, 3e
				Hass: University Calculus Early Transcendentals, 4e
				High School Math Algebra 1 ©2012 (YTIs)
				High School Math Algebra 2 ©2012 (YTIs)
				High School Math Geometry ©2012 (YTIs)
				Hornsby: A Graphical Approach to Algebra &amp; Trigonometry, 7e
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 6e
				Hornsby: A Graphical Approach to College Algebra, 6e
				Hornsby: A Graphical Approach to College Algebra, 7e
				Hornsby: A Graphical Approach to Precalculus with Limits, 6e
				Hornsby: A Graphical Approach to Precalculus with Limits, 7e
				Indiana Algebra 1 ©2017
				Indiana Algebra 2 ©2017
				Indiana Geometry ©2017
				Interactive Calculus Early Transcendentals, 1e Demo
				IPRO-Irv-XLTestbk1
				Janet Testing Book
				Janus Math NextGen2 L
				Knewton Math test book
				Larson: Elementary Statistics: Picturing the World, 6e
				Larson: Elementary Statistics: Picturing the World, 7e
				Larson: Elementary Statistics: Picturing the World, 8e
				Lay: Linear Algebra and Its Applications, 5e
				Lay: Linear Algebra and Its Applications, 6e
				Lehmann: A Pathway to Introductory Statistics, 1e
				Lehmann: A Pathway to Introductory Statistics, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 2e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications, 3e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 2e
				Lehmann: Elementary and Intermediate Algebra: Functions and Authentic Apps, 3e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 5e
				Lehmann: Intermediate Algebra: Functions &amp; Authentic Applications, 6e
				Levine: Statistics for Managers Using Microsoft Excel, 8e
				Levine: Statistics for Managers Using Microsoft Excel, 9e
				Lial/Greenwell/Ritchey: Calculus with Applications Brief, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, 12e
				Lial/Greenwell/Ritchey: Calculus with Applications, Brief Version, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 10e
				Lial/Greenwell/Ritchey: Finite Mathematics &amp; Calculus with Applications, 11e
				Lial/Greenwell/Ritchey: Finite Mathematics, 12e
				Lial: Algebra &amp; Trigonometry for College Readiness Media Update
				Lial: Algebra and Trigonometry for College Readiness
				Lial: Algebra for College Students , 9e
				Lial: Algebra for College Students, 8e
				Lial: Basic College Mathematics, 10e
				Lial: Basic Math, Introductory Algebra, and Intermediate Algebra, 1e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra, 12e
				Lial: Beginning Algebra, 13e
				Lial: Beginning and Intermediate Algebra, 7e
				Lial: Calculus with Applications, 11e
				Lial: College Algebra and Trigonometry, 6e
				Lial: College Algebra and Trigonometry, 7e
				Lial: College Algebra, 12e
				Lial: College Algebra, 13e
				Lial: Developmental Mathematics, 4e
				Lial: Essentials of College Algebra, 11e
				Lial: Essentials of College Algebra, 12e
				Lial: Finite Mathematics with Applications, 11e
				Lial: Finite Mathematics with Applications, 12e
				Lial: Finite Mathematics, 11e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 13e
				Lial: Intermediate Algebra, 13e V2
				Lial: Introductory Algebra, 11e
				Lial: Introductory and Intermediate Algebra, 6e
				Lial: Mathematics with Applications, 11e
				Lial: Mathematics with Applications, 12e
				Lial: Prealgebra &amp; Introductory Algebra, 4e
				Lial: Prealgebra, 3e
				Lial: Prealgebra, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 7e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Long: Mathematical Reasoning for Elementary Teachers, 7e
				Long: Mathematical Reasoning for Elementary Teachers, 7e Media Update
				Martin-Gay: Algebra 1
				Martin-Gay: Algebra 1 (Texas Course)
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Algebra: A Combined Approach, 5e
				Martin-Gay: Algebra: A Combined Approach, 6e 
				Martin-Gay: Basic College Mathematics with Early Integers, 3e
				Martin-Gay: Basic College Mathematics with Early Integers, 4e
				Martin-Gay: Basic College Mathematics, 5e
				Martin-Gay: Basic College Mathematics, 6e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 6e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Beginning Algebra, 8e
				Martin-Gay: Developmental Mathematics, 3e
				Martin-Gay: Developmental Mathematics, 4e
				Martin-Gay: Geometry
				Martin-Gay: Inter. Algebra: Math for College Readiness (FL Edition)
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 6e 
				Martin-Gay: Intermediate Algebra, 6e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra: Graphing Approach, 5e
				Martin-Gay: Introductory Algebra, 5e
				Martin-Gay: Introductory Algebra, 6e
				Martin-Gay: Path to College Mathematics, 1e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 4e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 5e
				Martin-Gay: Prealgebra, 7e
				Martin-Gay: Prealgebra, 8e
				McClave: A First Course in Statistics, 12e
				McClave: Statistics for Business and Economics, 13e
				McClave: Statistics for Business and Economics, 14e
				McClave: Statistics, 13e
				McClave: Statistics, 13e MyLab Revision with Technology Updates
				Medway School District: High School Integrated Math Series
				Miller: Mathematical Ideas 11e and Expanded 11e (retired)
				Miller: Mathematical Ideas, 13e
				Miller: Mathematical Ideas, 14e
				MXL Player Functional Test Book
				MyMathLab test book A2
				Nagle: Fundamentals of Differential Eq w/ Boundary Value Prob, 7e Digital Update
				Nagle: Fundamentals of Differential Equations w/ Boundary Value Problems, 7e
				Nagle: Fundamentals of Differential Equations, 9e
				Nagle: Fundamentals of Differential Equations, 9e Digital Update
				Neuhauser/Roper: Calculus for Biology and Medicine, 4e
				Newbold: Statistics for Business and Economics, 8e
				NextGen Media Accounting Test Book
				NextGen Media Math Test Book--REAL 
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--ShehanNew
				NextGen Media Math Test Book--testsamplpe
				NextGen Media Math Test BookRH
				Pearson Pre-Calculus 11 (Canada)
				Pirnot: Mathematics All Around, 5e
				Pirnot: Mathematics All Around, 6e
				Pirnot: Mathematics All Around, 7e
				Pre-calculus 12 (Canada)
				Prentice Hall Algebra 1 ©2011 (YTIs)
				Prentice Hall Algebra 2 ©2011 (YTIs)
				Prentice Hall Algebra 2 and Trigonometry (New York Edition)
				Prentice Hall Geometry (New York Edition)
				Prentice Hall Geometry ©2011 (YTIs)
				Prentice Hall Integrated Algebra (New York Edition)
				Ratti/McWaters/Skrzypek/Fresh/Bernard: Precalculus A Right Triangle Approach, 5e
				Ratti: College Algebra &amp; Trigonometry, 3e
				Ratti: College Algebra &amp; Trigonometry, 4e
				Ratti: College Algebra, 3e
				Ratti: College Algebra, 4e
				Ratti: Precalculus A Right Triangle Approach, 4e
				Ratti: Precalculus Essentials
				Ratti: Precalculus: A Right Triangle Approach, 3e
				Ratti: Precalculus: A Unit Circle Approach, 3e 
				Ritchey/Rickard/Merkin: Interactive Finite Mathematics
				Rockswold: Algebra and Trigonometry with Modeling &amp; Visualization, 6e
				Rockswold: Beginning &amp; Intermediate Algebra, 4e 
				Rockswold: Beginning Algebra with Apps and Visualization, 3e
				Rockswold: College Algebra with Modeling and Visualization, 6e
				Rockswold: Developmental Mathematics, 2e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 5e
				Rockswold: Precalculus with Modeling &amp; Visualization, 6e
				Salzman: Mathematics for Business, 10e
				Saunders: Mathematics for the Trades: A Guided Approach, 11e
				School of One (New York)
				Schulz: Precalculus, 2e
				Schulz: Precalculus, 2e Digital Update
				Sharpe/De Veaux/Velleman: Business Statistics, 4e Digital Update
				Sharpe: Business Statistics, 3e
				Stine: Statistics for Business: Decision Making and Analysis, 3e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan: Algebra &amp; Trigonometry, 10e
				Sullivan: Algebra &amp; Trigonometry, 11e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 7e
				Sullivan: Algebra and Trigonometry Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra Enhanced w/ Graphing Utilities, 7e
				Sullivan: College Algebra Enhanced with Graphing Utilities, 8e
				Sullivan: College Algebra, 10e
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra: Concepts Through Functions, 4e
				Sullivan: Developmental Math, 2e
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary &amp; Intermediate Algebra, 3e
				Sullivan: Elementary &amp; Intermediate Algebra, 4e
				Sullivan: Elementary Algebra
				Sullivan: Elementary Algebra, 4e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Fundamentals of Statistics, 6e
				Sullivan: Intermediate Algebra, 4e
				Sullivan: Precalculus Enhanced w/ Graphing Utilities, 6e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 7e
				Sullivan: Precalculus Enhanced with Graphing Utilities, 8e
				Sullivan: Precalculus, 10e
				Sullivan: Precalculus, 11e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Right Triangle Approach, 4e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 3e
				Sullivan: Precalculus: Concepts Through Functions, A Unit Circle Approach, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5e
				Sullivan: Statistics: Informed Decisions Using Data, 6e
				Sullivan: Trigonometry A Unit Circle Approach, 10e
				Sullivan: Trigonometry: A Unit Circle Approach, 11e
				Tannenbaum: Excursions in Modern Mathematics, 10e
				Tannenbaum: Excursions in Modern Mathematics, 9e
				Tennessee Senior Finite Mathematics: An Experiential Approach
				Tennessee: Senior Bridge Mathematics 2011
				Tennessee: Senior Bridge Mathematics 2016
				Texas Algebra I ©2016
				Texas Algebra II ©2016
				Texas Geometry ©2016
				Texas: Mathematical Models with Applications, Texas Edition (2016)
				Texas: Precalculus Enhanced with Graphing Utilities, Texas Edition
				Texas: Precalculus: Graphical, Numerical, Algebraic, Texas Edition
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals, 15e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 13e
				Thomas’ Calculus Early Transcendentals, 13e
				Thomas’ Calculus Early Transcendentals, 14e
				Thomas’ Calculus, 14e
				Tobey: Basic College Mathematics, 5e
				Tobey: Basic College Mathematics, 8e
				Tobey: Basic College Mathematics, 9e
				Tobey: Beginning Algebra, 9e
				Tobey: Beginning Algebra: Early Graphing, 4e
				Tobey: Beginning and Intermediate Algebra, 5e
				Tobey: Beginning and Intermediate Algebra, 6e
				Tobey: Intermediate Algebra, 8e
				Trigsted: Interactive Developmental Math: Prealg, Beginning &amp; Inter Algebra, 2e
				Triola: Biostatistics for the Biological and Health Sciences, 2e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics using Excel, 7e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 4e
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 13e
				Triola: Elementary Statistics, 14e
				Triola: Essentials of Statistics, 5e
				Triola: Essentials of Statistics, 6e
				Triola: Essentials of Statistics, 7e
				University of Maryland University College: STAT 200: Elementary Statistics
				Utah School Districts: Mathematics I Common Core
				Utah School Districts: Mathematics II Common Core
				Utah School Districts: Mathematics III Common Core
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e
				Walpole: Probability &amp; Statistics for Engineers &amp; Scientists, 9e MyLab Update
				Washington/Evans: Basic Technical Mathematics with Calculus, 12e
				Washington/Evans: Basic Technical Mathematics, 12e
				Washington: Basic Technical Math w/ Calculus, 11e
				Washington: Basic Technical Math with Calculus, 10e
				Washington: Basic Technical Math, 10e
				Washington: Basic Technical Math, 11e
				Weiss: Elementary Statistics, 9e
				Weiss: Introductory Statistics, 10e
				Weiss: Introductory Statistics, 10e MyLab Revision with Tech Updates
				Woodbury: Elementary &amp; Intermediate Algebra, 4e
				Woodbury: Intermediate Algebra: A STEM Approach, 1e
				Zuro: Discovering Mathematics, 1e DEMO

			&quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
