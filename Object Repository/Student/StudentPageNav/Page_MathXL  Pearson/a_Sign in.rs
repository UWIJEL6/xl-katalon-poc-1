<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign in</name>
   <tag></tag>
   <elementGuidId>c81f1547-264b-4555-bc00-504171c794d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick=&quot;return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.button.button-big-icon.bg-color-match-medium</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3317a47b-5eb8-4652-a120-8552689d4a6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')</value>
      <webElementGuid>4740415f-4af3-4a40-9258-819cd906752e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://www.mathxl.com/login_mxl.htm</value>
      <webElementGuid>def82ad2-94d5-40a2-8270-feda02c9df44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-big-icon bg-color-match-medium</value>
      <webElementGuid>32ecb553-cd10-4d9f-b144-80971e6e0151</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>09e90257-b03f-4016-aeac-eb0712d4b07c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths cssfilters&quot;]/body[@class=&quot;has-announcement green has-two-or-less-featured-link-categories&quot;]/section[@class=&quot;hero hero--bg-img uses-mask mask-is--gradient-top random-image js--random-bg&quot;]/div[@class=&quot;wrapper no-col-stack @480.col-stack&quot;]/div[@class=&quot;col col-5&quot;]/div[@class=&quot;sign-in&quot;]/p[2]/a[@class=&quot;button button-big-icon bg-color-match-medium&quot;]</value>
      <webElementGuid>0bfe2e82-4d05-48d9-99d8-6a1f4c8889a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')&quot;]</value>
      <webElementGuid>504e0c56-5495-4ebf-b196-c4bf0897cc78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign in')]</value>
      <webElementGuid>915a85ea-3ba4-450f-99a6-e638f6fcc1f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[1]/following::a[1]</value>
      <webElementGuid>b6c8b13b-7835-4bb4-84e7-aaee57101322</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn more'])[1]/following::a[1]</value>
      <webElementGuid>3d95a9ae-98e2-4899-a49f-5d018722ca6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot username or password?'])[1]/preceding::a[1]</value>
      <webElementGuid>d68933c2-9aee-47c5-9569-945afc628c94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MyLab Math/MyLab Statistics users, sign in here'])[1]/preceding::a[2]</value>
      <webElementGuid>661bad50-22ce-4b08-b609-dff7e7cf48c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign in']/parent::*</value>
      <webElementGuid>03ab26c1-cc4d-4d1b-9d6f-ecbdb9dfce17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://www.mathxl.com/login_mxl.htm']</value>
      <webElementGuid>e5a781c6-a57d-4085-a227-0ce9016d3b8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/p[2]/a</value>
      <webElementGuid>550e26f1-7674-4c44-b1c2-8c3a1733c08c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://www.mathxl.com/login_mxl.htm' and (text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>77fa7bc6-3fb5-4afa-b542-4d9bfba4ab64</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
