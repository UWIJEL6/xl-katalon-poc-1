<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Current</name>
   <tag></tag>
   <elementGuidId>fadc3d95-a576-4bf3-9d6f-7eff36af41e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='current-tab']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#current-tab</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1efdb050-6a6d-44fb-a546-2f0c3b230c04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>current-tab</value>
      <webElementGuid>4c48c899-bb95-4cc3-8005-695331c65519</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>970aa154-b028-4143-93a9-0d9d2513eb13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>664b020b-83c5-4ba9-9865-54ad0d4454e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>aa8f2432-aabf-479e-8bcb-f692f7746cf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Current</value>
      <webElementGuid>00bf6fcb-2db8-4f80-bc0c-a7d338bf9b14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;current-tab&quot;)</value>
      <webElementGuid>efce3598-dc70-47c6-8385-5d4ac3154c2f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='current-tab']</value>
      <webElementGuid>cc999378-b172-4e1d-9a70-48d3de48750b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[2]/ul/li/a</value>
      <webElementGuid>f27e29bb-5493-4726-8de0-4f867aeeb1d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Current')]</value>
      <webElementGuid>cbec01f8-5bc4-4582-99bf-93316f114308</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Browser Check'])[2]/following::a[1]</value>
      <webElementGuid>562926e3-af23-484a-8066-22622439e48e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tours &amp; Tips'])[1]/following::a[2]</value>
      <webElementGuid>206a54b4-5965-4b84-99bf-5b115c1d2890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='--%'])[1]/preceding::a[2]</value>
      <webElementGuid>4bf9ab84-7412-4235-9810-2d4ee65a3ec0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show progress and scores'])[1]/preceding::a[2]</value>
      <webElementGuid>de39d054-ca7d-4af7-b609-eee75713f31c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Current']/parent::*</value>
      <webElementGuid>4dfa3eb4-53d0-4a29-92a0-91a8f3862712</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[7]</value>
      <webElementGuid>d8902441-b843-4b8c-9cad-f4047ccbee0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a</value>
      <webElementGuid>b8ccf3ce-ed09-4b56-b79a-d2eb992757ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'current-tab' and @href = '#' and (text() = 'Current' or . = 'Current')]</value>
      <webElementGuid>0deed0fa-41b6-4b17-81c3-fc57193e1cea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
