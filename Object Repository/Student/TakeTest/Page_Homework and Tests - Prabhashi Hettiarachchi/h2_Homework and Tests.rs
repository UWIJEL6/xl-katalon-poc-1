<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Homework and Tests</name>
   <tag></tag>
   <elementGuidId>4483d877-23de-4549-ba24-a147ea2855b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#PageHeaderTitle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[@id='PageHeaderTitle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>585680de-02d7-4d06-b021-889c58317009</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PageHeaderTitle</value>
      <webElementGuid>92bf6985-f063-4f41-b392-f44811fec2f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>integrationheaderstyle PageHeaderTitle header-component-float-left header-font-size</value>
      <webElementGuid>b0daf79e-ec89-48a5-888f-64d320710074</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Homework and Tests</value>
      <webElementGuid>75373823-60bb-416c-a416-9a50ec1d5e66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PageHeaderTitle&quot;)</value>
      <webElementGuid>c8029e33-82e5-46ac-ad47-6d3ca295bd18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>be50e366-90af-43af-95ef-90a4bef096fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PageHeaderTitle</value>
      <webElementGuid>0444afcc-8693-451a-8e65-b495cc342adb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>integrationheaderstyle PageHeaderTitle header-component-float-left header-font-size</value>
      <webElementGuid>5c5096d3-3f9e-4833-bf84-cdc0aa66477a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Homework and Tests</value>
      <webElementGuid>b6a85783-b1d1-458a-a98e-38209e6a0cff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PageHeaderTitle&quot;)</value>
      <webElementGuid>c31e9bc0-6641-44b9-9736-a3486a119f44</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h2[@id='PageHeaderTitle']</value>
      <webElementGuid>273071f3-08d4-463c-84be-003e804e300c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_PageHeader1_HeaderTitleH2Div']/h2</value>
      <webElementGuid>a8febbed-507f-4c40-9055-9d6533765098</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NextGen Media Math Test Book--REAL'])[1]/following::h2[1]</value>
      <webElementGuid>f2372340-03f1-4325-a5f4-9ea400b2a7d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prabhashi ins1'])[1]/following::h2[1]</value>
      <webElementGuid>e0a6ea50-262c-47be-b385-9878e8cde7dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print this page'])[1]/preceding::h2[1]</value>
      <webElementGuid>9c682031-37d3-4339-aa4b-83a87579d672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[3]/preceding::h2[1]</value>
      <webElementGuid>422f8b4d-c7ab-47c1-ad9f-f9f848219494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>e5c3a4f2-9b11-4585-a516-9dd34ebf2a01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[@id = 'PageHeaderTitle' and (text() = '
            Homework and Tests' or . = '
            Homework and Tests')]</value>
      <webElementGuid>60db6da4-935e-4154-bd67-0b4e10f474fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div/div[3]/div/div[2]/h2</value>
      <webElementGuid>17564ef5-541e-48b3-90f7-073e180b2745</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hiranthi Insnew'])[1]/following::h2[1]</value>
      <webElementGuid>60a44405-ca87-452c-b395-b38aa1d80500</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
