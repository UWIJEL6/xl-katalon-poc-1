<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Incorrect 0</name>
   <tag></tag>
   <elementGuidId>d1a9054e-97e5-4230-a15f-b37580aea555</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='exPanel']/section/ul/li[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>6125eceb-d883-4b76-b3fd-6633d4b9074f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Incorrect: 0</value>
      <webElementGuid>039dfde5-724a-40d8-939c-1d74a88da063</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exPanel&quot;)/section[@class=&quot;table-score-details overviewtotalstest&quot;]/ul[1]/li[4]</value>
      <webElementGuid>930b0142-1425-4715-ad81-ae6f0dc0d144</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='exPanel']/section/ul/li[4]</value>
      <webElementGuid>952da3d2-48fa-4218-912f-762f4107059d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Partial Credit: 0'])[1]/following::li[1]</value>
      <webElementGuid>21eac302-117a-4202-ab7d-c3900df80f09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Correct: 5'])[1]/following::li[2]</value>
      <webElementGuid>60562c3e-8334-4bd6-b15f-01c1189cff2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Incomplete: 0'])[1]/preceding::li[1]</value>
      <webElementGuid>638f168c-4eb8-4c04-a7ac-8aceaa900992</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 1'])[1]/preceding::li[2]</value>
      <webElementGuid>73a1b467-49a3-44f3-9066-b9caa6852fd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Incorrect: 0']/parent::*</value>
      <webElementGuid>ce3274b3-47ff-4133-9500-b21c7269bfa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[4]</value>
      <webElementGuid>d923d99c-a672-4f8f-b290-412b75851024</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Incorrect: 0' or . = 'Incorrect: 0')]</value>
      <webElementGuid>45f07925-2df6-4a48-a06e-18fd7601af59</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
