<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Review  Test TestingTestAssignment</name>
   <tag></tag>
   <elementGuidId>2bbe9b93-df2e-4531-821d-c793290b100a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.assignmentTitle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dijit_layout_ContentPane_0']/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2511d06a-228a-47f6-9391-22f8471a457e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>heading</value>
      <webElementGuid>1e0e6a08-0dfc-4742-a1ec-4cceb53224c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-level</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>7aed53ee-62ad-440e-ad9b-021eb5381a8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>assignmentTitle</value>
      <webElementGuid>3d4876d5-f924-4244-b54c-710ac9b89fdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>assignmentTitle</value>
      <webElementGuid>fca265a3-881a-4da5-953c-30ec6cd9b742</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Review  Test: TestingTestAssignment</value>
      <webElementGuid>ffc27bb2-af27-41e3-9f59-c927f7ddb58f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_0&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutLeft&quot;]/span[@class=&quot;assignmentTitle&quot;]</value>
      <webElementGuid>6eec9a44-5d8c-4b0e-b013-1a3c738964e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Student/TakeTest/Page_TestingTestAssignment/iframe_Return to Homework_activityFrame (3)</value>
      <webElementGuid>d615dfb6-5101-447a-9498-61b3fbd4d718</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_0']/div/div/span</value>
      <webElementGuid>c0ef4523-d333-459c-8854-4c0a41afa780</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 1,'])[1]/preceding::span[2]</value>
      <webElementGuid>eb951da7-12bf-4068-aaf1-14c86979c84f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Review']/parent::*</value>
      <webElementGuid>21d99771-7b80-4650-b28c-ea2e9073cbdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>eae17257-5f06-45f4-870a-e8b5626cfdb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Review  Test: TestingTestAssignment' or . = 'Review  Test: TestingTestAssignment')]</value>
      <webElementGuid>cb8730e1-246b-410f-af91-075727da18ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
