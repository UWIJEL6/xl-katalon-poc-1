<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Submit test (3)</name>
   <tag></tag>
   <elementGuidId>f22065a8-6aea-468d-9c32-5c36ea09ef59</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_7</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='xl_dijit-bootstrap_Button_7']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e72311a0-3f1b-4ec1-9332-8b15df1560a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2677ff57-8f1f-41e8-b75f-e7f479de571a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>724eeeb4-c869-4bfd-b883-aa102bd3bedd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>6c47e4e1-c02c-4591-9dc6-cd0fcffc8d4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_7</value>
      <webElementGuid>a9ead3b2-ec86-4c96-8c07-3ebeca0875f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_7</value>
      <webElementGuid>fa0be6ac-be26-4c0a-b342-2c17e5e62273</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit test</value>
      <webElementGuid>5ec73d75-4174-4abf-aab1-9b8f60a8f1f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_7&quot;)</value>
      <webElementGuid>0973386e-6f10-4157-a50a-deba1efca562</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Student/TakeTest/Page_TestingTestAssignment/iframe_Opens in a new window_ctl00_ctl00_In_5f7b40_1_2</value>
      <webElementGuid>c69f65e0-787c-42e9-bafa-5197f5c4dd4a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_7']</value>
      <webElementGuid>3f0f41f0-114f-4dfa-849b-9046bc29da9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_ConfirmationDialog_0']/div/div[4]/div[2]/button[2]</value>
      <webElementGuid>32905bff-58d9-4a89-81a5-e251f88dd14a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::button[1]</value>
      <webElementGuid>aa147a25-edb1-4959-9b20-4d48999cf461</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='pop-up content ends'])[1]/following::button[2]</value>
      <webElementGuid>2e101d84-4c77-440d-a42e-24268935a8b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Submit test']/parent::*</value>
      <webElementGuid>ef6305ff-dc21-4743-b295-7558ecef924c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button[2]</value>
      <webElementGuid>d72cc7c6-cec8-4332-bb26-530fb74c1d61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_7' and (text() = 'Submit test' or . = 'Submit test')]</value>
      <webElementGuid>dd978c58-f23d-48b9-9bc6-4c52c9586d87</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
