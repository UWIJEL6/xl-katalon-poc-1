<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Test TestingTestAssignment</name>
   <tag></tag>
   <elementGuidId>e44d94d8-2fb7-4e98-bcf7-4295a3b037ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dijit_layout_ContentPane_0']/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.assignmentTitle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9058367f-b805-47df-88a3-0cbee524e5b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>heading</value>
      <webElementGuid>27aa5dd3-7ef4-46dd-8ef9-05e63a623010</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-level</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>19d7b397-c45b-424e-b6f9-81594b749566</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>assignmentTitle</value>
      <webElementGuid>1eb173a5-4718-450b-b4f5-75536eb87bfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>assignmentTitle</value>
      <webElementGuid>9f598cb5-a3d0-4e4b-ab67-6ef2e4c58023</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test: TestingTestAssignment</value>
      <webElementGuid>75620783-9011-4a41-8513-892bf3209dea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_0&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutLeft&quot;]/span[@class=&quot;assignmentTitle&quot;]</value>
      <webElementGuid>53690420-810a-4bbc-9bc8-63fba922d47f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Student/TakeTest/Page_TestingTestAssignment/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>6a430ae7-75e9-493a-a1d0-d44c320e8071</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_0']/div/div/span</value>
      <webElementGuid>bc7209b4-e3c0-44d6-9001-6d28c7d52700</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 1 of 5'])[1]/preceding::span[2]</value>
      <webElementGuid>6abecfe3-4d0d-4fbb-aa2c-141edab6bc95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='TestingTestAssignment']/parent::*</value>
      <webElementGuid>0654f4b0-9ff1-4c12-bac5-31370d44a7c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>a3ea470b-f8ed-42b0-9df6-d29f2311300e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Test: TestingTestAssignment' or . = 'Test: TestingTestAssignment')]</value>
      <webElementGuid>44482406-1eb7-49f4-a717-e44f1df9fe87</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
