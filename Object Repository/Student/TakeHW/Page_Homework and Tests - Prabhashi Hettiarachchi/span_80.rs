<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_80</name>
   <tag></tag>
   <elementGuidId>8f67409d-0e0a-4e29-ab53-e6491bc8ab68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='H_624552696']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#H_624552696 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>f58ffe17-3852-4fc1-9559-b637cdc3187d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>5eb1f2ca-6108-4fee-b698-49b0b55281b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>80%</value>
      <webElementGuid>28fec7d4-5077-45cf-8cba-8e09f2402d09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;H_624552696&quot;)/span[1]</value>
      <webElementGuid>d58c1b85-5dbd-478e-9296-d77d080407db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='H_624552696']/span</value>
      <webElementGuid>371ae03b-e386-42e6-b9aa-19310787ecd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingHomeworkAssignment'])[1]/following::span[2]</value>
      <webElementGuid>3305982f-31aa-400a-9c78-e23e024c818a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[9]/following::span[2]</value>
      <webElementGuid>b1693039-4542-410f-973f-6a5f0e2852b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quiz'])[1]/preceding::span[1]</value>
      <webElementGuid>9eea2a43-50fd-4a54-9856-57710b350879</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingQuizAssignment'])[1]/preceding::span[2]</value>
      <webElementGuid>75b7575d-31af-4322-84ee-7d8be964428d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='80%']/parent::*</value>
      <webElementGuid>62c0eb6a-26bb-4ca6-8ba9-28910afc97fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[5]/div/span</value>
      <webElementGuid>d775d599-bf80-411f-9e88-ebf45236c69c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '80%' or . = '80%')]</value>
      <webElementGuid>9daa64e1-8d4c-4915-95e7-e5ee5ab8d021</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
