<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign in</name>
   <tag></tag>
   <elementGuidId>cd20733c-a458-4005-8066-87132d302350</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#mainButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='mainButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>708a5aa6-1981-47c5-9f77-406ead97ba97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>80f1dc75-382c-42e8-b8ca-5e9bb917e123</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mainButton</value>
      <webElementGuid>bf6f7d10-10c6-4d66-9867-2dda2e4caba1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>!signingIn</value>
      <webElementGuid>21643293-c1c4-44af-b9b5-958a52004dc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-btn__cta--btn_xlarge full-width ng-binding ng-scope</value>
      <webElementGuid>6c408bfb-c5be-4d78-9e52-d432e0eabc34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>getCustomButtonClassname()</value>
      <webElementGuid>c7c4e61b-a6ab-435f-89a5-969c82bfc4ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>a4071b06-6533-4041-9d86-648d2762b46c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainButton&quot;)</value>
      <webElementGuid>8f53a655-f5b4-4382-a5db-5b1709ec59b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>667e0f42-8d55-43d5-8b5b-bc02db5cfa10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>7f3a9164-d8f9-43ef-b429-9067d1b6387a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mainButton</value>
      <webElementGuid>a93c8135-682d-409f-8ef0-79889310f903</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>!signingIn</value>
      <webElementGuid>9d33df0c-f8da-4b1c-b882-74c3dad2d215</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-btn__cta--btn_xlarge full-width ng-binding ng-scope</value>
      <webElementGuid>00c874e3-2700-493b-ad19-55e4163361fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>getCustomButtonClassname()</value>
      <webElementGuid>41b577b9-9bb3-4e2c-ba9d-4834ffdfc921</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>a5ce69f3-2478-4284-baf5-e986618455dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainButton&quot;)</value>
      <webElementGuid>5a5e935c-c2df-428b-bde5-652bd3e1cf7e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='mainButton']</value>
      <webElementGuid>94fe2512-9891-4bf2-8f2d-b5c1d9aa62af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mainForm']/div[5]/button</value>
      <webElementGuid>54009dcf-13f0-4501-8e77-bc16921c5f8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your username or password?'])[1]/following::button[1]</value>
      <webElementGuid>b31bd616-95d1-4e13-ab07-d56aa6de49a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password is hidden'])[1]/following::button[1]</value>
      <webElementGuid>f9bd139e-65a6-40e1-a78b-b53e8399432b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::button[1]</value>
      <webElementGuid>19a96085-1cc7-4f70-a2d9-f30488038137</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/button</value>
      <webElementGuid>a37edfa6-f1c8-48ca-a375-0e67986d5f07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and @id = 'mainButton' and (text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>1c1fd1cb-11be-48b5-a536-f6de762e42ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
