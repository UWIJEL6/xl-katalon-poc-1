<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_Opens in a new window_ctl00_ctl00_In_5f7b40_1_2_3</name>
   <tag></tag>
   <elementGuidId>32d43fb3-2b60-4c48-bb6b-6d8c2d2f0afa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_PlayerHtml5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='ctl00_ctl00_InsideForm_MasterContent_PlayerHtml5']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>69a9b662-cca0-4894-b070-e8429e69c8f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://mylabsxl-ppe.pearson.com/Service/PlayerLaunch.ashx?custom_adaptive=false&amp;custom_aggressive_recording=null&amp;custom_chapter_id=0&amp;custom_content_area_id=null&amp;custom_discipline=math&amp;custom_from_ask=false&amp;custom_from_cqb=false&amp;custom_grade_all=false&amp;custom_grade_essay=false&amp;custom_homework_id=0&amp;custom_homework_result_id=0&amp;custom_is_adaptive_remediation_enabled=false&amp;custom_is_container=false&amp;custom_is_cqb_print=false&amp;custom_is_review=true&amp;custom_is_scratch=false&amp;custom_item_analysis_preview=null&amp;custom_mode=test&amp;custom_needs_study=false&amp;custom_number_attempts=null&amp;custom_objective_id=0&amp;custom_pool_type=1&amp;custom_product_id=null&amp;custom_prof_view=null&amp;custom_section_id=0&amp;custom_single=false&amp;custom_start_question=1&amp;custom_test_auto_record_time=0&amp;custom_test_id=76999561&amp;custom_test_result_id=411989256&amp;custom_use_tux=false&amp;custom_xl_user_id=29711944&amp;lti_message_type=basic-lti-launch-request&amp;lti_version=LTI-1p0&amp;oauth_consumer_key=xl_html5_player&amp;oauth_nonce=647860&amp;oauth_signature=aL3ZYx56ruXzlSxhYqAGyt5h3gQ%3d&amp;oauth_signature_method=HMAC-SHA1&amp;oauth_timestamp=1651662019&amp;oauth_version=1.0&amp;resource_link_id=any&amp;tool_consumer_instance_guid=xlhtml5playerlauncher</value>
      <webElementGuid>da1df9f7-f6e2-4172-9ca5-f335d60fb840</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_PlayerHtml5</value>
      <webElementGuid>337d12bc-45be-4ad7-9ca4-eb2cbb8e9dfe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Question Viewer</value>
      <webElementGuid>26e8cb24-2447-4f81-bcbd-2fdf9f952362</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>100%</value>
      <webElementGuid>462a0f9b-28c5-4e83-b0bf-475180c99ee2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>558</value>
      <webElementGuid>1797b9b8-751e-4e6d-9ac4-9b9a509a3792</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>no</value>
      <webElementGuid>d0953987-2379-4585-9708-adb4d48d3852</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_PlayerHtml5&quot;)</value>
      <webElementGuid>82cb41d8-4619-4400-a97b-823855792fc8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='ctl00_ctl00_InsideForm_MasterContent_PlayerHtml5']</value>
      <webElementGuid>33f09cb4-09e5-435e-88cd-d1c2a395358c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_PnlPlayers']/table/tbody/tr/td/iframe</value>
      <webElementGuid>b0cdd5fd-0b48-4ae2-b081-fc2904679cb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/iframe</value>
      <webElementGuid>9f09c358-146d-4e1a-8d7b-dcc617a783a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = 'https://mylabsxl-ppe.pearson.com/Service/PlayerLaunch.ashx?custom_adaptive=false&amp;custom_aggressive_recording=null&amp;custom_chapter_id=0&amp;custom_content_area_id=null&amp;custom_discipline=math&amp;custom_from_ask=false&amp;custom_from_cqb=false&amp;custom_grade_all=false&amp;custom_grade_essay=false&amp;custom_homework_id=0&amp;custom_homework_result_id=0&amp;custom_is_adaptive_remediation_enabled=false&amp;custom_is_container=false&amp;custom_is_cqb_print=false&amp;custom_is_review=true&amp;custom_is_scratch=false&amp;custom_item_analysis_preview=null&amp;custom_mode=test&amp;custom_needs_study=false&amp;custom_number_attempts=null&amp;custom_objective_id=0&amp;custom_pool_type=1&amp;custom_product_id=null&amp;custom_prof_view=null&amp;custom_section_id=0&amp;custom_single=false&amp;custom_start_question=1&amp;custom_test_auto_record_time=0&amp;custom_test_id=76999561&amp;custom_test_result_id=411989256&amp;custom_use_tux=false&amp;custom_xl_user_id=29711944&amp;lti_message_type=basic-lti-launch-request&amp;lti_version=LTI-1p0&amp;oauth_consumer_key=xl_html5_player&amp;oauth_nonce=647860&amp;oauth_signature=aL3ZYx56ruXzlSxhYqAGyt5h3gQ%3d&amp;oauth_signature_method=HMAC-SHA1&amp;oauth_timestamp=1651662019&amp;oauth_version=1.0&amp;resource_link_id=any&amp;tool_consumer_instance_guid=xlhtml5playerlauncher' and @id = 'ctl00_ctl00_InsideForm_MasterContent_PlayerHtml5' and @title = 'Question Viewer']</value>
      <webElementGuid>4a715052-9ea8-42ee-a801-1127840faa1d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
