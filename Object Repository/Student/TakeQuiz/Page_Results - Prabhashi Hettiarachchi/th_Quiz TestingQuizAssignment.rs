<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>th_Quiz TestingQuizAssignment</name>
   <tag></tag>
   <elementGuidId>35b33782-0438-464a-8af0-b53002e78ffe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[(text() = 'Quiz TestingQuizAssignment' or . = 'Quiz TestingQuizAssignment')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>th</value>
      <webElementGuid>a0dc01f9-1469-4f04-b2dd-157c4770fdb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>leftcol fullwidth </value>
      <webElementGuid>720795b3-ff54-4a0f-b426-09eb89bc8009</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scope</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>202e8733-9a19-45d1-b97e-c49e807db074</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>60e24c0b-e81c-4734-900e-e073d7adc794</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Quiz TestingQuizAssignment</value>
      <webElementGuid>7901ffdb-c2e7-47ad-84db-920ee567ea23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row241553502&quot;)/th[@class=&quot;leftcol fullwidth&quot;]</value>
      <webElementGuid>966a256f-0d0a-4c87-a9f6-7b51590801f1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row241553502']/th</value>
      <webElementGuid>6d7cc5ee-54a3-4d2e-9086-e21df553be65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date Worked'])[1]/following::th[1]</value>
      <webElementGuid>c5c13df9-bf65-461a-9c43-f785618636c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date Started'])[1]/following::th[2]</value>
      <webElementGuid>a0f7272b-fa55-4316-9f11-410a56c22e41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='TestingQuizAssignment']/parent::*</value>
      <webElementGuid>d2ad3994-df77-4c08-8f3e-e6d5e81243e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/div/table/tbody/tr/th</value>
      <webElementGuid>20869426-18bc-4b84-a855-c02b594c062a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//th[(text() = 'Quiz TestingQuizAssignment' or . = 'Quiz TestingQuizAssignment')]</value>
      <webElementGuid>322a4d3f-cbe4-43b7-8546-dbf365950e74</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
