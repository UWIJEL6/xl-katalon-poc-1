<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Permanently Delete Results</name>
   <tag></tag>
   <elementGuidId>b309af97-b726-4dea-a7f0-6767b893fd5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DeleteConfirmYes</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='DeleteConfirmYes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>768b7357-9d21-4ee4-998c-6af269580bbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DeleteConfirmYes</value>
      <webElementGuid>d0b10b65-5956-44fa-9bbb-fbb42ca42771</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>575df996-52ce-4689-9970-19574ed051d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
      <webElementGuid>28e38a5d-6c81-49c8-9e03-e3deeaf79922</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Permanently Delete Results</value>
      <webElementGuid>412d7953-1848-4c9c-8025-239687d28286</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DeleteConfirmYes&quot;)</value>
      <webElementGuid>67c0e9ff-2bbd-4773-9daa-d9fd42190f3b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='DeleteConfirmYes']</value>
      <webElementGuid>53a23ca3-f656-48f6-b108-07a4a3e602d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='DeleteConfirmModal']/div/div/div[3]/button</value>
      <webElementGuid>b3837732-5844-4803-bc2c-6efc091e06ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Warning:'])[1]/following::button[1]</value>
      <webElementGuid>7f1e45a6-4c68-4caa-949d-1e5da86d9f6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::button[1]</value>
      <webElementGuid>0722a910-866c-48d3-bd97-562cf82912eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::button[2]</value>
      <webElementGuid>9ffcab81-2fca-46fb-a2b5-756801317993</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Permanently Delete Results']/parent::*</value>
      <webElementGuid>3c5643bd-43cd-4f74-8886-f45a7104157e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>cc259faa-006e-45ba-84e9-8fe41b0c653c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'DeleteConfirmYes' and @type = 'button' and (text() = 'Permanently Delete Results' or . = 'Permanently Delete Results')]</value>
      <webElementGuid>d3d39762-cd6b-4c27-b860-4bd40da39593</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
