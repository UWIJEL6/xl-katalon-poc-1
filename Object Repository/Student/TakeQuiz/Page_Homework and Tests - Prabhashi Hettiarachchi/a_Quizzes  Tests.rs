<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Quizzes  Tests</name>
   <tag></tag>
   <elementGuidId>a16c1569-ad9e-4a58-9912-84258dddc79a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='ul_All Assignments']/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5005020f-9e2e-4175-b32d-c114f9bfdb16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-menu-id</name>
      <type>Main</type>
      <value>All_Assignments</value>
      <webElementGuid>977716ce-ec74-42ce-8c38-af73a0dda20b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09','')</value>
      <webElementGuid>0cb1ec88-13a7-4c49-b97d-960c985d1c08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Quizzes &amp; Tests</value>
      <webElementGuid>11dfb4ef-14e9-402d-8d44-a057b99511e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ul_All Assignments&quot;)/li[3]/a[1]</value>
      <webElementGuid>2880608f-4284-4b3d-b0c2-545b6ea080cf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='ul_All Assignments']/li[3]/a</value>
      <webElementGuid>e405759a-40ef-4330-bd4a-10ccb4d7895c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Quizzes &amp; Tests')]</value>
      <webElementGuid>9e069555-3621-4156-a294-6093c7df3caa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[1]/following::a[1]</value>
      <webElementGuid>e5b5ba14-6efe-486c-ac62-44cfa396f4fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Assignments'])[2]/following::a[2]</value>
      <webElementGuid>55ce8b26-2eaf-405e-b298-4876ad72dec5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[1]/preceding::a[1]</value>
      <webElementGuid>03f2183b-2a28-49d1-a9a1-b23553ab3aee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[2]/preceding::a[1]</value>
      <webElementGuid>5ebd3fec-bd45-442d-8a6d-98c749b3b6f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Quizzes &amp; Tests']/parent::*</value>
      <webElementGuid>6071ad40-195c-4181-90ba-ad9954110d2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09','')&quot;)]</value>
      <webElementGuid>209ee91c-e268-40b4-8515-30ae1b0ac489</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div/div/ul/li[3]/a</value>
      <webElementGuid>796ebc54-8e22-4e62-9354-33d4d60fb4d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:__doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Quizzes &amp; Tests' or . = 'Quizzes &amp; Tests')]</value>
      <webElementGuid>aaf42100-389d-479c-af44-4f86cb939c96</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
