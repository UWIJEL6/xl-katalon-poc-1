<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_All Assignments</name>
   <tag></tag>
   <elementGuidId>f1cc668a-ff39-4289-8ff3-1a6ba903c322</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[@id='gridtitle']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.PagerText</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8e64a9a9-7edb-4c19-9f1a-09b80d37f5c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>PagerText</value>
      <webElementGuid>7028010c-a575-4a24-811a-eafce93749a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					            All Assignments
					        </value>
      <webElementGuid>b832829b-f082-417d-8fda-2fbb2fff6e22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gridtitle&quot;)/span[@class=&quot;PagerText&quot;]</value>
      <webElementGuid>3b41e402-7f5e-4040-8012-0bc3d4d5d33a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//h3[@id='gridtitle']/span</value>
      <webElementGuid>2d1d41c3-6f00-47d3-8d98-6b26d21f500f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[2]/following::span[1]</value>
      <webElementGuid>84d6cfc0-6f18-42f2-933f-acedd0c88ade</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[1]/following::span[2]</value>
      <webElementGuid>c176e634-a785-4bd2-8848-f38200a2be27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Due'])[1]/preceding::span[1]</value>
      <webElementGuid>b018c9d8-33c9-440c-ac8e-e1490f686738</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prereq'])[1]/preceding::span[1]</value>
      <webElementGuid>05809caa-bc29-40f8-8ec2-c490221d118c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3/span</value>
      <webElementGuid>e7d2ef74-39a0-4c6b-be93-5c77b78e7557</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
					            All Assignments
					        ' or . = '
					            All Assignments
					        ')]</value>
      <webElementGuid>08daf268-92b5-4e63-bbb3-018a788d66c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
