<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_80</name>
   <tag></tag>
   <elementGuidId>28f19dda-677b-4009-810e-5e0a29e9ce79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Q_241553502']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Q_241553502 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3bfc0cc8-6512-4028-a6b6-ebcb35c9ed50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>0eb01236-5ec8-46d0-a723-9d6c8e4af19d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>80%</value>
      <webElementGuid>ba6820d3-3083-449c-a6c5-7efd02e75b09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Q_241553502&quot;)/span[1]</value>
      <webElementGuid>58728a29-5b96-43b8-ab4f-ef1ea13d4da3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Q_241553502']/span</value>
      <webElementGuid>52c54cd7-22cd-4ac1-baf9-29fdc183e5aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingQuizAssignment'])[1]/following::span[2]</value>
      <webElementGuid>65082edf-5007-4a7a-a9a1-dcfa639410e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[2]/preceding::span[1]</value>
      <webElementGuid>eee1243f-8338-4478-994f-f9fcb7a733a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingTestAssignment'])[1]/preceding::span[2]</value>
      <webElementGuid>c298c944-3702-46c2-9518-74c88beea188</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='80%']/parent::*</value>
      <webElementGuid>04b10632-3666-48b2-be6d-341d8889fb1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[5]/div/span</value>
      <webElementGuid>aa3b0c83-3eb6-4765-bd28-92143a6a1611</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '80%' or . = '80%')]</value>
      <webElementGuid>a8505ef7-4c54-4536-b799-c4777bd538ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
