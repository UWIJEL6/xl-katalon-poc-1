<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Incorrect 0</name>
   <tag></tag>
   <elementGuidId>ea59e9b5-b2c8-490b-85d6-6e59b98950e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='exPanel']/section/ul/li[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>2ff97f47-a934-46d3-bafe-44fd1d8d2ed7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Incorrect: 0</value>
      <webElementGuid>cdb78859-d2b4-484a-b390-1ea6d6cadaeb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exPanel&quot;)/section[@class=&quot;table-score-details overviewtotalstest&quot;]/ul[1]/li[4]</value>
      <webElementGuid>36f10bd2-69a7-4fa7-b3f9-01678cd99da1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='exPanel']/section/ul/li[4]</value>
      <webElementGuid>80e3fdb2-ff53-481d-9f42-7a8c20dcdb2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Partial Credit: 0'])[1]/following::li[1]</value>
      <webElementGuid>3c6429fd-4686-41cd-87a5-8f6e87751a55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Correct: 4'])[1]/following::li[2]</value>
      <webElementGuid>45df4055-8395-4fc8-87cc-0296ae07a271</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Incomplete: 1'])[1]/preceding::li[1]</value>
      <webElementGuid>e64345cf-fc74-4874-9b83-2d922f52d2cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 1'])[1]/preceding::li[2]</value>
      <webElementGuid>b383e4d4-9e2b-4b75-8dc8-5b45862c0160</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Incorrect: 0']/parent::*</value>
      <webElementGuid>9e3d6624-f088-4b4f-8079-1d5a131e9325</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[4]</value>
      <webElementGuid>7c7dcb5f-50bd-4d9f-9000-be548ba3c298</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Incorrect: 0' or . = 'Incorrect: 0')]</value>
      <webElementGuid>ffb22127-7d62-4740-af96-5bf8317eee5a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
